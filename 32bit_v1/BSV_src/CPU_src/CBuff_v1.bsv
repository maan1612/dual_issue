/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Neel Gala
Email ID : neelgala@gmail.com

Description : This module implements the completion buffer.

the buffer has two pointers - iidx and ridx. iidx is like a tail pointer to which new entries are allocated and ridx is the head pointer
from which old entries a removed. A counter is mainted which counts the number of reserved entries in the buffer. the buffer is said
to be full when this counter value = size of the buffer. The buffer is empty is the counter value is 0. These conditions are used
to ensure proper firing of the rules/methods in higher level modules instantiatin this module.

*/
 package  CBuff; 

   // parameterized buffer with arguments : type of data to be stored in each entry, depth of buffer.
  interface CBuff#(type data_type, numeric type size);
   method ActionValue#(Tuple2#(Bit#(TLog#(size)),Bit#(TLog#(size)))) getTwoToken(); 
   method ActionValue#(Bit#(TLog#(size))) getOneToken();
   method Action put_1(Bit#(TLog#(size)) tok, data_type d); // this method updates a previously reserved slot with valid data.
   method Action put_2(Bit#(TLog#(size)) tok, data_type d);
   method ActionValue#(Maybe#(data_type)) getOneResult(); // this method return the valid data at the head of the buffer.
   method ActionValue#(Tuple2#(Maybe#(data_type),Maybe#(data_type))) getTwoResult(); // this method return the valid data at the head of the buffer.
   method Action deqOneResult(); // this increments the head of the buffer.
   method Action deqTwoResult(); // this increments the head of the buffer.
   method Action clear_partial(Bit#(TLog#(size)) tok); // clear specific number of tokens based on mispredication in a pipeline
   method Action clear_all(); // clear all the entries in the buffer.
   method Bool notFull();
   endinterface

   module mkMyCBuff(CBuff#(data_type,size))
   provisos(Bits#(Maybe#(data_type), a__));

   Reg#(Bit#(TLog#(size))) iidx <- mkReg(0); // pointer to issue a 1st new entry. like a tail pointer.
   Reg#(Bit#(TLog#(size))) ridx <- mkReg(0); // pointer to return a valid entry in order.
   Wire#(Bool) wr_cleaning_partial <- mkDWire(False);
   Wire#(Bool) isfull <- mkDWire(True);
   Integer vsize = valueOf(size) - 1;
   Bit#(TLog#(size)) sz = fromInteger(vsize);

   Reg#(Maybe#(data_type)) cb[vsize+1][4] ; // actuall buffer holding the data
    for(Integer i=0; i< vsize+1 ;i=i+1)
        cb[i]  <- mkCReg(4,Invalid);

    //method to get the tokens either one or two 
    rule rl_full_empty;
      if(iidx == ridx && isValid(cb[ridx][0])) isfull <= True;
      else isfull <= False;
    endrule

    //method ActionValue#(Bit#(TLog#(size))) getOneToken()if((iidx != ridx) || (iidx == ridx && !(isValid(cb[ridx][2]))));  
    method ActionValue#(Bit#(TLog#(size))) getOneToken()if((iidx+1 != ridx));
      cb[iidx][2]<=tagged Invalid; // make the head invalid.
      iidx<=iidx +1; // increment issue pointer to point to a new head.
      $display( "\n",$time," getOneToken fired token1 : %d and ridx: %d",iidx, ridx);
      return iidx; // return token
    endmethod
 
    method ActionValue#(Tuple2#(Bit#(TLog#(size)),Bit#(TLog#(size)))) getTwoToken() if(iidx+1 != ridx && iidx+2 != ridx);  
      cb[iidx][2]<=tagged Invalid; // make the 1st head invalid
      cb[iidx +1][2]<=tagged Invalid; //make the 2nd head invalid

      iidx<=iidx+2;
      $display("\n",$time," getTwoToken fired token1 : %d ,token2 : %d and ridx:%d",iidx,iidx+1,ridx);
      return tuple2( iidx, iidx +1); // return token
      // end
    endmethod

    // this method validates previously reserved slot in the buffer with valid data.
    method Action put_1(Bit#(TLog#(size)) idx, data_type d);// if(!(cnt[1] >=sz-1));
      $display( $time," Put_1 fired put value at token:%d ", idx);
      cb[idx][1]<= tagged Valid d;
    endmethod

    method Action put_2(Bit#(TLog#(size)) idx, data_type d);//if(!(cnt[1] >= sz-1));
      cb[idx][1]<= tagged Valid d;
    endmethod

    // this method returns the data in the head of the buffer pointed by ridx only if it has
    // valid data in the entry and there is atleast one reserved slot in the buffer.

    method Action clear_partial(Bit#(TLog#(size)) cli); // get the clear index
   // wr_cleaning_partial <= True;
    if((cli<=iidx && cli>=ridx) || (cli>=iidx && cli<=ridx)) begin
      if(cli == iidx) $display( $time," Clear Partial fired Nothing changed: cli:%d, iidx:%d",cli,cli);
      else if(cli + 2 == ridx ) begin
        iidx <= cli+1;
        cb[cli+1][3] <= tagged Invalid;
        $display( $time," Clear Partial fired 1 reg cleared-  cli is:%d,iidx was:%d iidx changed to:%d ridx is :%d",cli,iidx,cli+1,ridx) ;
      end
      else begin
      iidx <= cli+1;
        cb[cli+1][3] <= tagged Invalid;
        cb[cli+2][3] <= tagged Invalid;
        $display( $time," Clear Partial fired 2 reg cleared-  cli is:%d,iidx was:%d iidx changed to:%d ridx is :%d",cli,iidx,cli+1,ridx) ;
      end
    end
    endmethod
    method Bool notFull();
    return isfull;
    endmethod

    method ActionValue#(Maybe#(data_type)) getOneResult()if(cb[ridx][0] matches tagged Valid .x ); 
      ridx<= ridx+1;
      $display( $time," getOneResult fired ridx:%d",ridx);
      return tagged Valid x;
    endmethod
    method ActionValue#(Tuple2#(Maybe#(data_type),Maybe#(data_type))) getTwoResult()if(cb[ridx][0] matches tagged Valid .x &&& wr_cleaning_partial==False);
      if(cb[ridx+1][0] matches tagged Valid .y ) begin 
         ridx<= ridx+2;
         $display($time," getTwoResult fired x:%4d, y:%4d and ridx:%d",x,y,ridx);
         return tuple2(tagged Valid x,tagged Valid y);
      end
      else if(!isValid(cb[ridx+1][0])) begin
      ridx<= ridx==sz?0:ridx+1;
      $display($time," getTwoResult fired x:%4d, y:Invalid and ridx:%d",x,ridx);
      return tuple2(tagged Valid x,tagged Invalid);
      end
      else return tuple2(tagged Invalid,tagged Invalid);
    endmethod

    method Action deqOneResult() if(isValid(cb[ridx][0] ));
         cb[ridx][0]<= tagged Invalid;
         $display( $time," deqOneResult fired");
    endmethod

    method Action deqTwoResult() if(isValid(cb[ridx][0]) ); 
      if(isValid(cb[ridx+1][0]))begin
         cb[ridx][0]<= tagged Invalid;
         cb[ridx+1][0]<= tagged Invalid;
         $display( $time," deqTwoResult fired");
      end
      else begin
         cb[ridx][0]<= tagged Invalid;
         $display( $time," deqTwoResult fired-but can deq only one");
      end
    endmethod
    method Action clear_all();
      cb[0][3]<= tagged Invalid;
      cb[1][3] <= tagged Invalid;
      ridx<=0;
      iidx<=0;
    endmethod
endmodule
endpackage 