
package Tb_cbuff;
	import CBuff ::* ;
	import ConfigReg :: *;
	import DReg :: * ;
	import FIFO::*;
	import FIFOF::*;

`define Buff_size 8
(*synthesize*)
module mkTbcbuff(Empty);
// provisos (Bits#(value,32));
	CBuff#(Bit#(32), `Buff_size) rob <- mkMyCBuff();
	Reg#(int) ctr1 <- mkReg(0);
	Reg#(int) ctr2 <- mkReg(0);
	Reg#(Bit#(32)) value1 <- mkReg(0);
	Reg#(Bit#(32)) value2 <- mkReg(0);
	//Reg#(Bit#(32)) result <- mkReg(0);
	// Reg#(Maybe#(Bit#(TLog#(`Buff_size)))) token_1 <- mkDReg(tagged Invalid);
	// Reg#(Maybe#(Bit#(TLog#(`Buff_size)))) token_2 <- mkDReg(tagged Invalid);
	FIFOF#(Maybe#(Bit#(TLog#(`Buff_size)))) ff_token_1 <-mkFIFOF();
	FIFOF#(Maybe#(Bit#(TLog#(`Buff_size)))) ff_token_2 <-mkFIFOF();

	Reg#(Bool) put_one_flag <- mkConfigReg(False);
	Reg#(Bool) put_two_flag <- mkDReg(False);
	//Reg#(Bool) rg_cleaning_partial <- mkDReg(False);
	Wire#(Bool) wr_cleaning_partial <- mkDWire(False);
	Reg#(Bool) rg_cleaning_partial <- mkDReg(False);
	//Reg#(Bit#(TAdd#(TLog#(`Buff_size),1))) cnt <- mkReg(0);
	//Reg#(Bool) put_one_flag <- mkReg(False);
	rule alw_fire ;
		ctr1 <= ctr1 + 1;
		ctr2 <= ctr2 + 1;
		value1 <= value1 + 10;
		value2 <= value2 + 5 ;
		//$display($time," value1: %4d value2: %4d",value1, value2);
	endrule
	//(* descending_urgency = "clear_part,counter1" *)
	rule counter1 if(ctr1 > 1);
		if(ctr1 == 5) begin
		Tuple2#(Bit#(TLog#(`Buff_size)),Bit#(TLog#(`Buff_size))) tokens <- rob.getTwoToken(); // get two tokens 
		match {.t1, .t2} = tokens; 
		// token_1 <= tagged Valid t1;
		// token_2 <= tagged Valid t2;
		ff_token_1.enq(tagged Valid t1);
		ff_token_2.enq(tagged Valid t2);
		end
		else begin
		// if(ctr1% 7 == 0 || ctr1 % 8 == 0 ) begin
			let t1 <- rob.getOneToken(); // get one token 
			ff_token_1.enq(tagged Valid t1);
		    // put_one_flag <= True;
			$display($time," ctr1:%3d Got token : %d",ctr1,t1);
		 end
		// else begin
			// Tuple2#(Bit#(TLog#(`Buff_size)),Bit#(TLog#(`Buff_size))) tokens <- rob.getTwoToken(); // get two tokens 
			// match {.t1, .t2} = tokens; 
			// token_1 <= tagged Valid t1;
			// token_2 <= tagged Valid t2;
			// //put_one_flag <= False;
			// //put_two_flag <= True;
			// $display($time," ctr1:%3d Got token1 :%d  token2 :%d",ctr1,t1,t2);
		// end
		// else begin
		// Tuple2#(Bit#(TLog#(`Buff_size)),Bit#(TLog#(`Buff_size))) tokens <- rob.getTwoToken(); // get two tokens 
		// match {.t1, .t2} = tokens; 
		// // token_1 <= tagged Valid t1;
		// // token_2 <= tagged Valid t2;
		// ff_token_1.enq(tagged Valid t1);
		// ff_token_2.enq(tagged Valid t2);
		// end

		

	endrule

	rule putvalue if (ctr1 > 2 &&& ff_token_1.first() matches tagged Valid .token1);
		
		// if(put_one_flag == True ) begin


		if(ff_token_2.first() matches tagged Valid .token2 ) begin 
			if (token1 != token2) begin
			rob.put_1(token1,value1);
			rob.put_2(token2,value2);
			end
		end
		else begin 
		rob.put_1(token1,value1);
		ff_token_1.deq;
		end
		//$display($time," put value1 :%4d at token1:%d ",value1,token1);
		//$display($time," put value1 :%4d at token1:%d ",value1,token1);
		//put_one_flag <= False;
		// end
		// // else 
		// else if(ff_token_2.first() matches tagged Valid .token2 ) begin 
		// 	if (token1 != token2) begin
		// 	rob.put_1(token1,value1);
		// 	rob.put_2(token2,value2);
		// 	ff_token_1.deq;
		// 	ff_token_2.deq;
		// 	$display($time," put value1 :%4d at token1:%d and value2:%4d at token2:%d ",value1,token1,value2,token2);
		// 	end
		// end
	endrule

	rule counter2 if(ctr2>4 && (ctr2 %8==0 || ctr2 %9 ==0)) ;//if(ctr2 > 4);
		
	 //    let result1 = rob.getOneResult();
		// //$display($time," Result: %d from token : %d",result1,token);
		// $display($time," Result: %d ",result1);
		// rob.deqOneResult();
		// if(ctr2 %3 == 0, ) begin 
		// let result1 = rob.getOneResult();
		// //$display($time," Result: %d from token : %d",result1,token);
		// $display($time," ctr2:%d One result Result:%d ",ctr2, result1);
		// rob.deqOneResult();
		// end
		// else begin 
			//if(ctr2%7 == 0 && isValid(rob.getOneResult)) begin
				// Maybe#(Bit#(32)) result <- rob.getOneResult();
				// if(result matches tagged Valid .x ) 
				// $display($time," ctr2:%4d Got One result-  Result1:%4d ",ctr2,x);
				// rob.deqOneResult();
			//end
			// else begin
			Tuple2#(Maybe#(Bit#(32)),Maybe#(Bit#(32))) results <- rob.getTwoResult();
			match {.result1, .result2} = results;
			if (result1 matches tagged Valid .x &&& result2 matches tagged Valid .y )
				$display($time," ctr2:%3d Got two results- Result1:%4d  Result2:%4d ",ctr2,x,y);
			else if (result1 matches tagged Valid .x &&& !isValid(result2))
				$display($time," ctr2:%3d Got One result- Result1:%4d ",ctr2,x);
			else $display($time," ctr2:%3d No Results");
			
			rob.deqTwoResult();

		// end
		//end
	endrule

	//rule clear_part if(ctr2>1 && (ctr2 %10==0 || ctr2 %9 ==0));
		//rob.clear_partial(token_1);
		// rg_cleaning_partial <= True;
		// wr_cleaning_partial <= True;
		//token_1 <= token_1-1;
		//$display($time," Clear partial from token %d ",token_1);
	// endrule
	// rule clear_part if(ctr2 > 8 && (ctr2 %10==0 || ctr2 %9 ==0));
	// 	if(ff_token_1.first() matches tagged Valid .token1) begin
	// 	// rg_cleaning_partial <= True;
	// 	// wr_cleaning_partial <= True;
	// 	rob.clear_partial(token1);
	// 	end
	// 	//token_1 <= token_1-1;
	// 	//$display($time," Clear partial from token %d ",token_1);
	// endrule

	rule finish if (ctr2 > 35);
	    $display($time,"*********stop*********");
		//rob.clear_all();
		$finish(0);
	endrule
endmodule
endpackage




