`define Reg_width 32 // the register data width of the processor.
`define RegFileSize 32 // describes the size of ht register file in the processor.
`define Addr_width 32 // the address width
`define Buff_size 16
`define Loop 1
`define BAUD_RATE 130
`define Addr_space 16	//since we are leaving off the lower 2 bits of address(byte addressable memory), we have to 
`ifdef simulate
  `define BAUD_RATE 5 //130 //
  `define Addr_space 17	//since we are leaving off the lower 2 bits of address(byte addressable memory), we have to 
`endif

// Branch_predictor_paramters
`define size_of_table 1024        // indicates the number of entries of the Branch target Buffer.
`define bits_used_for_accessing 12 // Log2(`size_of_table)
/////////////////////////// CACHE RELATED PARAMETERS ////////////////////////////////
`define DCACHE_ADDR 32
`define DCACHE_WAYS 4
`define DCACHE_BLOCK_SIZE 4
`define DCACHE_WORD_SIZE 4
`define DCACHE_SETS 512

`define ICACHE_ADDR 32
`define ICACHE_WAYS 4
`define ICACHE_BLOCK_SIZE 8  
`define ICACHE_WORD_SIZE 4
`define ICACHE_SETS 512
/////////////////////////////////////////////////////////////////////////////////////

`define MISA_BITS   'h40141129 // 'h40101121 // A + F + I + M + U 
`define MTVEC_DEFUALT       30'h00000000
`define UTVEC_DEFAULT       30'h00000000
/////////////////////////// Register Mapping for Machine Mode Regs /////////////////
`define MVENDORID   'hF11 // Vendor ID                                                  
`define MARCHID     'hF12 // Architecture ID                                           
`define MIMPID      'hF13 // Implementation ID                                        
`define MHARTID     'hF14 // Hardware Thread ID                                      
`define MSTATUS     'h300 // Machine Status register                                
`define MISA        'h301 // ISA and extensions                                     
`define MEDELEG     'h302 // Machine exception delegation                               
`define MIDELEG     'h303 // Machine interrupt delegation                               
`define MIE         'h304 // Machine interrupt enable                                   
`define MTVEC       'h305 // Machine trap-handler base address                          
`define MSCRATCH    'h340 // Scratch rgister for machine trap hanglers                  
`define MEPC        'h341 // Machine exception program counter                          
`define MCAUSE      'h342 // Machine trap cause                                         
`define MBADADDR    'h343 // Machine bad address                                        
`define MIP         'h344 // Machine interrupt pending                                  
`define MBASE       'h380 // Base register                                              
`define MBOUND      'h381 // Bound register                                             
`define MIBASE      'h382 // Instruction Base register                                  
`define MIBOUND     'h383 // Instruction Bound register                                 
`define MDBASE      'h384 // Data Base Register                                         
`define MDBOUND     'h385 // Data Bound Register                                        
`define MCYCLE      'hB00 // Machine cycle counter                                      
`define MINSTRET    'hB02 // Machine instructions retired.                              
`define MCYCLEH     'hB80 // Upper 32 bits of mcycle                                   
`define MINSTRETH   'hB82 // Upper 32 bits of minstret.                                 
`define MUCOUNTEREN 'h320 // User-mode counter enable.                                  
`define UARTTX      'h77f // 
`define UARTRX      'h77e // 

`define USTATUS     'h000 // User status register
`define UIE         'h004 // User interrupt enable register
`define UTVEC       'h005 // User trap handler base address
`define USCRATCH    'h040 // Scratch register for user trap handlers
`define UEPC        'h041 // User exception program counter
`define UCAUSE      'h042 // User trap cause
`define UBADADDR    'h043 // User bad address
`define UIP         'h044 // User interrupt pending
`define FFLAGS      'h001 // FP Accrued exceptions
`define FRM         'h002 // FP Dynamic rounding mode
`define FCSR        'h003 // FP Control and status register
`define UCYCLE      'hC00 // cycle counter for RDCYCLE instruction.
`define UTIME       'hC01 // Tiemr for RDTIME instruction
`define UINSTRET    'hC02 // Instruction retired counter for RDINSTRET
`define UCYCLEH     'hC80 // Upper 32bits of UCYCLE
`define UTIMEH      'hC81 // Upper 32bits of UTIME
`define UINSTRETH   'hC82 // Upper 32bits of UINSTRET
////////////////////////////////////////////////////////////////////////////////////

