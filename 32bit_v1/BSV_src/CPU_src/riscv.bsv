/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Neel Gala
Email ID : neelgala@gmail.com

Description : 
This is the 64-bit core of the c_class processor. It containes rules for each stage. The description of each stage 
is given in the respective rules.
------------------------------------------------------------------------
*/

//mm- taking two instructions from cache using wire wr_ff_if_id_1_write

package riscv;

`include "defined_parameters.bsv"
import defined_types::*;
import ConfigReg::*;
import decoder::*;
import registerfile::*;
import execution_unit::*;
import memory_unit::*;
//import bpu_bimodal::*;
import defined_types::*;
import SpecialFIFOs::*;
import FIFO::*;
import FIFOF::*;
import DReg::*;
import set_associative_cache::*;
import CBuff::*;
	interface Ifc_riscv;
        // Methods for data fetch and write from/to the external evironment
    method Action _instruction_inputs(Data_from_mem mem_data);
    method ActionValue#(To_Memory#(32,4,8)) instruction_outputs_();
    method Action _data_inputs(From_Memory_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE,`DCACHE_BLOCK_SIZE) mem_data);
    method ActionValue#(To_Memory_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE,`DCACHE_BLOCK_SIZE)) data_outputs_();
    method    Action      sin(Bit#(1) in);
    method    Bit#(1)     sout();

	endinterface
	
	(*synthesize*)
	module mkriscv(Ifc_riscv);
  
    Reg#(Bit#(`Addr_width)) rg_pc <-mkReg('h80000000);
    FIFOF#(ID_IE_type) ff_id_ie_1 <-mkLFIFOF(); // instantiating ISB between decode and exec
    FIFOF#(ID_IE_type) ff_id_ie_2 <-mkLFIFOF();

		FIFOF#(IF_ID_type) ff_if_id_1 <-mkLFIFOF(); // instantiating ISB between fetch and decode
    FIFOF#(IF_ID_type) ff_if_id_2 <-mkLFIFOF();

		FIFOF#(IE_IMEM_type) ff_ie_imem <-mkLFIFOF(); // instantiating ISB between exec and memory
    FIFOF#(IE_IMEM_type) ff_ie_imem_2 <-mkLFIFOF();
		FIFOF#(IMEM_IWB_type) ff_imem_iwb <-mkLFIFOF(); // instantiating ISB between memory and write-back

    Wire#(Maybe#(Data_to_mem)) rg_data_to_instruction_memory <-mkDWire(tagged Invalid); // register to send data to the instruction cache
    Reg#(Maybe#(Data_from_mem)) rg_data_from_instruction_memory <-mkDReg(tagged Invalid); // register to receive data from the instruction cache
    Reg#(Maybe#(From_Memory_D#(32,4,4))) rg_data_from_data_memory <-mkDReg(tagged Invalid); // register to receive data from the instruction cache
		
    Wire#(Bool) wr_flush_decode_cache_1 <-mkDWire(False);	// if true inidicates that the entire pipe needs to be flushed
    Wire#(Bool) wr_flush_decode_cache_2 <-mkDWire(False);
    Wire#(Bool) wr_flush_all <-mkDWire(False);	// if true inidicates that the entire pipe needs to be flushed
    Wire#(Bool) wr_flush_exe_2 <-mkDWire(False);
    Wire#(Bool) wr_mem_bypass_1 <-mkDWire(False);
    Wire#(Bool) wr_mem_bypass_2 <-mkDWire(False);
    Wire#(Bool) wr_stall_exe_1 <- mkDWire(False);
    Wire#(Bool) wr_stall_exe_2 <- mkDWire(False);
    Wire#(Bool) wr_exe_1_firing <- mkDWire(False);
    Wire#(Bool) wr_exe_2_firing <- mkDWire(False);
    //ConfigReg#(Bool) flag_fire_exe_2 <- mkConfigReg(False);


		Wire#(Bit#(`Addr_width)) wr_effective_address_1 <-mkDWire(0); // captures the new pc when a branch is mispredicted.
    Wire#(Bit#(`Addr_width)) wr_effective_address_2 <-mkDWire(0);
		Wire#(Bit#(`Addr_width)) wr_effective_address1 <-mkDWire(0); // captures the new pc when a branch is mispredicted.
		Wire#(Maybe#(Operand_forwading_type)) wr_forward_from_MEM <-mkDWire(tagged Invalid);// holds the forwarded data from the memory stage
    //Wire#(Maybe#(Operand_forwading_type)) wr_forward_from_MEM_2 <-mkDWire(tagged Invalid);// holds the forwarded data from the memory stage
    Wire#(Maybe#(Trap_info)) wr_trap_data <-mkDWire(tagged Invalid);
    Wire#(Bit#(32)) wr_pc <-mkDWire(0);
    Wire#(Maybe#(IF_ID_type)) wr_ff_if_id_1_write <- mkDWire(tagged Invalid);
    Wire#(Maybe#(IF_ID_type)) wr_ff_if_id_2_write <- mkDWire(tagged Invalid);
    Wire#(Maybe#(ID_IE_type)) wr_ff_id_ie_1_write <- mkDWire(tagged Invalid);
    Wire#(Maybe#(ID_IE_type)) wr_ff_id_ie_2_write <- mkDWire(tagged Invalid);
    Wire#(Maybe#(IE_IMEM_type)) wr_ff_ie_imem_1_write <- mkDWire(tagged Invalid);
    Wire#(Maybe#(IE_IMEM_type)) wr_ff_ie_imem_2_write <- mkDWire(tagged Invalid);
    Wire#(Bit#(TLog#(`Buff_size))) wr_token_exe_1 <- mkDWire(0);
    Wire#(Bit#(TLog#(`Buff_size))) wr_token_exe_2 <- mkDWire(0);


    Ifc_execution_unit alu_unit_1 <-mkexecution_unit(); 
    Ifc_execution_unit alu_unit_2 <-mkexecution_unit(); 
    Ifc_registerfile register_file <-mkregisterfile();
    Ifc_memory_unit mem_unit <-mkmemory_unit();
    Ifc_icache icache <-mkicache();
    CBuff#(IE_IMEM_type, `Buff_size) rob <- mkMyCBuff();
//		Ifc_bpu_bimodal bpu <- mkbpu_bimodal();

(*descending_urgency = "rl_flush_first_two_stages1, rl_flush_first_two_stages2"*)
    rule rl_flush_first_two_stages1(wr_flush_decode_cache_1 && !wr_flush_all);
//      bpu._flush(wr_effective_address);
      rg_pc<=wr_effective_address_1;
      ff_if_id_1.clear();
      ff_if_id_2.clear();
      ff_id_ie_2.clear();
      rob.clear_partial(wr_token_exe_1);
      //register_file.validate_on_branch(True,wr_token_exe_1);
      //ff_ie_imem_2.clear();
      icache.flush();
    endrule
    rule rl_flush_first_two_stages2(wr_flush_decode_cache_2 && !wr_flush_all);
//      bpu._flush(wr_effective_address);
      rg_pc<=wr_effective_address_2;
      ff_if_id_1.clear();
      ff_if_id_2.clear();
      rob.clear_partial(wr_token_exe_2);
      icache.flush();
    endrule

    rule rl_flush_all_stages(wr_flush_all);
//      bpu._flush(wr_effective_address);
      rg_pc<=wr_effective_address1;
      ff_if_id_1.clear();
      ff_if_id_2.clear();
      ff_id_ie_1.clear();
      ff_id_ie_2.clear();
      icache.flush();
    endrule

    rule rl_send_pc_fetch_request(!wr_flush_decode_cache_1 && !wr_flush_decode_cache_2 && ff_if_id_1.notFull() && ff_if_id_2.notFull());
			$display($time,"\tFETCH: Address sent to I-Cache: %h",rg_pc);
        if (rg_pc[4:2] == 3'b111 ) begin
        icache.request_from_cpu(From_Cpu{address : truncate(rg_pc)});	
        rg_pc<=rg_pc+4;
        $display($time,"\tFETCH: Next PC: %h",rg_pc+4);
        end
        else begin
        icache.request_from_cpu(From_Cpu{address : truncate(rg_pc)}); 
        rg_pc<=rg_pc+8;
        $display($time,"\tFETCH: Next PC: %h",rg_pc+8);
        end
    endrule

    rule rg_receive_instruction(!wr_flush_decode_cache_1 && !wr_flush_decode_cache_2 && ff_if_id_1.notFull() && ff_if_id_2.notFull() );
      Maybe#(Exception_cause) exec=tagged Invalid;
      let resp_data<-icache.response_to_cpu;
        $display($time,"\t************* FETCH STAGE FIRING ************ PC: %h",resp_data.address);
        if(resp_data.misaligned_error==1)
          exec=tagged Valid Inst_addr_misaligned;
        else if (resp_data.bus_error==1)
          exec=tagged Valid Inst_access_fault;
 				// fill the next pipe fifo with the fetched instruction and the prediction type from the branch predictor
        //$display($time,"\tInstruction Fetched: %h \t PC: %h Prediction: ",resp_data.data_word,bpu.send_output_().prog_counter_,fshow(bpu.send_output_().prediction_));
        $display($time,"\tInstruction Fetched 2: %h \tInstruction Fetched 1: %h \t PC: %h Prediction: NotTaken",resp_data.data_word[63:32],resp_data.data_word[31:0],resp_data.address);
       
        if (resp_data.address[4:2] == 3'b111 ) begin
        let tok1 <- rob.getOneToken();
            $display($time,"\tFETCH: Block 8 data fetched from cache: %h",resp_data.address);
            wr_ff_if_id_1_write <= tagged Valid (IF_ID_type{ program_counter : resp_data.address,
                                instruction  :resp_data.data_word[63:32],
                                prediction:Predicted_notaken,
                                exception:exec,
                                token:tok1});
            wr_ff_if_id_2_write <= tagged Invalid;
        end
        else begin
        Tuple2#(Bit#(TLog#(`Buff_size)),Bit#(TLog#(`Buff_size))) tokens <- rob.getTwoToken(); // get two tokens 
        match {.tok1, .tok2} = tokens;
        $display($time,"\tFETCH: Normal data fetched from cache: %h",resp_data.address);
            wr_ff_if_id_1_write <= tagged Valid (IF_ID_type{ program_counter : resp_data.address,
                                instruction  :resp_data.data_word[31:0],
                                prediction:Predicted_notaken,
                                exception:exec,
                                token:tok1});
            wr_ff_if_id_2_write <= tagged Valid (IF_ID_type{ program_counter : resp_data.address + 4,
                                instruction  :resp_data.data_word[63:32],
                                prediction:Predicted_notaken,
                                exception:exec,
                                token:tok2});
        end
    endrule

    rule write_data_into_ff_if_id_1(wr_ff_if_id_1_write matches tagged Valid .data &&& !wr_flush_decode_cache_1 &&& !wr_flush_decode_cache_2 &&& !wr_flush_all );
      ff_if_id_1.enq(data);
    endrule
    rule write_data_into_ff_if_id_2(wr_ff_if_id_2_write matches tagged Valid .data &&& !wr_flush_decode_cache_1 &&& !wr_flush_decode_cache_2 &&& !wr_flush_all );
      ff_if_id_2.enq(data);
    endrule

    rule rl_get_instruction_from_cache(rg_data_from_instruction_memory matches tagged Valid .mem_data);
      icache.response_from_memory(From_Memory{data_line:truncate(mem_data.read_data),
                                              bus_error:mem_data.bus_error,
                                              misaligned_error:mem_data.misaligned_error,
                                              address:truncate(mem_data.address)});
    endrule

    (*preempts = "rl_decode_both, rl_decode_pipe1"*)
    rule rl_decode_both (!wr_flush_decode_cache_1 && !wr_flush_decode_cache_2 && !wr_flush_all && !wr_exe_2_firing && !wr_exe_1_firing);
      let x1 = fn_decoder(ff_if_id_1.first().instruction,ff_if_id_1.first.prediction);
      //ff_if_id_1.deq();
      let x2 = fn_decoder(ff_if_id_2.first().instruction,ff_if_id_2.first.prediction);
      //ff_if_id_2.deq();
      Maybe#(Exception_cause) exception1=ff_if_id_1.first().exception;
      if(exception1 matches tagged Invalid &&& x1.inst_type==ILLEGAL)
        exception1=tagged Valid Illegal_inst;
      wr_ff_id_ie_1_write<= tagged Valid(ID_IE_type{decoder_data:x1,
                              program_counter:ff_if_id_1.first().program_counter,
                              exception:exception1,
                              token:ff_if_id_1.first().token});

      Maybe#(Exception_cause) exception2=ff_if_id_2.first().exception;
      if(exception2 matches tagged Invalid &&& x2.inst_type==ILLEGAL)
        exception2=tagged Valid Illegal_inst;
      wr_ff_id_ie_2_write<= tagged Valid(ID_IE_type{decoder_data:x2,
                              program_counter:ff_if_id_2.first().program_counter,
                              exception:exception2,
                              token:ff_if_id_2.first().token});

      $display($time,"\t********** DECODE STAGE FIRING ************") ;
      $display($time,"\tDC_STAGE1: Instruction : %h token:%d PC : %h is ",ff_if_id_1.first().instruction,ff_if_id_1.first().token,ff_if_id_1.first().program_counter," ",fshow(x1.inst_type)," Rd_type: ",fshow(x1.rd_type));
      $display($time,"\tDC_STAGE2: Instruction : %h token:%d PC : %h is ",ff_if_id_2.first().instruction,ff_if_id_2.first().token,ff_if_id_2.first().program_counter," ",fshow(x2.inst_type)," Rd_type: ",fshow(x2.rd_type));
      $display($time,"\tRs1: %d",x1.rs1," ",fshow(x1.rs1_type)," Rs2: %d",x1.rs2," ",fshow(x1.rs2_type),`ifdef spfpu " Rs3: %d",x1.rs3, `endif " Rd: %d",x1.rd);  
      $display($time,"\tRs1: %d",x2.rs1," ",fshow(x2.rs1_type)," Rs2: %d",x2.rs2," ",fshow(x2.rs2_type),`ifdef spfpu " Rs3: %d",x2.rs3, `endif " Rd: %d",x2.rd);   

    endrule

    rule rl_decode_pipe1 (!wr_flush_decode_cache_1 && !wr_flush_decode_cache_2 && !wr_flush_all && ff_if_id_1.first().program_counter[4:2] == 3'b111 && !wr_exe_2_firing && !wr_exe_1_firing );
      let x1 = fn_decoder(ff_if_id_1.first().instruction,ff_if_id_1.first.prediction);
      //ff_if_id_1.deq();
      Maybe#(Exception_cause) exception1=ff_if_id_1.first().exception;
      if(exception1 matches tagged Invalid &&& x1.inst_type==ILLEGAL)
        exception1=tagged Valid Illegal_inst;
      wr_ff_id_ie_1_write<= tagged Valid(ID_IE_type{decoder_data:x1,
                              program_counter:ff_if_id_1.first().program_counter,
                              exception:exception1,
                              token:ff_if_id_1.first().token});
      $display($time,"\t********** DECODE STAGE FIRING ************") ;
      $display($time,"\tDC_STAGE1.1: Instruction : %h token:%d PC : %h is ",ff_if_id_1.first().instruction,ff_if_id_1.first().token,ff_if_id_1.first().program_counter," ",fshow(x1.inst_type)," Rd_type: ",fshow(x1.rd_type));
      $display($time,"\tRs1: %d",x1.rs1," ",fshow(x1.rs1_type)," Rs2: %d",x1.rs2," ",fshow(x1.rs2_type),`ifdef spfpu " Rs3: %d",x1.rs3, `endif " Rd: %d",x1.rd);  

    endrule
    
    
    rule rl_operand_fetch_1;
      let decoded_data_1=ff_id_ie_1.first().decoder_data;
      let token1 = ff_id_ie_1.first().token;
      $display($time,"\tsending inputs from decode stage 1 to RF token: %d", ff_id_ie_1.first().token,"  Rs1: %d",decoded_data_1.rs1," "," Rs2: %d",decoded_data_1.rs2," Rd: %d",decoded_data_1.rd);
      if(decoded_data_1.inst_type!=PRIVILEGED) 
        register_file._inputs_from_decode_stage_1(decoded_data_1.rs1,
                                              decoded_data_1.rs1_type,
                                              decoded_data_1.rs2,
                                              decoded_data_1.rs2_type,
                                              `ifdef spfpu decoded_data_1.rs3,`endif
                                              decoded_data_1.rd,
                                              decoded_data_1.rd_type,
                                              True,
                                              decoded_data_1.funct3,
                                              truncate(decoded_data_1.immediate_value),
                                              token1);
    endrule
    rule rl_operand_fetch_2(!wr_flush_exe_2);
    let decoded_data_2=ff_id_ie_2.first().decoder_data;
    let token2 = ff_id_ie_2.first().token;
    $display($time,"\tsending inputs from decode stage 2 to RF token: %d", ff_id_ie_2.first().token,"  Rs1: %d",decoded_data_2.rs1," "," Rs2: %d",decoded_data_2.rs2," Rd: %d",decoded_data_2.rd);
    if(decoded_data_2.inst_type!=PRIVILEGED) 
        register_file._inputs_from_decode_stage_2(decoded_data_2.rs1,
                                              decoded_data_2.rs1_type,
                                              decoded_data_2.rs2,
                                              decoded_data_2.rs2_type,
                                              `ifdef spfpu decoded_data_2.rs3,`endif
                                              decoded_data_2.rd,
                                              decoded_data_2.rd_type,
                                              True,
                                              decoded_data_2.funct3,
                                              truncate(decoded_data_2.immediate_value),
                                              token2);
      endrule 
    

    // endrule
    (* preempts = "write_data_ff_id_ie_both,write_data_ff_id_ie_1"*)
    rule write_data_ff_id_ie_both (wr_ff_id_ie_1_write matches tagged Valid .data1 &&& wr_ff_id_ie_2_write matches tagged Valid .data2 &&& !wr_flush_all);
      ff_id_ie_1.enq(data1);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
      ff_if_id_1.deq();
      ff_id_ie_2.enq(data2);
      ff_if_id_2.deq();
       $display($time,"\t write in execution units");
    endrule 
    rule write_data_ff_id_ie_1 (wr_ff_id_ie_1_write matches tagged Valid .data &&& !wr_flush_all );
      ff_id_ie_1.enq(data);
      ff_if_id_1.deq();
      $display($time,"\t write in execution unit 1 only");
    endrule 

    
    (* conflict_free = "rl_execute_1,rl_execute_2" *)
    rule rl_execute_1(!wr_flush_all);
     $display($time,"\t********** EXECUTION STAGE FIRING1 ************ PC: :%h, token: %d",ff_id_ie_1.first().program_counter,ff_id_ie_1.first().token)  ;
      let decoded_data=ff_id_ie_1.first().decoder_data;
      wr_exe_1_firing <= True;
      let token1 = ff_id_ie_1.first().token;

      if(ff_id_ie_1.first.exception matches tagged Invalid)begin
        if(decoded_data.inst_type == ALU)begin
          if(register_file.output_to_decode_stage_1 matches tagged Valid .operands)begin
            $display($time,"\tExecution: ALU Operation");
            `ifdef spfpu
              $display($time,"\t Rs1: %h Rs2: %h Rs3: %h Immediate: %h",operands.rs1,operands.rs2,operands.rs3,decoded_data.immediate_value);
            `elsif
              $display($time,"\t Rs1: %h Rs2: %h Immediate: %h",operands.rs1,operands.rs2,decoded_data.immediate_value);
            `endif
            let x <- alu_unit_1.inputs(decoded_data.opcode, decoded_data.funct3, decoded_data.funct7,
                                            operands.rs1, operands.rs2, `ifdef spfpu operands.rs3, `endif
                                            decoded_data.immediate_value, ff_id_ie_1.first().program_counter,
                                            decoded_data.rd, decoded_data.pred_type,
                                            decoded_data.is_imm, decoded_data.rd_type);
            if(x matches tagged Valid .exec_result)begin
              //flag_fire_exe_2 <= True;
              ff_id_ie_1.deq(); // release the previous FIFO
              rob.put_1(token1,IE_IMEM_type{alu_result:exec_result.out,buffer_data:Buffer_data{
                            destination:exec_result.destination,
                            rd_type:ff_id_ie_1.first.decoder_data.rd_type,
                            program_counter:ff_id_ie_1.first().program_counter,
                            exception:exec_result.exception,
                            is_privilege:False},
                            token:ff_id_ie_1.first().token});
                $display($time,"\t Got result from the Arithmetic unit: %h :%h",exec_result.out.aluresult, exec_result.out.memory_data);
              if(exec_result.pred_result matches tagged Mispredicted .z)begin
                $display($time,"	Misprediction PC : %h New PC: %h",ff_id_ie_1.first().program_counter,z);
                wr_flush_decode_cache_1<=True;
                wr_flush_exe_2 <= True; 
                wr_token_exe_1 <= ff_id_ie_1.first().token;
               // rob.reset_tail(ff_id_ie_1.first().token);
                wr_effective_address_1<=z; // change the PC on flush
                //bpu._training(ff_id_ie.first.program_counter, exec_result.training_data.branch_address, exec_result.training_data.actual);
              end
            end
          end
        end
        else if(decoded_data.inst_type==PRIVILEGED)begin
            $display($time,"\tExecution: PRIVILEGE Operation");
            rob.put_1(token1,IE_IMEM_type{alu_result:Alu_output{aluresult:zeroExtend({decoded_data.opcode,decoded_data.funct3,decoded_data.immediate_value[11:0],decoded_data.rs1}), bypass:True,
                                                             memory_data:0, word_size:0,
                                                             signextend:0, mem_type:Load,fflags:0},
                                        buffer_data:Buffer_data{rd_type:IntegerRF,
                                                          destination:decoded_data.rd, exception:ff_id_ie_1.first.exception,
                                                          program_counter:ff_id_ie_1.first().program_counter,
                                                          is_privilege:True},
                                                          token:ff_id_ie_1.first().token});
            ff_id_ie_1.deq();
        end
        else if(decoded_data.inst_type==NOP)begin
          ff_id_ie_1.deq();
          $display($time,"\tExecution: NOP");
        end
      end
      else begin // Exception case
        rob.put_1(token1,IE_IMEM_type{alu_result:Alu_output{bypass:True,aluresult:?,memory_data:?,word_size:?,signextend:?,
                                                         mem_type:Load,fflags:0},
                                    buffer_data:Buffer_data{rd_type:IntegerRF,
                                                      destination:decoded_data.rd,
                                                      exception:ff_id_ie_1.first.exception,
                                                      program_counter:ff_id_ie_1.first().program_counter,
                                                      is_privilege:True},
                                                      token:ff_id_ie_1.first().token});
        $display($time,"\t Exception :",fshow(fromMaybe(?,ff_id_ie_1.first().exception))," Raised");
        ff_id_ie_1.deq();
      end
    endrule
    rule rl_execute_2(!wr_flush_all && !wr_flush_exe_2 );
      $display($time,"\t********** EXECUTION STAGE FIRING2 ************ PC: :%h, token: %d",ff_id_ie_2.first().program_counter,ff_id_ie_2.first().token)  ;
      let decoded_data=ff_id_ie_2.first().decoder_data;
      let token2 = ff_id_ie_2.first().token;
      wr_exe_2_firing <= True;
      if(ff_id_ie_2.first.exception matches tagged Invalid)begin
        if(decoded_data.inst_type == ALU)begin
          if(register_file.output_to_decode_stage_2 matches tagged Valid .operands)begin
            $display($time,"\tExecution: ALU Operation");
            `ifdef spfpu
              $display($time,"\t Rs1: %h Rs2: %h Rs3: %h Immediate: %h",operands.rs1,operands.rs2,operands.rs3,decoded_data.immediate_value);
            `elsif
              $display($time,"\t Rs1: %h Rs2: %h Immediate: %h",operands.rs1,operands.rs2,decoded_data.immediate_value);
            `endif
            let x <- alu_unit_2.inputs(decoded_data.opcode, decoded_data.funct3, decoded_data.funct7,
                                            operands.rs1, operands.rs2, `ifdef spfpu operands.rs3, `endif
                                            decoded_data.immediate_value, ff_id_ie_2.first().program_counter,
                                            decoded_data.rd, decoded_data.pred_type,
                                            decoded_data.is_imm, decoded_data.rd_type);
            if(x matches tagged Valid .exec_result)begin
              //flag_fire_exe_2 <= False;
              ff_id_ie_2.deq(); // release the previous FIFO
              rob.put_2(token2,IE_IMEM_type{alu_result:exec_result.out,buffer_data:Buffer_data{
                            destination:exec_result.destination,
                            rd_type:ff_id_ie_2.first.decoder_data.rd_type,
                            program_counter:ff_id_ie_2.first().program_counter,
                            exception:exec_result.exception,
                            is_privilege:False},
                            token:ff_id_ie_2.first().token});
                $display($time,"\t Got result from the Arithmetic unit: %h :%h",exec_result.out.aluresult, exec_result.out.memory_data);
              if(exec_result.pred_result matches tagged Mispredicted .z)begin
                $display($time,"  Misprediction PC : %h New PC: %h",ff_id_ie_2.first().program_counter,z);
                wr_flush_decode_cache_2<=True; 
                wr_token_exe_2 <= ff_id_ie_2.first().token;
                //rob.reset_tail(ff_id_ie_2.first().token);
                wr_effective_address_2<=z; // change the PC on flush
                //bpu._training(ff_id_ie.first.program_counter, exec_result.training_data.branch_address, exec_result.training_data.actual);
              end
            end
          end
        end
        else if(decoded_data.inst_type==PRIVILEGED)begin
            $display($time,"\tExecution: PRIVILEGE Operation");
            rob.put_2(token2,IE_IMEM_type{alu_result:Alu_output{aluresult:zeroExtend({decoded_data.opcode,decoded_data.funct3,decoded_data.immediate_value[11:0],decoded_data.rs1}), bypass:True,
                                                             memory_data:0, word_size:0,
                                                             signextend:0, mem_type:Load,fflags:0},
                                        buffer_data:Buffer_data{rd_type:IntegerRF,
                                                          destination:decoded_data.rd, exception:ff_id_ie_2.first.exception,
                                                          program_counter:ff_id_ie_2.first().program_counter,
                                                          is_privilege:True},
                                                          token:ff_id_ie_2.first().token});
            ff_id_ie_2.deq();
        end
        else if(decoded_data.inst_type==NOP)begin
          ff_id_ie_2.deq();
        end
      end
      else begin // Exception case
      rob.put_2(token2,IE_IMEM_type{alu_result:Alu_output{bypass:True,aluresult:?,memory_data:?,word_size:?,signextend:?,
                                                         mem_type:Load,fflags:0},
                                    buffer_data:Buffer_data{rd_type:IntegerRF,
                                                      destination:decoded_data.rd,
                                                      exception:ff_id_ie_2.first.exception,
                                                      program_counter:ff_id_ie_2.first().program_counter,
                                                      is_privilege:True},
                                                      token:ff_id_ie_2.first().token});
        $display($time,"\t Exception :",fshow(fromMaybe(?,ff_id_ie_2.first().exception))," Raised");
      ff_id_ie_2.deq();
      end
    endrule

    rule rl_forwarding_data_to_decode;
      register_file._forwarding_from_memory_1(wr_forward_from_MEM); // forwarding from memory unit.
      if(wr_forward_from_MEM matches  tagged Valid .data) 
      $display($time,"\t MEM_STAGE: forwarding data %h for reg %d and token %d",data.data_forward,data.rd_forward,data.token);
    endrule
    rule rl_memory_stage_buff_read(!wr_flush_all);
     Maybe#(IE_IMEM_type) result <- rob.getOneResult();
      rob.deqOneResult(); 
      if(result matches tagged Valid .data_mem ) begin
        ff_ie_imem.enq(data_mem);
      end

    endrule
    rule rl_memory_stage(!wr_flush_all);
      let buf_data=ff_ie_imem.first().buffer_data;
      let alu_result=ff_ie_imem.first().alu_result;

      $display($time,"\t*****************MEMORY STAGE*************************\t PC: %h Token: %d",buf_data.program_counter,ff_ie_imem.first().token);
      if(alu_result.bypass)begin 
        $display($time,"\t MEM_STAGE: Bypassed ");
        $display($time,"\t MEM_STAGE: Forwarding: Rd: %d %h",buf_data.destination,alu_result.aluresult," ",fshow(buf_data.rd_type));
        ff_ie_imem.deq;
        ff_imem_iwb.enq(IMEM_IWB_type{destination_value:alu_result.aluresult,
                                      badaddr:0,
                                      fflags:alu_result.fflags,
                                      buffer_data:buf_data,
                                      token:ff_ie_imem.first().token});
        wr_forward_from_MEM <= tagged Valid Operand_forwading_type {data_forward  : alu_result.aluresult,
                                rd_forward    : buf_data.destination,
                                rd_type       : buf_data.rd_type,
                                valid         : !buf_data.is_privilege,
                                token:ff_ie_imem.first().token
                              };
      end
      else begin
        let memresp <- mem_unit.communicate_with_core(alu_result);
        if(memresp matches tagged Valid .val)begin
          $display($time,"\t MEM_STAGE: Forwarding: Rd: %d %h",buf_data.destination,alu_result.aluresult," ",fshow(buf_data.rd_type));
          if(buf_data.exception matches tagged Invalid)
            buf_data.exception=val.exception;

          ff_ie_imem.deq;
          ff_imem_iwb.enq(IMEM_IWB_type{destination_value:val.destination_value,
                                        badaddr:val.badaddr,
                                        fflags:alu_result.fflags,
                                        buffer_data:buf_data,
                                        token:ff_ie_imem.first().token});
          wr_forward_from_MEM <= tagged Valid Operand_forwading_type {  data_forward  : val.destination_value,
                                rd_forward    :buf_data.destination,
                                rd_type       :buf_data.rd_type,
                                valid         : True,
                                token:ff_ie_imem.first().token};
        end
        else begin
          wr_forward_from_MEM <= tagged Valid Operand_forwading_type {  data_forward  : ?,
                                rd_forward    :buf_data.destination,
                                rd_type       :buf_data.rd_type,
                                valid         : False,
                                token:ff_ie_imem.first().token};
        end
      end
    endrule
    rule rl_write_back;
      let buf_data=ff_imem_iwb.first.buffer_data;
      let token = ff_imem_iwb.first().token;
      $display($time,"\t*****************WRITE BACK STAGE*************************\t PC: %h",buf_data.program_counter);
      ff_imem_iwb.deq(); // release the previous FIFO

      let csr_result<-register_file.csr_access_1(CSRInsn{rs1_data:0,
                                                      is_privilege:buf_data.is_privilege,
                                                      exception:buf_data.exception,
                                                      pc:buf_data.program_counter,
                                                      badaddr:ff_imem_iwb.first.badaddr,
                                                      rd_data:ff_imem_iwb.first.destination_value,
                                                      fflags:ff_imem_iwb.first.fflags});
      `ifdef simulate
        register_file._print_all_rf_1(buf_data.program_counter,buf_data.destination,csr_result.destination_value,buf_data.rd_type);
      `endif
      if(csr_result.redirect)begin
        $display("Flushing the PIPE. Jumping to Address: %h",csr_result.address);
        wr_flush_all<=True; 
        wr_effective_address1<=csr_result.address; // change the PC on flush
      end
      else begin
        register_file._inputs_from_writeback_stage_1(buf_data.destination,buf_data.rd_type,csr_result.destination_value,True,token);  
      end
    endrule

    rule send_data_to_dcache_from_memory(rg_data_from_data_memory matches tagged Valid .info);
      mem_unit.input_from_memory(info);
    endrule


    rule rl_clock;
      $display("	\n\n");
    endrule

		//////////////////////////// definition of methods /////////////////////////////////////////////////////////////////////////////////
    method Action _instruction_inputs(Data_from_mem mem_data);
      rg_data_from_instruction_memory<=tagged Valid mem_data;
    endmethod

    method ActionValue#(To_Memory#(32,4,8)) instruction_outputs_=icache.request_to_memory;

    method Action _data_inputs(From_Memory_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE,`DCACHE_BLOCK_SIZE) mem_data);
      rg_data_from_data_memory<= tagged Valid mem_data;
    endmethod

    method ActionValue#(To_Memory_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE,`DCACHE_BLOCK_SIZE)) data_outputs_=mem_unit.data_to_memory;

    method    Action      sin(Bit#(1) in);
      register_file.sin(in);
    endmethod
    method    Bit#(1)     sout();
      return register_file.sout;
    endmethod
	endmodule
endpackage


