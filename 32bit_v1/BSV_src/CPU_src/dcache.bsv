/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Neel Gala
Email ID : neelgala@gmail.com

Description: This module implements a parameterized blocking data cache. 
In case of Load-hit the data-cache simply sends a "word" (which could be 32 or 64 as defined during instantiation).
The core has to then further process this word and sign or zeroextend it incase it requires a byte or a half word.
In case of Load-miss the data cache fetches the entire line from the Main-Memory and then sends the respective word
to the core for further processing. Thus whenever the cache is enabled it will communicate with the external memory
only in line reads/write and nothing less than a line.
In case of Store-hit the data-cache updates the required bits (byte,half word or word) with the data provided by the
core. This is 2 cycle process since BRAMs are being used. The core is given an acknowledgement only in the second cycle.
In case of Store-miss the required line is fetched from the external memory in bursts of words. The specific which
requires a store is updated and the entire line is written into the cache.

During any miss if the line that needs to replaced is dirty then it is written to the external memory after the read/write
  operation of the new line is over.
*/
package  dcache;
  import defined_types::*;
  import ClientServer ::*;
  import GetPut       ::*;
  import Connectable  ::*;
  import FIFO ::*;
  import FIFOF ::*;
  import SpecialFIFOs::*;
  import BRAM::*;
  import ConfigReg::*;
  import Vector::*;
  import DReg::*;
  `include "defined_parameters.bsv"

  interface Ifc_set_associative_dcache#(numeric type addr_width,numeric type ways, numeric type word_size, numeric type block_size, numeric type sets);
    method Action request_from_cpu(From_Cpu_D#(addr_width,word_size) req, Bool enable);
    method Maybe#(To_Cpu_D#(addr_width,word_size)) response_to_cpu;
    method ActionValue#(To_Memory_D#(addr_width,word_size,block_size)) request_to_memory;
    method Action response_from_memory(From_Memory_D#(addr_width,word_size,block_size) resp);

    method Action clear_all();
  endinterface

  module mkset_associative_dcache#(parameter String name)(Ifc_set_associative_dcache#(_addr_width,_ways,_word_size,_block_size,_sets))
  	provisos(
		Log#(_word_size,log_word_size),
		Log#(_block_size,log_block_size),
		Log#(_sets,log_sets),
		Add#(intermediate2,log_sets,_addr_width),
		Add#(intermediate3,log_word_size,intermediate2),
		Add#(num_of_tag_bits,log_block_size,intermediate3),
		Add#(log_word_size,log_block_size,num_of_offset_bits),
  	Add#(a1,_word_size,8),							//to limit the word_size that user inputs to max. of 8 bytes (or doubleword)
    /* As per request of the BSV compiler */
    	Add#(a__, TMul#(_word_size, 8), TMul#(8, TMul#(_word_size, _block_size))),
    	Add#(c__, 1, TSub#(_addr_width, TAdd#(TLog#(_block_size),TLog#(_word_size)))),
     	Add#(b__, TMul#(8, _word_size), TMul#(8, TMul#(_word_size, _block_size))),
     	Add#(28, d__, _addr_width),
      Add#(e__, 16, TMul#(8, _word_size)),
      Add#(f__, 8, TMul#(8, _word_size)),
      Add#(g__, 16, TMul#(_word_size, 8)),
      Add#(h__, 8, TMul#(_word_size, 8)),
      Add#(num_of_tag_bits, i__, _addr_width),
      Add#(j__, TMul#(_word_size, 8), TMul#(_block_size, TMul#(8, _word_size)))
);

    let v_ways=valueOf(_ways);
    let v_sets=valueOf(_sets);
    let v_num_of_tag_bits=valueOf(num_of_tag_bits);
    let v_num_of_offset_bits=valueOf(num_of_offset_bits);
    let v_word_size=valueOf(_word_size);
    let v_block_size=valueOf(_block_size);
    let v_addr_width=valueOf(_addr_width);
    let v_num_of_bytes=valueOf(TLog#(_word_size)); // number of bits to represent each byte in a word

  // The following function finds the lower bit offset for the word that is being accessed (either for load or store) irrespective of the
  // size of transfer (byte, half word or word). For eg. if a word in the data cache is defined as 32-bits then the output
  // of this function can be 0, 32, 64.. and so on.
  function Bit#(TLog#(TMul#(8,TMul#(_word_size,_block_size)))) find_word_offset(Bit#(_addr_width) cpu_addr);
    let v_word_size=valueOf(_word_size); // number of bytes present in each word.
    Bit#(TLog#(TMul#(8,TMul#(_word_size,_block_size)))) lower_offset=0;
    for(Integer i=0;i<v_block_size;i=i+1)begin // iterate over number of words in a line
      if(fromInteger(i)==unpack(cpu_addr[v_num_of_offset_bits-1:v_num_of_bytes]))begin // the lower order bits used to access each word.
        lower_offset= fromInteger(v_word_size)*8*fromInteger(i); // calculating the lower bit index value. For. eg if word is 32bit. possible values are 0,32,64,96...
      end
    end
    return lower_offset;
  endfunction

  // The following function finds the lower bit offset of the byte that needs to be accessed by the core. 
  function Bit#(TLog#(TMul#(8,TMul#(_word_size,_block_size)))) find_byte_offset(Bit#(_addr_width) cpu_addr);
    let v_word_size=valueOf(_word_size);
    Bit#(TLog#(TMul#(8,TMul#(_word_size,_block_size)))) lower_offset=0;
    for(Integer i=0;i<v_word_size;i=i+1)begin // iterate over the number of bytes present in a word
      if(fromInteger(i)==unpack(cpu_addr[v_num_of_bytes-1:0]))begin // the lower order bits used to access each byte.
        lower_offset= 8*fromInteger(i); // calculating the byte offset within a word. For. eg if word is 4-bytes. possible values are 0,8,16 and 24
      end
    end
    return lower_offset;
  endfunction

  // The following function updates a particular data-line of the cache with a modified word. The placement of the modified word
  // is provided by the word_offset variable. The following lines of codes a dirty trick to get through constraints posed by the
  // bluespec compiler. 
  function Bit#(TMul#(8,TMul#(_word_size,_block_size))) update_data_line (Bit#(TLog#(TMul#(8,TMul#(_word_size,_block_size)))) word_offset, Bit#(TMul#(8,TMul#(_word_size,_block_size))) line, Bit#(TMul#(8,_word_size)) write_data);
    let x= line[fromInteger(8*v_word_size*v_block_size-1):word_offset+(8*fromInteger(v_word_size))];
    Bit#(word_offset) y = (word_offset==0)?0:line[word_offset-1:0];
    Bit#(TMul#(8,TMul#(_word_size,_block_size))) new_line={x,write_data}; 
    new_line=new_line<< word_offset;
    new_line=new_line|y;
    return new_line;
  endfunction

  // The following function updates a particular word with the data provided by the core. The update can be either a byte update,
  // a half word update or a word update which is defined using the byte_offset and transfer_size variables. The byte_offset
  // indicates the lower bit of the word from which update needs to begin and the transfer_size defines whether 8, 16 or 32 bits
  // need to be updated/
  function ActionValue#(Bit#(TMul#(_word_size,8))) update_word (Bit#(TLog#(TMul#(8,TMul#(_word_size,_block_size)))) byte_offset, Bit#(TMul#(_word_size,8)) word, Bit#(TMul#(8,_word_size)) write_data, Bit#(2) transfer_size)=
    actionvalue
    Bit#(byte_offset) y =(byte_offset==0?0:word[byte_offset-1:0]); // the byte_offset-1 computation rolls back when byte_offset=0 and hence the condition.
    Bit#(TMul#(8,_word_size)) new_word;
    if(transfer_size==0)begin // byte update
      let x=word[fromInteger(8*v_word_size)-1:byte_offset+8];
      new_word={x,write_data[7:0]}; // by default the upper bits are zero extended by the compiler
    end
    else if(transfer_size=='d1)begin // half word update
      let x=word[fromInteger(8*v_word_size)-1:byte_offset+16];
      new_word={x,write_data[15:0]}; // by default the upper bits are zero extended by the compiler 
    end
    else begin // a word update of 32bits.
      new_word=write_data;
    end
    $display("New_word: %h",new_word);
    new_word=new_word<<byte_offset;
    new_word=new_word|y;
    return new_word;
  endactionvalue;


    Wire#(Maybe#(To_Cpu_D#(_addr_width, _word_size))) wr_response_to_cpu <-mkDWire(tagged Invalid);
    FIFOF#(From_Cpu_D#(_addr_width,_word_size)) ff_request_from_cpu <-mkFIFOF1(); // using FIFO of the same size as BRAM Output FIFO depth.
//    FIFOF#(From_Cpu_D#(_addr_width,_word_size)) ff_memory_wait <-mkLFIFOF(); // using FIFO of the same size as BRAM Output FIFO depth.
    FIFOF#(To_Memory_D#(_addr_width,_word_size,_block_size)) ff_request_to_memory <-mkSizedBypassFIFOF(1);
    FIFOF#(From_Memory_D#(_addr_width,_word_size,_block_size)) ff_response_from_memory <-mkSizedBypassFIFOF(1);
    FIFOF#(Current_Store#(_ways,_addr_width,_block_size,_word_size)) ff_current_store <-mkSizedBypassFIFOF(1);

    Reg#(Bit#(TAdd#(1,TLog#(_sets)))) rg_index <-mkReg(0);
    Reg#(Bool) rg_initialize<-mkReg(True);
    Reg#(Bit#(TLog#(_ways))) rg_replace_block <-mkReg(0);
    Reg#(Bool) rg_enable <-mkReg(True);
    Reg#(Bit#(TMul#(8,TMul#(_word_size,_block_size)))) rg_data_line_frm_memory <-mkReg(0);
    Reg#(Bit#(TLog#(_block_size))) rg_word_count <-mkReg(0);
    Reg#(Bool) rg_dirty_line_write_back <-mkReg(False);
    Reg#(Bit#(TMul#(8,TMul#(_word_size,_block_size)))) rg_dirty_line_data <-mkReg(0);
    Reg#(Bit#(_addr_width)) rg_dirty_line_addr <-mkReg(0);
    Reg#(Bool) rg_line_write_stall <-mkReg(False);
    
		
    BRAM_Configure cfg = defaultValue ;
    cfg.latency=1;
    cfg.outFIFODepth=2; // for pipelined cache
    BRAM2Port#(Bit#(TLog#(_sets)), Bit#(num_of_tag_bits)) tag [v_ways]; // declaring as many tag arrays as there are number of `Ways. Depth of each array is the number of sets.
    BRAM2Port#(Bit#(TLog#(_sets)), Bit#(1)) valid [v_ways];     // declaring as many alid bit arrays as there are number of `Ways. Depth of each array is the number of sets.
    BRAM2Port#(Bit#(TLog#(_sets)), Bit#(TMul#(8,TMul#(_word_size,_block_size)))) data [v_ways]; // decalring similar data arrays. each of width equal to block size.
    BRAM2Port#(Bit#(TLog#(_sets)), Bit#(1)) dirty [v_ways];     // declaring as many dirty bit arrays as there are number of `Ways. Depth of each array is the number of sets.
	
    Reg#(Bit#(TSub#(_ways,1))) pseudo_lru [v_sets]; // This holds the bits for pseduo-lru replacement policy within each set.
    for(Integer i=0;i<v_sets;i=i+1)
      pseudo_lru[i]<-mkReg(0);
    
    for(Integer i=0;i<v_ways;i=i+1) begin
      tag[i] <- mkBRAM2Server(cfg);		
      data[i] <- mkBRAM2Server(cfg);
      valid[i]<-mkBRAM2Server(cfg);
      dirty[i]<-mkBRAM2Server(cfg);
    end

    // This rule initializes the cache so that each line is invalid and not dirty. This task will take
    // "set" number cycles to complete on each system reset.
    rule initialize_cache(rg_initialize);
    	rg_index<=rg_index+1;
      	for(Integer i=0;i<v_ways;i=i+1)begin
			    valid[i].portA.request.put(BRAMRequest{write:True,address:truncate(rg_index), datain:0,responseOnWrite:False});
			    dirty[i].portA.request.put(BRAMRequest{write:True,address:truncate(rg_index), datain:0,responseOnWrite:False});
      	end
      	if(rg_index==fromInteger(v_sets-1))
        	rg_initialize<=False;
		  $display($time,"\t",name,"\tFlushing Cache: %d",rg_initialize);
      //$display("Set bits: %d Block Bits: %d Word Bits: %d Tag Bits: %d",valueOf(log_sets),valueOf(log_block_size),valueOf(log_word_size),valueOf(num_of_tag_bits));
    endrule

    // When the cache is disabled the request is directly sent to the
    // external memory.
    rule cache_is_disabled(!rg_enable);
        ff_request_to_memory.enq(To_Memory_D {
                        				 address:ff_request_from_cpu.first().address,
                                 transfer_size:ff_request_from_cpu.first.transfer_size,
                                 data_line:zeroExtend(ff_request_from_cpu.first.data),
                                 burst_length:1,
                                 ld_st:ff_request_from_cpu.first.load_store});
    endrule

    // This rule is fired when a read/write request has been taken from the core and when cache is enabled.
    // If cache is disabled then no request is put in the BRAMs and thus this will not fire. 
    // The explicit use of rg_enable in the condition is to indicate the compiler that this rule
    // and the previous one are mutually exclusive.
    rule read_from_bram_and_load_buffer(!rg_initialize && rg_enable);
      let transfersize =ff_request_from_cpu.first.transfer_size;
      Bit#(num_of_tag_bits) tag_values[v_ways];	// hold the tag values
      Bit#(1) valid_values [v_ways];		// holds the valid bits
      Bit#(1) dirty_values [v_ways];		// holds the dirty bits
      Bit#(TMul#(8,TMul#(_word_size,_block_size))) data_values [v_ways]; // holds the cache lines.
      Bit#(TSub#(_addr_width,num_of_offset_bits)) input_tag=ff_request_from_cpu.first.address[v_addr_width-1:v_num_of_offset_bits]; // tag of the address from the core.
      Bit#(v_num_of_offset_bits) zero_offset_bits=0;
      // get responses form the respective BRAMs.
			for(Integer i=0;i<v_ways;i=i+1)begin
				valid_values[i]<-valid[i].portA.response.get();
				tag_values[i]<-tag[i].portA.response.get();
				data_values[i]<-data[i].portA.response.get();
				dirty_values[i]<-dirty[i].portA.response.get();
			end
      ///////////////////////////////////////////////// calculate the upper and lower offsets. ////////////////////////////////////////////////////////////
      let cpu_addr=ff_request_from_cpu.first.address;
      let word_offset=find_word_offset(cpu_addr); // find the offset of the word being accessed in the line.
      let byte_offset=find_byte_offset(cpu_addr); // find the offset of the first byte being accessed in the word.
      ///////////////////////////////////////////////////////////////// find which tag to replace using PLRU ////////////////////////////////////////////////////////////////////////////////
      $display($time,"\t",name,"\tByte offset : %d Word offset :%d for address %h",byte_offset,word_offset,cpu_addr);
      Integer replace_block=-1;                   // initialize to a non-existent block
      Bit#(TLog#(_sets)) set=cpu_addr[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits];
      Bit#(TSub#(_ways,1)) lru_bits = pseudo_lru[set];          // read current lru bits.
      Integer block_to_replace=0;
      for(Integer i=v_ways-1;i>=0;i=i-1)           // find if all the blocks are valid or not. If not chose one to replace
          if(valid_values[i]==0)begin
            replace_block=i;
            block_to_replace=i;                      // this is the block which will be accessed. This is used to update the lru bits.
          end
      if(replace_block==-1)begin                  // if all blocks are valid.
        // Ways = n
        // number of bits in lru = n-1
        // index is from 0 to n-2
        Integer left=0;
        Integer right=0;
        Integer i=0;
        $display($time,"\t",name,"\t%s:performing PLRU",name);
        while(i<(v_ways-1))begin
          left=i+1;
          right=i+2;
          if(lru_bits[v_ways-2-i]==0)
            i=i+left;
          else
            i=i+right;
        end
	      block_to_replace=fromInteger(i-v_ways+1);
      end
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			Integer matched_tag=-1;
      Bit#(_addr_width) address=ff_request_from_cpu.first.address;
      // this for loop checks if any of the lines have a tag that matches with that of the core address.
			for(Integer i=0; i<v_ways; i=i+1)begin
				if(valid_values[i]==1'b1 && tag_values[i]==ff_request_from_cpu.first.address[v_addr_width-1:v_addr_width-v_num_of_tag_bits])
					matched_tag=i;	// here this variable indicates which tags show a match.
			end
      // This condition checks if the access by the CPU is misaligned or not.
      if((transfersize=='b10 && address[1:0]!=0)||(transfersize=='b01 && address[0]!=0))begin // miss-aligned error.
        $display($time,"\t",name,"\t Misaligned Error due to Address: %h",cpu_addr);
        wr_response_to_cpu<= tagged Valid (To_Cpu_D {address:ff_request_from_cpu.first().address,
            				                  data_word:0,
                                      bus_error:0,
                                      load_store:ff_request_from_cpu.first.load_store,
                                      misaligned_error:1 });

      end
      else if(matched_tag!=-1)begin// on a hit return the required data to the completion buffer.
        if(ff_request_from_cpu.first().load_store==Load)begin // if it is a Load-hit simply written the required word to the core.
              $display($time,"\t",name,"\t Load Hit for address : %h Line: %h",ff_request_from_cpu.first.address,data_values[matched_tag]);
              wr_response_to_cpu<= tagged Valid (To_Cpu_D {address:ff_request_from_cpu.first().address,
                                        data_word:data_values[matched_tag][word_offset+(8*fromInteger(v_word_size))-1:word_offset],
                                        bus_error:0,
                                        load_store:ff_request_from_cpu.first.load_store,
                                        misaligned_error:0});
              ff_request_from_cpu.deq();
        end
        else begin // STORE operation had a hit in the cache.
          Bit#(TMul#(8,TMul#(_word_size,_block_size))) new_line=data_values[matched_tag];
          $display($time,"\t",name,"\t Store Hit for address : %h Line: %h",ff_request_from_cpu.first.address,data_values[matched_tag]);
          Bit#(TMul#(_word_size,8)) accessed_word1=data_values[matched_tag][word_offset+(8*fromInteger(v_word_size))-1:word_offset]; // read the word from the line.
          let accessed_word<-update_word(byte_offset,accessed_word1,ff_request_from_cpu.first.data,transfersize); // update the word
          new_line=update_data_line(word_offset,data_values[matched_tag],accessed_word); // update the line

          // Enqueue the info about the line update in the fifo which will fire the complete_current_store rule.
          ff_current_store.enq(Current_Store{address:ff_request_from_cpu.first().address,line:new_line, replace_block: fromInteger(matched_tag)});
        end
      ///////////////////////////////////////////////////////////////////////////// Find new LRU bits /////////////////////////////////////////////////////////////////////////////////////////////
      end
      else begin// If the operation is miss in the cache
        $display($time,"\t",name,"\tMiss for address : %h",ff_request_from_cpu.first.address);
      	matched_tag = block_to_replace;
        Bit#(v_addr_width) line_addr=address[v_addr_width-1:v_num_of_offset_bits]<<v_num_of_offset_bits;
        ff_request_to_memory.enq(To_Memory_D { // send the request to memory to 
                        				 address:line_addr,
                                 data_line:?,
                                 burst_length:fromInteger(v_block_size),
                                 transfer_size:fromInteger(valueOf(log_word_size)),
                                 ld_st:Load});
        rg_replace_block<=fromInteger(block_to_replace); // store the index of the line that needs to be re-written with the new line.
        if(dirty_values[fromInteger(block_to_replace)]==1)begin // if the replaced block is dirty tag a state to ensure it is written back to external memory.
          $display("Replacing line : %d",block_to_replace);
          rg_dirty_line_write_back<=True;
          rg_dirty_line_data<=data_values[fromInteger(block_to_replace)];
          // Following is the address of the line that needs to be replaced.
          rg_dirty_line_addr<={tag_values[fromInteger(block_to_replace)],cpu_addr[v_addr_width-v_num_of_tag_bits-1:0]};
        end
        if(ff_request_from_cpu.first.load_store==Store)begin
          $display($time,"\t",name,"\t Store operation Miss");
        end
      end
      Integer m=v_ways-1+matched_tag;
      Integer n=0;
      while(m>0)begin
          if(m%2==1)begin
              n=(m-1)/2;
              if(n<v_ways)
                  lru_bits[v_ways-2-n]=1;
          end
          else begin
              n=(m-2)/2;
              if(n<v_ways)
                  lru_bits[v_ways-2-n]=0;
          end
          m=n;
      end
      pseudo_lru[set]<=lru_bits; // update the LRU bits after the access is made
      $display($time,"\t",name,"\tChanged PLRU for set : %d with bits :%b",set,lru_bits);
    endrule

    // This rule ensures that when the memory has responded with acknowledgment that the dirty line store is
    // over and the ff_request_from_cpu FIFO is empty then dequeues the ff_response_from_memory FIFO and also
    // disables the rg_line_write_stall flag.
   rule empty_response_from_memory_after_dirty_line_write(!ff_request_from_cpu.notEmpty() && ff_response_from_memory.notEmpty() && !rg_initialize);
      $display($time,"\t",name,"\tEmptying the response FIFO");
      ff_response_from_memory.deq();
      if(rg_line_write_stall)
        rg_line_write_stall<=False;
    endrule

    
    // This rule will fire when the external memory has responded with some data for the cache. 
    // All line reads will come as bursts  to this rule. So if a line has 4 words then this rule will fire 4 times.
    // Each time it fires the line variable if updated. At the end of 4 words the line will be written in the BRAM if the cache is
    // enabled. Also as the words keep coming they are checked if it is required by the core or not. 
    rule got_response_from_memory(!rg_initialize && ff_response_from_memory.notEmpty);
      let resp=ff_response_from_memory.first();
      ff_response_from_memory.deq();
      if(rg_line_write_stall)begin // if this is a response for the write of a dirty line.
        rg_line_write_stall<=False;
        $display($time,"\t",name,"\tIgnoring ack for dirty line write");
      end
      else begin
        if(rg_word_count==ff_request_from_cpu.first().address[v_num_of_offset_bits-1:v_num_of_bytes])begin // if the word from the memory is begin accessed by the core as well.
          $display($time,"\t",name,"\tRg_word: %d Address Word: %d",rg_word_count,ff_request_from_cpu.first.address[3:2]);
          wr_response_to_cpu<= tagged Valid (To_Cpu_D{address:resp.address,data_word:resp.data_line,bus_error:resp.bus_error,misaligned_error:0,load_store:ff_request_from_cpu.first.load_store});
        end

        Bit#(TSub#(_addr_width,num_of_offset_bits)) input_tag=resp.address[v_addr_width-1:v_num_of_offset_bits];
        $display($time,"\t",name,"\tRecieved response from the memory. Address: :%h Tag : %h Data: %h ",resp.address,input_tag,resp.data_line);

        let word=resp.data_line;
        let byte_offset=find_byte_offset(ff_request_from_cpu.first.address);
        $display("Incoming Word: %h Core Data: %h Byte offset: %d transfer size: %d",word,ff_request_from_cpu.first.data,byte_offset,ff_request_from_cpu.first.transfer_size);
        if(ff_request_from_cpu.first.load_store==Store && rg_word_count==ff_request_from_cpu.first.address[v_num_of_offset_bits-1:v_num_of_bytes])begin // does the word need to be updated by the core data.
          $display("updating word");
          let w<-update_word(byte_offset,word,ff_request_from_cpu.first.data,ff_request_from_cpu.first.transfer_size);
          word=w;
        end
        $display("Updated word is :%h",word);
        let line=rg_data_line_frm_memory;
        line=line>>(v_word_size*8);
        line=line|{word,'d0};
        $display($time,"\t",name,"\t Current line: %h",line);
        rg_data_line_frm_memory<=line;

        if(rg_enable)begin // only if the cache is enabled
          if(rg_word_count==fromInteger(v_block_size-1))begin // when all the words for the line have been recieved.
            ff_request_from_cpu.deq(); // dequeue the FIFO indicating the end of a instruction.
            rg_word_count<=0; // reset the register
            $display($time,"\t",name,"\tWriting line :%h in cache",line);
            // update the tag with the latest address field
            tag[rg_replace_block].portB.request.put(BRAMRequest{write:True,address:resp.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits],datain:resp.address[v_addr_width-1:v_addr_width-v_num_of_tag_bits],responseOnWrite:False});
            // make the entry in the cache valid
            valid[rg_replace_block].portB.request.put(BRAMRequest{write:True,address:resp.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits],datain:1,responseOnWrite:False});
            // write the new/updated line into the cache.
            data[rg_replace_block].portB.request.put(BRAMRequest{write:True,address:resp.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits],datain:line,responseOnWrite:False});
            if(ff_request_from_cpu.first.load_store==Store)begin // if line is modified due to write mark it dirty.
              dirty[rg_replace_block].portB.request.put(BRAMRequest{write:True,address:resp.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits],datain:1,responseOnWrite:False});
            end
            else begin // else leave it as non-dirty.
              dirty[rg_replace_block].portB.request.put(BRAMRequest{write:True,address:resp.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits],datain:0,responseOnWrite:False});
            end
            // if the dirty line needs to be written back to main memory.
            if(rg_dirty_line_write_back)begin
              $display($time,"\t",name,"\t Sending Write request to Memory for dirty line Addr: %h Data: %h",{rg_dirty_line_addr[31:4],4'b0},rg_dirty_line_data);
              rg_dirty_line_write_back<=False;
              Bit#(v_addr_width) line_addr=rg_dirty_line_addr[v_addr_width-1:v_num_of_offset_bits]<<v_num_of_offset_bits;
              ff_request_to_memory.enq(To_Memory_D {
                                       address:line_addr,
                                       data_line:rg_dirty_line_data,
                                       transfer_size:v_word_size==8?0:v_word_size==16?1:2, // TODO: make this generic.
                                       burst_length:fromInteger(v_block_size),
                                       ld_st:Store});
              rg_line_write_stall<=True;
            end
          end
          else begin
            rg_word_count<=rg_word_count+1;
          end
        end
        else begin
          ff_request_from_cpu.deq;
        end
      end
    endrule

    // When the store is a hit the following rule is fired to write the updated line into the BRAMs and mark the line dirty.
    rule complete_current_store(!rg_initialize && !ff_response_from_memory.notEmpty && rg_enable);
      let info=ff_current_store.first();
      tag[info.replace_block].portB.request.put(BRAMRequest{write:True,address:info.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits],datain:info.address[v_addr_width-1:v_addr_width-v_num_of_tag_bits],responseOnWrite:False});
      valid[info.replace_block].portB.request.put(BRAMRequest{write:True,address:info.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits],datain:1,responseOnWrite:False});
      dirty[info.replace_block].portB.request.put(BRAMRequest{write:True,address:info.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits],datain:1,responseOnWrite:False});
      data[info.replace_block].portB.request.put(BRAMRequest{write:True,address:info.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits],datain:info.line,responseOnWrite:False});
      $display($time,"\t",name,"\t Updating Current Store in cache Address: %h with line: %h",info.address,info.line);
      wr_response_to_cpu<= tagged Valid (To_Cpu_D {address:ff_request_from_cpu.first().address,
                                    data_word:0, // this is just an acknowledgement to the CPU that STORE is over.
                                    bus_error:0,
                                    load_store:ff_request_from_cpu.first.load_store,
                                    misaligned_error:0});
      ff_current_store.deq();   
      ff_request_from_cpu.deq(); // only when the store is over deque the ff_request_from_cpu FIFO.
    endrule
    
    method Action request_from_cpu (From_Cpu_D#(_addr_width,_word_size) req, Bool enable)if(!rg_initialize);
      if(enable)
        for(Integer i=0;i<v_ways;i=i+1)begin // send address to the Block_rams
            tag[i].portA.request.put(BRAMRequest{write:False,address:req.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits], datain:?,responseOnWrite:False});
            data[i].portA.request.put(BRAMRequest{write:False,address:req.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits], datain:?,responseOnWrite:False});
            valid[i].portA.request.put(BRAMRequest{write:False,address:req.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits], datain:?,responseOnWrite:False});
            dirty[i].portA.request.put(BRAMRequest{write:False,address:req.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits], datain:?,responseOnWrite:False});
        end
      rg_enable<=enable;
      ff_request_from_cpu.enq(req); // enqueue the request for next stage

      `ifdef simulate
        Bit#(num_of_tag_bits) tag1=req.address[v_addr_width-1:v_num_of_tag_bits];
        Bit#(TLog#(_sets)) set1=req.address[v_addr_width-v_num_of_tag_bits-1:v_num_of_offset_bits];
        Bit#(num_of_offset_bits) off1=req.address[v_num_of_offset_bits-1:0];
        $display("\n",$time,"\t",name,"\tBRAM: recieved request for Address :%h Data: %h tag %d: Set : %d Offset :%d ",req.address,req.data,tag1,set1,off1,fshow(req.load_store));
      `endif
    endmethod

    method Maybe#(To_Cpu_D#(_addr_width,_word_size)) response_to_cpu if(!rg_initialize);
      return wr_response_to_cpu();
    endmethod

    method ActionValue#(To_Memory_D#(_addr_width,_word_size,_block_size)) request_to_memory if(!rg_initialize) ;
          ff_request_to_memory.deq;
      return ff_request_to_memory.first();
    endmethod

    method Action response_from_memory(From_Memory_D#(_addr_width,_word_size,_block_size) resp) if(!rg_initialize);
      ff_response_from_memory.enq(resp);
    endmethod
    method Action clear_all();
	  $display($time,"\t",name,"\tSET-CACHE CLEARED");
    endmethod
  endmodule
  
  interface Ifc_dcache;
    method Action request_from_cpu(From_Cpu_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE) req, Bool enable);
    method Maybe#(To_Cpu_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE)) response_to_cpu;
    method ActionValue#(To_Memory_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE,`DCACHE_BLOCK_SIZE)) request_to_memory;
    method Action response_from_memory(From_Memory_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE,`DCACHE_BLOCK_SIZE) resp);
    method Action clear_all();
  endinterface

  (*synthesize*)
  module mkdcache(Ifc_dcache);
    
    Ifc_set_associative_dcache#(`DCACHE_ADDR,`DCACHE_WAYS,`DCACHE_WORD_SIZE,`DCACHE_BLOCK_SIZE,`DCACHE_SETS) cache <-mkset_associative_dcache("DCACHE");
    method Action request_from_cpu(From_Cpu_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE) req, Bool enable);
      cache.request_from_cpu(req, enable);
    endmethod

    method Maybe#(To_Cpu_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE)) response_to_cpu = cache.response_to_cpu;
    method ActionValue#(To_Memory_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE,`DCACHE_BLOCK_SIZE)) request_to_memory = cache.request_to_memory;
    method Action response_from_memory(From_Memory_D#(`DCACHE_ADDR,`DCACHE_WORD_SIZE,`DCACHE_BLOCK_SIZE) resp);
      cache.response_from_memory(resp);
    endmethod
    method Action clear_all();
      cache.clear_all();
    endmethod

  endmodule
endpackage 
