#C CLASS PROCESSOR 

**Specs**
---------
1. 5-Stage In-order Pipeline supporting RV32IMF (A will be added soon)
2. Tournament branch predictor added as an extra stage
3. USB based debugger
4. AAPG automated torture cases

**NOTE**
---------
Go to AAPG folder to run regressive torture test cases for the processor.
The iverilog, ncverilog and modelsim folders hold scripts required to run verilog simulations
in the respective tools.

**Makefile Commands**
------------------------
1. make 
    * This will compile and link the code using bluesim. The linked binary will be present in the bin/ folder
2. make AAPG_bsim
    * This will compile and link the code in bluesim. Then call AAPG and run torture tests on the linked binary.
3. make AAPG_ncverilog
    * This command assumes that the verilog files are already present in the verilog/ folder. This command will then
    link them using Cadence's ncverilog tools and create a binary in the bin folder. Then the AAPG is called for
    running torture tests.
4. make AAPG_msim
    * This command assumes that the verilog files are already present in the verilog/ folder. This command will then
    link them using Mentor Graphics' modelsim tools and create a binary in the bin folder. Then the AAPG is called for
    running torture tests.
5. make AAPG_iverilog
    * This command assumes that the verilog files are already present in the verilog/ folder. This command will then
    link them using the open source iverilog tool and create a binary in the bin folder. Then the AAPG is called for
    running torture.
6. make generate_verilog
    * This command will simply compile the bluespec code to generate the verilog files and copy the necessary verilog
    library files from the bluespec folder to enable verilog simulations. The verilog files will be created
    in the verilog folder.
7. make generate_verilog_fpga
    * This command will simply compile the bluespec code to generate the verilog files and copy the necessary verilog
    library files from the bluespec folder to enable verilog synthesis. The verilog files will be created
    in the verilog_fpga folder. Simulations will not work using these verilog files.
8. make clean_bsim
    * This command will delete all files in bin and BSV_src/build_bsim folders.
9. make clean_verilog
    * This command will delete all files in verilog, verilog_fpga, bin and BSV_src/build_bsim folders.


