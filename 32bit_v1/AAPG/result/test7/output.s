
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	blt x31, x5, i_4
i_1:
	lw x9, 116(x2)
i_2:
	addi x26, x17, -2038
i_3:
	sh x15, 290(x2)
i_4:
	srai x25, x13, 4
i_5:
	sw x30, 320(x2)
i_6:
	lbu x28, -172(x2)
i_7:
	lhu x21, -48(x2)
i_8:
	ori x21, x20, 403
i_9:
	sltiu x11, x11, 192
i_10:
	lhu x21, 126(x2)
i_11:
	lhu x25, 476(x2)
i_12:
	sh x26, -160(x2)
i_13:
	sltiu x21, x3, -499
i_14:
	sb x21, 377(x2)
i_15:
	lh x25, 464(x2)
i_16:
	or x17, x20, x31
i_17:
	sb x21, -38(x2)
i_18:
	addi x14, x0, 5
i_19:
	srl x25, x9, x14
i_20:
	lw x9, 156(x2)
i_21:
	lhu x19, 160(x2)
i_22:
	srli x17, x8, 4
i_23:
	addi x3, x0, 1951
i_24:
	addi x8, x0, 1955
i_25:
	bgeu x9, x2, i_29
i_26:
	lhu x30, -126(x2)
i_27:
	lh x9, 300(x2)
i_28:
	lh x17, 358(x2)
i_29:
	lhu x11, -338(x2)
i_30:
	slti x19, x18, -87
i_31:
	xor x29, x4, x14
i_32:
	blt x11, x21, i_34
i_33:
	slli x25, x13, 1
i_34:
	nop
i_35:
	bne x23, x19, i_42
i_36:
	addi x3 , x3 , 1
	bltu x3, x8, i_25
i_37:
	addi x17, x0, 21
i_38:
	srl x1, x26, x17
i_39:
	lb x23, -431(x2)
i_40:
	lw x24, 16(x2)
i_41:
	lh x21, 426(x2)
i_42:
	lhu x15, 202(x2)
i_43:
	sh x23, -264(x2)
i_44:
	sh x7, -274(x2)
i_45:
	sb x26, 270(x2)
i_46:
	sw x17, 400(x2)
i_47:
	slt x23, x20, x24
i_48:
	add x22, x8, x31
i_49:
	lhu x15, -192(x2)
i_50:
	lh x11, 278(x2)
i_51:
	srai x24, x16, 1
i_52:
	lh x24, 222(x2)
i_53:
	lhu x20, -320(x2)
i_54:
	lh x27, -422(x2)
i_55:
	sltiu x19, x10, -1691
i_56:
	and x30, x1, x13
i_57:
	add x30, x3, x29
i_58:
	beq x19, x18, i_67
i_59:
	beq x19, x5, i_60
i_60:
	xori x30, x30, -1606
i_61:
	sw x30, 164(x2)
i_62:
	lw x26, 192(x2)
i_63:
	auipc x3, 979248
i_64:
	lw x8, 152(x2)
i_65:
	blt x20, x12, i_74
i_66:
	srli x8, x4, 3
i_67:
	sh x3, -374(x2)
i_68:
	nop
i_69:
	sw x28, 424(x2)
i_70:
	sb x6, 438(x2)
i_71:
	lw x29, -184(x2)
i_72:
	srai x18, x18, 4
i_73:
	sh x4, 458(x2)
i_74:
	sb x28, -441(x2)
i_75:
	nop
i_76:
	addi x28, x0, -2039
i_77:
	addi x4, x0, -2036
i_78:
	addi x28 , x28 , 1
	blt x28, x4, i_78
i_79:
	srli x4, x26, 4
i_80:
	slt x19, x27, x17
i_81:
	sb x5, 49(x2)
i_82:
	bgeu x11, x18, i_86
i_83:
	blt x12, x30, i_85
i_84:
	lb x22, -112(x2)
i_85:
	sltu x18, x16, x28
i_86:
	lb x9, 41(x2)
i_87:
	slli x16, x27, 2
i_88:
	addi x10, x0, -1992
i_89:
	addi x22, x0, -1988
i_90:
	beq x21, x2, i_99
i_91:
	andi x23, x23, -9
i_92:
	lhu x27, 280(x2)
i_93:
	slti x21, x23, 245
i_94:
	lw x23, -56(x2)
i_95:
	addi x10 , x10 , 1
	blt x10, x22, i_90
i_96:
	addi x4, x0, 1
i_97:
	sra x27, x5, x4
i_98:
	addi x27, x0, 8
i_99:
	sll x27, x27, x27
i_100:
	lbu x5, 211(x2)
i_101:
	bne x12, x15, i_104
i_102:
	lw x4, -412(x2)
i_103:
	lbu x4, 289(x2)
i_104:
	nop
i_105:
	nop
i_106:
	addi x22, x0, -1908
i_107:
	addi x4, x0, -1905
i_108:
	xor x23, x18, x31
i_109:
	andi x1, x22, -1338
i_110:
	addi x27, x0, 3
i_111:
	sll x1, x7, x27
i_112:
	sw x11, -44(x2)
i_113:
	addi x22 , x22 , 1
	bgeu x4, x22, i_108
i_114:
	lhu x22, 168(x2)
i_115:
	sub x21, x4, x4
i_116:
	lw x30, 132(x2)
i_117:
	bgeu x18, x22, i_118
i_118:
	bne x29, x26, i_126
i_119:
	sh x12, 300(x2)
i_120:
	ori x26, x17, 340
i_121:
	bltu x24, x6, i_125
i_122:
	beq x7, x29, i_131
i_123:
	bne x28, x17, i_128
i_124:
	xori x17, x3, 1290
i_125:
	and x3, x7, x17
i_126:
	lh x17, -388(x2)
i_127:
	lb x17, -65(x2)
i_128:
	srai x17, x12, 2
i_129:
	sh x15, 438(x2)
i_130:
	or x17, x7, x19
i_131:
	xor x21, x17, x26
i_132:
	sb x17, 428(x2)
i_133:
	lw x21, 52(x2)
i_134:
	sb x15, -368(x2)
i_135:
	slt x17, x21, x6
i_136:
	lhu x13, 316(x2)
i_137:
	auipc x15, 963419
i_138:
	lhu x13, 296(x2)
i_139:
	addi x4, x1, -2034
i_140:
	and x1, x4, x28
i_141:
	lhu x5, -476(x2)
i_142:
	addi x8, x0, 13
i_143:
	srl x3, x8, x8
i_144:
	lw x27, 168(x2)
i_145:
	beq x10, x27, i_152
i_146:
	srli x11, x30, 4
i_147:
	sb x20, -83(x2)
i_148:
	bge x4, x2, i_157
i_149:
	lb x11, 194(x2)
i_150:
	lb x20, -64(x2)
i_151:
	lw x17, 332(x2)
i_152:
	bgeu x28, x11, i_155
i_153:
	and x10, x12, x22
i_154:
	slti x17, x26, -738
i_155:
	sb x3, 420(x2)
i_156:
	lb x12, 437(x2)
i_157:
	slli x1, x3, 3
i_158:
	lbu x9, 140(x2)
i_159:
	bne x13, x12, i_168
i_160:
	andi x25, x23, 2006
i_161:
	lh x9, 70(x2)
i_162:
	sb x26, -137(x2)
i_163:
	sw x20, -136(x2)
i_164:
	lw x16, -280(x2)
i_165:
	blt x29, x1, i_173
i_166:
	xor x9, x1, x19
i_167:
	srli x5, x12, 3
i_168:
	bne x11, x13, i_172
i_169:
	addi x23, x0, 19
i_170:
	srl x13, x16, x23
i_171:
	sh x5, -26(x2)
i_172:
	lbu x23, -322(x2)
i_173:
	lui x29, 179451
i_174:
	beq x16, x31, i_183
i_175:
	addi x28, x20, -2030
i_176:
	bne x13, x29, i_178
i_177:
	lb x18, 316(x2)
i_178:
	slt x1, x28, x9
i_179:
	lh x20, -342(x2)
i_180:
	lhu x9, 340(x2)
i_181:
	addi x1, x0, 28
i_182:
	sra x28, x20, x1
i_183:
	auipc x4, 186157
i_184:
	add x30, x15, x4
i_185:
	sltu x23, x7, x13
i_186:
	addi x1, x0, -2011
i_187:
	addi x5, x0, -2009
i_188:
	sw x14, -20(x2)
i_189:
	bge x10, x8, i_197
i_190:
	sb x20, 253(x2)
i_191:
	addi x1 , x1 , 1
	bgeu x5, x1, i_188
i_192:
	srli x30, x30, 4
i_193:
	lhu x22, -450(x2)
i_194:
	sb x5, -93(x2)
i_195:
	addi x20, x24, -1972
i_196:
	slli x6, x24, 4
i_197:
	or x23, x20, x20
i_198:
	nop
i_199:
	addi x24, x0, -1853
i_200:
	addi x30, x0, -1850
i_201:
	bgeu x19, x25, i_208
i_202:
	sw x19, 204(x2)
i_203:
	addi x24 , x24 , 1
	bne x24, x30, i_201
i_204:
	ori x23, x22, -1172
i_205:
	bne x29, x17, i_209
i_206:
	bgeu x29, x18, i_216
i_207:
	lhu x6, -26(x2)
i_208:
	bge x29, x7, i_213
i_209:
	or x29, x6, x8
i_210:
	slti x1, x24, 1447
i_211:
	srli x13, x12, 3
i_212:
	addi x8, x10, -1409
i_213:
	bne x23, x1, i_214
i_214:
	sw x1, 196(x2)
i_215:
	sb x7, -133(x2)
i_216:
	lh x23, -366(x2)
i_217:
	sltiu x25, x4, 88
i_218:
	addi x1, x0, -1978
i_219:
	addi x20, x0, -1976
i_220:
	lbu x4, -65(x2)
i_221:
	lhu x18, -222(x2)
i_222:
	addi x1 , x1 , 1
	blt x1, x20, i_220
i_223:
	lhu x8, 308(x2)
i_224:
	bgeu x14, x19, i_231
i_225:
	sb x9, 409(x2)
i_226:
	sw x3, 180(x2)
i_227:
	blt x20, x9, i_231
i_228:
	sw x17, 36(x2)
i_229:
	bltu x21, x4, i_230
i_230:
	add x27, x21, x8
i_231:
	sb x11, -146(x2)
i_232:
	add x8, x2, x3
i_233:
	lui x13, 40327
i_234:
	blt x4, x26, i_244
i_235:
	lh x8, -302(x2)
i_236:
	lbu x1, 210(x2)
i_237:
	sltiu x24, x15, 243
i_238:
	addi x17, x0, 7
i_239:
	sll x17, x8, x17
i_240:
	add x22, x12, x16
i_241:
	slt x12, x27, x11
i_242:
	xor x22, x22, x25
i_243:
	bge x12, x2, i_251
i_244:
	beq x23, x24, i_246
i_245:
	srli x23, x22, 4
i_246:
	sh x9, 56(x2)
i_247:
	blt x12, x11, i_257
i_248:
	addi x4, x0, 13
i_249:
	srl x18, x22, x4
i_250:
	auipc x12, 804608
i_251:
	add x16, x20, x8
i_252:
	bgeu x13, x4, i_257
i_253:
	lb x9, 172(x2)
i_254:
	bne x25, x1, i_263
i_255:
	lbu x25, 318(x2)
i_256:
	lw x29, -376(x2)
i_257:
	bltu x16, x25, i_264
i_258:
	blt x26, x11, i_264
i_259:
	or x23, x29, x9
i_260:
	slt x22, x10, x18
i_261:
	add x10, x6, x8
i_262:
	sb x21, -324(x2)
i_263:
	lb x8, 82(x2)
i_264:
	slt x4, x17, x2
i_265:
	lbu x13, -17(x2)
i_266:
	addi x10, x0, 2033
i_267:
	addi x1, x0, 2037
i_268:
	sw x13, -372(x2)
i_269:
	lh x13, 218(x2)
i_270:
	lh x13, 294(x2)
i_271:
	sb x4, 119(x2)
i_272:
	sltu x8, x25, x4
i_273:
	sb x17, -268(x2)
i_274:
	addi x24, x18, -411
i_275:
	sw x26, 444(x2)
i_276:
	addi x10 , x10 , 1
	bge x1, x10, i_268
i_277:
	sltu x26, x23, x23
i_278:
	xori x1, x14, 2008
i_279:
	lh x26, -18(x2)
i_280:
	sb x15, 377(x2)
i_281:
	srai x26, x1, 1
i_282:
	lhu x1, -420(x2)
i_283:
	sh x31, -462(x2)
i_284:
	lb x11, 392(x2)
i_285:
	srai x11, x14, 2
i_286:
	addi x5, x10, -1613
i_287:
	sh x28, -258(x2)
i_288:
	sh x30, -204(x2)
i_289:
	lhu x25, -294(x2)
i_290:
	addi x25, x0, 6
i_291:
	sll x26, x25, x25
i_292:
	bne x31, x23, i_295
i_293:
	bge x17, x26, i_296
i_294:
	lb x3, -135(x2)
i_295:
	sb x21, -321(x2)
i_296:
	or x4, x6, x13
i_297:
	addi x4, x0, 8
i_298:
	srl x4, x17, x4
i_299:
	addi x23, x0, -1856
i_300:
	addi x20, x0, -1852
i_301:
	lhu x14, -386(x2)
i_302:
	addi x10, x4, 582
i_303:
	addi x26, x0, 8
i_304:
	srl x27, x1, x26
i_305:
	sh x20, 306(x2)
i_306:
	addi x23 , x23 , 1
	bltu x23, x20, i_300
i_307:
	srai x1, x15, 4
i_308:
	lui x5, 249818
i_309:
	slli x15, x21, 2
i_310:
	addi x1, x0, 28
i_311:
	sra x1, x18, x1
i_312:
	add x15, x10, x1
i_313:
	lw x9, 92(x2)
i_314:
	sb x14, -424(x2)
i_315:
	bge x14, x23, i_321
i_316:
	and x27, x20, x25
i_317:
	lb x23, -264(x2)
i_318:
	sb x5, 8(x2)
i_319:
	sw x19, -152(x2)
i_320:
	sh x18, 140(x2)
i_321:
	addi x26, x0, 23
i_322:
	sll x12, x29, x26
i_323:
	xori x12, x12, 1817
i_324:
	lb x27, -163(x2)
i_325:
	addi x27, x0, 5
i_326:
	srl x9, x12, x27
i_327:
	sh x5, -404(x2)
i_328:
	sb x1, -283(x2)
i_329:
	lb x6, -44(x2)
i_330:
	addi x24, x0, 26
i_331:
	sra x12, x13, x24
i_332:
	addi x9, x9, -835
i_333:
	blt x2, x6, i_341
i_334:
	sw x18, -8(x2)
i_335:
	addi x12, x0, 24
i_336:
	srl x9, x20, x12
i_337:
	sh x8, -188(x2)
i_338:
	lb x24, 485(x2)
i_339:
	sw x27, -140(x2)
i_340:
	sw x17, 408(x2)
i_341:
	lw x4, -360(x2)
i_342:
	sw x6, -464(x2)
i_343:
	addi x27, x0, -1896
i_344:
	addi x18, x0, -1892
i_345:
	srli x5, x6, 2
i_346:
	sh x1, 354(x2)
i_347:
	lbu x8, -378(x2)
i_348:
	addi x27 , x27 , 1
	bne  x18, x27, i_345
i_349:
	bgeu x26, x31, i_355
i_350:
	bne x12, x9, i_355
i_351:
	lb x24, 82(x2)
i_352:
	xori x4, x12, -847
i_353:
	sb x31, -172(x2)
i_354:
	sltiu x22, x12, -1229
i_355:
	bgeu x31, x17, i_356
i_356:
	lh x16, 72(x2)
i_357:
	lw x14, -236(x2)
i_358:
	sh x30, -278(x2)
i_359:
	sh x30, 28(x2)
i_360:
	lb x30, 443(x2)
i_361:
	lw x4, -264(x2)
i_362:
	blt x6, x6, i_372
i_363:
	bltu x3, x25, i_371
i_364:
	lhu x20, -246(x2)
i_365:
	addi x16, x0, 27
i_366:
	sra x27, x2, x16
i_367:
	lb x16, 61(x2)
i_368:
	bgeu x27, x20, i_378
i_369:
	bgeu x29, x25, i_378
i_370:
	and x20, x24, x17
i_371:
	lh x24, 326(x2)
i_372:
	add x12, x17, x5
i_373:
	sb x25, -217(x2)
i_374:
	bge x24, x11, i_375
i_375:
	addi x28, x0, 30
i_376:
	srl x5, x1, x28
i_377:
	bltu x20, x1, i_380
i_378:
	lw x21, 112(x2)
i_379:
	sw x22, -372(x2)
i_380:
	lhu x13, 106(x2)
i_381:
	lbu x20, 354(x2)
i_382:
	addi x22, x0, -1908
i_383:
	addi x5, x0, -1906
i_384:
	lw x27, 164(x2)
i_385:
	slt x4, x29, x12
i_386:
	addi x29, x30, -491
i_387:
	sw x4, -160(x2)
i_388:
	addi x12, x0, 18
i_389:
	sll x4, x29, x12
i_390:
	bgeu x8, x2, i_399
i_391:
	andi x27, x4, -1563
i_392:
	addi x22 , x22 , 1
	bltu x22, x5, i_384
i_393:
	bge x24, x18, i_401
i_394:
	lhu x24, -396(x2)
i_395:
	nop
i_396:
	lhu x20, 432(x2)
i_397:
	lb x20, 147(x2)
i_398:
	lbu x21, 365(x2)
i_399:
	sb x18, -394(x2)
i_400:
	and x20, x7, x25
i_401:
	lw x5, 396(x2)
i_402:
	lw x12, 48(x2)
i_403:
	addi x18, x0, -1859
i_404:
	addi x10, x0, -1857
i_405:
	sw x16, 192(x2)
i_406:
	xor x4, x24, x9
i_407:
	addi x18 , x18 , 1
	bge x10, x18, i_405
i_408:
	andi x21, x24, -1133
i_409:
	sw x10, 360(x2)
i_410:
	bne x12, x6, i_420
i_411:
	lw x8, -476(x2)
i_412:
	lhu x6, -254(x2)
i_413:
	slti x6, x6, 847
i_414:
	lbu x6, -394(x2)
i_415:
	lw x17, -360(x2)
i_416:
	and x9, x10, x26
i_417:
	addi x8, x0, 2
i_418:
	sra x6, x9, x8
i_419:
	lhu x17, -44(x2)
i_420:
	sb x31, -102(x2)
i_421:
	srai x27, x8, 2
i_422:
	lhu x18, 58(x2)
i_423:
	srai x28, x6, 3
i_424:
	srai x6, x26, 2
i_425:
	or x25, x19, x24
i_426:
	addi x8, x0, 30
i_427:
	sll x9, x3, x8
i_428:
	sltu x24, x26, x31
i_429:
	bgeu x15, x8, i_439
i_430:
	lw x18, -96(x2)
i_431:
	bge x26, x3, i_435
i_432:
	sh x13, -304(x2)
i_433:
	slti x9, x12, 1826
i_434:
	addi x15, x0, 10
i_435:
	sra x22, x18, x15
i_436:
	lhu x18, 32(x2)
i_437:
	lw x15, 476(x2)
i_438:
	sb x14, -108(x2)
i_439:
	lw x24, -116(x2)
i_440:
	slt x15, x6, x24
i_441:
	sh x24, 284(x2)
i_442:
	lbu x13, -373(x2)
i_443:
	andi x1, x13, -429
i_444:
	bge x22, x13, i_452
i_445:
	addi x29, x0, 1
i_446:
	srl x21, x29, x29
i_447:
	lh x13, -282(x2)
i_448:
	lhu x9, 306(x2)
i_449:
	lw x16, -408(x2)
i_450:
	sh x9, -204(x2)
i_451:
	lhu x25, 132(x2)
i_452:
	xori x15, x10, 345
i_453:
	lh x18, -160(x2)
i_454:
	xor x1, x27, x16
i_455:
	add x27, x15, x7
i_456:
	beq x16, x9, i_466
i_457:
	sltiu x9, x5, -978
i_458:
	sltu x24, x1, x17
i_459:
	sb x20, -458(x2)
i_460:
	lui x4, 115513
i_461:
	srli x14, x15, 1
i_462:
	lh x19, -406(x2)
i_463:
	bgeu x10, x11, i_464
i_464:
	sb x9, 471(x2)
i_465:
	lw x10, -88(x2)
i_466:
	sw x15, -356(x2)
i_467:
	slli x19, x7, 3
i_468:
	bgeu x1, x22, i_469
i_469:
	lbu x9, 106(x2)
i_470:
	sw x18, 384(x2)
i_471:
	bltu x26, x31, i_479
i_472:
	sw x1, 412(x2)
i_473:
	slt x1, x5, x25
i_474:
	lb x16, -355(x2)
i_475:
	and x28, x2, x28
i_476:
	sub x10, x31, x22
i_477:
	lb x13, 393(x2)
i_478:
	lhu x3, -144(x2)
i_479:
	lhu x20, -278(x2)
i_480:
	sw x9, 168(x2)
i_481:
	xori x5, x16, -622
i_482:
	lb x28, 39(x2)
i_483:
	addi x17, x0, 29
i_484:
	sra x16, x24, x17
i_485:
	bgeu x21, x28, i_492
i_486:
	xori x18, x17, 585
i_487:
	lhu x21, -114(x2)
i_488:
	addi x17, x14, -1423
i_489:
	lh x14, 24(x2)
i_490:
	lh x18, -8(x2)
i_491:
	auipc x14, 671432
i_492:
	lui x9, 109492
i_493:
	sltiu x16, x15, -499
i_494:
	addi x18, x0, 31
i_495:
	sra x8, x8, x18
i_496:
	addi x1, x8, -679
i_497:
	beq x4, x1, i_501
i_498:
	sw x1, -196(x2)
i_499:
	sub x27, x3, x24
i_500:
	lb x1, 285(x2)
i_501:
	lb x10, -77(x2)
i_502:
	sb x31, -384(x2)
i_503:
	bltu x11, x1, i_506
i_504:
	lbu x24, -222(x2)
i_505:
	slti x11, x12, 832
i_506:
	sh x27, -100(x2)
i_507:
	lb x1, -465(x2)
i_508:
	lb x19, 328(x2)
i_509:
	lhu x1, -66(x2)
i_510:
	sh x14, -56(x2)
i_511:
	sw x7, -136(x2)
i_512:
	beq x30, x7, i_513
i_513:
	lh x28, -370(x2)
i_514:
	add x22, x23, x13
i_515:
	addi x30, x0, 1944
i_516:
	addi x14, x0, 1948
i_517:
	nop
i_518:
	lh x1, 358(x2)
i_519:
	addi x30 , x30 , 1
	bgeu x14, x30, i_517
i_520:
	lbu x25, -4(x2)
i_521:
	addi x10, x0, 30
i_522:
	srl x24, x17, x10
i_523:
	sltiu x17, x3, 1887
i_524:
	sh x25, 372(x2)
i_525:
	lb x26, 232(x2)
i_526:
	lb x25, 198(x2)
i_527:
	addi x26, x0, 16
i_528:
	sra x22, x25, x26
i_529:
	sw x12, -328(x2)
i_530:
	sb x17, 166(x2)
i_531:
	lh x19, -324(x2)
i_532:
	lh x29, 330(x2)
i_533:
	sb x25, 289(x2)
i_534:
	addi x24, x0, 12
i_535:
	sll x26, x11, x24
i_536:
	bgeu x12, x3, i_542
i_537:
	bge x24, x29, i_543
i_538:
	lw x24, -32(x2)
i_539:
	bge x6, x2, i_540
i_540:
	lhu x16, -452(x2)
i_541:
	ori x4, x14, -40
i_542:
	lw x24, -188(x2)
i_543:
	xor x14, x6, x19
i_544:
	lui x14, 674546
i_545:
	srai x16, x10, 3
i_546:
	sw x14, 88(x2)
i_547:
	lb x9, -258(x2)
i_548:
	addi x4, x0, -1879
i_549:
	addi x28, x0, -1876
i_550:
	addi x23, x16, -494
i_551:
	lhu x10, 386(x2)
i_552:
	lb x12, 116(x2)
i_553:
	lhu x11, 6(x2)
i_554:
	addi x4 , x4 , 1
	bltu x4, x28, i_550
i_555:
	add x17, x27, x17
i_556:
	sltu x27, x11, x25
i_557:
	sh x16, 398(x2)
i_558:
	andi x8, x27, 188
i_559:
	lw x26, 88(x2)
i_560:
	addi x9, x0, 21
i_561:
	sll x1, x7, x9
i_562:
	lb x9, 63(x2)
i_563:
	lb x22, -386(x2)
i_564:
	addi x25, x0, -1969
i_565:
	addi x11, x0, -1967
i_566:
	lw x10, -292(x2)
i_567:
	sw x1, 352(x2)
i_568:
	addi x6, x22, -826
i_569:
	nop
i_570:
	lhu x22, -162(x2)
i_571:
	addi x25 , x25 , 1
	blt x25, x11, i_566
i_572:
	or x20, x5, x14
i_573:
	lh x21, -252(x2)
i_574:
	ori x6, x22, 532
i_575:
	blt x28, x19, i_579
i_576:
	sh x10, -292(x2)
i_577:
	sw x29, 184(x2)
i_578:
	bne x18, x21, i_582
i_579:
	sltu x22, x22, x22
i_580:
	addi x19, x0, 13
i_581:
	sra x23, x9, x19
i_582:
	sub x9, x9, x7
i_583:
	add x1, x21, x23
i_584:
	addi x28, x0, -1992
i_585:
	addi x22, x0, -1990
i_586:
	xor x1, x6, x5
i_587:
	sltu x18, x12, x14
i_588:
	lb x18, -122(x2)
i_589:
	lhu x5, -46(x2)
i_590:
	sub x23, x9, x9
i_591:
	xori x11, x9, -388
i_592:
	nop
i_593:
	slti x30, x1, 1049
i_594:
	addi x28 , x28 , 1
	bgeu x22, x28, i_586
i_595:
	lh x20, 282(x2)
i_596:
	bge x23, x4, i_600
i_597:
	andi x23, x27, -1561
i_598:
	sltiu x29, x27, 123
i_599:
	sh x17, -46(x2)
i_600:
	lh x23, -12(x2)
i_601:
	slti x29, x15, 1673
i_602:
	slt x16, x23, x7
i_603:
	lw x23, -200(x2)
i_604:
	lhu x29, 388(x2)
i_605:
	sw x1, 328(x2)
i_606:
	and x29, x8, x16
i_607:
	sltu x24, x23, x15
i_608:
	add x16, x27, x28
i_609:
	nop
i_610:
	nop
i_611:
	addi x6, x0, -1879
i_612:
	addi x14, x0, -1875
i_613:
	srai x9, x14, 1
i_614:
	bge x28, x3, i_618
i_615:
	sb x5, 217(x2)
i_616:
	lb x28, -388(x2)
i_617:
	lhu x10, -314(x2)
i_618:
	sb x8, 325(x2)
i_619:
	sw x11, -252(x2)
i_620:
	addi x6 , x6 , 1
	bgeu x14, x6, i_613
i_621:
	andi x25, x10, -1595
i_622:
	sh x1, 310(x2)
i_623:
	auipc x9, 1035254
i_624:
	addi x11, x0, 1861
i_625:
	addi x25, x0, 1863
i_626:
	sw x10, 52(x2)
i_627:
	lh x10, 202(x2)
i_628:
	addi x19, x0, 9
i_629:
	sll x8, x10, x19
i_630:
	addi x11 , x11 , 1
	bne x11, x25, i_626
i_631:
	and x3, x28, x19
i_632:
	lhu x11, -4(x2)
i_633:
	nop
i_634:
	lhu x5, -482(x2)
i_635:
	addi x28, x0, -1959
i_636:
	addi x19, x0, -1957
i_637:
	lh x25, 26(x2)
i_638:
	bge x29, x4, i_647
i_639:
	addi x28 , x28 , 1
	blt x28, x19, i_637
i_640:
	addi x18, x23, 857
i_641:
	and x6, x23, x30
i_642:
	sltiu x30, x1, -65
i_643:
	bge x6, x23, i_648
i_644:
	beq x18, x19, i_654
i_645:
	lh x25, -454(x2)
i_646:
	ori x19, x16, -802
i_647:
	lh x11, -300(x2)
i_648:
	bgeu x10, x20, i_655
i_649:
	lh x20, -320(x2)
i_650:
	lh x19, 372(x2)
i_651:
	sh x19, 334(x2)
i_652:
	sb x19, 320(x2)
i_653:
	auipc x22, 1020360
i_654:
	lbu x19, -311(x2)
i_655:
	lw x14, 200(x2)
i_656:
	lhu x11, -414(x2)
i_657:
	sh x17, 10(x2)
i_658:
	bltu x7, x11, i_665
i_659:
	sub x16, x30, x9
i_660:
	lbu x9, -380(x2)
i_661:
	bge x10, x20, i_666
i_662:
	auipc x25, 547971
i_663:
	lw x16, -424(x2)
i_664:
	lbu x8, -306(x2)
i_665:
	bgeu x30, x25, i_670
i_666:
	sw x12, 204(x2)
i_667:
	auipc x21, 908585
i_668:
	addi x3, x0, 30
i_669:
	sll x5, x10, x3
i_670:
	slli x12, x23, 3
i_671:
	bge x9, x26, i_672
i_672:
	slt x1, x19, x8
i_673:
	sh x19, -294(x2)
i_674:
	bge x30, x18, i_680
i_675:
	auipc x8, 616969
i_676:
	sw x1, -464(x2)
i_677:
	addi x29, x5, -1258
i_678:
	lb x8, -445(x2)
i_679:
	bne x20, x13, i_687
i_680:
	lw x1, 288(x2)
i_681:
	bgeu x9, x8, i_683
i_682:
	lhu x19, 354(x2)
i_683:
	sh x4, -362(x2)
i_684:
	lb x8, 115(x2)
i_685:
	srai x14, x8, 1
i_686:
	sw x16, -300(x2)
i_687:
	slli x8, x13, 1
i_688:
	or x16, x1, x3
i_689:
	andi x12, x21, 357
i_690:
	blt x1, x11, i_693
i_691:
	lb x21, 36(x2)
i_692:
	addi x10, x14, 877
i_693:
	andi x21, x8, -858
i_694:
	lw x14, -96(x2)
i_695:
	srli x10, x25, 2
i_696:
	srai x4, x29, 1
i_697:
	lbu x26, -121(x2)
i_698:
	addi x4, x0, 29
i_699:
	srl x14, x26, x4
i_700:
	lhu x1, 486(x2)
i_701:
	srli x16, x16, 4
i_702:
	lw x5, 196(x2)
i_703:
	lui x13, 754710
i_704:
	bgeu x19, x13, i_712
i_705:
	auipc x16, 696354
i_706:
	bltu x2, x14, i_712
i_707:
	sb x4, -20(x2)
i_708:
	lhu x4, 274(x2)
i_709:
	sw x23, 432(x2)
i_710:
	andi x14, x12, 861
i_711:
	sltu x27, x5, x5
i_712:
	bltu x25, x27, i_713
i_713:
	lb x26, 139(x2)
i_714:
	sh x4, 428(x2)
i_715:
	lui x25, 164338
i_716:
	srai x13, x4, 2
i_717:
	sw x29, -428(x2)
i_718:
	bgeu x27, x4, i_721
i_719:
	lhu x1, -396(x2)
i_720:
	or x10, x16, x4
i_721:
	lhu x30, 258(x2)
i_722:
	lhu x10, 228(x2)
i_723:
	addi x10, x19, -583
i_724:
	lhu x24, 270(x2)
i_725:
	lh x26, -346(x2)
i_726:
	bgeu x24, x8, i_734
i_727:
	srai x11, x13, 1
i_728:
	auipc x14, 475816
i_729:
	bltu x12, x25, i_735
i_730:
	xor x10, x31, x21
i_731:
	addi x26, x0, 12
i_732:
	sll x26, x8, x26
i_733:
	lhu x1, 38(x2)
i_734:
	sh x1, -486(x2)
i_735:
	lbu x9, 485(x2)
i_736:
	ori x26, x31, 582
i_737:
	add x6, x24, x25
i_738:
	lbu x25, -105(x2)
i_739:
	lbu x6, -170(x2)
i_740:
	beq x19, x9, i_747
i_741:
	sw x18, 128(x2)
i_742:
	add x21, x20, x30
i_743:
	sb x18, 105(x2)
i_744:
	beq x18, x17, i_748
i_745:
	add x14, x23, x4
i_746:
	sb x6, 350(x2)
i_747:
	auipc x25, 664204
i_748:
	nop
i_749:
	lw x25, -260(x2)
i_750:
	addi x30, x0, 2035
i_751:
	addi x28, x0, 2037
i_752:
	bltu x17, x8, i_755
i_753:
	lhu x11, -244(x2)
i_754:
	lbu x11, 376(x2)
i_755:
	and x25, x2, x21
i_756:
	addi x8, x0, 6
i_757:
	sll x3, x6, x8
i_758:
	addi x30 , x30 , 1
	bne x30, x28, i_752
i_759:
	sw x11, 164(x2)
i_760:
	slt x21, x30, x20
i_761:
	xor x22, x8, x28
i_762:
	lw x28, -16(x2)
i_763:
	lw x15, -304(x2)
i_764:
	addi x21, x0, 2038
i_765:
	addi x13, x0, 2042
i_766:
	addi x21 , x21 , 1
	bge x13, x21, i_766
i_767:
	xor x4, x22, x23
i_768:
	lb x26, 416(x2)
i_769:
	lbu x14, 433(x2)
i_770:
	ori x4, x21, 1485
i_771:
	ori x3, x22, 1737
i_772:
	slt x16, x3, x28
i_773:
	slt x22, x4, x19
i_774:
	bge x22, x10, i_781
i_775:
	lbu x16, 415(x2)
i_776:
	sw x6, 124(x2)
i_777:
	lbu x8, 362(x2)
i_778:
	lbu x8, 254(x2)
i_779:
	lb x5, 382(x2)
i_780:
	and x22, x8, x26
i_781:
	auipc x22, 266785
i_782:
	lbu x22, 297(x2)
i_783:
	sw x22, 24(x2)
i_784:
	bne x5, x8, i_789
i_785:
	sw x17, 396(x2)
i_786:
	xor x17, x4, x28
i_787:
	addi x26, x0, 12
i_788:
	sra x5, x5, x26
i_789:
	beq x22, x26, i_790
i_790:
	add x17, x17, x29
i_791:
	and x14, x17, x6
i_792:
	ori x29, x24, -135
i_793:
	lh x26, 236(x2)
i_794:
	ori x24, x17, 1386
i_795:
	addi x9, x0, 11
i_796:
	sll x17, x22, x9
i_797:
	lbu x22, -421(x2)
i_798:
	slli x29, x15, 1
i_799:
	lbu x18, -152(x2)
i_800:
	srli x25, x29, 2
i_801:
	lw x4, 388(x2)
i_802:
	bgeu x16, x29, i_803
i_803:
	blt x1, x9, i_813
i_804:
	sh x25, -158(x2)
i_805:
	sb x6, 304(x2)
i_806:
	lbu x27, -308(x2)
i_807:
	sw x3, -224(x2)
i_808:
	and x25, x13, x25
i_809:
	slt x27, x26, x19
i_810:
	add x13, x30, x17
i_811:
	lbu x17, 124(x2)
i_812:
	ori x16, x13, -738
i_813:
	sh x21, -414(x2)
i_814:
	xori x16, x3, 109
i_815:
	addi x20, x0, 1856
i_816:
	addi x9, x0, 1859
i_817:
	lhu x12, 154(x2)
i_818:
	addi x20 , x20 , 1
	bgeu x9, x20, i_817
i_819:
	sltiu x3, x16, 1908
i_820:
	addi x29, x0, 2
i_821:
	sll x16, x22, x29
i_822:
	sltiu x6, x12, -1753
i_823:
	srli x22, x6, 2
i_824:
	and x8, x22, x10
i_825:
	lb x25, 417(x2)
i_826:
	bne x30, x4, i_827
i_827:
	bge x1, x26, i_833
i_828:
	lw x26, -416(x2)
i_829:
	sh x26, 456(x2)
i_830:
	sb x23, 373(x2)
i_831:
	beq x7, x9, i_836
i_832:
	sb x27, 466(x2)
i_833:
	auipc x9, 1012315
i_834:
	blt x28, x26, i_835
i_835:
	bltu x21, x13, i_841
i_836:
	sh x3, -154(x2)
i_837:
	lbu x26, -6(x2)
i_838:
	lw x23, -176(x2)
i_839:
	bgeu x23, x28, i_844
i_840:
	lh x15, -226(x2)
i_841:
	addi x23, x23, -1497
i_842:
	sw x5, 296(x2)
i_843:
	lw x23, 372(x2)
i_844:
	beq x10, x19, i_852
i_845:
	and x18, x24, x4
i_846:
	lbu x19, 346(x2)
i_847:
	lbu x24, 283(x2)
i_848:
	xori x24, x3, 1326
i_849:
	srli x20, x23, 1
i_850:
	ori x29, x8, 181
i_851:
	auipc x8, 308321
i_852:
	slli x29, x10, 1
i_853:
	addi x23, x17, 445
i_854:
	addi x3, x0, -1904
i_855:
	addi x22, x0, -1900
i_856:
	bgeu x3, x12, i_857
i_857:
	slli x18, x9, 3
i_858:
	addi x11, x0, 23
i_859:
	srl x23, x22, x11
i_860:
	addi x3 , x3 , 1
	bne x3, x22, i_856
i_861:
	sh x20, -302(x2)
i_862:
	lhu x18, -246(x2)
i_863:
	addi x20, x0, 7
i_864:
	sll x10, x3, x20
i_865:
	lh x23, -54(x2)
i_866:
	bge x14, x10, i_869
i_867:
	sw x4, -372(x2)
i_868:
	add x13, x8, x20
i_869:
	lb x4, -120(x2)
i_870:
	bge x3, x29, i_879
i_871:
	sh x15, 306(x2)
i_872:
	sh x25, 330(x2)
i_873:
	andi x25, x29, 1090
i_874:
	bge x25, x10, i_882
i_875:
	lh x1, 400(x2)
i_876:
	sh x16, 422(x2)
i_877:
	lh x9, -284(x2)
i_878:
	add x11, x20, x5
i_879:
	lw x26, -268(x2)
i_880:
	sh x7, -136(x2)
i_881:
	sw x11, 308(x2)
i_882:
	lhu x30, -254(x2)
i_883:
	lw x1, 292(x2)
i_884:
	lhu x18, 18(x2)
i_885:
	xor x23, x28, x15
i_886:
	addi x13, x0, -1880
i_887:
	addi x20, x0, -1878
i_888:
	sltu x6, x17, x20
i_889:
	lhu x6, -356(x2)
i_890:
	lbu x5, 79(x2)
i_891:
	addi x8, x0, 5
i_892:
	sll x9, x8, x8
i_893:
	addi x13 , x13 , 1
	bne x13, x20, i_888
i_894:
	slli x18, x18, 2
i_895:
	beq x3, x9, i_899
i_896:
	sh x12, 260(x2)
i_897:
	lbu x4, -205(x2)
i_898:
	lhu x10, 276(x2)
i_899:
	bge x20, x6, i_901
i_900:
	bge x20, x20, i_908
i_901:
	lui x28, 61753
i_902:
	bge x19, x21, i_912
i_903:
	lh x18, -166(x2)
i_904:
	lbu x18, 420(x2)
i_905:
	sw x17, 120(x2)
i_906:
	beq x21, x31, i_913
i_907:
	sltu x11, x23, x26
i_908:
	sw x28, 196(x2)
i_909:
	andi x4, x29, -1013
i_910:
	lb x10, -209(x2)
i_911:
	lhu x17, 66(x2)
i_912:
	sw x16, 412(x2)
i_913:
	lh x22, -4(x2)
i_914:
	add x1, x2, x2
i_915:
	bne x17, x28, i_920
i_916:
	slt x28, x23, x25
i_917:
	sw x1, -460(x2)
i_918:
	lb x13, -263(x2)
i_919:
	lbu x13, 483(x2)
i_920:
	lh x16, -168(x2)
i_921:
	nop
i_922:
	addi x30, x0, 1886
i_923:
	addi x28, x0, 1888
i_924:
	lbu x1, -274(x2)
i_925:
	lhu x27, 180(x2)
i_926:
	lh x27, 410(x2)
i_927:
	and x8, x8, x26
i_928:
	lw x24, 440(x2)
i_929:
	addi x30 , x30 , 1
	blt x30, x28, i_924
i_930:
	sb x25, 218(x2)
i_931:
	bne x1, x27, i_938
i_932:
	sb x27, -48(x2)
i_933:
	slti x25, x24, -298
i_934:
	lh x1, -154(x2)
i_935:
	sb x7, 375(x2)
i_936:
	sltiu x3, x18, -2032
i_937:
	sltiu x28, x2, 1177
i_938:
	add x3, x20, x4
i_939:
	lb x3, 23(x2)
i_940:
	lw x4, -36(x2)
i_941:
	andi x20, x26, -242
i_942:
	lb x26, -348(x2)
i_943:
	slli x6, x6, 4
i_944:
	xor x28, x8, x16
i_945:
	sw x27, 424(x2)
i_946:
	lb x29, 72(x2)
i_947:
	bge x17, x11, i_948
i_948:
	lui x17, 273243
i_949:
	sb x15, 221(x2)
i_950:
	addi x27, x0, -2045
i_951:
	addi x15, x0, -2042
i_952:
	lhu x11, 438(x2)
i_953:
	sw x19, 32(x2)
i_954:
	lb x3, -70(x2)
i_955:
	lhu x20, -322(x2)
i_956:
	addi x27 , x27 , 1
	blt x27, x15, i_952
i_957:
	lbu x21, -342(x2)
i_958:
	lhu x16, 280(x2)
i_959:
	lh x11, -160(x2)
i_960:
	lh x20, -188(x2)
i_961:
	bge x28, x15, i_963
i_962:
	lui x3, 719323
i_963:
	sw x16, 128(x2)
i_964:
	sw x21, -408(x2)
i_965:
	addi x15, x0, -1966
i_966:
	addi x20, x0, -1964
i_967:
	srli x18, x6, 1
i_968:
	addi x6, x14, -333
i_969:
	addi x3, x0, -1838
i_970:
	addi x16, x0, -1835
i_971:
	ori x22, x7, 1297
i_972:
	addi x3 , x3 , 1
	blt x3, x16, i_971
i_973:
	lw x19, -108(x2)
i_974:
	lw x22, 432(x2)
i_975:
	addi x15 , x15 , 1
	bgeu x20, x15, i_967
i_976:
	and x19, x12, x23
i_977:
	lhu x21, 70(x2)
i_978:
	lh x3, -240(x2)
i_979:
	andi x28, x6, 1459
i_980:
	lhu x18, 106(x2)
i_981:
	sltiu x17, x27, -745
i_982:
	addi x13, x0, 1942
i_983:
	addi x19, x0, 1945
i_984:
	and x26, x17, x2
i_985:
	sub x18, x17, x24
i_986:
	sh x4, 286(x2)
i_987:
	lw x26, 412(x2)
i_988:
	sb x4, -451(x2)
i_989:
	addi x13 , x13 , 1
	bltu x13, x19, i_984
i_990:
	lbu x17, -335(x2)
i_991:
	sub x1, x26, x15
i_992:
	add x4, x9, x18
i_993:
	bge x6, x11, i_995
i_994:
	srli x4, x8, 3
i_995:
	sh x12, -150(x2)
i_996:
	slli x28, x24, 4
i_997:
	sub x4, x1, x18
i_998:
	lw x28, 108(x2)
i_999:
	lh x6, -250(x2)
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0x91274062
	.word 0x315271d4
	.word 0x8f5e747e
	.word 0xd32e681e
	.word 0x60f96f61
	.word 0x9965c84e
	.word 0x2ef8c38a
	.word 0xc01d599f
	.word 0xfeaba73f
	.word 0x550e421
	.word 0x2e160af8
	.word 0xadb02973
	.word 0x8e90a910
	.word 0x321ea13c
	.word 0x44180174
	.word 0xe54e5085
	.word 0xc9dfdc90
	.word 0x8a6ffc68
	.word 0xe7ab20
	.word 0xf7cb784a
	.word 0x64e26a94
	.word 0x357fa811
	.word 0x67f6154c
	.word 0x3dd35640
	.word 0x90582402
	.word 0xc37ec2c2
	.word 0xa12ebf7
	.word 0x6de942fb
	.word 0xe733d695
	.word 0x3c685135
	.word 0xd8bea6b3
	.word 0x1d2af8ee
	.word 0x42de3177
	.word 0xcb9e8ca0
	.word 0x7c8d54f5
	.word 0x56ef1113
	.word 0xa6bce20c
	.word 0x64c8314
	.word 0x3808a7c8
	.word 0xb3aa8988
	.word 0xef8f2d1a
	.word 0x21140245
	.word 0xe7acb86f
	.word 0xd16ebcd8
	.word 0x8e98e528
	.word 0x6b5d27a9
	.word 0x56106448
	.word 0xbe5e2e74
	.word 0x4b757f96
	.word 0x8d0775f1
	.word 0xc861872a
	.word 0x8c529960
	.word 0xc4a48f8b
	.word 0x9cdaae7d
	.word 0x74cc5037
	.word 0xe6a6b85b
	.word 0x8c167f75
	.word 0xbb6f86ff
	.word 0xf79e4f6c
	.word 0x8ba742fa
	.word 0xe5847857
	.word 0x7b6c96a
	.word 0x77df1358
	.word 0x9f620ff4
	.word 0xca213511
	.word 0x98e8cc12
	.word 0x228cda67
	.word 0x76f2dc78
	.word 0x7c920b3b
	.word 0xa610a2ac
	.word 0x4a2cfd71
	.word 0x4c8914ad
	.word 0x235014c3
	.word 0x224284e6
	.word 0x361c938a
	.word 0x4aedb1b6
	.word 0x54746ca9
	.word 0xfb49d84d
	.word 0x870e8ca8
	.word 0x3b54f340
	.word 0xb2e2b740
	.word 0x9347d951
	.word 0xb3257dfe
	.word 0x95fb083a
	.word 0xfeea2637
	.word 0x415eae43
	.word 0x913ce46c
	.word 0xc460276
	.word 0x31b51831
	.word 0x56bbfb92
	.word 0x60ad47ec
	.word 0x5412c98e
	.word 0xe23c55ae
	.word 0xa2693f9a
	.word 0x85141501
	.word 0x34461f1
	.word 0xceee0826
	.word 0x413d72ab
	.word 0xbda82247
	.word 0x5708f5d3
	.word 0x2702005d
	.word 0x2eb8d9ff
	.word 0xe212eabb
	.word 0x436f597f
	.word 0xf58bc0f8
	.word 0x4bd9751
	.word 0x53f8de9f
	.word 0xb612df45
	.word 0x5d07580b
	.word 0xaa452713
	.word 0x4c563bd9
	.word 0x3761004d
	.word 0xa0e5c6f4
	.word 0x7b5c07a1
	.word 0x87360aa5
	.word 0x8cdd0ce6
	.word 0x3c4621ae
	.word 0x2f88da88
	.word 0xcfa7d25d
	.word 0x8b3e9980
	.word 0xe558aa4e
	.word 0xb8b60df9
	.word 0xd5384782
	.word 0x7e9a3cea
	.word 0x967c7abc
	.word 0xd3a64ee2
	.word 0x1b1e1df9
	.word 0xfadfd86b
	.word 0xb39ea115
	.word 0x18aa28c0
	.word 0xf5d76759
	.word 0x893a8eaf
	.word 0xd42c5384
	.word 0x4f6b8ba9
	.word 0x9b61c7a
	.word 0xf72850c2
	.word 0x1cc1ab62
	.word 0x18761123
	.word 0xe2e1dc61
	.word 0x4fe98993
	.word 0x420875c9
	.word 0xb4e9aa65
	.word 0xd3473546
	.word 0x418d1023
	.word 0xa7b904c7
	.word 0xf3d4489c
	.word 0xb66331e1
	.word 0x3da15216
	.word 0x4f90868e
	.word 0xa8108b55
	.word 0xf09b1f83
	.word 0x73ae8668
	.word 0x96537a29
	.word 0x4a489c56
	.word 0xa8e5fd62
	.word 0x99e700af
	.word 0x6be23631
	.word 0x1371ca1c
	.word 0xe0db882e
	.word 0xc3638a1b
	.word 0x32a7c5f
	.word 0x2b54ae7d
	.word 0xbcebded7
	.word 0x7ee21d14
	.word 0xc8824b7
	.word 0xeeef87a4
	.word 0xbd4b132b
	.word 0x4faafdc6
	.word 0xc1d7f577
	.word 0x5dc8803d
	.word 0x84a3d4b9
	.word 0x4c30dc14
	.word 0x9dfb156d
	.word 0x81339b50
	.word 0xdc36477c
	.word 0xfba8a914
	.word 0x90825151
	.word 0xab0e741d
	.word 0xcaef7ef7
	.word 0x4edfa219
	.word 0xf139be1b
	.word 0x8bd6888a
	.word 0x115a0df0
	.word 0xc76de6c5
	.word 0x5132e15e
	.word 0xc961bde0
	.word 0x7229e955
	.word 0x233aa67
	.word 0xa03d63ee
	.word 0xc5edcdd1
	.word 0xcef84e56
	.word 0x75484c7e
	.word 0x2c7c726f
	.word 0xcdac8f4a
	.word 0x11429da7
	.word 0xc6251e8c
	.word 0x8c852cbf
	.word 0x92185ecc
	.word 0xff9a199a
	.word 0x12c4ead0
	.word 0xf71bcd65
	.word 0x7709f434
	.word 0x9bba4dce
	.word 0x6b37bd13
	.word 0x5f2c66d9
	.word 0x9c0f75b7
	.word 0x990eaf0e
	.word 0x52167f50
	.word 0x5497205e
	.word 0xb361e59
	.word 0x130b28fe
	.word 0x9af18965
	.word 0x34cecb1a
	.word 0x3e2a7dcd
	.word 0x4fae3d6d
	.word 0xa599e207
	.word 0xd3b22c9a
	.word 0x70717112
	.word 0xba16284e
	.word 0xd9ea50bf
	.word 0xd2ca6c97
	.word 0x5440bfd1
	.word 0xbb9c4875
	.word 0x6905fff5
	.word 0xae6a23cc
	.word 0xe385a193
	.word 0x68873662
	.word 0x2a02346
	.word 0xb0cc2ce
	.word 0x5ddc3ced
	.word 0xd6c3446f
	.word 0x210e2877
	.word 0xf81ade09
	.word 0xc67c80c8
	.word 0x2130bf19
	.word 0x2c423858
	.word 0x9df9ebd6
	.word 0x10468046
	.word 0x3e846336
	.word 0x411a05e6
	.word 0xb689cf7e
	.word 0x9099fadc
	.word 0xcc712567
	.word 0xd50603dc
	.word 0xbd0a60fe
	.word 0xda926eac
	.word 0x1e716087
	.word 0xd0fe5dae
	.word 0x2cf054d8
	.word 0xc393040e
	.word 0x20bbbbcd
	.word 0xb48e0497
	.word 0x7e2d4393
	.word 0x222ee70b
	.word 0x9da6e8dd
	.word 0x3403f2f4
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
