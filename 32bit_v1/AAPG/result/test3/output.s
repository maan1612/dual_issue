
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	beq x18, x8, i_5
i_1:
	nop
i_2:
	srai x15, x11, 4
i_3:
	sb x6, -232(x2)
i_4:
	lw x4, 404(x2)
i_5:
	sh x15, -28(x2)
i_6:
	slti x25, x24, 1976
i_7:
	bltu x6, x7, i_15
i_8:
	xori x5, x5, -1929
i_9:
	or x25, x4, x19
i_10:
	lh x23, 74(x2)
i_11:
	lb x8, -173(x2)
i_12:
	sh x10, 398(x2)
i_13:
	lbu x11, 286(x2)
i_14:
	bgeu x3, x27, i_19
i_15:
	lbu x11, -17(x2)
i_16:
	beq x8, x24, i_17
i_17:
	lhu x24, -140(x2)
i_18:
	sw x5, 352(x2)
i_19:
	sw x18, 276(x2)
i_20:
	addi x26, x0, 21
i_21:
	sll x24, x11, x26
i_22:
	addi x30, x0, -1910
i_23:
	addi x6, x0, -1908
i_24:
	sh x6, 394(x2)
i_25:
	lui x11, 159432
i_26:
	andi x26, x13, -1830
i_27:
	addi x30 , x30 , 1
	bltu x30, x6, i_23
i_28:
	lh x6, 438(x2)
i_29:
	addi x13, x30, -762
i_30:
	xori x6, x26, 328
i_31:
	lh x17, -312(x2)
i_32:
	or x28, x25, x6
i_33:
	blt x19, x3, i_43
i_34:
	or x19, x4, x5
i_35:
	sb x18, 344(x2)
i_36:
	addi x5, x0, 13
i_37:
	sll x27, x15, x5
i_38:
	sb x27, -366(x2)
i_39:
	addi x17, x0, 18
i_40:
	srl x18, x30, x17
i_41:
	lbu x21, -427(x2)
i_42:
	lhu x27, -364(x2)
i_43:
	sh x10, 372(x2)
i_44:
	auipc x17, 432648
i_45:
	blt x17, x27, i_51
i_46:
	sb x27, -250(x2)
i_47:
	and x23, x27, x23
i_48:
	lhu x23, 62(x2)
i_49:
	lbu x14, 309(x2)
i_50:
	lhu x21, -458(x2)
i_51:
	sh x5, 406(x2)
i_52:
	addi x4, x10, 1730
i_53:
	sh x2, -420(x2)
i_54:
	beq x11, x4, i_55
i_55:
	sh x8, 194(x2)
i_56:
	sltu x26, x23, x23
i_57:
	beq x12, x2, i_62
i_58:
	sub x26, x23, x4
i_59:
	lhu x6, -166(x2)
i_60:
	add x14, x13, x26
i_61:
	sw x23, 88(x2)
i_62:
	lhu x4, -260(x2)
i_63:
	slli x23, x29, 2
i_64:
	sb x30, 133(x2)
i_65:
	lh x3, 178(x2)
i_66:
	auipc x4, 194525
i_67:
	srai x21, x25, 1
i_68:
	lb x29, 164(x2)
i_69:
	beq x10, x2, i_78
i_70:
	addi x4, x0, 12
i_71:
	srl x21, x8, x4
i_72:
	sh x21, -458(x2)
i_73:
	sb x18, 184(x2)
i_74:
	lbu x21, -96(x2)
i_75:
	lhu x22, 302(x2)
i_76:
	sb x16, -19(x2)
i_77:
	lb x3, 90(x2)
i_78:
	lh x15, -420(x2)
i_79:
	bgeu x15, x16, i_81
i_80:
	addi x22, x0, 30
i_81:
	sll x20, x31, x22
i_82:
	sub x27, x14, x18
i_83:
	sb x7, -316(x2)
i_84:
	lw x17, -328(x2)
i_85:
	sw x30, -228(x2)
i_86:
	sb x17, 73(x2)
i_87:
	sltu x14, x13, x14
i_88:
	lh x17, 350(x2)
i_89:
	lb x5, -78(x2)
i_90:
	add x14, x5, x28
i_91:
	add x5, x3, x23
i_92:
	sw x5, -208(x2)
i_93:
	nop
i_94:
	addi x5, x0, 2019
i_95:
	addi x23, x0, 2022
i_96:
	addi x21, x0, 26
i_97:
	sll x21, x28, x21
i_98:
	addi x5 , x5 , 1
	bne x5, x23, i_96
i_99:
	lw x21, 24(x2)
i_100:
	lh x12, -158(x2)
i_101:
	bne x12, x10, i_104
i_102:
	lhu x28, 434(x2)
i_103:
	srai x10, x17, 1
i_104:
	sb x5, 80(x2)
i_105:
	sb x27, 207(x2)
i_106:
	addi x27, x24, -47
i_107:
	sw x12, -156(x2)
i_108:
	bgeu x12, x6, i_111
i_109:
	bgeu x8, x7, i_119
i_110:
	slt x9, x27, x26
i_111:
	sub x9, x5, x24
i_112:
	and x19, x24, x22
i_113:
	or x18, x26, x12
i_114:
	lb x12, -133(x2)
i_115:
	lw x26, -344(x2)
i_116:
	bne x16, x31, i_126
i_117:
	sw x12, 140(x2)
i_118:
	sw x18, 172(x2)
i_119:
	lbu x8, -273(x2)
i_120:
	lbu x18, 252(x2)
i_121:
	beq x23, x1, i_127
i_122:
	lbu x12, 481(x2)
i_123:
	addi x17, x0, 17
i_124:
	sra x17, x28, x17
i_125:
	xori x26, x27, 1408
i_126:
	or x27, x30, x17
i_127:
	lw x27, 100(x2)
i_128:
	or x28, x12, x15
i_129:
	xor x10, x12, x10
i_130:
	sub x25, x20, x28
i_131:
	lh x10, 400(x2)
i_132:
	bltu x16, x25, i_136
i_133:
	sb x30, 334(x2)
i_134:
	lbu x19, -448(x2)
i_135:
	sh x14, 230(x2)
i_136:
	lb x3, 238(x2)
i_137:
	sw x21, -308(x2)
i_138:
	lbu x25, 447(x2)
i_139:
	lb x25, 32(x2)
i_140:
	ori x6, x27, -1786
i_141:
	sw x3, 64(x2)
i_142:
	sb x26, -324(x2)
i_143:
	lbu x3, 70(x2)
i_144:
	lhu x9, 224(x2)
i_145:
	addi x25, x0, 30
i_146:
	srl x18, x26, x25
i_147:
	addi x4, x0, 18
i_148:
	sra x13, x12, x4
i_149:
	lb x9, -418(x2)
i_150:
	lh x8, 112(x2)
i_151:
	lbu x15, -17(x2)
i_152:
	addi x8, x0, 26
i_153:
	sll x14, x28, x8
i_154:
	lh x30, -432(x2)
i_155:
	sw x15, 468(x2)
i_156:
	lw x12, -468(x2)
i_157:
	lhu x14, -370(x2)
i_158:
	addi x26, x0, 14
i_159:
	srl x22, x13, x26
i_160:
	beq x17, x27, i_161
i_161:
	sw x23, 68(x2)
i_162:
	blt x2, x10, i_167
i_163:
	sb x13, 27(x2)
i_164:
	lb x8, 20(x2)
i_165:
	sh x20, -110(x2)
i_166:
	add x27, x8, x11
i_167:
	and x20, x27, x25
i_168:
	slt x11, x3, x31
i_169:
	lbu x20, 176(x2)
i_170:
	addi x11, x0, 11
i_171:
	sra x1, x15, x11
i_172:
	sh x8, -4(x2)
i_173:
	bgeu x4, x27, i_181
i_174:
	sw x1, -352(x2)
i_175:
	lw x24, 96(x2)
i_176:
	addi x27, x27, 1854
i_177:
	slt x14, x17, x29
i_178:
	sb x18, 389(x2)
i_179:
	lh x23, -198(x2)
i_180:
	lh x25, 388(x2)
i_181:
	lbu x23, -435(x2)
i_182:
	sw x25, -412(x2)
i_183:
	addi x11, x0, -1974
i_184:
	addi x17, x0, -1971
i_185:
	lh x23, -368(x2)
i_186:
	ori x14, x6, -1514
i_187:
	auipc x14, 328589
i_188:
	bne x14, x27, i_194
i_189:
	beq x14, x25, i_199
i_190:
	sw x13, -452(x2)
i_191:
	lw x23, 484(x2)
i_192:
	nop
i_193:
	sh x24, 392(x2)
i_194:
	lbu x12, -363(x2)
i_195:
	slt x30, x12, x28
i_196:
	addi x11 , x11 , 1
	bne  x17, x11, i_185
i_197:
	sw x3, 252(x2)
i_198:
	sw x5, 56(x2)
i_199:
	sub x24, x19, x14
i_200:
	bge x3, x8, i_206
i_201:
	lhu x23, 146(x2)
i_202:
	andi x21, x2, -774
i_203:
	lhu x6, -466(x2)
i_204:
	xori x9, x12, 1364
i_205:
	slli x1, x1, 3
i_206:
	sb x22, -98(x2)
i_207:
	slli x17, x31, 3
i_208:
	lhu x14, 470(x2)
i_209:
	add x22, x13, x28
i_210:
	sh x12, -484(x2)
i_211:
	lhu x17, -108(x2)
i_212:
	addi x6, x0, 9
i_213:
	sra x27, x27, x6
i_214:
	addi x30, x0, 1877
i_215:
	addi x12, x0, 1881
i_216:
	lb x6, 375(x2)
i_217:
	slti x3, x16, 1859
i_218:
	sw x9, 344(x2)
i_219:
	lhu x6, -424(x2)
i_220:
	nop
i_221:
	sw x4, -452(x2)
i_222:
	addi x30 , x30 , 1
	bgeu x12, x30, i_216
i_223:
	lw x3, 80(x2)
i_224:
	blt x13, x10, i_226
i_225:
	sltiu x15, x6, 694
i_226:
	sb x15, 29(x2)
i_227:
	lb x10, 478(x2)
i_228:
	sb x21, -79(x2)
i_229:
	srli x26, x10, 1
i_230:
	sub x21, x21, x13
i_231:
	sw x11, 364(x2)
i_232:
	lh x21, 86(x2)
i_233:
	sh x16, 458(x2)
i_234:
	addi x19, x0, 1949
i_235:
	addi x6, x0, 1951
i_236:
	ori x20, x19, -580
i_237:
	nop
i_238:
	andi x30, x25, -1226
i_239:
	addi x19 , x19 , 1
	bltu x19, x6, i_236
i_240:
	lbu x30, -290(x2)
i_241:
	add x20, x10, x28
i_242:
	lhu x25, -154(x2)
i_243:
	lw x28, -216(x2)
i_244:
	lhu x28, 404(x2)
i_245:
	lh x22, 30(x2)
i_246:
	sw x22, 52(x2)
i_247:
	bltu x17, x12, i_255
i_248:
	lb x15, 394(x2)
i_249:
	bge x14, x11, i_253
i_250:
	slli x11, x7, 3
i_251:
	lw x29, 240(x2)
i_252:
	addi x11, x28, 937
i_253:
	blt x5, x21, i_258
i_254:
	addi x22, x0, 14
i_255:
	sra x28, x9, x22
i_256:
	lui x22, 10209
i_257:
	addi x14, x0, 8
i_258:
	sra x3, x28, x14
i_259:
	bge x7, x3, i_264
i_260:
	bne x30, x13, i_270
i_261:
	blt x12, x29, i_268
i_262:
	addi x21, x0, 7
i_263:
	sll x13, x17, x21
i_264:
	lh x14, -450(x2)
i_265:
	sh x10, 110(x2)
i_266:
	bltu x21, x5, i_270
i_267:
	lw x5, -116(x2)
i_268:
	addi x13, x3, 380
i_269:
	lhu x28, -338(x2)
i_270:
	slt x3, x3, x24
i_271:
	andi x9, x4, -766
i_272:
	addi x16, x5, 450
i_273:
	slti x9, x24, -98
i_274:
	sh x9, -458(x2)
i_275:
	lb x17, 341(x2)
i_276:
	lw x24, 8(x2)
i_277:
	beq x12, x14, i_287
i_278:
	sub x16, x9, x14
i_279:
	bne x12, x28, i_286
i_280:
	sltiu x30, x28, -746
i_281:
	sb x7, 324(x2)
i_282:
	lhu x28, -362(x2)
i_283:
	beq x16, x17, i_290
i_284:
	ori x16, x24, -148
i_285:
	lhu x28, 428(x2)
i_286:
	lw x5, 112(x2)
i_287:
	bgeu x29, x18, i_294
i_288:
	xori x23, x16, -322
i_289:
	auipc x23, 14917
i_290:
	sw x5, -344(x2)
i_291:
	lb x29, -111(x2)
i_292:
	slli x14, x9, 4
i_293:
	addi x22, x0, 1
i_294:
	srl x9, x26, x22
i_295:
	lw x9, -320(x2)
i_296:
	addi x29, x0, 1992
i_297:
	addi x25, x0, 1996
i_298:
	lui x28, 710604
i_299:
	addi x29 , x29 , 1
	bgeu x25, x29, i_298
i_300:
	lhu x9, 336(x2)
i_301:
	lhu x10, -296(x2)
i_302:
	lb x17, -192(x2)
i_303:
	srai x6, x13, 2
i_304:
	slli x21, x21, 1
i_305:
	beq x17, x31, i_313
i_306:
	slt x22, x22, x2
i_307:
	andi x17, x13, 430
i_308:
	sw x21, -436(x2)
i_309:
	lhu x20, -18(x2)
i_310:
	sltiu x4, x21, -27
i_311:
	lh x18, 54(x2)
i_312:
	lb x27, -260(x2)
i_313:
	sltu x27, x23, x22
i_314:
	add x9, x21, x25
i_315:
	lhu x22, 190(x2)
i_316:
	lb x15, -365(x2)
i_317:
	sb x10, -252(x2)
i_318:
	addi x17, x1, -349
i_319:
	lb x22, -485(x2)
i_320:
	slt x18, x17, x9
i_321:
	lhu x24, 40(x2)
i_322:
	addi x9, x0, -1959
i_323:
	addi x30, x0, -1955
i_324:
	sb x14, -267(x2)
i_325:
	lhu x17, -398(x2)
i_326:
	lb x14, 48(x2)
i_327:
	lbu x12, 250(x2)
i_328:
	slti x17, x12, 939
i_329:
	srai x3, x5, 1
i_330:
	srli x12, x3, 1
i_331:
	srli x16, x16, 3
i_332:
	addi x9 , x9 , 1
	blt x9, x30, i_324
i_333:
	or x25, x27, x28
i_334:
	lh x4, 460(x2)
i_335:
	bne x26, x14, i_340
i_336:
	auipc x16, 475113
i_337:
	lw x23, 320(x2)
i_338:
	lh x25, -406(x2)
i_339:
	sh x7, 244(x2)
i_340:
	slli x14, x28, 1
i_341:
	lw x25, -64(x2)
i_342:
	bltu x24, x18, i_352
i_343:
	lbu x18, -8(x2)
i_344:
	lhu x17, 56(x2)
i_345:
	addi x20, x10, -1945
i_346:
	sw x21, -164(x2)
i_347:
	nop
i_348:
	lbu x17, 153(x2)
i_349:
	and x18, x24, x4
i_350:
	lb x27, -19(x2)
i_351:
	lbu x11, -418(x2)
i_352:
	sw x6, 376(x2)
i_353:
	sh x21, -300(x2)
i_354:
	addi x25, x0, -1991
i_355:
	addi x23, x0, -1987
i_356:
	sw x27, -208(x2)
i_357:
	slt x20, x25, x24
i_358:
	lb x24, 18(x2)
i_359:
	sb x15, -154(x2)
i_360:
	lbu x12, 458(x2)
i_361:
	addi x25 , x25 , 1
	bltu x25, x23, i_356
i_362:
	auipc x9, 676880
i_363:
	sb x1, 385(x2)
i_364:
	xor x3, x10, x1
i_365:
	lb x30, 389(x2)
i_366:
	lw x22, -104(x2)
i_367:
	addi x6, x0, -1981
i_368:
	addi x9, x0, -1977
i_369:
	sw x20, 348(x2)
i_370:
	lh x13, -424(x2)
i_371:
	lhu x24, -22(x2)
i_372:
	lh x22, -224(x2)
i_373:
	sb x8, -486(x2)
i_374:
	sub x11, x20, x3
i_375:
	nop
i_376:
	sltu x13, x31, x31
i_377:
	addi x13, x0, 17
i_378:
	sll x13, x13, x13
i_379:
	lhu x25, 206(x2)
i_380:
	addi x6 , x6 , 1
	bge x9, x6, i_369
i_381:
	addi x6, x0, 18
i_382:
	sll x10, x24, x6
i_383:
	sh x5, -320(x2)
i_384:
	sh x10, -396(x2)
i_385:
	bge x25, x1, i_393
i_386:
	lhu x3, -218(x2)
i_387:
	slt x15, x3, x5
i_388:
	addi x21, x27, 286
i_389:
	addi x30, x0, 28
i_390:
	srl x17, x11, x30
i_391:
	lbu x3, -370(x2)
i_392:
	lh x11, 312(x2)
i_393:
	lb x30, 220(x2)
i_394:
	nop
i_395:
	addi x12, x0, 1879
i_396:
	addi x5, x0, 1882
i_397:
	lw x21, -180(x2)
i_398:
	lbu x4, 108(x2)
i_399:
	sh x2, -450(x2)
i_400:
	addi x12 , x12 , 1
	bge x5, x12, i_397
i_401:
	sh x18, -284(x2)
i_402:
	lb x11, -73(x2)
i_403:
	bne x6, x17, i_412
i_404:
	auipc x28, 128621
i_405:
	andi x9, x4, -843
i_406:
	blt x10, x3, i_408
i_407:
	lbu x3, -98(x2)
i_408:
	sb x6, 378(x2)
i_409:
	auipc x3, 920382
i_410:
	sh x9, 68(x2)
i_411:
	blt x18, x28, i_421
i_412:
	lb x20, -297(x2)
i_413:
	lb x20, 289(x2)
i_414:
	addi x9, x1, 1493
i_415:
	sh x28, 102(x2)
i_416:
	sw x9, -388(x2)
i_417:
	lb x11, 68(x2)
i_418:
	lhu x28, 442(x2)
i_419:
	lb x28, 189(x2)
i_420:
	lbu x21, 298(x2)
i_421:
	lw x27, -60(x2)
i_422:
	addi x28, x0, 30
i_423:
	sra x8, x10, x28
i_424:
	auipc x25, 409643
i_425:
	lh x5, -242(x2)
i_426:
	sub x17, x15, x27
i_427:
	sb x28, -216(x2)
i_428:
	xori x3, x2, 1241
i_429:
	lh x24, 432(x2)
i_430:
	sltiu x27, x24, 1553
i_431:
	lb x23, -89(x2)
i_432:
	lb x8, -431(x2)
i_433:
	sh x24, 124(x2)
i_434:
	addi x1, x11, 16
i_435:
	lb x26, 262(x2)
i_436:
	ori x20, x27, -940
i_437:
	blt x5, x27, i_446
i_438:
	lbu x21, -370(x2)
i_439:
	or x17, x26, x17
i_440:
	xor x8, x17, x24
i_441:
	lh x17, -440(x2)
i_442:
	addi x23, x20, 217
i_443:
	sw x16, -180(x2)
i_444:
	andi x16, x25, 35
i_445:
	lw x17, -180(x2)
i_446:
	lb x20, -28(x2)
i_447:
	lb x16, 383(x2)
i_448:
	nop
i_449:
	addi x25, x0, -1892
i_450:
	addi x26, x0, -1889
i_451:
	lb x16, -204(x2)
i_452:
	ori x11, x26, -1886
i_453:
	auipc x17, 588716
i_454:
	lb x3, -404(x2)
i_455:
	sb x3, 101(x2)
i_456:
	beq x13, x10, i_466
i_457:
	nop
i_458:
	addi x25 , x25 , 1
	bge x26, x25, i_451
i_459:
	bgeu x3, x17, i_468
i_460:
	bltu x24, x3, i_468
i_461:
	add x3, x2, x3
i_462:
	slti x24, x19, -1639
i_463:
	bgeu x24, x9, i_473
i_464:
	beq x31, x24, i_472
i_465:
	or x9, x24, x4
i_466:
	lb x24, -218(x2)
i_467:
	sltu x24, x23, x8
i_468:
	lh x9, -442(x2)
i_469:
	sb x16, -265(x2)
i_470:
	add x20, x7, x26
i_471:
	lui x27, 303553
i_472:
	add x9, x10, x18
i_473:
	lbu x9, 473(x2)
i_474:
	lh x14, 408(x2)
i_475:
	sh x13, 180(x2)
i_476:
	lbu x17, 30(x2)
i_477:
	andi x26, x24, 173
i_478:
	blt x7, x9, i_483
i_479:
	slt x15, x12, x22
i_480:
	lhu x24, 26(x2)
i_481:
	addi x28, x14, 1015
i_482:
	bne x24, x15, i_488
i_483:
	sb x23, -171(x2)
i_484:
	lw x3, 152(x2)
i_485:
	sw x14, 236(x2)
i_486:
	bge x12, x8, i_491
i_487:
	slli x8, x5, 3
i_488:
	lbu x3, 222(x2)
i_489:
	srai x22, x26, 2
i_490:
	sw x15, -232(x2)
i_491:
	addi x27, x0, 22
i_492:
	sll x20, x15, x27
i_493:
	addi x24, x19, 315
i_494:
	sb x20, -465(x2)
i_495:
	sw x6, -256(x2)
i_496:
	srai x19, x5, 2
i_497:
	lw x24, 204(x2)
i_498:
	bne x1, x16, i_502
i_499:
	lui x8, 1018497
i_500:
	lbu x4, 301(x2)
i_501:
	sw x23, -348(x2)
i_502:
	lw x10, 428(x2)
i_503:
	lb x6, -244(x2)
i_504:
	add x6, x3, x25
i_505:
	lbu x24, 40(x2)
i_506:
	lh x27, 296(x2)
i_507:
	sw x10, -252(x2)
i_508:
	bgeu x6, x18, i_517
i_509:
	sb x1, 390(x2)
i_510:
	nop
i_511:
	nop
i_512:
	sw x14, 124(x2)
i_513:
	lhu x14, 198(x2)
i_514:
	lbu x17, -463(x2)
i_515:
	lb x14, 143(x2)
i_516:
	lh x26, -68(x2)
i_517:
	sb x30, -156(x2)
i_518:
	lw x6, 152(x2)
i_519:
	addi x11, x0, 1914
i_520:
	addi x10, x0, 1916
i_521:
	sb x22, -434(x2)
i_522:
	sh x3, 78(x2)
i_523:
	addi x11 , x11 , 1
	bge x10, x11, i_521
i_524:
	lh x10, 170(x2)
i_525:
	slli x10, x29, 4
i_526:
	bltu x25, x11, i_531
i_527:
	beq x23, x10, i_537
i_528:
	bge x18, x24, i_537
i_529:
	and x18, x10, x18
i_530:
	lb x19, -98(x2)
i_531:
	lb x25, -317(x2)
i_532:
	sh x18, 292(x2)
i_533:
	sh x29, -172(x2)
i_534:
	lui x6, 213751
i_535:
	lb x27, -397(x2)
i_536:
	lhu x9, 330(x2)
i_537:
	addi x6, x0, 29
i_538:
	sra x16, x11, x6
i_539:
	addi x11, x0, -1937
i_540:
	addi x24, x0, -1934
i_541:
	lw x6, 136(x2)
i_542:
	or x9, x11, x7
i_543:
	lh x30, 356(x2)
i_544:
	addi x6, x0, 1
i_545:
	sra x6, x13, x6
i_546:
	addi x11 , x11 , 1
	blt x11, x24, i_541
i_547:
	lui x9, 1006583
i_548:
	sw x6, -432(x2)
i_549:
	lw x17, 220(x2)
i_550:
	bgeu x17, x28, i_557
i_551:
	sw x11, 56(x2)
i_552:
	xori x1, x20, -34
i_553:
	bltu x6, x17, i_556
i_554:
	sh x31, -24(x2)
i_555:
	bge x15, x1, i_562
i_556:
	xor x9, x9, x9
i_557:
	andi x6, x17, -367
i_558:
	sh x9, 100(x2)
i_559:
	addi x8, x0, 21
i_560:
	sra x17, x11, x8
i_561:
	addi x6, x0, 19
i_562:
	srl x17, x6, x6
i_563:
	or x23, x11, x19
i_564:
	lw x11, 32(x2)
i_565:
	srai x30, x2, 2
i_566:
	addi x23, x0, 17
i_567:
	srl x24, x29, x23
i_568:
	bge x23, x26, i_574
i_569:
	auipc x26, 24052
i_570:
	nop
i_571:
	sltu x15, x12, x30
i_572:
	nop
i_573:
	lh x30, -236(x2)
i_574:
	lui x30, 244642
i_575:
	sltiu x15, x14, 269
i_576:
	addi x6, x0, 1918
i_577:
	addi x11, x0, 1921
i_578:
	lw x10, 228(x2)
i_579:
	lb x16, -187(x2)
i_580:
	addi x6 , x6 , 1
	bltu x6, x11, i_578
i_581:
	addi x14, x0, 8
i_582:
	sra x3, x22, x14
i_583:
	sb x26, 196(x2)
i_584:
	lb x28, 95(x2)
i_585:
	add x11, x23, x22
i_586:
	xori x30, x30, -575
i_587:
	sb x15, 461(x2)
i_588:
	lh x30, -302(x2)
i_589:
	lw x11, -408(x2)
i_590:
	blt x6, x24, i_594
i_591:
	srli x24, x28, 1
i_592:
	sw x17, -188(x2)
i_593:
	beq x19, x22, i_597
i_594:
	sh x21, -398(x2)
i_595:
	lbu x12, 249(x2)
i_596:
	lh x26, -258(x2)
i_597:
	sw x4, -92(x2)
i_598:
	bne x11, x30, i_608
i_599:
	sh x23, -364(x2)
i_600:
	lb x4, -61(x2)
i_601:
	lb x26, -192(x2)
i_602:
	sub x26, x29, x2
i_603:
	auipc x4, 736553
i_604:
	lw x25, -128(x2)
i_605:
	and x20, x29, x20
i_606:
	lw x20, 292(x2)
i_607:
	sh x8, -188(x2)
i_608:
	sw x29, 324(x2)
i_609:
	sltu x20, x28, x21
i_610:
	addi x16, x0, -1868
i_611:
	addi x25, x0, -1864
i_612:
	nop
i_613:
	bne x25, x24, i_621
i_614:
	addi x16 , x16 , 1
	blt x16, x25, i_612
i_615:
	lhu x16, -260(x2)
i_616:
	sh x20, 424(x2)
i_617:
	lb x20, -68(x2)
i_618:
	lb x1, 385(x2)
i_619:
	lbu x13, -310(x2)
i_620:
	sh x6, -338(x2)
i_621:
	sh x6, -68(x2)
i_622:
	slti x27, x15, 1933
i_623:
	addi x9, x0, -1846
i_624:
	addi x21, x0, -1843
i_625:
	nop
i_626:
	addi x22, x0, 17
i_627:
	sll x13, x22, x22
i_628:
	addi x9 , x9 , 1
	bltu x9, x21, i_625
i_629:
	sh x16, 180(x2)
i_630:
	sltu x13, x20, x23
i_631:
	sh x26, -124(x2)
i_632:
	lh x19, 74(x2)
i_633:
	sh x6, -382(x2)
i_634:
	sw x22, 472(x2)
i_635:
	sb x23, 334(x2)
i_636:
	nop
i_637:
	addi x24, x0, -1861
i_638:
	addi x22, x0, -1858
i_639:
	sltu x19, x19, x1
i_640:
	addi x24 , x24 , 1
	bgeu x22, x24, i_639
i_641:
	lw x28, -236(x2)
i_642:
	bge x24, x20, i_648
i_643:
	and x28, x14, x19
i_644:
	bge x12, x24, i_651
i_645:
	add x24, x24, x6
i_646:
	sw x8, -20(x2)
i_647:
	sb x14, -216(x2)
i_648:
	lbu x4, -264(x2)
i_649:
	slti x8, x27, -883
i_650:
	lbu x24, -373(x2)
i_651:
	sw x18, -396(x2)
i_652:
	sh x28, 56(x2)
i_653:
	add x24, x4, x6
i_654:
	lh x4, -168(x2)
i_655:
	lb x8, 37(x2)
i_656:
	srai x4, x9, 4
i_657:
	lh x4, 212(x2)
i_658:
	xor x13, x8, x15
i_659:
	xori x9, x5, -626
i_660:
	slt x18, x9, x13
i_661:
	lh x20, -62(x2)
i_662:
	add x9, x18, x23
i_663:
	auipc x8, 749049
i_664:
	sb x8, 21(x2)
i_665:
	lhu x10, -462(x2)
i_666:
	add x3, x7, x1
i_667:
	lhu x29, -180(x2)
i_668:
	sltu x18, x16, x27
i_669:
	lhu x5, 168(x2)
i_670:
	sw x29, -372(x2)
i_671:
	lw x30, 288(x2)
i_672:
	sh x18, -474(x2)
i_673:
	sw x16, 436(x2)
i_674:
	lhu x20, 50(x2)
i_675:
	andi x21, x5, -1749
i_676:
	lw x18, 112(x2)
i_677:
	or x20, x3, x21
i_678:
	sltu x21, x5, x17
i_679:
	lh x17, 474(x2)
i_680:
	slti x9, x30, 1239
i_681:
	add x14, x21, x21
i_682:
	lb x12, 427(x2)
i_683:
	sw x10, -220(x2)
i_684:
	add x17, x27, x19
i_685:
	auipc x5, 777805
i_686:
	sb x14, 283(x2)
i_687:
	lhu x18, -434(x2)
i_688:
	add x9, x6, x14
i_689:
	sb x18, -283(x2)
i_690:
	lw x10, 348(x2)
i_691:
	sw x21, -152(x2)
i_692:
	lw x18, 64(x2)
i_693:
	lw x8, -184(x2)
i_694:
	xori x23, x11, 545
i_695:
	sb x14, -9(x2)
i_696:
	xori x5, x17, -2020
i_697:
	srai x17, x13, 3
i_698:
	sb x28, 36(x2)
i_699:
	sb x16, 447(x2)
i_700:
	beq x6, x12, i_701
i_701:
	lui x28, 205941
i_702:
	lh x9, -140(x2)
i_703:
	srai x12, x11, 1
i_704:
	sw x25, -80(x2)
i_705:
	lh x25, -154(x2)
i_706:
	sltu x14, x2, x6
i_707:
	blt x22, x19, i_714
i_708:
	sh x11, 282(x2)
i_709:
	lw x11, 384(x2)
i_710:
	lw x22, 168(x2)
i_711:
	slti x20, x29, 815
i_712:
	slt x20, x2, x25
i_713:
	lbu x20, -141(x2)
i_714:
	sb x13, 42(x2)
i_715:
	addi x27, x0, 7
i_716:
	srl x20, x3, x27
i_717:
	addi x28, x0, 2028
i_718:
	addi x8, x0, 2030
i_719:
	auipc x27, 867194
i_720:
	sh x2, -270(x2)
i_721:
	addi x28 , x28 , 1
	blt x28, x8, i_718
i_722:
	lb x3, -403(x2)
i_723:
	lw x8, -200(x2)
i_724:
	sw x9, -416(x2)
i_725:
	bltu x15, x12, i_734
i_726:
	lb x29, -426(x2)
i_727:
	or x20, x18, x29
i_728:
	lui x8, 1018025
i_729:
	blt x19, x31, i_733
i_730:
	lui x5, 317045
i_731:
	sh x14, 248(x2)
i_732:
	slli x8, x1, 3
i_733:
	ori x21, x28, 456
i_734:
	andi x20, x8, 152
i_735:
	bne x19, x1, i_744
i_736:
	lbu x13, 171(x2)
i_737:
	add x1, x28, x21
i_738:
	xori x19, x11, -134
i_739:
	lh x13, -174(x2)
i_740:
	lh x1, -250(x2)
i_741:
	lw x19, 44(x2)
i_742:
	slt x17, x23, x27
i_743:
	sh x29, 54(x2)
i_744:
	sw x29, -448(x2)
i_745:
	slti x23, x17, -1299
i_746:
	addi x27, x0, 2026
i_747:
	addi x1, x0, 2030
i_748:
	add x8, x22, x25
i_749:
	nop
i_750:
	sh x18, 246(x2)
i_751:
	sltu x9, x11, x8
i_752:
	addi x27 , x27 , 1
	blt x27, x1, i_748
i_753:
	xor x23, x26, x13
i_754:
	slli x1, x23, 2
i_755:
	sb x23, 81(x2)
i_756:
	sb x5, -290(x2)
i_757:
	lh x12, -152(x2)
i_758:
	srli x24, x9, 4
i_759:
	lhu x5, -174(x2)
i_760:
	addi x25, x0, -1872
i_761:
	addi x12, x0, -1868
i_762:
	lbu x24, -70(x2)
i_763:
	lbu x1, 162(x2)
i_764:
	slti x27, x2, 844
i_765:
	lb x24, -297(x2)
i_766:
	lb x8, -249(x2)
i_767:
	sh x9, 42(x2)
i_768:
	lh x9, -234(x2)
i_769:
	addi x25 , x25 , 1
	blt x25, x12, i_762
i_770:
	lh x22, -384(x2)
i_771:
	lb x19, -282(x2)
i_772:
	bltu x22, x14, i_780
i_773:
	lb x8, -190(x2)
i_774:
	auipc x20, 385295
i_775:
	sw x19, 96(x2)
i_776:
	lh x22, -368(x2)
i_777:
	addi x9, x22, -1676
i_778:
	slli x14, x23, 1
i_779:
	sltu x9, x29, x9
i_780:
	add x29, x11, x31
i_781:
	sw x14, 296(x2)
i_782:
	addi x28, x0, 5
i_783:
	sra x30, x9, x28
i_784:
	sw x24, -316(x2)
i_785:
	add x30, x17, x8
i_786:
	lw x23, 216(x2)
i_787:
	lhu x21, 398(x2)
i_788:
	ori x17, x23, -289
i_789:
	add x13, x29, x29
i_790:
	slt x21, x26, x28
i_791:
	lhu x12, 482(x2)
i_792:
	sb x12, 378(x2)
i_793:
	and x11, x21, x13
i_794:
	add x27, x18, x16
i_795:
	lbu x4, 261(x2)
i_796:
	blt x2, x10, i_806
i_797:
	lh x21, 48(x2)
i_798:
	lh x18, -186(x2)
i_799:
	lbu x27, -328(x2)
i_800:
	lb x20, -242(x2)
i_801:
	sb x16, -352(x2)
i_802:
	or x29, x28, x13
i_803:
	bgeu x30, x2, i_809
i_804:
	bge x21, x11, i_814
i_805:
	auipc x1, 684220
i_806:
	lw x1, -332(x2)
i_807:
	auipc x1, 1003383
i_808:
	sh x20, -466(x2)
i_809:
	slt x10, x11, x20
i_810:
	nop
i_811:
	lhu x17, -428(x2)
i_812:
	sb x27, 114(x2)
i_813:
	nop
i_814:
	lbu x3, 232(x2)
i_815:
	nop
i_816:
	addi x25, x0, 1983
i_817:
	addi x21, x0, 1985
i_818:
	sw x7, -216(x2)
i_819:
	bgeu x3, x11, i_823
i_820:
	addi x25 , x25 , 1
	bne x25, x21, i_818
i_821:
	lb x28, -454(x2)
i_822:
	auipc x28, 401986
i_823:
	sb x22, -445(x2)
i_824:
	sh x16, 214(x2)
i_825:
	lbu x8, -419(x2)
i_826:
	lb x9, -100(x2)
i_827:
	slli x28, x28, 3
i_828:
	sh x14, 24(x2)
i_829:
	sub x28, x2, x2
i_830:
	auipc x20, 480456
i_831:
	sb x20, 108(x2)
i_832:
	add x3, x22, x9
i_833:
	bne x2, x17, i_841
i_834:
	sub x8, x14, x6
i_835:
	lhu x5, -240(x2)
i_836:
	blt x28, x8, i_846
i_837:
	lw x16, -76(x2)
i_838:
	sh x14, -204(x2)
i_839:
	srai x9, x3, 1
i_840:
	lh x10, 434(x2)
i_841:
	sltu x14, x14, x28
i_842:
	lb x28, 175(x2)
i_843:
	lbu x10, 422(x2)
i_844:
	sb x7, 452(x2)
i_845:
	sw x30, -392(x2)
i_846:
	auipc x29, 486506
i_847:
	andi x10, x10, -1026
i_848:
	lhu x30, -138(x2)
i_849:
	or x29, x24, x9
i_850:
	and x13, x31, x7
i_851:
	add x21, x27, x29
i_852:
	add x16, x18, x3
i_853:
	sb x4, 239(x2)
i_854:
	ori x30, x26, 1861
i_855:
	addi x17, x0, 1
i_856:
	sll x23, x14, x17
i_857:
	addi x4, x0, 1958
i_858:
	addi x14, x0, 1961
i_859:
	lh x17, 258(x2)
i_860:
	slti x10, x29, 1075
i_861:
	sub x17, x24, x14
i_862:
	bltu x31, x29, i_863
i_863:
	lw x24, -396(x2)
i_864:
	lb x29, 404(x2)
i_865:
	sh x22, -48(x2)
i_866:
	sh x10, -246(x2)
i_867:
	sw x24, -452(x2)
i_868:
	bne x14, x29, i_872
i_869:
	addi x4 , x4 , 1
	bge x14, x4, i_858
i_870:
	sb x17, -412(x2)
i_871:
	sh x3, -282(x2)
i_872:
	lbu x17, 214(x2)
i_873:
	add x27, x14, x10
i_874:
	sh x22, -342(x2)
i_875:
	sh x17, -312(x2)
i_876:
	addi x10, x0, -1898
i_877:
	addi x25, x0, -1896
i_878:
	add x16, x10, x25
i_879:
	lb x22, -448(x2)
i_880:
	beq x20, x17, i_890
i_881:
	slli x30, x14, 3
i_882:
	sw x9, 388(x2)
i_883:
	lhu x9, -164(x2)
i_884:
	and x30, x30, x7
i_885:
	sb x22, 171(x2)
i_886:
	addi x10 , x10 , 1
	bne x10, x25, i_878
i_887:
	sltiu x9, x25, -1741
i_888:
	sb x13, 352(x2)
i_889:
	srli x13, x31, 4
i_890:
	sb x1, -95(x2)
i_891:
	lui x13, 602647
i_892:
	addi x30, x0, 1961
i_893:
	addi x22, x0, 1964
i_894:
	sb x27, -368(x2)
i_895:
	addi x19, x4, -860
i_896:
	sw x19, -248(x2)
i_897:
	sb x17, 300(x2)
i_898:
	lui x15, 522077
i_899:
	sw x2, -380(x2)
i_900:
	lhu x1, 12(x2)
i_901:
	addi x30 , x30 , 1
	bne x30, x22, i_894
i_902:
	blt x17, x17, i_908
i_903:
	slti x6, x19, -635
i_904:
	slti x19, x15, -1977
i_905:
	blt x8, x5, i_909
i_906:
	lbu x13, 147(x2)
i_907:
	srai x26, x30, 1
i_908:
	lbu x5, -295(x2)
i_909:
	ori x5, x13, 1705
i_910:
	nop
i_911:
	addi x8, x0, -1933
i_912:
	addi x21, x0, -1930
i_913:
	bltu x8, x17, i_919
i_914:
	addi x8 , x8 , 1
	bge x21, x8, i_913
i_915:
	sb x2, -399(x2)
i_916:
	bgeu x21, x5, i_919
i_917:
	sw x25, 332(x2)
i_918:
	sb x5, 56(x2)
i_919:
	sw x25, 388(x2)
i_920:
	lb x29, -283(x2)
i_921:
	lw x29, 196(x2)
i_922:
	lh x8, 6(x2)
i_923:
	sh x3, -84(x2)
i_924:
	add x20, x4, x18
i_925:
	lh x18, -262(x2)
i_926:
	auipc x8, 555779
i_927:
	sltiu x12, x8, 146
i_928:
	lw x16, -8(x2)
i_929:
	or x17, x29, x13
i_930:
	sh x31, 70(x2)
i_931:
	addi x23, x0, 31
i_932:
	srl x8, x29, x23
i_933:
	beq x12, x11, i_939
i_934:
	and x14, x9, x25
i_935:
	lb x25, 320(x2)
i_936:
	addi x14, x14, -655
i_937:
	bge x14, x5, i_945
i_938:
	sh x25, -74(x2)
i_939:
	bgeu x27, x15, i_946
i_940:
	lhu x14, 208(x2)
i_941:
	add x1, x13, x20
i_942:
	lw x22, 16(x2)
i_943:
	sw x24, -112(x2)
i_944:
	nop
i_945:
	addi x4, x15, -74
i_946:
	lb x9, 426(x2)
i_947:
	auipc x24, 267953
i_948:
	addi x15, x0, -1903
i_949:
	addi x16, x0, -1900
i_950:
	xori x19, x4, -1276
i_951:
	lh x9, -410(x2)
i_952:
	sltiu x23, x1, 1977
i_953:
	addi x15 , x15 , 1
	blt x15, x16, i_950
i_954:
	sh x5, 330(x2)
i_955:
	or x5, x8, x10
i_956:
	srli x15, x22, 3
i_957:
	bge x23, x22, i_958
i_958:
	addi x16, x0, 20
i_959:
	srl x30, x10, x16
i_960:
	bge x10, x29, i_967
i_961:
	srai x29, x13, 3
i_962:
	bltu x11, x24, i_964
i_963:
	sh x5, -24(x2)
i_964:
	lb x29, 280(x2)
i_965:
	lbu x27, 246(x2)
i_966:
	lb x19, 414(x2)
i_967:
	slt x5, x21, x30
i_968:
	sw x9, 32(x2)
i_969:
	lb x29, -459(x2)
i_970:
	sltiu x1, x4, -1577
i_971:
	sb x17, -483(x2)
i_972:
	beq x5, x31, i_975
i_973:
	sw x9, -412(x2)
i_974:
	bgeu x19, x5, i_982
i_975:
	sh x16, 474(x2)
i_976:
	lw x12, -308(x2)
i_977:
	blt x1, x31, i_985
i_978:
	sw x27, -476(x2)
i_979:
	slti x9, x29, 342
i_980:
	sw x7, 388(x2)
i_981:
	add x26, x11, x10
i_982:
	sw x27, 308(x2)
i_983:
	sh x4, -158(x2)
i_984:
	lbu x4, 34(x2)
i_985:
	addi x12, x4, -744
i_986:
	auipc x8, 85238
i_987:
	xor x4, x29, x29
i_988:
	lb x18, 135(x2)
i_989:
	lh x1, 46(x2)
i_990:
	xor x1, x7, x12
i_991:
	lb x10, -389(x2)
i_992:
	andi x20, x1, 301
i_993:
	and x16, x14, x12
i_994:
	sub x1, x29, x10
i_995:
	bge x20, x20, i_1004
i_996:
	sb x1, 18(x2)
i_997:
	bltu x27, x29, i_1004
i_998:
	lbu x18, 25(x2)
i_999:
	lh x5, -80(x2)
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0x99805539
	.word 0x5ffc8689
	.word 0xf2a06a5c
	.word 0x7c786e47
	.word 0xf8ac349
	.word 0xdb74abce
	.word 0x754700e2
	.word 0x25083686
	.word 0x8ec581a
	.word 0xe0e30bb
	.word 0xac3e4da9
	.word 0x970ea11d
	.word 0x603c5571
	.word 0x80391d39
	.word 0x12d93398
	.word 0x2c0afcfc
	.word 0x8f0fda7e
	.word 0x3b858bd5
	.word 0x2eee4ac8
	.word 0xfcb1c671
	.word 0xf3ad5144
	.word 0xbe5931d2
	.word 0x4384585b
	.word 0xf04f9ac0
	.word 0xee7cb77e
	.word 0xa04831fc
	.word 0x8d42440a
	.word 0x97e2a9ee
	.word 0x6f4ad5de
	.word 0xfcac8078
	.word 0xaa621ff6
	.word 0x942af7c1
	.word 0xc77c3559
	.word 0x98ea24a3
	.word 0xbc918952
	.word 0x25efeecc
	.word 0xac97b636
	.word 0xdb82ed37
	.word 0x375cc0ca
	.word 0x99ed51ba
	.word 0x5e17ee76
	.word 0x778d0fe9
	.word 0x9e69a8eb
	.word 0xacea26d9
	.word 0x26bd379c
	.word 0x41a3b66e
	.word 0xa7c4ebaa
	.word 0xf72bd273
	.word 0x1e4b8ee0
	.word 0x103f9607
	.word 0xceb62b9a
	.word 0xe3b21422
	.word 0xa334aa0e
	.word 0x451a76a6
	.word 0x287a3aa
	.word 0x7e3b0621
	.word 0xa606a4bd
	.word 0xf9b3b42e
	.word 0xfbcfd04c
	.word 0xd0a3c074
	.word 0x2cad3938
	.word 0xd39e25f9
	.word 0xc4499505
	.word 0x9eefd227
	.word 0x6cc3005c
	.word 0x84f45016
	.word 0x3ada51f5
	.word 0xad7e7022
	.word 0xe5620b9d
	.word 0x3192edb5
	.word 0x2deba97c
	.word 0x9fb30078
	.word 0xc124eaca
	.word 0xed84f9cb
	.word 0x1d55886
	.word 0xdc841c07
	.word 0xb29dee2d
	.word 0x655016ef
	.word 0x18b5940c
	.word 0xd6cccf47
	.word 0xddec57ce
	.word 0xc24e65a5
	.word 0xc2bbed79
	.word 0x25994ff0
	.word 0x9e63e64f
	.word 0x93db9cd4
	.word 0x761dead5
	.word 0xa7351989
	.word 0xdde965bd
	.word 0x34943648
	.word 0xaa607963
	.word 0xdb306838
	.word 0x62141c52
	.word 0x2e3a660f
	.word 0x13a460f8
	.word 0x788d760a
	.word 0x2a6fecd3
	.word 0x5957d75b
	.word 0xe4cb593d
	.word 0x4d21566f
	.word 0xe80ff1a
	.word 0x1c02cf8e
	.word 0x161b1d14
	.word 0x936aa99a
	.word 0x99852490
	.word 0xcbdbd541
	.word 0xf9dab8b9
	.word 0x616d325a
	.word 0x625b0739
	.word 0xc3130d36
	.word 0x210af928
	.word 0xf7b0e339
	.word 0xbd97b1f0
	.word 0xf5c4d654
	.word 0x7ea9760f
	.word 0x28971a82
	.word 0xc4bbcc40
	.word 0x88e38eed
	.word 0xe356a45c
	.word 0x8b2f19
	.word 0x8761774b
	.word 0x8e7a5dd
	.word 0xb3fd2a85
	.word 0x357fd014
	.word 0xf9b25f52
	.word 0xaf1bed94
	.word 0x9662a7ab
	.word 0x9c1580d4
	.word 0x7b69a0c1
	.word 0x772e254f
	.word 0xc7250715
	.word 0xc163965
	.word 0x68b55054
	.word 0xa3fdeba
	.word 0x3216f804
	.word 0xeeac9f68
	.word 0xf4c177ea
	.word 0xa39602f
	.word 0xa6de86a9
	.word 0x4ee27b6a
	.word 0x60942dd8
	.word 0x4f886e8c
	.word 0xf16607a4
	.word 0xd1a9f4c6
	.word 0x80913431
	.word 0x9a381f7b
	.word 0x4f69e640
	.word 0xfbd36dcb
	.word 0x35946181
	.word 0x95c6919c
	.word 0xf3862081
	.word 0xd00c17db
	.word 0x6ff93eb5
	.word 0x13f68a11
	.word 0x7f24f78b
	.word 0x39c579de
	.word 0x174e6bd5
	.word 0x469b7b86
	.word 0xffa383c2
	.word 0xc6e65140
	.word 0x8381aa34
	.word 0x12b25547
	.word 0x88cc6c71
	.word 0x136e3071
	.word 0x9170cfe
	.word 0xbd393ff4
	.word 0xa1ff98c5
	.word 0x6793ea7c
	.word 0x584a0ae6
	.word 0x7ec1f86c
	.word 0x85474fa5
	.word 0x7609f0be
	.word 0xd23af182
	.word 0x8193cdd2
	.word 0x9ddde29e
	.word 0x96dc1e62
	.word 0x7a9f997a
	.word 0xaa3bfd21
	.word 0x1261a6f9
	.word 0xad5de84d
	.word 0xb42d3642
	.word 0x2c22eb3a
	.word 0xef596ce6
	.word 0x452ce18b
	.word 0x6cc09c5f
	.word 0x1197af62
	.word 0x1c10374e
	.word 0xf8516cd7
	.word 0x6212b57d
	.word 0x5b5b9265
	.word 0x2bbc3b55
	.word 0x940d04a3
	.word 0x267b812
	.word 0xc388f2b8
	.word 0x43612892
	.word 0xd7ad532a
	.word 0x483da1b7
	.word 0x251c5589
	.word 0x9a798d72
	.word 0x168ca39b
	.word 0x363a68d4
	.word 0xd4902d87
	.word 0x76f23bcc
	.word 0x1bbd15c0
	.word 0xf00f49cd
	.word 0xbad2ffa4
	.word 0x84c4d197
	.word 0xb6e06061
	.word 0x6cf57a47
	.word 0xd42a0bf6
	.word 0xd0eedd8f
	.word 0x9630e7d5
	.word 0x5b6c14bd
	.word 0x31ef1e32
	.word 0x2ab82965
	.word 0x83d31c44
	.word 0x476ae38a
	.word 0x58300b4f
	.word 0x1042dab8
	.word 0x1f84f001
	.word 0x3acb81bd
	.word 0x30b59357
	.word 0x9ae5623a
	.word 0xc95b7aa4
	.word 0x85aabc25
	.word 0x8daadf58
	.word 0x7c75a58c
	.word 0xc65217fb
	.word 0x93ecb961
	.word 0x96464ace
	.word 0xbac4c6a5
	.word 0x7b75c0ed
	.word 0x629aabd8
	.word 0x5ce4e906
	.word 0xe522e9d5
	.word 0x7d27a98f
	.word 0x105c6774
	.word 0xe5bf5af1
	.word 0x59cc6c8c
	.word 0x1682f3cc
	.word 0xea470399
	.word 0xd41f856a
	.word 0x4c2b2ec5
	.word 0xd5cea41
	.word 0x5b3cbdb8
	.word 0xefed94d9
	.word 0x3f0efc9d
	.word 0x545365ef
	.word 0xa1eb28c8
	.word 0x33b9cfb8
	.word 0x3e4f47d4
	.word 0xbf2481df
	.word 0xe61d382e
	.word 0xfe6e6a0f
	.word 0x1fcba384
	.word 0xc689d845
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
