
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	lbu x22, 112(x2)
i_1:
	sh x6, -42(x2)
i_2:
	lh x22, 112(x2)
i_3:
	lh x18, 488(x2)
i_4:
	lbu x18, -131(x2)
i_5:
	slti x18, x18, -1800
i_6:
	lbu x9, -111(x2)
i_7:
	lw x10, 184(x2)
i_8:
	lw x8, -428(x2)
i_9:
	sw x22, 480(x2)
i_10:
	or x22, x10, x15
i_11:
	sh x3, -218(x2)
i_12:
	lw x22, -272(x2)
i_13:
	andi x26, x30, 493
i_14:
	lbu x6, 334(x2)
i_15:
	sh x22, -338(x2)
i_16:
	beq x30, x13, i_23
i_17:
	sb x21, 83(x2)
i_18:
	add x30, x23, x15
i_19:
	bgeu x18, x3, i_23
i_20:
	addi x8, x0, 6
i_21:
	srl x13, x4, x8
i_22:
	srai x28, x28, 2
i_23:
	auipc x28, 594736
i_24:
	and x28, x8, x14
i_25:
	lh x8, 350(x2)
i_26:
	addi x30, x0, 2007
i_27:
	addi x20, x0, 2010
i_28:
	lh x19, -76(x2)
i_29:
	add x14, x23, x28
i_30:
	addi x11, x0, -1979
i_31:
	addi x13, x0, -1975
i_32:
	sltu x6, x23, x24
i_33:
	bltu x21, x14, i_41
i_34:
	lw x31, -212(x2)
i_35:
	addi x11 , x11 , 1
	bltu x11, x13, i_32
i_36:
	lw x27, -116(x2)
i_37:
	sb x29, 361(x2)
i_38:
	addi x30 , x30 , 1
	blt x30, x20, i_28
i_39:
	or x7, x13, x12
i_40:
	lh x14, 480(x2)
i_41:
	srai x26, x13, 3
i_42:
	lb x14, -65(x2)
i_43:
	blt x18, x1, i_51
i_44:
	lhu x1, -338(x2)
i_45:
	sub x1, x26, x26
i_46:
	srli x1, x7, 2
i_47:
	bne x14, x13, i_49
i_48:
	and x1, x31, x2
i_49:
	bge x21, x30, i_57
i_50:
	lhu x4, -150(x2)
i_51:
	lh x17, 320(x2)
i_52:
	sw x4, 460(x2)
i_53:
	addi x9, x20, 311
i_54:
	sh x9, -70(x2)
i_55:
	lui x7, 1032722
i_56:
	lh x21, -76(x2)
i_57:
	lui x28, 523335
i_58:
	lhu x20, 14(x2)
i_59:
	addi x26, x0, -2022
i_60:
	addi x27, x0, -2020
i_61:
	and x16, x22, x7
i_62:
	addi x26 , x26 , 1
	blt x26, x27, i_61
i_63:
	sb x28, -395(x2)
i_64:
	lbu x26, 26(x2)
i_65:
	beq x20, x14, i_73
i_66:
	lw x14, -260(x2)
i_67:
	or x14, x2, x20
i_68:
	slt x9, x28, x5
i_69:
	slt x22, x16, x20
i_70:
	lbu x16, 107(x2)
i_71:
	add x18, x18, x18
i_72:
	sh x12, -332(x2)
i_73:
	lh x18, -232(x2)
i_74:
	lb x11, -289(x2)
i_75:
	addi x26, x0, 1887
i_76:
	addi x16, x0, 1890
i_77:
	slti x29, x22, -906
i_78:
	sb x6, 439(x2)
i_79:
	addi x26 , x26 , 1
	bgeu x16, x26, i_77
i_80:
	bne x19, x18, i_89
i_81:
	lbu x7, 220(x2)
i_82:
	sub x16, x16, x7
i_83:
	lh x29, -294(x2)
i_84:
	bge x13, x29, i_92
i_85:
	xori x18, x26, 905
i_86:
	sb x10, 330(x2)
i_87:
	xori x15, x19, -1338
i_88:
	bge x29, x31, i_90
i_89:
	slli x22, x7, 1
i_90:
	and x17, x25, x22
i_91:
	sw x27, 276(x2)
i_92:
	add x21, x18, x31
i_93:
	nop
i_94:
	addi x17, x0, 1942
i_95:
	addi x12, x0, 1946
i_96:
	add x15, x3, x18
i_97:
	lh x18, -118(x2)
i_98:
	sh x18, 432(x2)
i_99:
	add x19, x28, x28
i_100:
	ori x30, x8, -1722
i_101:
	lh x10, 204(x2)
i_102:
	andi x22, x26, 2032
i_103:
	lw x21, -172(x2)
i_104:
	addi x17 , x17 , 1
	bne x17, x12, i_96
i_105:
	bne x1, x10, i_112
i_106:
	lb x27, 24(x2)
i_107:
	slt x3, x3, x10
i_108:
	sltiu x10, x8, 63
i_109:
	ori x13, x10, -2046
i_110:
	addi x6, x0, 30
i_111:
	srl x27, x2, x6
i_112:
	blt x6, x19, i_122
i_113:
	sw x7, 136(x2)
i_114:
	bgeu x9, x5, i_121
i_115:
	add x27, x8, x27
i_116:
	sw x10, -20(x2)
i_117:
	auipc x10, 789525
i_118:
	lh x26, 270(x2)
i_119:
	add x13, x26, x31
i_120:
	bltu x20, x18, i_122
i_121:
	lhu x10, -314(x2)
i_122:
	addi x4, x0, 15
i_123:
	sra x13, x4, x4
i_124:
	addi x13, x0, 11
i_125:
	srl x4, x19, x13
i_126:
	beq x7, x26, i_133
i_127:
	slti x7, x28, -1198
i_128:
	nop
i_129:
	lbu x28, 79(x2)
i_130:
	nop
i_131:
	lh x7, 290(x2)
i_132:
	addi x25, x25, 853
i_133:
	lh x7, 182(x2)
i_134:
	andi x10, x23, -1206
i_135:
	addi x4, x0, 2030
i_136:
	addi x19, x0, 2032
i_137:
	sw x7, -356(x2)
i_138:
	addi x20, x0, 20
i_139:
	srl x3, x15, x20
i_140:
	sh x7, 24(x2)
i_141:
	lh x18, 48(x2)
i_142:
	addi x4 , x4 , 1
	bgeu x19, x4, i_137
i_143:
	lb x28, -315(x2)
i_144:
	sw x9, 184(x2)
i_145:
	add x28, x24, x3
i_146:
	addi x27, x0, 28
i_147:
	sll x18, x27, x27
i_148:
	sb x5, 226(x2)
i_149:
	addi x4, x0, 13
i_150:
	srl x4, x8, x4
i_151:
	addi x25, x0, 24
i_152:
	sll x8, x8, x25
i_153:
	lw x30, -220(x2)
i_154:
	or x25, x31, x25
i_155:
	bge x13, x11, i_163
i_156:
	add x14, x25, x20
i_157:
	lb x8, -158(x2)
i_158:
	sltiu x21, x6, 956
i_159:
	bne x21, x26, i_165
i_160:
	lh x27, -108(x2)
i_161:
	beq x2, x27, i_170
i_162:
	slli x26, x24, 4
i_163:
	sb x7, -284(x2)
i_164:
	lbu x4, -293(x2)
i_165:
	addi x1, x27, -29
i_166:
	addi x11, x0, 18
i_167:
	sll x10, x27, x11
i_168:
	slt x18, x21, x18
i_169:
	bgeu x8, x10, i_173
i_170:
	lui x8, 613057
i_171:
	or x29, x25, x3
i_172:
	bgeu x19, x26, i_174
i_173:
	lui x13, 885804
i_174:
	bltu x21, x27, i_175
i_175:
	bgeu x11, x26, i_181
i_176:
	lbu x1, -458(x2)
i_177:
	lhu x8, 362(x2)
i_178:
	lw x11, -456(x2)
i_179:
	addi x29, x0, 27
i_180:
	sll x22, x3, x29
i_181:
	blt x6, x23, i_185
i_182:
	bge x20, x1, i_189
i_183:
	addi x8, x0, 11
i_184:
	sra x26, x22, x8
i_185:
	addi x13, x0, 8
i_186:
	srl x15, x13, x13
i_187:
	lb x4, 343(x2)
i_188:
	slt x13, x17, x4
i_189:
	lh x31, 132(x2)
i_190:
	lb x1, -316(x2)
i_191:
	sw x1, -28(x2)
i_192:
	sw x2, -456(x2)
i_193:
	andi x11, x11, -1815
i_194:
	lw x31, 28(x2)
i_195:
	sb x20, -337(x2)
i_196:
	lbu x21, 360(x2)
i_197:
	slli x11, x21, 2
i_198:
	lh x11, -340(x2)
i_199:
	sltu x11, x2, x1
i_200:
	beq x4, x26, i_201
i_201:
	sltu x1, x23, x25
i_202:
	lhu x20, 338(x2)
i_203:
	auipc x3, 405783
i_204:
	bgeu x15, x1, i_207
i_205:
	sw x10, 344(x2)
i_206:
	lh x3, 178(x2)
i_207:
	addi x8, x0, 31
i_208:
	srl x1, x4, x8
i_209:
	sh x6, -442(x2)
i_210:
	sltiu x6, x1, 1516
i_211:
	bne x24, x25, i_215
i_212:
	lh x15, -440(x2)
i_213:
	sw x7, 52(x2)
i_214:
	sh x8, 92(x2)
i_215:
	lhu x27, 206(x2)
i_216:
	slti x27, x22, 1915
i_217:
	sw x13, 280(x2)
i_218:
	lh x13, 446(x2)
i_219:
	slt x15, x15, x15
i_220:
	bge x10, x16, i_222
i_221:
	bgeu x31, x15, i_223
i_222:
	sh x31, 240(x2)
i_223:
	lb x15, 412(x2)
i_224:
	xori x19, x26, 1877
i_225:
	slli x15, x19, 2
i_226:
	sb x13, -357(x2)
i_227:
	lb x21, -410(x2)
i_228:
	srli x31, x2, 1
i_229:
	sh x26, -386(x2)
i_230:
	lhu x27, 462(x2)
i_231:
	lb x21, -318(x2)
i_232:
	bge x16, x19, i_238
i_233:
	sub x7, x28, x22
i_234:
	bge x14, x14, i_240
i_235:
	sw x11, 212(x2)
i_236:
	xor x8, x15, x15
i_237:
	xori x30, x9, -1372
i_238:
	blt x14, x27, i_247
i_239:
	slli x24, x26, 1
i_240:
	lb x26, -439(x2)
i_241:
	lh x26, -436(x2)
i_242:
	xori x17, x19, -1343
i_243:
	sb x17, 449(x2)
i_244:
	lhu x30, -282(x2)
i_245:
	xor x10, x6, x3
i_246:
	blt x30, x2, i_253
i_247:
	sw x10, 88(x2)
i_248:
	slt x17, x17, x2
i_249:
	lw x16, -208(x2)
i_250:
	lh x17, -250(x2)
i_251:
	nop
i_252:
	lbu x18, 427(x2)
i_253:
	slti x5, x17, 1953
i_254:
	sltu x21, x17, x12
i_255:
	addi x7, x0, 1973
i_256:
	addi x27, x0, 1976
i_257:
	sh x17, 346(x2)
i_258:
	bge x17, x16, i_266
i_259:
	addi x14, x9, -7
i_260:
	lh x23, -252(x2)
i_261:
	bge x29, x11, i_265
i_262:
	addi x7 , x7 , 1
	bne x7, x27, i_257
i_263:
	lh x16, 184(x2)
i_264:
	lh x22, 328(x2)
i_265:
	bltu x21, x22, i_267
i_266:
	andi x17, x16, 26
i_267:
	lbu x8, 34(x2)
i_268:
	srli x16, x30, 4
i_269:
	bgeu x4, x4, i_277
i_270:
	lbu x6, 402(x2)
i_271:
	lhu x28, 262(x2)
i_272:
	lbu x24, -333(x2)
i_273:
	bge x6, x1, i_281
i_274:
	lb x30, -64(x2)
i_275:
	lhu x6, -48(x2)
i_276:
	bgeu x24, x27, i_281
i_277:
	srai x12, x6, 3
i_278:
	blt x13, x30, i_279
i_279:
	addi x6, x12, -1465
i_280:
	xor x26, x8, x14
i_281:
	srli x14, x14, 4
i_282:
	andi x14, x2, 1330
i_283:
	slt x7, x11, x26
i_284:
	lb x31, -48(x2)
i_285:
	addi x9, x0, -1965
i_286:
	addi x26, x0, -1962
i_287:
	sb x19, 97(x2)
i_288:
	lw x11, 52(x2)
i_289:
	addi x9 , x9 , 1
	bltu x9, x26, i_287
i_290:
	lw x16, -172(x2)
i_291:
	ori x13, x11, 156
i_292:
	lb x5, 266(x2)
i_293:
	sw x8, -408(x2)
i_294:
	lbu x11, 375(x2)
i_295:
	sb x16, 69(x2)
i_296:
	lbu x4, 382(x2)
i_297:
	lh x13, 308(x2)
i_298:
	beq x19, x10, i_306
i_299:
	slli x24, x23, 2
i_300:
	sb x13, -173(x2)
i_301:
	beq x16, x3, i_305
i_302:
	lbu x11, 202(x2)
i_303:
	lw x22, -208(x2)
i_304:
	srli x26, x4, 1
i_305:
	lh x31, 122(x2)
i_306:
	bgeu x7, x12, i_309
i_307:
	lui x20, 404578
i_308:
	sh x24, -140(x2)
i_309:
	and x11, x24, x29
i_310:
	sb x31, -469(x2)
i_311:
	andi x27, x10, -1249
i_312:
	lw x22, 4(x2)
i_313:
	bne x3, x11, i_315
i_314:
	sw x24, -248(x2)
i_315:
	sh x2, 218(x2)
i_316:
	addi x20, x0, 14
i_317:
	sll x27, x7, x20
i_318:
	lhu x22, 254(x2)
i_319:
	add x21, x22, x5
i_320:
	srai x16, x2, 1
i_321:
	lh x13, 94(x2)
i_322:
	sw x7, 296(x2)
i_323:
	lui x24, 343413
i_324:
	sltu x10, x3, x21
i_325:
	andi x9, x22, 570
i_326:
	slt x3, x24, x11
i_327:
	sw x7, 180(x2)
i_328:
	addi x14, x0, 4
i_329:
	sll x16, x25, x14
i_330:
	sltu x11, x9, x14
i_331:
	ori x22, x28, 1670
i_332:
	sh x14, -36(x2)
i_333:
	sh x16, 172(x2)
i_334:
	addi x4, x0, 21
i_335:
	srl x11, x16, x4
i_336:
	xor x14, x7, x13
i_337:
	lhu x15, -258(x2)
i_338:
	sw x19, -92(x2)
i_339:
	lb x17, 144(x2)
i_340:
	bne x20, x14, i_341
i_341:
	lbu x16, -217(x2)
i_342:
	sltiu x14, x14, -557
i_343:
	addi x5, x0, 22
i_344:
	sll x31, x14, x5
i_345:
	addi x14, x0, 29
i_346:
	sra x31, x28, x14
i_347:
	sb x9, 301(x2)
i_348:
	addi x31, x0, 27
i_349:
	sra x15, x5, x31
i_350:
	sw x27, 232(x2)
i_351:
	bgeu x5, x5, i_355
i_352:
	lbu x7, 169(x2)
i_353:
	slli x8, x4, 3
i_354:
	sub x8, x1, x16
i_355:
	bltu x25, x8, i_360
i_356:
	lb x28, -13(x2)
i_357:
	sb x6, 454(x2)
i_358:
	lui x4, 787245
i_359:
	ori x28, x21, -367
i_360:
	sub x1, x24, x27
i_361:
	lw x8, 424(x2)
i_362:
	addi x25, x0, 1964
i_363:
	addi x27, x0, 1968
i_364:
	sw x23, 132(x2)
i_365:
	lh x24, -16(x2)
i_366:
	addi x25 , x25 , 1
	bge x27, x25, i_364
i_367:
	sub x1, x6, x31
i_368:
	lh x4, -280(x2)
i_369:
	lhu x31, 218(x2)
i_370:
	sw x8, 332(x2)
i_371:
	bge x27, x23, i_372
i_372:
	lbu x21, 488(x2)
i_373:
	xor x15, x31, x1
i_374:
	lh x31, -88(x2)
i_375:
	add x31, x8, x27
i_376:
	sw x3, -148(x2)
i_377:
	sh x14, 86(x2)
i_378:
	sh x25, -460(x2)
i_379:
	addi x31, x0, 1852
i_380:
	addi x11, x0, 1854
i_381:
	addi x3, x14, -91
i_382:
	xor x8, x3, x11
i_383:
	or x13, x3, x8
i_384:
	sw x7, -36(x2)
i_385:
	xor x5, x7, x12
i_386:
	addi x31 , x31 , 1
	bne x31, x11, i_381
i_387:
	sh x28, -110(x2)
i_388:
	sh x13, 488(x2)
i_389:
	lh x13, 48(x2)
i_390:
	and x13, x5, x2
i_391:
	add x8, x31, x7
i_392:
	sh x2, -130(x2)
i_393:
	sw x29, 248(x2)
i_394:
	add x22, x24, x11
i_395:
	srli x23, x9, 2
i_396:
	sb x23, 138(x2)
i_397:
	nop
i_398:
	lbu x1, 119(x2)
i_399:
	addi x22, x0, 1838
i_400:
	addi x19, x0, 1842
i_401:
	addi x22 , x22 , 1
	bne x22, x19, i_401
i_402:
	lw x11, -216(x2)
i_403:
	sb x11, -304(x2)
i_404:
	lw x1, 420(x2)
i_405:
	slli x19, x26, 4
i_406:
	sb x22, 222(x2)
i_407:
	addi x8, x0, 6
i_408:
	sll x5, x15, x8
i_409:
	xori x19, x14, -313
i_410:
	lb x14, 229(x2)
i_411:
	sh x15, 396(x2)
i_412:
	slli x19, x12, 1
i_413:
	lhu x26, 428(x2)
i_414:
	bne x19, x19, i_422
i_415:
	sltiu x7, x13, -1306
i_416:
	addi x24, x0, 12
i_417:
	srl x16, x2, x24
i_418:
	lbu x19, 453(x2)
i_419:
	addi x7, x0, 28
i_420:
	sll x25, x24, x7
i_421:
	sltiu x7, x19, -803
i_422:
	ori x26, x1, -466
i_423:
	lhu x23, -216(x2)
i_424:
	sub x1, x27, x6
i_425:
	lhu x18, -34(x2)
i_426:
	sw x4, -476(x2)
i_427:
	lbu x7, -314(x2)
i_428:
	sltiu x29, x26, -1869
i_429:
	sb x10, -280(x2)
i_430:
	sh x7, -386(x2)
i_431:
	lhu x16, -116(x2)
i_432:
	lbu x15, -148(x2)
i_433:
	bge x11, x19, i_435
i_434:
	bgeu x13, x24, i_438
i_435:
	bne x7, x5, i_436
i_436:
	addi x19, x26, 414
i_437:
	addi x26, x0, 9
i_438:
	srl x13, x16, x26
i_439:
	lhu x13, 368(x2)
i_440:
	srai x4, x13, 2
i_441:
	sh x13, -376(x2)
i_442:
	bltu x6, x14, i_445
i_443:
	sub x11, x11, x11
i_444:
	slt x26, x28, x13
i_445:
	sltiu x29, x24, 1085
i_446:
	bltu x15, x22, i_448
i_447:
	lbu x17, -403(x2)
i_448:
	lw x24, 392(x2)
i_449:
	lh x7, -228(x2)
i_450:
	sh x6, -78(x2)
i_451:
	sw x7, 28(x2)
i_452:
	addi x17, x0, 1911
i_453:
	addi x7, x0, 1914
i_454:
	lh x29, -196(x2)
i_455:
	sltu x9, x7, x14
i_456:
	sb x23, -104(x2)
i_457:
	ori x11, x14, -1687
i_458:
	addi x17 , x17 , 1
	bltu x17, x7, i_454
i_459:
	lw x7, 204(x2)
i_460:
	sw x13, 308(x2)
i_461:
	lhu x10, 334(x2)
i_462:
	sh x16, -452(x2)
i_463:
	lb x9, -208(x2)
i_464:
	slli x29, x7, 4
i_465:
	slli x4, x7, 2
i_466:
	lb x22, 63(x2)
i_467:
	addi x7, x0, 11
i_468:
	sll x17, x6, x7
i_469:
	sh x23, -346(x2)
i_470:
	lh x12, -86(x2)
i_471:
	lh x12, -6(x2)
i_472:
	sub x7, x21, x21
i_473:
	lh x9, -188(x2)
i_474:
	xori x9, x22, 227
i_475:
	lb x23, -68(x2)
i_476:
	lbu x8, -310(x2)
i_477:
	bltu x3, x25, i_483
i_478:
	lhu x3, 374(x2)
i_479:
	lw x3, 264(x2)
i_480:
	bgeu x16, x11, i_485
i_481:
	sh x11, -454(x2)
i_482:
	lb x8, 11(x2)
i_483:
	sw x16, -244(x2)
i_484:
	lh x8, -474(x2)
i_485:
	sb x8, -12(x2)
i_486:
	sb x15, -482(x2)
i_487:
	xori x8, x18, 672
i_488:
	sub x31, x22, x15
i_489:
	and x31, x8, x15
i_490:
	sw x9, -300(x2)
i_491:
	xori x22, x31, 1962
i_492:
	ori x1, x10, 372
i_493:
	blt x19, x11, i_500
i_494:
	lh x9, -336(x2)
i_495:
	lw x11, 124(x2)
i_496:
	lh x19, -26(x2)
i_497:
	or x5, x24, x19
i_498:
	sw x14, -312(x2)
i_499:
	sltiu x7, x31, -405
i_500:
	sw x29, -400(x2)
i_501:
	sb x21, -239(x2)
i_502:
	addi x21, x0, 5
i_503:
	sll x6, x29, x21
i_504:
	lb x29, 355(x2)
i_505:
	xor x3, x6, x11
i_506:
	lhu x11, 248(x2)
i_507:
	lbu x18, 451(x2)
i_508:
	sw x17, 184(x2)
i_509:
	lw x27, 224(x2)
i_510:
	lb x3, 319(x2)
i_511:
	beq x16, x3, i_512
i_512:
	lui x16, 838205
i_513:
	lb x16, 201(x2)
i_514:
	beq x27, x19, i_518
i_515:
	lb x22, -330(x2)
i_516:
	sw x6, 68(x2)
i_517:
	sub x31, x17, x21
i_518:
	lb x6, -292(x2)
i_519:
	srai x31, x16, 4
i_520:
	beq x16, x20, i_524
i_521:
	lw x16, 336(x2)
i_522:
	bne x16, x4, i_528
i_523:
	addi x4, x0, 10
i_524:
	srl x11, x8, x4
i_525:
	sw x11, 224(x2)
i_526:
	sh x15, 230(x2)
i_527:
	xor x25, x26, x30
i_528:
	addi x22, x0, 6
i_529:
	srl x11, x25, x22
i_530:
	add x5, x28, x23
i_531:
	sltu x16, x5, x25
i_532:
	lh x30, 372(x2)
i_533:
	addi x18, x0, 20
i_534:
	sra x29, x11, x18
i_535:
	lw x23, -164(x2)
i_536:
	sltu x26, x26, x30
i_537:
	slli x5, x31, 4
i_538:
	lh x18, -260(x2)
i_539:
	lw x31, -448(x2)
i_540:
	sb x30, -269(x2)
i_541:
	bgeu x3, x13, i_549
i_542:
	lhu x8, 212(x2)
i_543:
	slt x11, x3, x5
i_544:
	sltu x27, x27, x17
i_545:
	addi x27, x0, 27
i_546:
	sll x3, x9, x27
i_547:
	addi x19, x11, 929
i_548:
	lh x17, 234(x2)
i_549:
	lb x30, 151(x2)
i_550:
	bltu x29, x27, i_557
i_551:
	bltu x17, x30, i_553
i_552:
	lhu x8, 74(x2)
i_553:
	nop
i_554:
	sb x12, 36(x2)
i_555:
	srli x20, x26, 2
i_556:
	srai x4, x23, 3
i_557:
	lbu x23, 427(x2)
i_558:
	lw x23, 168(x2)
i_559:
	addi x29, x0, -2000
i_560:
	addi x12, x0, -1996
i_561:
	lh x24, -236(x2)
i_562:
	bne x20, x4, i_563
i_563:
	lb x20, 348(x2)
i_564:
	lbu x20, 33(x2)
i_565:
	sb x28, 47(x2)
i_566:
	addi x29 , x29 , 1
	bge x12, x29, i_561
i_567:
	lb x6, -206(x2)
i_568:
	lhu x6, -368(x2)
i_569:
	lw x4, -372(x2)
i_570:
	lh x24, -118(x2)
i_571:
	beq x20, x20, i_576
i_572:
	add x19, x4, x23
i_573:
	nop
i_574:
	addi x12, x0, 26
i_575:
	srl x13, x27, x12
i_576:
	nop
i_577:
	nop
i_578:
	addi x27, x0, 2003
i_579:
	addi x20, x0, 2007
i_580:
	bge x7, x21, i_590
i_581:
	sb x6, -350(x2)
i_582:
	srai x17, x28, 4
i_583:
	blt x12, x17, i_593
i_584:
	nop
i_585:
	addi x17, x6, -1567
i_586:
	addi x27 , x27 , 1
	bne x27, x20, i_580
i_587:
	bge x31, x25, i_595
i_588:
	blt x21, x28, i_596
i_589:
	lbu x26, -316(x2)
i_590:
	lh x14, 264(x2)
i_591:
	bgeu x29, x16, i_597
i_592:
	bge x30, x6, i_600
i_593:
	beq x28, x5, i_603
i_594:
	xor x7, x23, x23
i_595:
	lbu x28, -363(x2)
i_596:
	bne x28, x28, i_600
i_597:
	lbu x23, -223(x2)
i_598:
	sh x8, -442(x2)
i_599:
	addi x28, x29, -1411
i_600:
	addi x11, x0, 17
i_601:
	sra x20, x11, x11
i_602:
	bge x16, x22, i_607
i_603:
	lbu x12, 199(x2)
i_604:
	add x6, x11, x25
i_605:
	add x20, x29, x12
i_606:
	bge x28, x26, i_608
i_607:
	lbu x12, -318(x2)
i_608:
	auipc x12, 1020190
i_609:
	addi x19, x0, 21
i_610:
	sll x19, x10, x19
i_611:
	addi x6, x2, -544
i_612:
	sb x10, 3(x2)
i_613:
	lhu x19, -136(x2)
i_614:
	addi x22, x0, 3
i_615:
	sll x10, x19, x22
i_616:
	and x6, x11, x29
i_617:
	lhu x31, 140(x2)
i_618:
	andi x11, x6, 1973
i_619:
	lb x29, -105(x2)
i_620:
	addi x9, x0, 12
i_621:
	sll x31, x20, x9
i_622:
	lh x29, 176(x2)
i_623:
	lb x25, 335(x2)
i_624:
	beq x29, x29, i_631
i_625:
	lhu x22, -196(x2)
i_626:
	sh x15, -246(x2)
i_627:
	bne x25, x9, i_628
i_628:
	slt x29, x20, x3
i_629:
	sh x29, 36(x2)
i_630:
	lh x4, -70(x2)
i_631:
	lhu x21, -274(x2)
i_632:
	sb x26, -207(x2)
i_633:
	sub x1, x6, x24
i_634:
	auipc x6, 806475
i_635:
	lh x26, 140(x2)
i_636:
	lbu x7, 177(x2)
i_637:
	addi x24, x0, 1982
i_638:
	addi x13, x0, 1985
i_639:
	addi x17, x0, 11
i_640:
	sra x30, x7, x17
i_641:
	ori x29, x29, -1910
i_642:
	sh x29, 66(x2)
i_643:
	bne x16, x13, i_644
i_644:
	andi x3, x4, 800
i_645:
	bgeu x28, x2, i_652
i_646:
	sb x5, 11(x2)
i_647:
	addi x24 , x24 , 1
	bltu x24, x13, i_639
i_648:
	sw x17, 128(x2)
i_649:
	addi x20, x0, 3
i_650:
	sra x19, x11, x20
i_651:
	lh x30, -388(x2)
i_652:
	bge x18, x24, i_655
i_653:
	addi x13, x0, 11
i_654:
	sll x18, x13, x13
i_655:
	lh x13, 468(x2)
i_656:
	sh x18, -2(x2)
i_657:
	sw x20, -216(x2)
i_658:
	lbu x8, -347(x2)
i_659:
	lh x1, -236(x2)
i_660:
	lh x20, -342(x2)
i_661:
	sw x3, 52(x2)
i_662:
	addi x24, x0, 19
i_663:
	sll x24, x30, x24
i_664:
	lw x26, -240(x2)
i_665:
	lb x13, -467(x2)
i_666:
	andi x17, x24, -685
i_667:
	addi x26, x0, 1
i_668:
	sra x13, x30, x26
i_669:
	blt x26, x6, i_674
i_670:
	lhu x25, -34(x2)
i_671:
	sh x30, 80(x2)
i_672:
	lb x26, -198(x2)
i_673:
	andi x15, x1, -1984
i_674:
	srli x8, x25, 4
i_675:
	xor x10, x10, x24
i_676:
	addi x29, x0, 2007
i_677:
	addi x22, x0, 2010
i_678:
	sb x6, -262(x2)
i_679:
	addi x29 , x29 , 1
	bge x22, x29, i_678
i_680:
	lw x22, -472(x2)
i_681:
	lbu x1, 435(x2)
i_682:
	sw x26, 344(x2)
i_683:
	sub x12, x8, x29
i_684:
	sh x16, 36(x2)
i_685:
	lbu x22, 156(x2)
i_686:
	slti x9, x2, 14
i_687:
	srli x16, x11, 1
i_688:
	sw x22, -256(x2)
i_689:
	srai x25, x24, 2
i_690:
	sh x5, -332(x2)
i_691:
	lbu x20, -324(x2)
i_692:
	sb x22, -447(x2)
i_693:
	lb x22, 125(x2)
i_694:
	sh x16, 466(x2)
i_695:
	lw x29, 220(x2)
i_696:
	lw x25, -156(x2)
i_697:
	lw x20, 4(x2)
i_698:
	lw x21, -108(x2)
i_699:
	lh x1, -70(x2)
i_700:
	addi x20, x0, 26
i_701:
	sll x7, x13, x20
i_702:
	bge x22, x19, i_707
i_703:
	bge x4, x21, i_707
i_704:
	sw x21, -148(x2)
i_705:
	lb x21, 7(x2)
i_706:
	lb x7, 369(x2)
i_707:
	add x26, x21, x14
i_708:
	bgeu x6, x26, i_718
i_709:
	sb x11, -118(x2)
i_710:
	sb x21, -450(x2)
i_711:
	addi x20, x0, 29
i_712:
	srl x21, x5, x20
i_713:
	sw x9, 280(x2)
i_714:
	beq x24, x20, i_719
i_715:
	lh x21, 384(x2)
i_716:
	addi x9, x21, 629
i_717:
	sb x19, -119(x2)
i_718:
	slli x21, x21, 4
i_719:
	lw x9, -200(x2)
i_720:
	add x14, x21, x2
i_721:
	addi x22, x0, 15
i_722:
	sra x3, x7, x22
i_723:
	sw x9, 144(x2)
i_724:
	add x18, x23, x13
i_725:
	lbu x7, 126(x2)
i_726:
	lhu x18, -52(x2)
i_727:
	bge x19, x3, i_735
i_728:
	xor x17, x19, x25
i_729:
	add x19, x12, x5
i_730:
	lb x5, -41(x2)
i_731:
	and x24, x5, x7
i_732:
	sw x3, -452(x2)
i_733:
	lb x5, 30(x2)
i_734:
	lbu x26, 455(x2)
i_735:
	slli x5, x14, 1
i_736:
	lbu x14, -485(x2)
i_737:
	addi x17, x0, 1913
i_738:
	addi x28, x0, 1915
i_739:
	nop
i_740:
	lw x21, 236(x2)
i_741:
	add x14, x2, x3
i_742:
	addi x3, x0, 16
i_743:
	sra x10, x5, x3
i_744:
	addi x17 , x17 , 1
	bne x17, x28, i_739
i_745:
	and x3, x16, x10
i_746:
	sh x11, -256(x2)
i_747:
	lw x23, 188(x2)
i_748:
	lhu x9, -232(x2)
i_749:
	lbu x1, -427(x2)
i_750:
	sw x12, -252(x2)
i_751:
	lui x1, 486708
i_752:
	bgeu x1, x27, i_753
i_753:
	bgeu x29, x3, i_760
i_754:
	srai x1, x14, 2
i_755:
	andi x13, x13, -693
i_756:
	sltiu x14, x17, 657
i_757:
	beq x23, x7, i_759
i_758:
	lbu x17, -417(x2)
i_759:
	sw x12, -308(x2)
i_760:
	sh x23, -286(x2)
i_761:
	blt x30, x11, i_769
i_762:
	and x12, x7, x5
i_763:
	blt x22, x19, i_765
i_764:
	sb x15, 150(x2)
i_765:
	slli x6, x28, 3
i_766:
	sw x13, 240(x2)
i_767:
	addi x16, x0, 24
i_768:
	srl x1, x17, x16
i_769:
	and x17, x14, x27
i_770:
	lh x20, 374(x2)
i_771:
	sltu x8, x16, x8
i_772:
	lh x30, 168(x2)
i_773:
	lbu x30, -459(x2)
i_774:
	sw x8, -464(x2)
i_775:
	bge x6, x29, i_780
i_776:
	or x29, x6, x9
i_777:
	and x30, x20, x6
i_778:
	lb x6, -173(x2)
i_779:
	lb x29, 372(x2)
i_780:
	lb x22, 112(x2)
i_781:
	srai x4, x14, 2
i_782:
	slt x13, x19, x31
i_783:
	lhu x24, 52(x2)
i_784:
	bgeu x6, x22, i_793
i_785:
	lh x22, -106(x2)
i_786:
	slti x4, x4, 1144
i_787:
	and x25, x27, x1
i_788:
	sw x31, 256(x2)
i_789:
	lb x28, 277(x2)
i_790:
	bltu x24, x18, i_793
i_791:
	addi x4, x0, 2
i_792:
	srl x12, x21, x4
i_793:
	sltiu x25, x4, -931
i_794:
	sh x11, -60(x2)
i_795:
	bne x11, x27, i_797
i_796:
	lh x25, 122(x2)
i_797:
	lw x12, -184(x2)
i_798:
	lb x20, 381(x2)
i_799:
	lw x7, -56(x2)
i_800:
	lh x12, 398(x2)
i_801:
	sb x11, -19(x2)
i_802:
	lbu x20, -111(x2)
i_803:
	sw x9, -268(x2)
i_804:
	sw x7, 280(x2)
i_805:
	sh x14, -38(x2)
i_806:
	ori x14, x14, 2014
i_807:
	sw x26, 12(x2)
i_808:
	lw x12, 260(x2)
i_809:
	add x10, x9, x12
i_810:
	lh x16, -348(x2)
i_811:
	lhu x10, 200(x2)
i_812:
	lh x3, -96(x2)
i_813:
	addi x16, x0, 31
i_814:
	sll x16, x7, x16
i_815:
	lb x30, -20(x2)
i_816:
	lb x21, -439(x2)
i_817:
	lhu x11, -150(x2)
i_818:
	blt x30, x15, i_819
i_819:
	sw x12, 116(x2)
i_820:
	sb x30, -306(x2)
i_821:
	lb x20, 112(x2)
i_822:
	lh x26, -38(x2)
i_823:
	lh x30, -136(x2)
i_824:
	slt x10, x29, x24
i_825:
	nop
i_826:
	andi x19, x14, 455
i_827:
	addi x27, x0, 1865
i_828:
	addi x26, x0, 1868
i_829:
	sltu x19, x12, x27
i_830:
	sltiu x14, x24, -452
i_831:
	addi x25, x0, 2030
i_832:
	addi x24, x0, 2033
i_833:
	sb x24, -269(x2)
i_834:
	addi x25 , x25 , 1
	bne  x24, x25, i_833
i_835:
	xori x31, x25, 261
i_836:
	sh x1, -400(x2)
i_837:
	addi x27 , x27 , 1
	blt x27, x26, i_829
i_838:
	slti x19, x28, -1544
i_839:
	sw x29, 144(x2)
i_840:
	lw x27, -344(x2)
i_841:
	lhu x14, -328(x2)
i_842:
	lb x7, 262(x2)
i_843:
	lb x10, 52(x2)
i_844:
	lb x31, -91(x2)
i_845:
	sw x27, 224(x2)
i_846:
	sltiu x16, x31, -280
i_847:
	sub x7, x3, x24
i_848:
	lhu x24, 432(x2)
i_849:
	addi x24, x18, 1064
i_850:
	blt x25, x31, i_859
i_851:
	lw x31, -196(x2)
i_852:
	sh x26, 426(x2)
i_853:
	addi x27, x0, 9
i_854:
	sll x31, x11, x27
i_855:
	lh x11, -478(x2)
i_856:
	sh x27, -120(x2)
i_857:
	lhu x31, 92(x2)
i_858:
	lw x13, -68(x2)
i_859:
	lhu x27, 266(x2)
i_860:
	lb x27, 95(x2)
i_861:
	beq x19, x27, i_870
i_862:
	lhu x28, -202(x2)
i_863:
	lw x27, 392(x2)
i_864:
	sw x27, 12(x2)
i_865:
	bge x16, x18, i_866
i_866:
	ori x31, x11, 1150
i_867:
	sh x8, 414(x2)
i_868:
	lhu x27, -242(x2)
i_869:
	lw x11, -4(x2)
i_870:
	lh x27, 442(x2)
i_871:
	addi x11, x19, 1147
i_872:
	slt x11, x10, x3
i_873:
	sb x6, -333(x2)
i_874:
	lui x9, 234442
i_875:
	addi x11, x0, 1982
i_876:
	addi x18, x0, 1986
i_877:
	addi x11 , x11 , 1
	bgeu x18, x11, i_877
i_878:
	auipc x29, 42859
i_879:
	bltu x29, x10, i_886
i_880:
	bltu x1, x17, i_890
i_881:
	xori x17, x19, -804
i_882:
	sb x9, 262(x2)
i_883:
	addi x29, x0, 6
i_884:
	sra x10, x9, x29
i_885:
	bne x21, x23, i_888
i_886:
	bltu x29, x16, i_895
i_887:
	lb x25, -21(x2)
i_888:
	sh x25, -10(x2)
i_889:
	lbu x27, 89(x2)
i_890:
	bltu x21, x9, i_899
i_891:
	lw x5, 272(x2)
i_892:
	beq x1, x18, i_898
i_893:
	sh x25, -8(x2)
i_894:
	beq x21, x17, i_898
i_895:
	lhu x4, 318(x2)
i_896:
	lb x3, 403(x2)
i_897:
	sb x10, -236(x2)
i_898:
	bge x17, x7, i_905
i_899:
	sb x15, 221(x2)
i_900:
	bltu x3, x18, i_904
i_901:
	lw x17, 324(x2)
i_902:
	bne x4, x4, i_904
i_903:
	lbu x19, 329(x2)
i_904:
	auipc x8, 358722
i_905:
	xori x23, x30, 1901
i_906:
	lbu x19, 350(x2)
i_907:
	blt x25, x26, i_917
i_908:
	sh x8, 392(x2)
i_909:
	sh x25, -26(x2)
i_910:
	slti x18, x9, 644
i_911:
	xori x19, x2, -1687
i_912:
	sltiu x25, x11, -1419
i_913:
	lhu x10, 468(x2)
i_914:
	lh x25, -110(x2)
i_915:
	addi x4, x23, 76
i_916:
	lbu x3, -43(x2)
i_917:
	auipc x29, 746604
i_918:
	sltu x21, x25, x30
i_919:
	slt x8, x29, x21
i_920:
	sb x2, -421(x2)
i_921:
	addi x4, x0, 8
i_922:
	sra x26, x27, x4
i_923:
	add x28, x21, x26
i_924:
	ori x25, x10, 573
i_925:
	xori x26, x26, 1542
i_926:
	lh x22, -54(x2)
i_927:
	lb x6, 342(x2)
i_928:
	lw x1, -472(x2)
i_929:
	lbu x28, 435(x2)
i_930:
	lh x27, 152(x2)
i_931:
	sub x12, x4, x18
i_932:
	sw x28, 364(x2)
i_933:
	sb x31, -75(x2)
i_934:
	sw x20, -276(x2)
i_935:
	sltiu x18, x17, 1521
i_936:
	or x1, x12, x27
i_937:
	lw x6, 108(x2)
i_938:
	sub x27, x6, x20
i_939:
	lw x31, -196(x2)
i_940:
	lw x3, -484(x2)
i_941:
	sb x25, -181(x2)
i_942:
	bge x1, x20, i_951
i_943:
	blt x16, x31, i_950
i_944:
	addi x16, x22, -778
i_945:
	sb x19, 475(x2)
i_946:
	lh x16, 54(x2)
i_947:
	lw x3, -152(x2)
i_948:
	srli x16, x20, 3
i_949:
	lh x14, -440(x2)
i_950:
	lb x9, -483(x2)
i_951:
	lbu x9, -339(x2)
i_952:
	ori x9, x1, 182
i_953:
	lw x27, 28(x2)
i_954:
	addi x13, x0, -1877
i_955:
	addi x23, x0, -1874
i_956:
	lh x1, -148(x2)
i_957:
	sw x10, 392(x2)
i_958:
	or x21, x26, x3
i_959:
	sh x29, 180(x2)
i_960:
	lh x24, 52(x2)
i_961:
	sltiu x31, x29, -1948
i_962:
	lw x16, 316(x2)
i_963:
	addi x13 , x13 , 1
	bne x13, x23, i_956
i_964:
	lw x29, 36(x2)
i_965:
	sltu x29, x20, x26
i_966:
	sb x22, -363(x2)
i_967:
	lhu x29, 486(x2)
i_968:
	lw x29, -192(x2)
i_969:
	lhu x30, -20(x2)
i_970:
	srli x20, x7, 1
i_971:
	lhu x16, 16(x2)
i_972:
	slt x7, x24, x3
i_973:
	addi x1, x0, 31
i_974:
	sll x30, x10, x1
i_975:
	lw x20, -484(x2)
i_976:
	bge x22, x12, i_985
i_977:
	or x18, x3, x17
i_978:
	lui x3, 684688
i_979:
	andi x6, x20, 2032
i_980:
	slli x18, x21, 1
i_981:
	or x20, x31, x14
i_982:
	add x5, x18, x1
i_983:
	lbu x18, -44(x2)
i_984:
	lw x3, 164(x2)
i_985:
	nop
i_986:
	sb x13, -91(x2)
i_987:
	addi x12, x0, -1930
i_988:
	addi x14, x0, -1927
i_989:
	addi x12 , x12 , 1
	bltu x12, x14, i_989
i_990:
	addi x12, x13, 1175
i_991:
	andi x7, x5, 118
i_992:
	sb x28, 3(x2)
i_993:
	lhu x21, -358(x2)
i_994:
	add x6, x7, x20
i_995:
	lbu x7, 472(x2)
i_996:
	lh x6, -50(x2)
i_997:
	ori x30, x7, 1948
i_998:
	addi x7, x0, 1993
i_999:
	addi x31, x0, 1995
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0xabba26f5
	.word 0x174413e6
	.word 0x6ab8e251
	.word 0xab6ec012
	.word 0xddc78dec
	.word 0x727ea6b5
	.word 0x8af65f31
	.word 0x82636100
	.word 0x77019312
	.word 0x651ce384
	.word 0x7f2bbc2e
	.word 0x8c4d7311
	.word 0xde8e2d6e
	.word 0xa0b86de
	.word 0xa31db6a8
	.word 0x117b4953
	.word 0x4e91fc90
	.word 0x4aebdbbe
	.word 0xd8a606c5
	.word 0x93e2c3af
	.word 0x537d146e
	.word 0xde8c3df6
	.word 0xea9d3527
	.word 0x3c1814d4
	.word 0x962c5096
	.word 0xe1381e6c
	.word 0x4d323667
	.word 0x1a9a5663
	.word 0x3f8b62df
	.word 0x5a0911fc
	.word 0xf72e4f2c
	.word 0x9af4b90c
	.word 0x118f0cfc
	.word 0x15681cca
	.word 0xb0cb1503
	.word 0x44d9d3ae
	.word 0xbd4c3d83
	.word 0xa0e40613
	.word 0x71f16ae5
	.word 0xb0b50380
	.word 0xe55eb37c
	.word 0x767cb967
	.word 0x145076cf
	.word 0xaa3e4b69
	.word 0x5129aac1
	.word 0xb1d82d30
	.word 0x8d74690b
	.word 0x98f07600
	.word 0xbec329f
	.word 0x76d8519a
	.word 0xff5d5a51
	.word 0x66458578
	.word 0xe3085558
	.word 0x65feda61
	.word 0x6efee472
	.word 0x222505bc
	.word 0x58b3e7b1
	.word 0x5bc33225
	.word 0xd0a2cbb6
	.word 0xcbfd0aae
	.word 0x25ca127d
	.word 0xa0ae6f3e
	.word 0x7a3fc604
	.word 0x34e0aeaf
	.word 0x75963410
	.word 0x9f5499c6
	.word 0x9c294c19
	.word 0xc9470168
	.word 0xb3bb143
	.word 0x5d3f3f90
	.word 0xae7914ba
	.word 0x80cb22db
	.word 0x5b4d2806
	.word 0x65c01ed7
	.word 0xfea68433
	.word 0xdf6716f8
	.word 0xd5a38877
	.word 0xf6641206
	.word 0x903146be
	.word 0xa0667410
	.word 0x8cce01e
	.word 0x7d7c88b
	.word 0x256d476d
	.word 0x84b0052b
	.word 0x32967010
	.word 0xe07ee9e8
	.word 0x7f6b4e6
	.word 0x3387e471
	.word 0x2b523148
	.word 0x96b95d10
	.word 0xe3cd5904
	.word 0x3254f301
	.word 0xd17fbb6f
	.word 0x42568404
	.word 0x82818c11
	.word 0xdaed33cb
	.word 0x454efe11
	.word 0xc01eb0ee
	.word 0x3a40aefe
	.word 0xe960f883
	.word 0x69f8cb9d
	.word 0xd5c2f0a0
	.word 0x776d9d9
	.word 0x7bc89ff4
	.word 0x1eb45019
	.word 0xcfac116
	.word 0xb0f0b49d
	.word 0x9279d191
	.word 0x5228ea2
	.word 0x7f0e0607
	.word 0x6b520fa2
	.word 0xf121f0f3
	.word 0x96e57c
	.word 0xab4c7922
	.word 0x8a325739
	.word 0x5dd613c2
	.word 0xc5552a42
	.word 0x653960e7
	.word 0x9ada529f
	.word 0x52dde0c7
	.word 0x2df7d2d9
	.word 0x133e0b74
	.word 0x45501708
	.word 0x2d4a7f66
	.word 0x12783535
	.word 0x7148238c
	.word 0x79019eaa
	.word 0xf081aeb7
	.word 0xce68a3d9
	.word 0xddad7652
	.word 0x8c269a2c
	.word 0xfe590deb
	.word 0x8dc7cd62
	.word 0x5fd3fedb
	.word 0x946245b
	.word 0xcacfe359
	.word 0x10a85b26
	.word 0x51d8bfe7
	.word 0xbce3142
	.word 0x4037a99b
	.word 0x55800204
	.word 0xb9d25031
	.word 0x20655c01
	.word 0xb3e6828a
	.word 0xf6117fa6
	.word 0x33fbf475
	.word 0x827a3cea
	.word 0x624ac82d
	.word 0xe1433dee
	.word 0xa6a7fb6c
	.word 0xb7b9701f
	.word 0xd461ce3b
	.word 0x1d85d011
	.word 0xf9ee1da7
	.word 0xe34b63e
	.word 0xe6838b89
	.word 0x47104171
	.word 0x574fe62e
	.word 0x7f6834c0
	.word 0x474c2c4a
	.word 0x8c3e349c
	.word 0x85bfbf81
	.word 0x35696c35
	.word 0x1bb89bb6
	.word 0x780c5a91
	.word 0xf22f4412
	.word 0x6c73dee1
	.word 0xe5c39ce
	.word 0xe276f013
	.word 0x5ea2c09b
	.word 0x4aa1190c
	.word 0xe6ddd5bd
	.word 0x536f0406
	.word 0xc77e383b
	.word 0x8fbd4932
	.word 0xdf27ad65
	.word 0xd279bf46
	.word 0x2df9d979
	.word 0xf54e1cf2
	.word 0x74378ce
	.word 0x7c992c8e
	.word 0x3df1ab8b
	.word 0xb86e08af
	.word 0x441725c2
	.word 0xa88843b6
	.word 0xbb91e9cd
	.word 0x95d859f5
	.word 0x5ff69f84
	.word 0x89c455d8
	.word 0xc846c43b
	.word 0x9a0dceb3
	.word 0x90c516e
	.word 0xf09d8ffa
	.word 0xdeec10f6
	.word 0x3bc83e76
	.word 0xe827e52b
	.word 0x4dc7654e
	.word 0x794f509c
	.word 0xde6436c8
	.word 0x996e6be9
	.word 0x2baac511
	.word 0x2e4e3fe0
	.word 0xacb074b3
	.word 0xa588d05
	.word 0x878d395e
	.word 0x31839aa
	.word 0x387d1d11
	.word 0xfb00e9c4
	.word 0x6d0f22da
	.word 0x42e724a1
	.word 0xd4c20462
	.word 0x6f436b3d
	.word 0x1d20092a
	.word 0xcbf8ad6f
	.word 0xd9e6dc99
	.word 0xa3f7df40
	.word 0x68dc6058
	.word 0xafeeabbb
	.word 0x75e2f72
	.word 0xdd93d17e
	.word 0x7ebdc87d
	.word 0xddc4b3e3
	.word 0x6b72a654
	.word 0xd9bb9056
	.word 0x5deee464
	.word 0x7ccf81c5
	.word 0xea02c9c5
	.word 0xef828e2e
	.word 0x2e2a5499
	.word 0x9903ea09
	.word 0x47209067
	.word 0xd792e96f
	.word 0x8587c201
	.word 0xfda72275
	.word 0xbca954ba
	.word 0x9707f8dd
	.word 0x6b17f743
	.word 0x5e6e61cf
	.word 0xc858e16d
	.word 0x8a16b774
	.word 0x960eb684
	.word 0x7028c2ec
	.word 0xa02f29e8
	.word 0xff020208
	.word 0xde6f825f
	.word 0x1265e1cd
	.word 0x950ad94a
	.word 0x315b7c22
	.word 0xd69be38b
	.word 0x1cffe5bc
	.word 0x3b777721
	.word 0xcb025381
	.word 0x655c97a7
	.word 0x884d553a
	.word 0xbc4fca49
	.word 0xebc9aa19
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
