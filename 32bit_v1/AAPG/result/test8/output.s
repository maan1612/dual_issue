
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	ori x24, x13, 1763
i_1:
	bge x26, x29, i_10
i_2:
	sw x27, -32(x2)
i_3:
	add x29, x4, x29
i_4:
	andi x22, x16, -1229
i_5:
	add x22, x19, x6
i_6:
	lbu x16, 195(x2)
i_7:
	lbu x30, -177(x2)
i_8:
	lb x30, -203(x2)
i_9:
	sw x8, -236(x2)
i_10:
	lhu x22, 292(x2)
i_11:
	xori x30, x17, 228
i_12:
	addi x16, x0, -1880
i_13:
	addi x27, x0, -1877
i_14:
	andi x25, x1, -827
i_15:
	slt x10, x1, x14
i_16:
	addi x16 , x16 , 1
	blt x16, x27, i_14
i_17:
	bltu x31, x11, i_21
i_18:
	sb x31, 27(x2)
i_19:
	addi x9, x0, 27
i_20:
	sll x11, x25, x9
i_21:
	or x6, x24, x19
i_22:
	bltu x5, x2, i_32
i_23:
	add x26, x8, x11
i_24:
	addi x15, x0, 23
i_25:
	srl x9, x7, x15
i_26:
	srai x11, x7, 2
i_27:
	lh x1, 46(x2)
i_28:
	blt x8, x7, i_32
i_29:
	lui x23, 816401
i_30:
	beq x29, x21, i_34
i_31:
	lhu x15, -252(x2)
i_32:
	xori x18, x29, -424
i_33:
	ori x23, x28, -1995
i_34:
	sh x20, -364(x2)
i_35:
	sh x29, 330(x2)
i_36:
	add x18, x11, x20
i_37:
	slt x20, x23, x23
i_38:
	sb x5, 20(x2)
i_39:
	sh x1, 114(x2)
i_40:
	addi x27, x0, 28
i_41:
	srl x6, x26, x27
i_42:
	lbu x23, 426(x2)
i_43:
	sb x3, 262(x2)
i_44:
	sb x28, -50(x2)
i_45:
	lhu x17, -2(x2)
i_46:
	lbu x3, 296(x2)
i_47:
	slti x22, x31, 623
i_48:
	addi x3, x0, 4
i_49:
	srl x20, x14, x3
i_50:
	sb x3, 180(x2)
i_51:
	lhu x22, 218(x2)
i_52:
	lhu x26, -196(x2)
i_53:
	lb x4, -226(x2)
i_54:
	lbu x9, -96(x2)
i_55:
	add x23, x28, x18
i_56:
	lhu x15, -46(x2)
i_57:
	addi x17, x0, 5
i_58:
	srl x10, x5, x17
i_59:
	lh x19, -192(x2)
i_60:
	sw x22, 20(x2)
i_61:
	sltu x19, x19, x23
i_62:
	xori x23, x10, 95
i_63:
	sb x5, 264(x2)
i_64:
	sh x7, -478(x2)
i_65:
	lw x10, 236(x2)
i_66:
	bne x1, x28, i_73
i_67:
	lw x10, -132(x2)
i_68:
	lbu x11, -448(x2)
i_69:
	lb x27, 103(x2)
i_70:
	sh x5, -120(x2)
i_71:
	xor x20, x23, x11
i_72:
	lh x11, 48(x2)
i_73:
	lbu x26, 216(x2)
i_74:
	lw x20, -192(x2)
i_75:
	xori x1, x12, 1282
i_76:
	sh x17, -274(x2)
i_77:
	blt x31, x8, i_80
i_78:
	lh x13, 430(x2)
i_79:
	lw x23, 256(x2)
i_80:
	ori x22, x5, -1614
i_81:
	lh x26, 10(x2)
i_82:
	lb x15, 71(x2)
i_83:
	sh x9, 184(x2)
i_84:
	lw x5, -412(x2)
i_85:
	slti x5, x26, 2004
i_86:
	lh x15, -258(x2)
i_87:
	sb x14, 177(x2)
i_88:
	or x5, x23, x1
i_89:
	sltu x5, x15, x11
i_90:
	addi x8, x24, 1660
i_91:
	addi x24, x0, 1914
i_92:
	addi x4, x0, 1918
i_93:
	beq x1, x18, i_95
i_94:
	add x8, x15, x24
i_95:
	lw x8, -20(x2)
i_96:
	nop
i_97:
	slt x14, x19, x23
i_98:
	lhu x1, -212(x2)
i_99:
	sb x19, 135(x2)
i_100:
	addi x1, x0, 16
i_101:
	sll x23, x13, x1
i_102:
	addi x24 , x24 , 1
	bne x24, x4, i_93
i_103:
	lhu x19, 194(x2)
i_104:
	andi x23, x12, 1696
i_105:
	andi x23, x23, -1208
i_106:
	slti x13, x26, -1720
i_107:
	lb x9, -452(x2)
i_108:
	sltiu x10, x2, 190
i_109:
	slli x19, x28, 2
i_110:
	lh x29, -2(x2)
i_111:
	slti x21, x5, 442
i_112:
	sltiu x30, x19, -1967
i_113:
	lbu x15, -357(x2)
i_114:
	sb x17, -359(x2)
i_115:
	bgeu x19, x13, i_125
i_116:
	blt x13, x9, i_119
i_117:
	addi x23, x0, 17
i_118:
	sll x6, x14, x23
i_119:
	add x16, x10, x16
i_120:
	lw x8, 452(x2)
i_121:
	beq x28, x21, i_129
i_122:
	lb x25, -165(x2)
i_123:
	sw x4, 204(x2)
i_124:
	bge x7, x3, i_128
i_125:
	sh x8, -134(x2)
i_126:
	lbu x8, 434(x2)
i_127:
	sltiu x3, x25, -315
i_128:
	lhu x26, -100(x2)
i_129:
	bgeu x16, x19, i_136
i_130:
	addi x8, x31, -846
i_131:
	sb x5, 94(x2)
i_132:
	add x1, x11, x30
i_133:
	srai x30, x7, 3
i_134:
	sub x20, x11, x13
i_135:
	blt x23, x26, i_145
i_136:
	sw x25, -324(x2)
i_137:
	auipc x18, 749828
i_138:
	sb x7, -417(x2)
i_139:
	lh x1, -30(x2)
i_140:
	slti x30, x22, 1461
i_141:
	bgeu x7, x26, i_142
i_142:
	lbu x8, 80(x2)
i_143:
	sh x26, -304(x2)
i_144:
	and x1, x26, x30
i_145:
	lw x26, 272(x2)
i_146:
	lh x26, -268(x2)
i_147:
	add x8, x30, x13
i_148:
	addi x8, x0, 16
i_149:
	sra x29, x28, x8
i_150:
	sw x30, 104(x2)
i_151:
	srai x28, x20, 2
i_152:
	sw x14, 84(x2)
i_153:
	lhu x20, 296(x2)
i_154:
	sb x20, -373(x2)
i_155:
	addi x27, x0, 12
i_156:
	sra x21, x29, x27
i_157:
	lui x24, 1037961
i_158:
	lhu x20, -392(x2)
i_159:
	addi x23, x0, 27
i_160:
	sra x5, x18, x23
i_161:
	lw x21, 424(x2)
i_162:
	lbu x6, -167(x2)
i_163:
	addi x11, x0, -1864
i_164:
	addi x20, x0, -1860
i_165:
	lhu x8, -130(x2)
i_166:
	xor x21, x21, x9
i_167:
	lb x6, 359(x2)
i_168:
	sb x12, 296(x2)
i_169:
	lw x8, -188(x2)
i_170:
	sw x4, -472(x2)
i_171:
	addi x11 , x11 , 1
	bge x20, x11, i_165
i_172:
	bne x12, x10, i_181
i_173:
	sw x21, 432(x2)
i_174:
	andi x9, x6, -29
i_175:
	lw x25, 152(x2)
i_176:
	lw x22, 232(x2)
i_177:
	lh x29, 246(x2)
i_178:
	slt x18, x6, x25
i_179:
	slti x30, x22, 1097
i_180:
	lw x25, 184(x2)
i_181:
	sw x17, -196(x2)
i_182:
	lhu x21, 234(x2)
i_183:
	lw x25, -408(x2)
i_184:
	addi x25, x0, 31
i_185:
	sra x21, x2, x25
i_186:
	xori x20, x21, -1769
i_187:
	lbu x20, -251(x2)
i_188:
	lui x20, 668121
i_189:
	lui x22, 908432
i_190:
	andi x21, x15, -1307
i_191:
	xori x20, x20, -713
i_192:
	beq x7, x27, i_201
i_193:
	addi x12, x0, 2
i_194:
	srl x30, x15, x12
i_195:
	sh x5, 80(x2)
i_196:
	lh x30, -74(x2)
i_197:
	sh x20, -296(x2)
i_198:
	lhu x9, -470(x2)
i_199:
	sh x6, -166(x2)
i_200:
	add x9, x19, x5
i_201:
	srai x28, x24, 1
i_202:
	add x6, x19, x4
i_203:
	sw x22, 276(x2)
i_204:
	lh x6, -40(x2)
i_205:
	bge x12, x30, i_215
i_206:
	sb x23, 461(x2)
i_207:
	slli x25, x11, 3
i_208:
	sb x20, 111(x2)
i_209:
	nop
i_210:
	srli x9, x30, 2
i_211:
	addi x5, x0, 15
i_212:
	srl x9, x12, x5
i_213:
	nop
i_214:
	lw x9, -396(x2)
i_215:
	nop
i_216:
	lw x30, -372(x2)
i_217:
	addi x6, x0, 2018
i_218:
	addi x25, x0, 2021
i_219:
	lw x11, 364(x2)
i_220:
	add x11, x18, x11
i_221:
	bne x14, x28, i_228
i_222:
	sltu x21, x25, x28
i_223:
	addi x6 , x6 , 1
	bgeu x25, x6, i_219
i_224:
	lh x28, -380(x2)
i_225:
	sb x29, 386(x2)
i_226:
	blt x11, x5, i_235
i_227:
	bltu x14, x26, i_231
i_228:
	addi x21, x0, 1
i_229:
	sra x21, x4, x21
i_230:
	sh x2, -320(x2)
i_231:
	lw x28, -468(x2)
i_232:
	bne x28, x27, i_234
i_233:
	addi x25, x0, 3
i_234:
	sll x19, x22, x25
i_235:
	addi x19, x0, 3
i_236:
	sll x27, x23, x19
i_237:
	and x18, x13, x22
i_238:
	nop
i_239:
	lb x28, -442(x2)
i_240:
	addi x15, x0, 2029
i_241:
	addi x14, x0, 2031
i_242:
	sub x19, x22, x20
i_243:
	sh x11, -408(x2)
i_244:
	addi x15 , x15 , 1
	bne x15, x14, i_242
i_245:
	addi x20, x0, 29
i_246:
	srl x21, x12, x20
i_247:
	add x12, x14, x22
i_248:
	andi x21, x21, -1165
i_249:
	blt x20, x27, i_256
i_250:
	beq x12, x6, i_260
i_251:
	lh x30, -476(x2)
i_252:
	sw x16, -480(x2)
i_253:
	lh x8, -288(x2)
i_254:
	sh x20, 280(x2)
i_255:
	lh x5, 126(x2)
i_256:
	sb x2, -400(x2)
i_257:
	slti x27, x3, 1874
i_258:
	addi x27, x0, 17
i_259:
	sra x6, x3, x27
i_260:
	sw x6, 244(x2)
i_261:
	nop
i_262:
	addi x4, x0, 1940
i_263:
	addi x3, x0, 1942
i_264:
	lbu x6, -210(x2)
i_265:
	xori x25, x1, 455
i_266:
	blt x18, x27, i_267
i_267:
	lhu x18, -94(x2)
i_268:
	sb x16, -24(x2)
i_269:
	bgeu x23, x26, i_276
i_270:
	sw x14, 300(x2)
i_271:
	addi x15, x0, 29
i_272:
	sra x17, x15, x15
i_273:
	addi x4 , x4 , 1
	bgeu x3, x4, i_264
i_274:
	lw x28, -312(x2)
i_275:
	lui x15, 240299
i_276:
	addi x6, x0, 26
i_277:
	srl x17, x30, x6
i_278:
	srli x29, x12, 1
i_279:
	srli x10, x5, 2
i_280:
	bne x10, x30, i_289
i_281:
	bge x1, x17, i_286
i_282:
	lb x24, -448(x2)
i_283:
	sh x17, 66(x2)
i_284:
	lui x25, 299941
i_285:
	lh x12, 340(x2)
i_286:
	sh x20, -300(x2)
i_287:
	nop
i_288:
	lh x22, 138(x2)
i_289:
	lhu x13, -14(x2)
i_290:
	lh x9, 418(x2)
i_291:
	addi x17, x0, -1907
i_292:
	addi x11, x0, -1905
i_293:
	addi x17 , x17 , 1
	bltu x17, x11, i_293
i_294:
	lbu x24, 337(x2)
i_295:
	ori x15, x14, 974
i_296:
	sb x22, 3(x2)
i_297:
	blt x29, x21, i_303
i_298:
	lb x15, 331(x2)
i_299:
	lui x15, 25846
i_300:
	lbu x22, -258(x2)
i_301:
	sw x5, -192(x2)
i_302:
	xori x22, x15, 1150
i_303:
	sltu x30, x30, x23
i_304:
	bgeu x28, x23, i_305
i_305:
	lw x18, 312(x2)
i_306:
	lw x19, 476(x2)
i_307:
	sh x28, 14(x2)
i_308:
	lhu x29, -478(x2)
i_309:
	sb x4, -35(x2)
i_310:
	lh x13, 42(x2)
i_311:
	nop
i_312:
	xori x4, x17, 223
i_313:
	addi x24, x0, 1921
i_314:
	addi x28, x0, 1925
i_315:
	slli x4, x21, 1
i_316:
	addi x24 , x24 , 1
	bne  x28, x24, i_315
i_317:
	sw x5, -156(x2)
i_318:
	slt x30, x8, x4
i_319:
	lw x4, 160(x2)
i_320:
	srai x30, x13, 2
i_321:
	add x30, x30, x23
i_322:
	bne x20, x21, i_327
i_323:
	sb x13, -373(x2)
i_324:
	sb x3, 319(x2)
i_325:
	auipc x21, 1014399
i_326:
	add x21, x2, x4
i_327:
	lbu x9, 365(x2)
i_328:
	lb x29, 220(x2)
i_329:
	addi x4, x0, -1932
i_330:
	addi x13, x0, -1930
i_331:
	sb x7, -149(x2)
i_332:
	addi x4 , x4 , 1
	bgeu x13, x4, i_331
i_333:
	bge x29, x7, i_340
i_334:
	addi x9, x0, 26
i_335:
	sra x13, x9, x9
i_336:
	sub x25, x12, x29
i_337:
	addi x21, x0, 27
i_338:
	srl x16, x9, x21
i_339:
	sh x14, -386(x2)
i_340:
	bne x7, x25, i_343
i_341:
	bge x9, x16, i_346
i_342:
	bgeu x15, x9, i_349
i_343:
	addi x20, x0, 9
i_344:
	srl x8, x27, x20
i_345:
	sw x30, 24(x2)
i_346:
	lb x9, -170(x2)
i_347:
	sh x22, 62(x2)
i_348:
	sw x12, -88(x2)
i_349:
	add x10, x14, x20
i_350:
	beq x23, x1, i_355
i_351:
	lbu x26, 372(x2)
i_352:
	lh x20, 50(x2)
i_353:
	and x26, x8, x26
i_354:
	lh x8, 298(x2)
i_355:
	slt x20, x20, x9
i_356:
	nop
i_357:
	addi x3, x0, -1978
i_358:
	addi x14, x0, -1976
i_359:
	lh x21, -100(x2)
i_360:
	sh x8, -252(x2)
i_361:
	addi x18, x0, 1948
i_362:
	addi x20, x0, 1951
i_363:
	addi x18 , x18 , 1
	bgeu x20, x18, i_363
i_364:
	lh x21, -294(x2)
i_365:
	sb x1, 388(x2)
i_366:
	addi x3 , x3 , 1
	blt x3, x14, i_359
i_367:
	bltu x31, x20, i_373
i_368:
	lb x21, -345(x2)
i_369:
	ori x13, x13, -2002
i_370:
	srai x17, x5, 1
i_371:
	addi x27, x0, 14
i_372:
	srl x1, x15, x27
i_373:
	sb x20, 136(x2)
i_374:
	lbu x15, -42(x2)
i_375:
	bltu x31, x1, i_379
i_376:
	sh x26, 22(x2)
i_377:
	slti x14, x4, 1692
i_378:
	addi x4, x0, 7
i_379:
	srl x15, x24, x4
i_380:
	addi x16, x4, -791
i_381:
	addi x11, x0, 27
i_382:
	sra x5, x15, x11
i_383:
	addi x10, x0, -2018
i_384:
	addi x4, x0, -2016
i_385:
	sub x17, x5, x23
i_386:
	lui x5, 285992
i_387:
	addi x5, x8, -1174
i_388:
	sw x4, -220(x2)
i_389:
	lw x3, -80(x2)
i_390:
	ori x30, x5, 2034
i_391:
	lw x13, 184(x2)
i_392:
	slli x24, x27, 2
i_393:
	lh x5, 50(x2)
i_394:
	addi x10 , x10 , 1
	bne x10, x4, i_384
i_395:
	and x27, x30, x10
i_396:
	lhu x27, 356(x2)
i_397:
	lb x10, -322(x2)
i_398:
	lw x23, 380(x2)
i_399:
	addi x9, x0, 15
i_400:
	srl x13, x12, x9
i_401:
	addi x9, x3, -1533
i_402:
	auipc x12, 620758
i_403:
	lbu x3, 210(x2)
i_404:
	lb x3, 437(x2)
i_405:
	addi x12, x0, 1941
i_406:
	addi x4, x0, 1943
i_407:
	srai x1, x1, 3
i_408:
	lh x3, -294(x2)
i_409:
	sub x27, x12, x7
i_410:
	addi x28, x0, 31
i_411:
	sra x30, x18, x28
i_412:
	nop
i_413:
	lw x22, -96(x2)
i_414:
	xor x3, x5, x22
i_415:
	addi x12 , x12 , 1
	bne x12, x4, i_407
i_416:
	sw x23, 44(x2)
i_417:
	sb x27, 287(x2)
i_418:
	sh x23, -152(x2)
i_419:
	bltu x4, x1, i_427
i_420:
	addi x1, x0, 31
i_421:
	sll x27, x12, x1
i_422:
	sw x18, 100(x2)
i_423:
	sw x28, 64(x2)
i_424:
	add x4, x16, x15
i_425:
	addi x24, x0, 6
i_426:
	sll x15, x9, x24
i_427:
	bltu x2, x2, i_436
i_428:
	auipc x22, 339634
i_429:
	bge x21, x9, i_431
i_430:
	sw x7, -264(x2)
i_431:
	sltu x15, x15, x14
i_432:
	sh x18, 220(x2)
i_433:
	beq x10, x1, i_442
i_434:
	slti x15, x2, -641
i_435:
	addi x24, x0, 28
i_436:
	sll x15, x24, x24
i_437:
	bge x15, x23, i_442
i_438:
	sltu x24, x20, x15
i_439:
	lbu x25, -137(x2)
i_440:
	blt x14, x15, i_447
i_441:
	srai x25, x15, 2
i_442:
	bgeu x16, x15, i_443
i_443:
	and x25, x31, x20
i_444:
	sh x7, -96(x2)
i_445:
	bgeu x15, x24, i_455
i_446:
	sh x16, -384(x2)
i_447:
	lui x21, 444873
i_448:
	lhu x21, 280(x2)
i_449:
	bne x23, x16, i_459
i_450:
	lhu x4, -382(x2)
i_451:
	lhu x12, -90(x2)
i_452:
	sw x25, -340(x2)
i_453:
	lbu x19, 166(x2)
i_454:
	ori x17, x12, 439
i_455:
	srli x20, x25, 3
i_456:
	addi x12, x0, 15
i_457:
	sll x17, x16, x12
i_458:
	add x5, x30, x18
i_459:
	lbu x30, -167(x2)
i_460:
	or x3, x30, x12
i_461:
	lhu x5, 302(x2)
i_462:
	nop
i_463:
	lb x1, 402(x2)
i_464:
	addi x10, x0, 2009
i_465:
	addi x11, x0, 2013
i_466:
	xori x6, x27, 875
i_467:
	nop
i_468:
	lw x18, -276(x2)
i_469:
	sub x18, x17, x6
i_470:
	srai x4, x19, 1
i_471:
	lhu x20, -80(x2)
i_472:
	sb x26, 70(x2)
i_473:
	addi x10 , x10 , 1
	bgeu x11, x10, i_466
i_474:
	sltiu x25, x4, -1993
i_475:
	lw x18, -256(x2)
i_476:
	beq x5, x16, i_481
i_477:
	sh x17, 56(x2)
i_478:
	sb x24, 181(x2)
i_479:
	sw x6, 4(x2)
i_480:
	lbu x24, -339(x2)
i_481:
	sb x30, 287(x2)
i_482:
	ori x5, x25, -1470
i_483:
	lw x1, -460(x2)
i_484:
	lhu x30, -414(x2)
i_485:
	bltu x12, x5, i_493
i_486:
	lbu x11, -33(x2)
i_487:
	sh x20, -106(x2)
i_488:
	sb x1, 387(x2)
i_489:
	slt x5, x23, x28
i_490:
	lh x30, 296(x2)
i_491:
	lhu x1, 242(x2)
i_492:
	bltu x4, x22, i_496
i_493:
	nop
i_494:
	xor x26, x25, x1
i_495:
	sb x16, -68(x2)
i_496:
	sw x29, 164(x2)
i_497:
	sltu x5, x12, x29
i_498:
	addi x24, x0, -1905
i_499:
	addi x22, x0, -1902
i_500:
	sw x3, -432(x2)
i_501:
	nop
i_502:
	addi x24 , x24 , 1
	bltu x24, x22, i_500
i_503:
	lb x3, 274(x2)
i_504:
	slti x22, x17, 727
i_505:
	lbu x10, -48(x2)
i_506:
	lbu x23, 246(x2)
i_507:
	sh x1, 180(x2)
i_508:
	lhu x22, -88(x2)
i_509:
	lui x23, 394123
i_510:
	sw x20, 56(x2)
i_511:
	ori x27, x23, 1448
i_512:
	sb x23, -93(x2)
i_513:
	addi x11, x0, 31
i_514:
	srl x25, x27, x11
i_515:
	slli x23, x25, 4
i_516:
	sh x11, 242(x2)
i_517:
	lw x9, -124(x2)
i_518:
	blt x26, x31, i_524
i_519:
	sb x3, 287(x2)
i_520:
	lbu x3, -291(x2)
i_521:
	lw x6, -276(x2)
i_522:
	sb x19, 124(x2)
i_523:
	sw x23, 404(x2)
i_524:
	sh x3, 398(x2)
i_525:
	lhu x3, -356(x2)
i_526:
	addi x27, x10, -1369
i_527:
	lb x10, 482(x2)
i_528:
	addi x15, x0, 17
i_529:
	srl x14, x3, x15
i_530:
	bgeu x30, x26, i_535
i_531:
	srli x30, x18, 4
i_532:
	slt x3, x31, x1
i_533:
	sh x26, -376(x2)
i_534:
	blt x12, x21, i_536
i_535:
	bge x19, x3, i_545
i_536:
	sh x17, 2(x2)
i_537:
	blt x13, x15, i_538
i_538:
	srai x3, x3, 1
i_539:
	beq x1, x17, i_548
i_540:
	sb x15, 240(x2)
i_541:
	andi x17, x17, 36
i_542:
	and x1, x10, x24
i_543:
	srai x16, x8, 2
i_544:
	lw x8, 108(x2)
i_545:
	sltu x10, x26, x9
i_546:
	ori x13, x8, -1075
i_547:
	lh x6, 238(x2)
i_548:
	add x12, x16, x4
i_549:
	sh x24, 484(x2)
i_550:
	sub x16, x16, x23
i_551:
	add x30, x16, x13
i_552:
	bne x30, x11, i_557
i_553:
	bgeu x10, x1, i_557
i_554:
	sb x27, -367(x2)
i_555:
	addi x11, x0, 4
i_556:
	sra x10, x20, x11
i_557:
	lb x16, -292(x2)
i_558:
	add x26, x28, x25
i_559:
	ori x3, x21, 777
i_560:
	auipc x5, 942103
i_561:
	xor x3, x2, x23
i_562:
	sh x9, 326(x2)
i_563:
	ori x8, x26, -1227
i_564:
	lbu x9, 309(x2)
i_565:
	or x19, x19, x5
i_566:
	or x26, x10, x23
i_567:
	addi x18, x0, 22
i_568:
	sll x18, x19, x18
i_569:
	sub x14, x28, x26
i_570:
	auipc x12, 149114
i_571:
	sh x18, 148(x2)
i_572:
	bge x24, x18, i_575
i_573:
	bge x18, x26, i_580
i_574:
	slti x22, x1, 213
i_575:
	lbu x18, -166(x2)
i_576:
	sub x1, x12, x22
i_577:
	sb x13, 159(x2)
i_578:
	sltu x22, x14, x22
i_579:
	lbu x14, 87(x2)
i_580:
	sb x18, -24(x2)
i_581:
	xor x14, x8, x28
i_582:
	bltu x31, x5, i_587
i_583:
	sltu x28, x14, x12
i_584:
	srai x14, x9, 1
i_585:
	lb x29, -285(x2)
i_586:
	bltu x17, x9, i_591
i_587:
	lhu x28, 440(x2)
i_588:
	addi x10, x0, 22
i_589:
	srl x8, x28, x10
i_590:
	xori x11, x10, -1886
i_591:
	auipc x11, 193799
i_592:
	sb x31, 232(x2)
i_593:
	bgeu x25, x13, i_601
i_594:
	lb x25, -37(x2)
i_595:
	sh x7, -354(x2)
i_596:
	sb x28, -81(x2)
i_597:
	slt x25, x14, x14
i_598:
	slli x5, x24, 4
i_599:
	lbu x11, 458(x2)
i_600:
	sw x14, 28(x2)
i_601:
	bge x17, x29, i_603
i_602:
	lb x22, 211(x2)
i_603:
	lhu x15, -336(x2)
i_604:
	lh x1, -384(x2)
i_605:
	addi x29, x0, -1954
i_606:
	addi x28, x0, -1951
i_607:
	addi x24, x0, 19
i_608:
	sll x1, x6, x24
i_609:
	addi x22, x29, 1219
i_610:
	lw x24, -128(x2)
i_611:
	lbu x15, 351(x2)
i_612:
	slt x14, x1, x29
i_613:
	sw x15, 292(x2)
i_614:
	addi x29 , x29 , 1
	blt x29, x28, i_607
i_615:
	ori x1, x11, 1013
i_616:
	andi x10, x16, -1866
i_617:
	bgeu x31, x4, i_621
i_618:
	lw x25, 236(x2)
i_619:
	sb x24, 401(x2)
i_620:
	and x24, x20, x31
i_621:
	sb x9, 367(x2)
i_622:
	xori x20, x20, 1485
i_623:
	lw x5, -460(x2)
i_624:
	addi x10, x0, -1911
i_625:
	addi x16, x0, -1908
i_626:
	sh x23, -2(x2)
i_627:
	lh x20, 284(x2)
i_628:
	lh x24, -368(x2)
i_629:
	bne x5, x20, i_639
i_630:
	lhu x20, -482(x2)
i_631:
	sb x18, 285(x2)
i_632:
	lw x8, 196(x2)
i_633:
	lh x24, 210(x2)
i_634:
	lh x8, -218(x2)
i_635:
	addi x10 , x10 , 1
	bne x10, x16, i_626
i_636:
	lhu x11, -42(x2)
i_637:
	bne x9, x22, i_646
i_638:
	ori x17, x4, 126
i_639:
	xori x26, x1, -326
i_640:
	sh x21, 296(x2)
i_641:
	sb x29, 53(x2)
i_642:
	srai x29, x26, 2
i_643:
	lw x14, 416(x2)
i_644:
	sw x30, -136(x2)
i_645:
	nop
i_646:
	sb x29, 46(x2)
i_647:
	sw x19, 8(x2)
i_648:
	addi x24, x0, -2036
i_649:
	addi x23, x0, -2034
i_650:
	nop
i_651:
	and x29, x12, x29
i_652:
	add x12, x8, x12
i_653:
	addi x24 , x24 , 1
	bge x23, x24, i_650
i_654:
	add x29, x11, x31
i_655:
	addi x15, x0, 7
i_656:
	sll x15, x15, x15
i_657:
	bge x14, x16, i_658
i_658:
	lhu x9, -260(x2)
i_659:
	sltu x15, x9, x15
i_660:
	slt x23, x15, x6
i_661:
	sltu x22, x5, x25
i_662:
	slt x18, x24, x22
i_663:
	lw x22, 48(x2)
i_664:
	lw x17, -48(x2)
i_665:
	bgeu x25, x12, i_672
i_666:
	srai x26, x10, 1
i_667:
	sb x4, -372(x2)
i_668:
	lbu x10, -171(x2)
i_669:
	beq x10, x26, i_676
i_670:
	lhu x1, -372(x2)
i_671:
	lhu x10, 432(x2)
i_672:
	lhu x10, -212(x2)
i_673:
	bge x30, x26, i_680
i_674:
	and x10, x6, x1
i_675:
	lbu x30, 104(x2)
i_676:
	lb x6, -254(x2)
i_677:
	add x10, x5, x19
i_678:
	addi x24, x27, -1780
i_679:
	blt x28, x6, i_681
i_680:
	srli x10, x14, 3
i_681:
	add x9, x9, x23
i_682:
	sw x12, 284(x2)
i_683:
	add x24, x25, x18
i_684:
	xor x25, x2, x30
i_685:
	xor x22, x2, x3
i_686:
	lbu x18, -395(x2)
i_687:
	bgeu x5, x31, i_688
i_688:
	bltu x22, x10, i_697
i_689:
	beq x21, x3, i_693
i_690:
	beq x29, x25, i_696
i_691:
	bltu x31, x25, i_699
i_692:
	sltiu x25, x17, -774
i_693:
	sw x30, 480(x2)
i_694:
	lhu x24, 252(x2)
i_695:
	lw x11, 144(x2)
i_696:
	lb x11, 267(x2)
i_697:
	lui x19, 387413
i_698:
	lh x9, -280(x2)
i_699:
	sb x30, -293(x2)
i_700:
	bne x9, x2, i_710
i_701:
	lb x3, -371(x2)
i_702:
	lhu x9, -314(x2)
i_703:
	slt x9, x3, x31
i_704:
	sltu x30, x21, x9
i_705:
	lhu x11, 456(x2)
i_706:
	add x3, x21, x28
i_707:
	add x9, x31, x17
i_708:
	beq x5, x2, i_716
i_709:
	lb x10, 386(x2)
i_710:
	lui x1, 480629
i_711:
	lbu x17, -165(x2)
i_712:
	sub x30, x10, x21
i_713:
	auipc x9, 620353
i_714:
	lh x30, -8(x2)
i_715:
	blt x21, x14, i_720
i_716:
	sb x19, -188(x2)
i_717:
	ori x12, x30, 544
i_718:
	bge x22, x25, i_721
i_719:
	sb x2, 162(x2)
i_720:
	blt x21, x17, i_723
i_721:
	sltu x9, x25, x2
i_722:
	lb x18, -212(x2)
i_723:
	srli x17, x5, 3
i_724:
	blt x21, x13, i_734
i_725:
	sw x17, -308(x2)
i_726:
	slti x17, x16, -1543
i_727:
	sh x17, -322(x2)
i_728:
	lh x18, -70(x2)
i_729:
	srli x24, x25, 3
i_730:
	lhu x17, -242(x2)
i_731:
	lhu x30, 86(x2)
i_732:
	addi x12, x26, -544
i_733:
	sw x5, 396(x2)
i_734:
	beq x4, x16, i_737
i_735:
	beq x6, x22, i_737
i_736:
	bgeu x26, x17, i_741
i_737:
	sb x30, -463(x2)
i_738:
	sltiu x26, x10, -76
i_739:
	sb x17, -51(x2)
i_740:
	lb x29, -269(x2)
i_741:
	sw x16, -348(x2)
i_742:
	sltiu x4, x13, -1861
i_743:
	addi x30, x0, -1968
i_744:
	addi x10, x0, -1964
i_745:
	lhu x16, -38(x2)
i_746:
	auipc x9, 871598
i_747:
	lh x9, -84(x2)
i_748:
	sb x24, -419(x2)
i_749:
	lw x19, -108(x2)
i_750:
	nop
i_751:
	slt x18, x18, x1
i_752:
	addi x30 , x30 , 1
	bne x30, x10, i_745
i_753:
	srai x19, x10, 4
i_754:
	blt x24, x13, i_762
i_755:
	lh x29, -146(x2)
i_756:
	slt x9, x14, x2
i_757:
	lui x9, 628155
i_758:
	blt x5, x9, i_765
i_759:
	lhu x9, -236(x2)
i_760:
	beq x21, x19, i_766
i_761:
	lhu x29, -188(x2)
i_762:
	lh x29, -178(x2)
i_763:
	sh x30, 28(x2)
i_764:
	bgeu x9, x2, i_769
i_765:
	lw x29, -232(x2)
i_766:
	lw x10, 308(x2)
i_767:
	sh x28, -220(x2)
i_768:
	beq x8, x21, i_775
i_769:
	or x23, x25, x24
i_770:
	sh x24, 156(x2)
i_771:
	nop
i_772:
	andi x24, x31, -2002
i_773:
	nop
i_774:
	addi x5, x0, 4
i_775:
	srl x14, x6, x5
i_776:
	sb x20, 331(x2)
i_777:
	addi x16, x0, 1930
i_778:
	addi x27, x0, 1933
i_779:
	srli x18, x8, 4
i_780:
	sb x9, 64(x2)
i_781:
	lh x30, -354(x2)
i_782:
	addi x16 , x16 , 1
	blt x16, x27, i_779
i_783:
	addi x21, x0, 2
i_784:
	sll x9, x29, x21
i_785:
	lb x16, -420(x2)
i_786:
	sb x3, -193(x2)
i_787:
	lhu x11, -168(x2)
i_788:
	sw x22, -356(x2)
i_789:
	add x29, x29, x27
i_790:
	addi x18, x0, 30
i_791:
	sra x9, x10, x18
i_792:
	sw x10, -68(x2)
i_793:
	bne x18, x26, i_794
i_794:
	bgeu x19, x21, i_801
i_795:
	lb x10, 18(x2)
i_796:
	lw x23, 368(x2)
i_797:
	lw x23, 392(x2)
i_798:
	sub x8, x1, x23
i_799:
	add x18, x10, x22
i_800:
	blt x27, x10, i_810
i_801:
	sw x31, -236(x2)
i_802:
	andi x16, x18, 530
i_803:
	beq x4, x21, i_807
i_804:
	lh x19, 488(x2)
i_805:
	lbu x25, -145(x2)
i_806:
	slt x16, x16, x10
i_807:
	lbu x18, 119(x2)
i_808:
	lw x17, -284(x2)
i_809:
	addi x8, x0, 27
i_810:
	sra x19, x15, x8
i_811:
	bgeu x25, x19, i_817
i_812:
	sh x5, -290(x2)
i_813:
	srli x23, x10, 3
i_814:
	and x9, x13, x21
i_815:
	lhu x6, 236(x2)
i_816:
	addi x15, x0, 28
i_817:
	sra x27, x23, x15
i_818:
	xor x12, x14, x5
i_819:
	lb x27, -406(x2)
i_820:
	lbu x12, 137(x2)
i_821:
	addi x21, x16, 1028
i_822:
	ori x12, x4, -298
i_823:
	sw x15, -272(x2)
i_824:
	or x14, x4, x21
i_825:
	sltu x23, x16, x24
i_826:
	addi x15, x0, -1851
i_827:
	addi x4, x0, -1848
i_828:
	lw x23, -52(x2)
i_829:
	addi x15 , x15 , 1
	bne x15, x4, i_828
i_830:
	sb x23, -263(x2)
i_831:
	blt x28, x7, i_839
i_832:
	lh x28, 394(x2)
i_833:
	lw x8, 256(x2)
i_834:
	lhu x27, 250(x2)
i_835:
	andi x29, x29, -908
i_836:
	sh x2, -444(x2)
i_837:
	add x17, x24, x12
i_838:
	lw x12, 184(x2)
i_839:
	sb x12, -340(x2)
i_840:
	sh x11, 134(x2)
i_841:
	lui x27, 795866
i_842:
	lbu x12, 240(x2)
i_843:
	lb x3, -60(x2)
i_844:
	sltu x26, x13, x8
i_845:
	sw x9, 180(x2)
i_846:
	lbu x24, 42(x2)
i_847:
	lhu x5, 422(x2)
i_848:
	lw x9, 456(x2)
i_849:
	srli x13, x8, 2
i_850:
	addi x29, x5, 1710
i_851:
	lbu x20, 399(x2)
i_852:
	lh x13, -260(x2)
i_853:
	bge x21, x16, i_859
i_854:
	lhu x11, 414(x2)
i_855:
	lw x16, -24(x2)
i_856:
	addi x9, x16, -857
i_857:
	lui x6, 684006
i_858:
	sw x10, 164(x2)
i_859:
	sb x23, 14(x2)
i_860:
	sh x26, -286(x2)
i_861:
	lh x26, 54(x2)
i_862:
	auipc x26, 852255
i_863:
	xori x20, x27, -1291
i_864:
	bge x21, x16, i_869
i_865:
	andi x27, x12, -750
i_866:
	lhu x21, -182(x2)
i_867:
	lw x19, -268(x2)
i_868:
	lh x20, -106(x2)
i_869:
	or x13, x18, x1
i_870:
	add x23, x11, x4
i_871:
	lbu x1, 358(x2)
i_872:
	sb x20, 170(x2)
i_873:
	bltu x5, x13, i_882
i_874:
	xori x30, x20, -1144
i_875:
	lh x13, -444(x2)
i_876:
	lh x1, -84(x2)
i_877:
	add x21, x30, x9
i_878:
	lb x30, -448(x2)
i_879:
	xori x29, x11, -1645
i_880:
	or x30, x9, x21
i_881:
	xor x29, x1, x21
i_882:
	sh x8, 22(x2)
i_883:
	lb x14, 282(x2)
i_884:
	lw x11, 8(x2)
i_885:
	lh x21, 154(x2)
i_886:
	beq x22, x22, i_893
i_887:
	add x14, x7, x21
i_888:
	lw x21, 356(x2)
i_889:
	lw x29, -80(x2)
i_890:
	lh x19, 404(x2)
i_891:
	lb x20, -330(x2)
i_892:
	lh x29, -172(x2)
i_893:
	sh x27, 116(x2)
i_894:
	lhu x29, -192(x2)
i_895:
	srli x26, x30, 1
i_896:
	bge x26, x15, i_903
i_897:
	nop
i_898:
	nop
i_899:
	sltiu x27, x26, 273
i_900:
	sh x2, 274(x2)
i_901:
	lbu x10, -80(x2)
i_902:
	sh x6, -280(x2)
i_903:
	nop
i_904:
	lbu x10, 382(x2)
i_905:
	addi x26, x0, -1871
i_906:
	addi x15, x0, -1869
i_907:
	bne x6, x17, i_914
i_908:
	lw x17, 16(x2)
i_909:
	addi x26 , x26 , 1
	bne  x15, x26, i_907
i_910:
	bgeu x26, x27, i_915
i_911:
	srai x6, x18, 4
i_912:
	lhu x9, -438(x2)
i_913:
	lw x10, -360(x2)
i_914:
	lw x19, 12(x2)
i_915:
	lhu x5, 414(x2)
i_916:
	lb x27, 462(x2)
i_917:
	sb x26, -140(x2)
i_918:
	addi x26, x0, 31
i_919:
	srl x26, x11, x26
i_920:
	srai x13, x28, 4
i_921:
	lbu x10, 239(x2)
i_922:
	auipc x28, 872205
i_923:
	lw x11, -72(x2)
i_924:
	lw x19, 76(x2)
i_925:
	lh x22, 254(x2)
i_926:
	add x30, x28, x4
i_927:
	sltiu x9, x5, -1598
i_928:
	slti x28, x23, -1391
i_929:
	sh x28, -426(x2)
i_930:
	lbu x11, -113(x2)
i_931:
	sh x24, 136(x2)
i_932:
	srli x15, x3, 3
i_933:
	lui x3, 364475
i_934:
	lw x27, -128(x2)
i_935:
	lw x6, -112(x2)
i_936:
	sub x24, x22, x7
i_937:
	sb x19, -472(x2)
i_938:
	sw x27, 200(x2)
i_939:
	ori x6, x19, -89
i_940:
	blt x25, x16, i_948
i_941:
	sw x15, 356(x2)
i_942:
	sltiu x8, x11, 1440
i_943:
	sltu x25, x2, x9
i_944:
	sb x12, -236(x2)
i_945:
	sub x4, x28, x21
i_946:
	add x21, x8, x28
i_947:
	bgeu x1, x4, i_954
i_948:
	sw x21, -484(x2)
i_949:
	lw x3, 488(x2)
i_950:
	sb x8, -82(x2)
i_951:
	beq x4, x5, i_960
i_952:
	sb x25, 243(x2)
i_953:
	blt x21, x20, i_960
i_954:
	sw x22, 220(x2)
i_955:
	bge x11, x25, i_962
i_956:
	ori x26, x23, 1183
i_957:
	xori x6, x28, -645
i_958:
	sh x4, 194(x2)
i_959:
	sh x25, -164(x2)
i_960:
	lhu x5, 22(x2)
i_961:
	sw x21, 72(x2)
i_962:
	xori x9, x4, -1680
i_963:
	sub x4, x18, x17
i_964:
	lhu x16, 38(x2)
i_965:
	addi x22, x0, -1836
i_966:
	addi x6, x0, -1834
i_967:
	lui x25, 696823
i_968:
	sw x14, 156(x2)
i_969:
	xori x14, x20, 1076
i_970:
	addi x22 , x22 , 1
	bne x22, x6, i_967
i_971:
	xori x17, x19, 1872
i_972:
	ori x14, x29, 743
i_973:
	blt x22, x18, i_975
i_974:
	or x22, x4, x22
i_975:
	sh x17, 250(x2)
i_976:
	xori x27, x25, -1349
i_977:
	nop
i_978:
	lh x20, 58(x2)
i_979:
	addi x24, x0, 1959
i_980:
	addi x9, x0, 1963
i_981:
	sw x21, 476(x2)
i_982:
	addi x10, x9, 1360
i_983:
	sh x9, -40(x2)
i_984:
	sh x29, -234(x2)
i_985:
	lbu x13, -41(x2)
i_986:
	sw x31, -200(x2)
i_987:
	beq x27, x20, i_993
i_988:
	or x19, x9, x21
i_989:
	lbu x19, -212(x2)
i_990:
	addi x24 , x24 , 1
	bltu x24, x9, i_981
i_991:
	sb x6, 458(x2)
i_992:
	lui x12, 26132
i_993:
	blt x3, x30, i_1000
i_994:
	blt x13, x24, i_998
i_995:
	srli x8, x8, 4
i_996:
	lw x20, 476(x2)
i_997:
	bgeu x23, x13, i_1003
i_998:
	lb x1, 230(x2)
i_999:
	blt x4, x1, i_1003
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0xf7653f45
	.word 0xc8fabc79
	.word 0x58c0223f
	.word 0xc9ab13b6
	.word 0xaf19e5d4
	.word 0x808b1b4f
	.word 0x83e5511b
	.word 0xb3a13f61
	.word 0x453f9760
	.word 0x4e00c528
	.word 0x6aa44ec4
	.word 0x737a8348
	.word 0x7643df8d
	.word 0xeb5412ab
	.word 0x31286231
	.word 0x40d43097
	.word 0xceaab41d
	.word 0xe85381af
	.word 0x77aeb02
	.word 0xa77aa391
	.word 0x799c695f
	.word 0xecebde0a
	.word 0x7c26b79a
	.word 0x22d2b13e
	.word 0x6210e724
	.word 0xf120daf5
	.word 0x1f77911c
	.word 0x525a458d
	.word 0x559d3efb
	.word 0xa242bcd8
	.word 0x28d4d183
	.word 0xa2a0aa9e
	.word 0xd5d095c3
	.word 0x6aa03d09
	.word 0x34b67c0b
	.word 0x8e3e3b93
	.word 0x9e640fbb
	.word 0xa23eca09
	.word 0x9bd64187
	.word 0xadea2ed0
	.word 0xb4ad6265
	.word 0x6ab59b02
	.word 0xdd34d728
	.word 0x902ca1f1
	.word 0x51cbd252
	.word 0xdaf77b18
	.word 0x9b0268b5
	.word 0xd9a94488
	.word 0xd070cd5a
	.word 0x1c39bd19
	.word 0x24f50c41
	.word 0x5c360b77
	.word 0x7d2e7e95
	.word 0x46fdbdd7
	.word 0x7e55e941
	.word 0x1ed2222c
	.word 0x8923d80
	.word 0xb37f6854
	.word 0x8609ac5f
	.word 0xf3b9e577
	.word 0xe44b93ab
	.word 0x799386fa
	.word 0xb72523db
	.word 0xaeedde4e
	.word 0xee2d439c
	.word 0x49c78e3f
	.word 0x92fb91e1
	.word 0x16984034
	.word 0xa0d9a785
	.word 0x99169c5
	.word 0x57e3c99a
	.word 0x93219b7a
	.word 0x79043b3e
	.word 0x27916ce5
	.word 0x97302427
	.word 0x3033478c
	.word 0x22e22968
	.word 0xb686d7be
	.word 0xa066b120
	.word 0x6c4c9067
	.word 0x93820c08
	.word 0x1542de71
	.word 0x589fe9ab
	.word 0xba1417a5
	.word 0x7ba9bee2
	.word 0x7c9fe2b0
	.word 0x52ef191e
	.word 0x74c9b049
	.word 0x20a4b1da
	.word 0x65d7756
	.word 0x4b63d88d
	.word 0xb2cf671
	.word 0x40ea1e9f
	.word 0xa9baff13
	.word 0x6cf3fac3
	.word 0xcf3e5a36
	.word 0x8916da37
	.word 0xb01dfd5f
	.word 0x10ea4fa7
	.word 0x100449ed
	.word 0xc924bdee
	.word 0x4f2b6db9
	.word 0xad8fdec
	.word 0x2b644d6c
	.word 0x70648254
	.word 0x3572b158
	.word 0x4598d744
	.word 0x84eb2b4f
	.word 0x9191aa17
	.word 0x82cc8d51
	.word 0x7080f8c
	.word 0x2ed646dc
	.word 0x6681a9d6
	.word 0x3bb2b4a4
	.word 0x86fb3f9f
	.word 0xe010c257
	.word 0x2bc58c29
	.word 0x555d316f
	.word 0xe08d5c54
	.word 0x44586928
	.word 0x680fea79
	.word 0xa5f68c0e
	.word 0x358aaafb
	.word 0xacdccecf
	.word 0x4310a725
	.word 0x906fcd9a
	.word 0x8e84bec8
	.word 0x1657ba68
	.word 0x7ae3dd8b
	.word 0x2ac45a2c
	.word 0x7cb6325c
	.word 0x58ba87ef
	.word 0xff05ab6d
	.word 0x93b3ddad
	.word 0xc0743c4
	.word 0x7b7c33df
	.word 0xf7cfb0b8
	.word 0xbff77636
	.word 0x4033bd4c
	.word 0xbbc141e7
	.word 0xc03fe993
	.word 0xc9a70ccd
	.word 0xfc971bda
	.word 0x9e9c47f8
	.word 0xcae4b13e
	.word 0xa7c5f2a
	.word 0x67957949
	.word 0x7098c45f
	.word 0x8381f4c2
	.word 0xc199d8d7
	.word 0x4b1be8cc
	.word 0x7a4e9b99
	.word 0x6d7ba5bc
	.word 0xffe012c6
	.word 0x2dc92c5e
	.word 0xdd1cb18
	.word 0xf7b131d8
	.word 0xff169ff8
	.word 0x178c89ab
	.word 0x8c816c17
	.word 0x5d4d18b3
	.word 0xa42e3c6d
	.word 0x7706bb8c
	.word 0xe9368a0e
	.word 0x5ef6ba9c
	.word 0x7a43e55f
	.word 0x32a35cc
	.word 0x428d5e39
	.word 0xe38a2cf8
	.word 0x52a984b7
	.word 0xe8df379a
	.word 0xe8524917
	.word 0x3b0eb586
	.word 0x47957a80
	.word 0x176f90db
	.word 0xf0dfcd5f
	.word 0x1ac76118
	.word 0xaade61b1
	.word 0xf7a6aff3
	.word 0x7996e327
	.word 0xe69d4296
	.word 0x5eb59691
	.word 0x297f96c
	.word 0x27466644
	.word 0xb23c49a5
	.word 0x3c360709
	.word 0xb4508974
	.word 0x9c58bc55
	.word 0x6796c39e
	.word 0x7ccccf05
	.word 0x27931cc0
	.word 0x6fac30cc
	.word 0xa178475
	.word 0x172e9e7e
	.word 0x4c4a5209
	.word 0x5f999b9
	.word 0x9167f2fa
	.word 0xbbc9fc23
	.word 0x40ff89ff
	.word 0x8aa9f414
	.word 0x34e92883
	.word 0xba01cfed
	.word 0xe7c8f7bf
	.word 0xc872fe73
	.word 0x6b9856e3
	.word 0xacfaf985
	.word 0xc9bea345
	.word 0xe490bcf
	.word 0x88a3c469
	.word 0x103ab2f6
	.word 0xa9fbed43
	.word 0x37a181ec
	.word 0x6fc923d5
	.word 0xfa5e167
	.word 0xe5ebdacc
	.word 0x57dfd35b
	.word 0x35a47b00
	.word 0xbd03e37f
	.word 0xf2ec29f7
	.word 0xd96931e9
	.word 0xd3420066
	.word 0x18548e19
	.word 0xd082490a
	.word 0x96235063
	.word 0x28484e9a
	.word 0x80299d78
	.word 0x41334396
	.word 0x9435a968
	.word 0x111da49
	.word 0x70ce91ac
	.word 0xdf728a5c
	.word 0x21d13ae1
	.word 0x6861ae87
	.word 0xb1744e1
	.word 0x11efe0a0
	.word 0x856e60b5
	.word 0x72972261
	.word 0x4c374ecb
	.word 0x9897bd97
	.word 0xd4f04bd
	.word 0x411a040
	.word 0x67dea74
	.word 0x44ec3e3e
	.word 0x60a07c98
	.word 0x338aa4c6
	.word 0x7fa9be03
	.word 0xf9567d82
	.word 0x2751e442
	.word 0xbea58b30
	.word 0x957c88b6
	.word 0x9f26581b
	.word 0xbe1a42c4
	.word 0x5c941330
	.word 0x3cb57ba8
	.word 0x9a79cbe
	.word 0x2e0218f7
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
