
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	add x16, x18, x21
i_1:
	lbu x8, 44(x2)
i_2:
	srli x24, x10, 2
i_3:
	sb x28, 370(x2)
i_4:
	slt x10, x28, x16
i_5:
	lhu x10, -404(x2)
i_6:
	lhu x25, -78(x2)
i_7:
	nop
i_8:
	addi x18, x0, -2047
i_9:
	addi x5, x0, -2044
i_10:
	xori x15, x23, -892
i_11:
	lw x12, -192(x2)
i_12:
	srai x10, x31, 2
i_13:
	lbu x23, 422(x2)
i_14:
	sb x5, -181(x2)
i_15:
	lw x4, 120(x2)
i_16:
	lh x4, 248(x2)
i_17:
	or x4, x12, x29
i_18:
	addi x18 , x18 , 1
	blt x18, x5, i_10
i_19:
	lw x13, -260(x2)
i_20:
	lw x25, -164(x2)
i_21:
	lb x24, -348(x2)
i_22:
	sh x19, 172(x2)
i_23:
	add x1, x20, x8
i_24:
	sw x28, -176(x2)
i_25:
	lbu x16, 285(x2)
i_26:
	lbu x22, -116(x2)
i_27:
	addi x28, x0, 13
i_28:
	sll x25, x9, x28
i_29:
	sub x16, x3, x4
i_30:
	lbu x17, 351(x2)
i_31:
	sub x23, x28, x27
i_32:
	lw x1, -224(x2)
i_33:
	sb x31, 383(x2)
i_34:
	srai x27, x14, 3
i_35:
	bltu x27, x7, i_37
i_36:
	lw x15, -324(x2)
i_37:
	beq x27, x29, i_41
i_38:
	lbu x29, -319(x2)
i_39:
	bne x4, x13, i_43
i_40:
	lh x15, 420(x2)
i_41:
	lb x30, -352(x2)
i_42:
	sh x13, 26(x2)
i_43:
	beq x6, x2, i_51
i_44:
	srai x30, x5, 3
i_45:
	sw x30, -440(x2)
i_46:
	sh x19, 262(x2)
i_47:
	auipc x10, 398739
i_48:
	addi x19, x21, -1023
i_49:
	bgeu x25, x22, i_58
i_50:
	sw x5, 436(x2)
i_51:
	lh x25, -422(x2)
i_52:
	sw x29, -212(x2)
i_53:
	sb x19, 179(x2)
i_54:
	addi x10, x14, 1697
i_55:
	auipc x22, 841353
i_56:
	slli x22, x25, 1
i_57:
	ori x30, x27, -1610
i_58:
	srli x12, x30, 4
i_59:
	lb x3, 213(x2)
i_60:
	lw x27, -216(x2)
i_61:
	addi x14, x0, 1991
i_62:
	addi x26, x0, 1995
i_63:
	sb x30, 43(x2)
i_64:
	lb x27, -208(x2)
i_65:
	slt x10, x3, x26
i_66:
	slti x3, x1, 747
i_67:
	sw x10, -12(x2)
i_68:
	xori x11, x26, -1554
i_69:
	srai x15, x8, 1
i_70:
	xor x18, x18, x12
i_71:
	addi x12, x0, 19
i_72:
	srl x21, x10, x12
i_73:
	addi x14 , x14 , 1
	blt x14, x26, i_63
i_74:
	andi x24, x21, -1882
i_75:
	blt x28, x28, i_81
i_76:
	xori x3, x26, 468
i_77:
	slti x28, x18, 1537
i_78:
	xori x27, x20, 1464
i_79:
	slti x30, x18, -1951
i_80:
	nop
i_81:
	sw x24, 480(x2)
i_82:
	sw x14, 52(x2)
i_83:
	addi x9, x0, -1907
i_84:
	addi x21, x0, -1905
i_85:
	lui x24, 779176
i_86:
	sb x22, 305(x2)
i_87:
	lhu x15, 338(x2)
i_88:
	ori x30, x14, 1784
i_89:
	sh x21, -428(x2)
i_90:
	lhu x30, 396(x2)
i_91:
	slt x25, x21, x25
i_92:
	addi x9 , x9 , 1
	bge x21, x9, i_85
i_93:
	sh x6, 116(x2)
i_94:
	lbu x22, -25(x2)
i_95:
	beq x28, x29, i_97
i_96:
	sh x30, 312(x2)
i_97:
	xor x22, x31, x7
i_98:
	lh x26, -212(x2)
i_99:
	addi x11, x0, 29
i_100:
	srl x15, x15, x11
i_101:
	sltiu x22, x14, 33
i_102:
	sh x31, -38(x2)
i_103:
	add x26, x19, x25
i_104:
	sh x24, -294(x2)
i_105:
	lui x27, 293100
i_106:
	lbu x21, -204(x2)
i_107:
	bltu x12, x2, i_117
i_108:
	sh x24, 246(x2)
i_109:
	sh x18, -426(x2)
i_110:
	slti x22, x30, 1204
i_111:
	sub x22, x19, x30
i_112:
	lw x30, -12(x2)
i_113:
	lhu x30, -388(x2)
i_114:
	bne x10, x30, i_116
i_115:
	and x25, x27, x19
i_116:
	bgeu x19, x25, i_120
i_117:
	lhu x27, 128(x2)
i_118:
	sb x1, 305(x2)
i_119:
	bne x14, x1, i_122
i_120:
	slt x29, x28, x29
i_121:
	bne x20, x25, i_127
i_122:
	beq x2, x14, i_125
i_123:
	sb x30, -141(x2)
i_124:
	and x12, x5, x13
i_125:
	lbu x5, -473(x2)
i_126:
	bgeu x16, x28, i_133
i_127:
	sh x3, -306(x2)
i_128:
	lh x20, 208(x2)
i_129:
	lb x12, 141(x2)
i_130:
	xori x15, x13, 873
i_131:
	or x3, x27, x9
i_132:
	bgeu x3, x20, i_133
i_133:
	lh x1, -420(x2)
i_134:
	bne x15, x13, i_140
i_135:
	lw x21, 488(x2)
i_136:
	lbu x27, 342(x2)
i_137:
	sw x9, 440(x2)
i_138:
	addi x23, x0, 21
i_139:
	srl x11, x26, x23
i_140:
	sub x4, x29, x7
i_141:
	slti x5, x22, -135
i_142:
	addi x1, x0, 1885
i_143:
	addi x21, x0, 1889
i_144:
	addi x18, x8, 350
i_145:
	addi x1 , x1 , 1
	bne  x21, x1, i_144
i_146:
	xori x4, x8, 295
i_147:
	sh x3, 244(x2)
i_148:
	addi x1, x0, 1944
i_149:
	addi x8, x0, 1946
i_150:
	slt x3, x28, x12
i_151:
	nop
i_152:
	lh x30, 102(x2)
i_153:
	addi x21, x0, 12
i_154:
	srl x23, x14, x21
i_155:
	lh x5, -248(x2)
i_156:
	addi x1 , x1 , 1
	bgeu x8, x1, i_150
i_157:
	sb x15, 305(x2)
i_158:
	lhu x23, 186(x2)
i_159:
	slt x22, x21, x22
i_160:
	sltiu x21, x21, -314
i_161:
	lh x29, 362(x2)
i_162:
	sb x18, 82(x2)
i_163:
	addi x5, x2, 974
i_164:
	bgeu x26, x24, i_173
i_165:
	xor x26, x5, x17
i_166:
	lbu x8, -359(x2)
i_167:
	blt x7, x5, i_174
i_168:
	slt x10, x10, x18
i_169:
	auipc x23, 239519
i_170:
	andi x10, x23, -893
i_171:
	blt x13, x20, i_172
i_172:
	sw x14, -440(x2)
i_173:
	or x10, x20, x4
i_174:
	addi x30, x30, -1930
i_175:
	bge x13, x16, i_176
i_176:
	sw x15, 336(x2)
i_177:
	lhu x13, -306(x2)
i_178:
	addi x27, x0, -2026
i_179:
	addi x10, x0, -2022
i_180:
	add x11, x12, x30
i_181:
	addi x27 , x27 , 1
	bgeu x10, x27, i_180
i_182:
	sltiu x30, x4, -1217
i_183:
	add x23, x3, x3
i_184:
	lbu x27, -127(x2)
i_185:
	bne x22, x20, i_194
i_186:
	sh x8, -486(x2)
i_187:
	bltu x20, x28, i_189
i_188:
	lw x11, 112(x2)
i_189:
	lbu x25, 453(x2)
i_190:
	sltu x19, x28, x19
i_191:
	lw x6, -244(x2)
i_192:
	sltiu x6, x29, -172
i_193:
	sh x22, 424(x2)
i_194:
	lhu x18, -416(x2)
i_195:
	addi x20, x0, 8
i_196:
	srl x22, x28, x20
i_197:
	addi x9, x0, 1919
i_198:
	addi x19, x0, 1923
i_199:
	bge x14, x6, i_209
i_200:
	auipc x20, 701797
i_201:
	addi x9 , x9 , 1
	bge x19, x9, i_198
i_202:
	sw x18, 92(x2)
i_203:
	sltu x6, x20, x5
i_204:
	lbu x18, -365(x2)
i_205:
	lh x15, -426(x2)
i_206:
	lbu x11, -119(x2)
i_207:
	bltu x13, x23, i_216
i_208:
	lw x15, -244(x2)
i_209:
	lbu x1, 232(x2)
i_210:
	beq x20, x22, i_220
i_211:
	sltu x20, x12, x2
i_212:
	slt x22, x13, x20
i_213:
	ori x12, x6, -1792
i_214:
	andi x17, x5, -414
i_215:
	sb x29, 391(x2)
i_216:
	add x20, x5, x31
i_217:
	bgeu x16, x20, i_218
i_218:
	sw x11, -192(x2)
i_219:
	sh x18, 136(x2)
i_220:
	lbu x12, 232(x2)
i_221:
	lb x12, -180(x2)
i_222:
	sh x10, -112(x2)
i_223:
	sub x17, x1, x21
i_224:
	lbu x20, -306(x2)
i_225:
	lh x10, -274(x2)
i_226:
	beq x12, x14, i_233
i_227:
	blt x2, x21, i_232
i_228:
	bgeu x8, x6, i_236
i_229:
	lb x6, -238(x2)
i_230:
	addi x21, x8, -262
i_231:
	lb x5, -350(x2)
i_232:
	sw x7, 264(x2)
i_233:
	lh x11, 60(x2)
i_234:
	lhu x15, 168(x2)
i_235:
	srai x22, x13, 3
i_236:
	sw x22, -340(x2)
i_237:
	andi x22, x5, -1926
i_238:
	lbu x22, -380(x2)
i_239:
	addi x11, x0, -1952
i_240:
	addi x13, x0, -1949
i_241:
	lh x12, -334(x2)
i_242:
	addi x5, x23, -711
i_243:
	slti x6, x30, -843
i_244:
	lbu x24, -279(x2)
i_245:
	lh x25, -256(x2)
i_246:
	srai x3, x24, 3
i_247:
	lhu x30, -64(x2)
i_248:
	lb x27, -45(x2)
i_249:
	addi x11 , x11 , 1
	blt x11, x13, i_241
i_250:
	slti x26, x27, -1113
i_251:
	lhu x16, 214(x2)
i_252:
	sub x30, x31, x12
i_253:
	addi x4, x28, -669
i_254:
	sltiu x30, x30, -1263
i_255:
	sh x13, -134(x2)
i_256:
	lw x28, 136(x2)
i_257:
	auipc x21, 232582
i_258:
	bgeu x25, x28, i_263
i_259:
	andi x21, x15, 376
i_260:
	lh x28, -200(x2)
i_261:
	sw x25, 28(x2)
i_262:
	lh x19, 156(x2)
i_263:
	sw x14, -408(x2)
i_264:
	srai x17, x16, 1
i_265:
	addi x14, x0, -2001
i_266:
	addi x13, x0, -1999
i_267:
	xori x21, x13, -1734
i_268:
	nop
i_269:
	bltu x19, x2, i_274
i_270:
	add x3, x14, x16
i_271:
	addi x9, x0, 24
i_272:
	sll x24, x17, x9
i_273:
	lw x24, 312(x2)
i_274:
	sub x17, x14, x17
i_275:
	lw x24, 176(x2)
i_276:
	bltu x24, x18, i_283
i_277:
	addi x14 , x14 , 1
	bltu x14, x13, i_267
i_278:
	lbu x21, 215(x2)
i_279:
	lbu x9, -186(x2)
i_280:
	nop
i_281:
	sb x2, 0(x2)
i_282:
	sw x20, 280(x2)
i_283:
	lbu x1, -67(x2)
i_284:
	sb x7, -249(x2)
i_285:
	addi x20, x0, -2011
i_286:
	addi x17, x0, -2009
i_287:
	add x1, x14, x20
i_288:
	addi x20 , x20 , 1
	bgeu x17, x20, i_287
i_289:
	sw x1, 364(x2)
i_290:
	bltu x14, x26, i_291
i_291:
	lhu x3, -302(x2)
i_292:
	bgeu x17, x20, i_293
i_293:
	lhu x9, -16(x2)
i_294:
	xor x20, x20, x19
i_295:
	addi x8, x0, 16
i_296:
	sll x3, x7, x8
i_297:
	bne x8, x11, i_305
i_298:
	sb x8, -237(x2)
i_299:
	lhu x6, -294(x2)
i_300:
	bltu x27, x4, i_309
i_301:
	slli x1, x29, 4
i_302:
	nop
i_303:
	auipc x28, 204789
i_304:
	sw x24, 292(x2)
i_305:
	lb x30, 223(x2)
i_306:
	nop
i_307:
	lb x24, 201(x2)
i_308:
	nop
i_309:
	sub x28, x29, x29
i_310:
	lhu x10, -54(x2)
i_311:
	addi x23, x0, -2003
i_312:
	addi x1, x0, -2000
i_313:
	blt x24, x18, i_314
i_314:
	lh x15, 248(x2)
i_315:
	bne x2, x29, i_321
i_316:
	lhu x21, 478(x2)
i_317:
	addi x23 , x23 , 1
	blt x23, x1, i_313
i_318:
	lw x23, -268(x2)
i_319:
	lb x24, 88(x2)
i_320:
	xori x25, x22, 303
i_321:
	slti x23, x11, -1150
i_322:
	sh x26, -336(x2)
i_323:
	sltiu x10, x25, -851
i_324:
	and x4, x14, x30
i_325:
	bltu x10, x25, i_335
i_326:
	lh x23, -124(x2)
i_327:
	addi x13, x0, 8
i_328:
	sra x15, x30, x13
i_329:
	lw x1, -276(x2)
i_330:
	sw x3, 124(x2)
i_331:
	lb x8, -93(x2)
i_332:
	addi x26, x0, 1
i_333:
	sra x4, x12, x26
i_334:
	or x14, x7, x15
i_335:
	lh x15, -480(x2)
i_336:
	lbu x1, 117(x2)
i_337:
	lhu x30, 372(x2)
i_338:
	ori x30, x30, 1420
i_339:
	slt x24, x28, x15
i_340:
	lh x5, 108(x2)
i_341:
	bgeu x5, x30, i_342
i_342:
	lb x1, -17(x2)
i_343:
	sw x4, -172(x2)
i_344:
	sltu x1, x10, x5
i_345:
	lb x19, -41(x2)
i_346:
	lw x10, -384(x2)
i_347:
	blt x19, x7, i_348
i_348:
	bge x6, x24, i_356
i_349:
	xori x21, x21, -922
i_350:
	bgeu x22, x24, i_351
i_351:
	and x10, x10, x18
i_352:
	sh x28, -180(x2)
i_353:
	lw x19, -312(x2)
i_354:
	bltu x26, x16, i_359
i_355:
	lbu x24, -265(x2)
i_356:
	bge x17, x10, i_366
i_357:
	blt x8, x24, i_361
i_358:
	and x19, x24, x22
i_359:
	andi x18, x24, 803
i_360:
	lbu x14, 382(x2)
i_361:
	lbu x6, -233(x2)
i_362:
	sw x1, -176(x2)
i_363:
	lhu x24, 210(x2)
i_364:
	lh x14, 62(x2)
i_365:
	slli x14, x2, 4
i_366:
	lb x9, 321(x2)
i_367:
	bgeu x25, x17, i_368
i_368:
	beq x9, x24, i_375
i_369:
	sw x24, -164(x2)
i_370:
	sub x16, x12, x14
i_371:
	lb x4, 46(x2)
i_372:
	sh x6, 418(x2)
i_373:
	bltu x12, x6, i_377
i_374:
	lh x13, -82(x2)
i_375:
	lb x18, 322(x2)
i_376:
	addi x4, x24, 1865
i_377:
	lh x29, -28(x2)
i_378:
	addi x29, x2, -192
i_379:
	sb x19, 152(x2)
i_380:
	beq x29, x23, i_381
i_381:
	sh x31, 28(x2)
i_382:
	sh x19, -362(x2)
i_383:
	lhu x17, 228(x2)
i_384:
	slt x18, x5, x17
i_385:
	nop
i_386:
	addi x6, x0, 1969
i_387:
	addi x22, x0, 1972
i_388:
	lbu x5, 73(x2)
i_389:
	xor x16, x6, x11
i_390:
	sltu x19, x6, x3
i_391:
	slli x13, x11, 1
i_392:
	lbu x18, -468(x2)
i_393:
	sltu x18, x9, x9
i_394:
	sw x8, -208(x2)
i_395:
	sh x13, 400(x2)
i_396:
	sb x12, 473(x2)
i_397:
	addi x6 , x6 , 1
	bge x22, x6, i_388
i_398:
	lhu x18, -166(x2)
i_399:
	sh x18, 160(x2)
i_400:
	addi x23, x0, 25
i_401:
	srl x1, x31, x23
i_402:
	sw x23, -336(x2)
i_403:
	lhu x18, 104(x2)
i_404:
	sh x18, -226(x2)
i_405:
	lw x23, 272(x2)
i_406:
	ori x23, x23, 1891
i_407:
	bgeu x7, x27, i_408
i_408:
	sh x11, -404(x2)
i_409:
	lh x21, -484(x2)
i_410:
	bgeu x9, x23, i_415
i_411:
	sb x28, 298(x2)
i_412:
	and x28, x13, x30
i_413:
	srai x27, x2, 2
i_414:
	lb x23, 220(x2)
i_415:
	lhu x28, -4(x2)
i_416:
	sh x31, 418(x2)
i_417:
	srai x21, x31, 2
i_418:
	bltu x3, x28, i_423
i_419:
	bne x28, x12, i_424
i_420:
	bne x26, x14, i_423
i_421:
	lb x25, -372(x2)
i_422:
	lb x28, 21(x2)
i_423:
	lh x21, -278(x2)
i_424:
	lbu x9, -450(x2)
i_425:
	auipc x9, 530958
i_426:
	addi x17, x0, 1937
i_427:
	addi x18, x0, 1941
i_428:
	lui x9, 36779
i_429:
	slti x21, x21, -704
i_430:
	addi x17 , x17 , 1
	blt x17, x18, i_428
i_431:
	blt x17, x29, i_433
i_432:
	lhu x4, -236(x2)
i_433:
	bne x29, x6, i_441
i_434:
	sb x29, 215(x2)
i_435:
	lbu x14, -162(x2)
i_436:
	sltiu x6, x6, -968
i_437:
	sh x30, -44(x2)
i_438:
	lbu x25, -351(x2)
i_439:
	sw x22, 472(x2)
i_440:
	lh x30, 214(x2)
i_441:
	nop
i_442:
	and x23, x17, x11
i_443:
	addi x11, x0, -1845
i_444:
	addi x30, x0, -1842
i_445:
	nop
i_446:
	slli x23, x23, 3
i_447:
	addi x11 , x11 , 1
	bge x30, x11, i_445
i_448:
	bgeu x17, x17, i_451
i_449:
	sh x19, -208(x2)
i_450:
	sltu x23, x4, x23
i_451:
	lw x23, 184(x2)
i_452:
	sb x3, -106(x2)
i_453:
	blt x3, x23, i_454
i_454:
	sltiu x23, x13, 474
i_455:
	add x6, x20, x19
i_456:
	lbu x19, -455(x2)
i_457:
	sh x4, -250(x2)
i_458:
	and x17, x28, x16
i_459:
	sw x6, -160(x2)
i_460:
	lw x5, 36(x2)
i_461:
	lh x19, 168(x2)
i_462:
	sh x4, 460(x2)
i_463:
	bltu x19, x16, i_466
i_464:
	blt x30, x19, i_465
i_465:
	sltiu x9, x8, -1940
i_466:
	lb x4, 44(x2)
i_467:
	addi x15, x28, 1026
i_468:
	or x15, x15, x9
i_469:
	sb x16, 381(x2)
i_470:
	bge x21, x17, i_473
i_471:
	bne x27, x29, i_479
i_472:
	sw x12, -284(x2)
i_473:
	lh x15, 190(x2)
i_474:
	lh x9, -44(x2)
i_475:
	lui x27, 759637
i_476:
	lb x15, -344(x2)
i_477:
	add x20, x2, x29
i_478:
	sb x24, 128(x2)
i_479:
	lh x22, -320(x2)
i_480:
	sltu x19, x27, x8
i_481:
	sw x18, -384(x2)
i_482:
	lw x18, -232(x2)
i_483:
	addi x28, x0, 17
i_484:
	sll x24, x19, x28
i_485:
	lbu x12, -79(x2)
i_486:
	sub x23, x20, x19
i_487:
	sh x20, 84(x2)
i_488:
	xor x1, x31, x19
i_489:
	auipc x15, 262857
i_490:
	andi x15, x15, -409
i_491:
	lw x15, 396(x2)
i_492:
	lb x19, 452(x2)
i_493:
	ori x13, x22, -1928
i_494:
	lbu x13, -127(x2)
i_495:
	lbu x29, 414(x2)
i_496:
	slli x28, x22, 4
i_497:
	bge x28, x25, i_498
i_498:
	lh x25, 174(x2)
i_499:
	lhu x15, 126(x2)
i_500:
	lb x25, -463(x2)
i_501:
	bgeu x9, x6, i_510
i_502:
	bltu x3, x15, i_512
i_503:
	lw x17, 260(x2)
i_504:
	lw x9, 156(x2)
i_505:
	sh x31, 190(x2)
i_506:
	beq x9, x18, i_509
i_507:
	sw x9, -324(x2)
i_508:
	add x23, x30, x1
i_509:
	sh x8, -478(x2)
i_510:
	or x28, x9, x22
i_511:
	sh x9, 320(x2)
i_512:
	beq x2, x10, i_515
i_513:
	lb x29, 449(x2)
i_514:
	lhu x15, 420(x2)
i_515:
	xor x17, x28, x25
i_516:
	bltu x6, x25, i_524
i_517:
	and x25, x13, x29
i_518:
	sh x31, -84(x2)
i_519:
	lb x9, 471(x2)
i_520:
	slti x29, x14, -592
i_521:
	lbu x3, 46(x2)
i_522:
	sb x6, 486(x2)
i_523:
	lb x6, 159(x2)
i_524:
	bltu x29, x23, i_533
i_525:
	xori x23, x23, 1996
i_526:
	lw x23, 296(x2)
i_527:
	lhu x23, -296(x2)
i_528:
	blt x3, x6, i_529
i_529:
	lhu x6, 150(x2)
i_530:
	addi x10, x0, 6
i_531:
	srl x26, x15, x10
i_532:
	srai x20, x26, 3
i_533:
	bgeu x4, x8, i_541
i_534:
	lb x27, 322(x2)
i_535:
	lhu x11, -194(x2)
i_536:
	lbu x11, -179(x2)
i_537:
	sub x23, x12, x29
i_538:
	addi x16, x0, 9
i_539:
	sra x26, x8, x16
i_540:
	lb x27, -228(x2)
i_541:
	lbu x19, 438(x2)
i_542:
	sh x12, -192(x2)
i_543:
	addi x13, x0, -1913
i_544:
	addi x9, x0, -1909
i_545:
	auipc x26, 1514
i_546:
	addi x13 , x13 , 1
	bne x13, x9, i_545
i_547:
	bge x3, x5, i_555
i_548:
	slt x19, x2, x26
i_549:
	lw x29, -224(x2)
i_550:
	lh x27, -458(x2)
i_551:
	lh x18, 154(x2)
i_552:
	lbu x21, 160(x2)
i_553:
	lhu x25, -432(x2)
i_554:
	bne x8, x21, i_563
i_555:
	sb x30, -455(x2)
i_556:
	lb x18, -12(x2)
i_557:
	auipc x14, 888175
i_558:
	lhu x30, -434(x2)
i_559:
	lw x27, -232(x2)
i_560:
	lh x26, -52(x2)
i_561:
	lw x24, -424(x2)
i_562:
	sw x19, -396(x2)
i_563:
	bge x12, x27, i_566
i_564:
	xor x12, x19, x14
i_565:
	or x6, x28, x6
i_566:
	lb x6, 215(x2)
i_567:
	slli x29, x12, 3
i_568:
	lbu x11, -219(x2)
i_569:
	addi x24, x0, 29
i_570:
	sll x21, x24, x24
i_571:
	sb x28, 268(x2)
i_572:
	lw x29, -108(x2)
i_573:
	lh x29, 54(x2)
i_574:
	sw x1, -88(x2)
i_575:
	and x16, x11, x28
i_576:
	lhu x28, -178(x2)
i_577:
	sltiu x23, x13, -585
i_578:
	lw x29, 300(x2)
i_579:
	and x9, x5, x14
i_580:
	lb x22, 357(x2)
i_581:
	lb x18, 340(x2)
i_582:
	sh x18, 246(x2)
i_583:
	sb x2, -34(x2)
i_584:
	sltu x22, x18, x22
i_585:
	lh x13, -160(x2)
i_586:
	auipc x4, 389621
i_587:
	addi x3, x0, 26
i_588:
	sra x5, x5, x3
i_589:
	addi x22, x0, -1902
i_590:
	addi x13, x0, -1900
i_591:
	slli x20, x4, 3
i_592:
	xor x23, x24, x24
i_593:
	sb x11, 37(x2)
i_594:
	addi x22 , x22 , 1
	blt x22, x13, i_590
i_595:
	lb x4, 103(x2)
i_596:
	sh x20, 184(x2)
i_597:
	sltu x8, x16, x22
i_598:
	lb x16, -289(x2)
i_599:
	addi x14, x0, -2015
i_600:
	addi x28, x0, -2012
i_601:
	lh x15, 178(x2)
i_602:
	lb x3, -77(x2)
i_603:
	or x16, x3, x19
i_604:
	addi x14 , x14 , 1
	bne  x28, x14, i_601
i_605:
	srai x3, x31, 2
i_606:
	lh x22, 52(x2)
i_607:
	sub x8, x8, x22
i_608:
	sw x22, 64(x2)
i_609:
	addi x13, x0, 5
i_610:
	sra x15, x30, x13
i_611:
	lw x3, -208(x2)
i_612:
	bltu x10, x3, i_614
i_613:
	nop
i_614:
	lhu x15, -80(x2)
i_615:
	sh x8, -164(x2)
i_616:
	addi x16, x0, 1840
i_617:
	addi x24, x0, 1842
i_618:
	lhu x13, -172(x2)
i_619:
	blt x5, x22, i_623
i_620:
	lb x19, 347(x2)
i_621:
	lbu x9, -437(x2)
i_622:
	sw x17, -440(x2)
i_623:
	slti x30, x28, 1365
i_624:
	nop
i_625:
	lhu x29, 184(x2)
i_626:
	addi x16 , x16 , 1
	bltu x16, x24, i_618
i_627:
	slti x5, x19, 25
i_628:
	slt x17, x1, x11
i_629:
	andi x17, x7, 873
i_630:
	sh x19, 348(x2)
i_631:
	lhu x29, 0(x2)
i_632:
	lhu x9, -120(x2)
i_633:
	addi x26, x0, 24
i_634:
	sll x1, x5, x26
i_635:
	and x9, x20, x26
i_636:
	bge x1, x1, i_637
i_637:
	auipc x9, 103920
i_638:
	sltiu x9, x13, 1292
i_639:
	sb x1, -347(x2)
i_640:
	addi x13, x0, 5
i_641:
	sll x3, x27, x13
i_642:
	lbu x9, -430(x2)
i_643:
	addi x19, x0, 17
i_644:
	sll x4, x20, x19
i_645:
	lh x4, 40(x2)
i_646:
	lb x14, -38(x2)
i_647:
	lbu x4, 229(x2)
i_648:
	lh x21, 308(x2)
i_649:
	addi x29, x0, 28
i_650:
	sll x19, x3, x29
i_651:
	lbu x6, 485(x2)
i_652:
	lb x15, 175(x2)
i_653:
	blt x8, x29, i_657
i_654:
	lbu x19, -86(x2)
i_655:
	bltu x3, x22, i_661
i_656:
	lh x29, 146(x2)
i_657:
	srli x19, x22, 4
i_658:
	add x6, x30, x2
i_659:
	lw x19, -288(x2)
i_660:
	lb x21, -468(x2)
i_661:
	lbu x1, 255(x2)
i_662:
	bne x23, x10, i_668
i_663:
	beq x30, x13, i_666
i_664:
	lb x26, -193(x2)
i_665:
	nop
i_666:
	slti x25, x25, 406
i_667:
	sub x22, x31, x30
i_668:
	nop
i_669:
	lhu x12, 478(x2)
i_670:
	addi x4, x0, -1902
i_671:
	addi x13, x0, -1900
i_672:
	sb x13, -123(x2)
i_673:
	lhu x22, -462(x2)
i_674:
	lh x12, -162(x2)
i_675:
	sw x25, 28(x2)
i_676:
	or x30, x12, x13
i_677:
	lh x11, -196(x2)
i_678:
	sb x11, 392(x2)
i_679:
	addi x27, x0, 28
i_680:
	sra x8, x12, x27
i_681:
	addi x4 , x4 , 1
	blt x4, x13, i_672
i_682:
	sb x19, 67(x2)
i_683:
	sb x7, 209(x2)
i_684:
	lbu x3, -132(x2)
i_685:
	bltu x4, x8, i_688
i_686:
	xor x26, x6, x15
i_687:
	or x27, x12, x8
i_688:
	sw x6, -400(x2)
i_689:
	srli x21, x18, 1
i_690:
	sh x24, 470(x2)
i_691:
	sub x19, x17, x11
i_692:
	lh x5, -434(x2)
i_693:
	sb x25, 398(x2)
i_694:
	lh x28, 106(x2)
i_695:
	srai x27, x18, 4
i_696:
	lbu x21, 316(x2)
i_697:
	sw x22, 388(x2)
i_698:
	sw x27, -192(x2)
i_699:
	xor x21, x23, x27
i_700:
	add x27, x19, x21
i_701:
	sltiu x28, x19, -1733
i_702:
	beq x13, x20, i_704
i_703:
	lhu x19, -338(x2)
i_704:
	lb x20, -400(x2)
i_705:
	sh x22, -210(x2)
i_706:
	bltu x4, x29, i_712
i_707:
	srai x20, x21, 4
i_708:
	slli x11, x12, 3
i_709:
	slli x29, x11, 2
i_710:
	sh x6, -322(x2)
i_711:
	sub x26, x17, x11
i_712:
	sw x28, -384(x2)
i_713:
	nop
i_714:
	addi x11, x0, -2034
i_715:
	addi x5, x0, -2031
i_716:
	nop
i_717:
	lbu x28, -183(x2)
i_718:
	addi x18, x0, -1995
i_719:
	addi x19, x0, -1993
i_720:
	ori x27, x4, -1818
i_721:
	lbu x6, -276(x2)
i_722:
	addi x18 , x18 , 1
	blt x18, x19, i_720
i_723:
	srai x20, x30, 4
i_724:
	lhu x30, -470(x2)
i_725:
	srai x15, x2, 1
i_726:
	addi x11 , x11 , 1
	bltu x11, x5, i_716
i_727:
	and x30, x26, x29
i_728:
	sh x4, -342(x2)
i_729:
	slt x4, x30, x24
i_730:
	sltu x6, x10, x31
i_731:
	sh x10, 356(x2)
i_732:
	or x8, x15, x4
i_733:
	sh x31, -44(x2)
i_734:
	sltu x28, x30, x19
i_735:
	lhu x14, 302(x2)
i_736:
	lb x3, 12(x2)
i_737:
	lh x14, -94(x2)
i_738:
	sw x12, -296(x2)
i_739:
	lb x20, 152(x2)
i_740:
	srai x27, x6, 4
i_741:
	or x12, x11, x22
i_742:
	blt x17, x11, i_749
i_743:
	lb x12, -460(x2)
i_744:
	lh x17, 202(x2)
i_745:
	lhu x27, 456(x2)
i_746:
	bltu x12, x30, i_756
i_747:
	lw x10, 212(x2)
i_748:
	lw x27, 196(x2)
i_749:
	srli x28, x19, 2
i_750:
	addi x27, x0, 18
i_751:
	srl x20, x2, x27
i_752:
	lhu x3, -204(x2)
i_753:
	addi x28, x0, 2
i_754:
	sra x19, x14, x28
i_755:
	lui x11, 202401
i_756:
	nop
i_757:
	nop
i_758:
	addi x29, x0, 1939
i_759:
	addi x11, x0, 1943
i_760:
	add x24, x21, x28
i_761:
	ori x25, x30, -1632
i_762:
	lh x25, -386(x2)
i_763:
	ori x3, x6, -1433
i_764:
	nop
i_765:
	sb x20, -51(x2)
i_766:
	addi x29 , x29 , 1
	bltu x29, x11, i_760
i_767:
	lbu x6, 106(x2)
i_768:
	lw x1, 456(x2)
i_769:
	lb x16, 104(x2)
i_770:
	addi x21, x0, -1979
i_771:
	addi x29, x0, -1976
i_772:
	lui x30, 216707
i_773:
	addi x21 , x21 , 1
	bltu x21, x29, i_772
i_774:
	sw x21, -372(x2)
i_775:
	andi x27, x9, -936
i_776:
	sb x29, 282(x2)
i_777:
	lh x14, -446(x2)
i_778:
	lw x23, -56(x2)
i_779:
	slt x27, x28, x27
i_780:
	lui x27, 554298
i_781:
	xor x12, x16, x30
i_782:
	lhu x27, 318(x2)
i_783:
	and x10, x10, x14
i_784:
	beq x16, x27, i_787
i_785:
	lh x28, 316(x2)
i_786:
	lh x11, -356(x2)
i_787:
	sh x19, -180(x2)
i_788:
	sb x21, -305(x2)
i_789:
	addi x10, x0, 1946
i_790:
	addi x27, x0, 1950
i_791:
	addi x12, x29, -1244
i_792:
	lb x19, -98(x2)
i_793:
	slt x29, x16, x4
i_794:
	lb x18, 66(x2)
i_795:
	nop
i_796:
	addi x10 , x10 , 1
	bltu x10, x27, i_791
i_797:
	lb x3, -462(x2)
i_798:
	lbu x17, -478(x2)
i_799:
	lbu x24, -430(x2)
i_800:
	sltiu x10, x19, -179
i_801:
	sw x24, -312(x2)
i_802:
	sb x15, 345(x2)
i_803:
	bne x9, x29, i_810
i_804:
	slli x19, x8, 3
i_805:
	lh x14, 402(x2)
i_806:
	beq x29, x28, i_815
i_807:
	slt x23, x3, x10
i_808:
	sb x6, 398(x2)
i_809:
	addi x17, x0, 29
i_810:
	sra x23, x6, x17
i_811:
	sw x21, -44(x2)
i_812:
	lhu x23, 406(x2)
i_813:
	lhu x29, -416(x2)
i_814:
	lh x15, -386(x2)
i_815:
	lui x10, 503855
i_816:
	or x5, x5, x10
i_817:
	sb x10, 200(x2)
i_818:
	lhu x10, 304(x2)
i_819:
	bgeu x31, x28, i_822
i_820:
	lw x10, 300(x2)
i_821:
	beq x15, x10, i_828
i_822:
	addi x3, x2, -1761
i_823:
	lh x3, 352(x2)
i_824:
	sh x27, 88(x2)
i_825:
	slti x3, x31, 1828
i_826:
	add x27, x26, x27
i_827:
	sh x27, 54(x2)
i_828:
	lhu x27, 120(x2)
i_829:
	sb x14, 208(x2)
i_830:
	or x25, x22, x6
i_831:
	bltu x5, x4, i_838
i_832:
	sh x25, -334(x2)
i_833:
	bge x2, x9, i_835
i_834:
	sh x11, -176(x2)
i_835:
	sw x29, 180(x2)
i_836:
	sw x28, -112(x2)
i_837:
	or x27, x20, x3
i_838:
	lh x6, -298(x2)
i_839:
	xori x3, x20, -1297
i_840:
	lbu x20, 413(x2)
i_841:
	addi x27, x0, 17
i_842:
	sra x19, x31, x27
i_843:
	lh x19, 392(x2)
i_844:
	bne x3, x20, i_848
i_845:
	lh x19, -130(x2)
i_846:
	lh x5, -198(x2)
i_847:
	sb x17, -153(x2)
i_848:
	sub x19, x15, x26
i_849:
	lh x8, 358(x2)
i_850:
	bne x30, x24, i_856
i_851:
	andi x11, x2, -858
i_852:
	addi x8, x0, 31
i_853:
	sra x11, x16, x8
i_854:
	lh x24, 396(x2)
i_855:
	and x24, x19, x10
i_856:
	bne x19, x24, i_861
i_857:
	sw x29, 240(x2)
i_858:
	xor x19, x2, x16
i_859:
	or x23, x19, x19
i_860:
	sw x11, 192(x2)
i_861:
	sw x1, 396(x2)
i_862:
	and x6, x24, x19
i_863:
	sltiu x19, x29, 1920
i_864:
	lhu x30, 384(x2)
i_865:
	sh x27, -466(x2)
i_866:
	addi x17, x0, 2031
i_867:
	addi x24, x0, 2033
i_868:
	addi x17 , x17 , 1
	bne x17, x24, i_868
i_869:
	srli x24, x31, 3
i_870:
	lw x30, -24(x2)
i_871:
	sltu x24, x13, x25
i_872:
	srai x1, x4, 4
i_873:
	bne x17, x6, i_878
i_874:
	lh x11, 376(x2)
i_875:
	sltiu x5, x13, 1284
i_876:
	lbu x25, 314(x2)
i_877:
	srli x13, x29, 4
i_878:
	addi x19, x0, 30
i_879:
	srl x19, x10, x19
i_880:
	lh x18, -258(x2)
i_881:
	sw x4, -140(x2)
i_882:
	bltu x5, x19, i_891
i_883:
	sb x17, -351(x2)
i_884:
	addi x21, x0, 17
i_885:
	sll x12, x1, x21
i_886:
	lhu x13, -426(x2)
i_887:
	sh x29, 418(x2)
i_888:
	sltiu x18, x28, 1611
i_889:
	beq x18, x4, i_893
i_890:
	lw x18, 28(x2)
i_891:
	add x17, x3, x17
i_892:
	srai x21, x19, 4
i_893:
	lhu x13, -340(x2)
i_894:
	nop
i_895:
	addi x29, x0, 1949
i_896:
	addi x4, x0, 1952
i_897:
	addi x30, x26, -1609
i_898:
	xori x21, x30, -1501
i_899:
	or x11, x4, x11
i_900:
	sh x12, -466(x2)
i_901:
	andi x17, x17, -1361
i_902:
	srai x25, x11, 4
i_903:
	nop
i_904:
	addi x29 , x29 , 1
	bgeu x4, x29, i_897
i_905:
	sh x10, 36(x2)
i_906:
	bne x11, x21, i_914
i_907:
	beq x12, x3, i_914
i_908:
	sh x4, 370(x2)
i_909:
	ori x12, x6, -27
i_910:
	lbu x9, -98(x2)
i_911:
	bge x12, x14, i_918
i_912:
	slti x3, x26, 15
i_913:
	bgeu x24, x18, i_915
i_914:
	sh x9, -304(x2)
i_915:
	nop
i_916:
	add x28, x27, x15
i_917:
	srli x3, x6, 3
i_918:
	nop
i_919:
	lh x28, 416(x2)
i_920:
	addi x29, x0, 1916
i_921:
	addi x9, x0, 1919
i_922:
	addi x29 , x29 , 1
	bge x9, x29, i_922
i_923:
	xori x6, x20, 325
i_924:
	sb x4, -1(x2)
i_925:
	beq x19, x2, i_935
i_926:
	sb x2, -398(x2)
i_927:
	lb x11, -73(x2)
i_928:
	slt x23, x31, x30
i_929:
	lh x13, 232(x2)
i_930:
	beq x10, x22, i_939
i_931:
	lui x23, 542458
i_932:
	addi x6, x11, -1981
i_933:
	lw x10, 424(x2)
i_934:
	auipc x23, 961004
i_935:
	sltu x22, x4, x17
i_936:
	lw x4, 380(x2)
i_937:
	lb x26, 413(x2)
i_938:
	xori x18, x30, 44
i_939:
	bgeu x26, x12, i_942
i_940:
	xor x24, x19, x22
i_941:
	bge x2, x18, i_947
i_942:
	xor x25, x10, x22
i_943:
	ori x22, x21, -910
i_944:
	sw x17, -320(x2)
i_945:
	lw x21, 428(x2)
i_946:
	sub x23, x21, x14
i_947:
	lh x27, 80(x2)
i_948:
	sw x3, 420(x2)
i_949:
	lbu x21, 54(x2)
i_950:
	lb x3, 447(x2)
i_951:
	addi x29, x0, 18
i_952:
	sll x3, x6, x29
i_953:
	lhu x3, 34(x2)
i_954:
	xor x15, x21, x15
i_955:
	add x15, x30, x18
i_956:
	lbu x10, -128(x2)
i_957:
	lw x27, -484(x2)
i_958:
	andi x1, x23, -1968
i_959:
	addi x26, x0, 2
i_960:
	sll x15, x9, x26
i_961:
	lb x23, -418(x2)
i_962:
	bge x28, x9, i_966
i_963:
	addi x23, x0, 16
i_964:
	srl x23, x18, x23
i_965:
	lb x30, 144(x2)
i_966:
	sh x14, 456(x2)
i_967:
	beq x29, x23, i_971
i_968:
	slti x23, x23, -1547
i_969:
	slli x9, x26, 4
i_970:
	lhu x26, 372(x2)
i_971:
	lb x16, -418(x2)
i_972:
	auipc x16, 790383
i_973:
	bltu x23, x13, i_978
i_974:
	lhu x4, -120(x2)
i_975:
	lw x4, 320(x2)
i_976:
	blt x30, x24, i_977
i_977:
	and x6, x16, x17
i_978:
	addi x16, x0, 4
i_979:
	sra x24, x24, x16
i_980:
	lhu x24, 288(x2)
i_981:
	and x19, x12, x19
i_982:
	beq x7, x6, i_989
i_983:
	lw x29, -120(x2)
i_984:
	beq x31, x17, i_988
i_985:
	lbu x20, -274(x2)
i_986:
	bge x21, x5, i_988
i_987:
	andi x15, x26, 85
i_988:
	lw x23, -252(x2)
i_989:
	add x24, x4, x30
i_990:
	sh x31, -268(x2)
i_991:
	lh x20, 392(x2)
i_992:
	lw x29, 416(x2)
i_993:
	lbu x3, -387(x2)
i_994:
	lbu x14, 360(x2)
i_995:
	sh x26, -360(x2)
i_996:
	lb x24, 487(x2)
i_997:
	sb x18, 181(x2)
i_998:
	sw x18, 120(x2)
i_999:
	addi x27, x10, -1099
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0x215a910a
	.word 0x3b6db878
	.word 0x38d4312
	.word 0xe43a324c
	.word 0xa61d532b
	.word 0xce2828cc
	.word 0xab420d33
	.word 0xa37a67e5
	.word 0x3a82bb12
	.word 0x190b1a46
	.word 0x27d2a828
	.word 0xf9e445ca
	.word 0x9983c8d5
	.word 0x9d7bb90d
	.word 0x9edff056
	.word 0x267ac028
	.word 0x2717adf5
	.word 0x40621006
	.word 0xe2dde4b2
	.word 0x7b48f3c1
	.word 0xa1b8e2f3
	.word 0xc3cbc047
	.word 0xa1afa22d
	.word 0x2fdd3f01
	.word 0x9f98f5b6
	.word 0xdf2b0805
	.word 0x6550a5e8
	.word 0xc23cb129
	.word 0x253ebc51
	.word 0x64a469b4
	.word 0x27b7ae
	.word 0xc14eb791
	.word 0xffd92e4
	.word 0xdb1cd65e
	.word 0x49192c74
	.word 0xb76b318d
	.word 0x8dcef711
	.word 0xd4c3ff82
	.word 0x12161763
	.word 0x8f747ee1
	.word 0x3752c996
	.word 0x98677480
	.word 0x5403b2ad
	.word 0x4739f59c
	.word 0x2c88ed72
	.word 0x2584edeb
	.word 0xfa9589d2
	.word 0x58b88df8
	.word 0x6dc126b3
	.word 0x421df751
	.word 0xba9d781c
	.word 0x62824d06
	.word 0x8e83f135
	.word 0x32a308bc
	.word 0x93c704db
	.word 0xc415342e
	.word 0xae9bd9a4
	.word 0xcaaeed9d
	.word 0xd5311f0a
	.word 0x8a53ebfc
	.word 0x17912b67
	.word 0x4e036319
	.word 0x4f70e1a5
	.word 0xa37efa77
	.word 0xe434d559
	.word 0x856cf478
	.word 0xcbc9fc47
	.word 0xc9005eb5
	.word 0x647f956e
	.word 0x269e41f8
	.word 0x28f5cdea
	.word 0x8172306d
	.word 0xf04e8949
	.word 0x33f969e6
	.word 0x5ee6d1c4
	.word 0x14831042
	.word 0x431812b3
	.word 0x514f4beb
	.word 0xa53cd418
	.word 0xcab49435
	.word 0xe72bdb54
	.word 0xe2582a1a
	.word 0x32fa8b3a
	.word 0x2eed7d01
	.word 0xe1d61146
	.word 0xf8248020
	.word 0x97955934
	.word 0xb34cf046
	.word 0xbe79e018
	.word 0xf8bc2723
	.word 0x750d3f8d
	.word 0x89f106f7
	.word 0xfe21ab5c
	.word 0x83c78929
	.word 0x9f931e8d
	.word 0x608ac554
	.word 0x3a7a5f61
	.word 0x112b4ae
	.word 0x65a8080c
	.word 0x53687c34
	.word 0xb6d06ee5
	.word 0x23f9d752
	.word 0x1331f61b
	.word 0xa8d1bd14
	.word 0x43d67f3b
	.word 0xc6d6c370
	.word 0xc336d8b6
	.word 0x73776ca0
	.word 0x166bbc49
	.word 0x8c3e501a
	.word 0xabd21720
	.word 0xc5636f1b
	.word 0xd845ffaa
	.word 0x503ab6a8
	.word 0xba500f07
	.word 0x2a39a902
	.word 0xa7ed353c
	.word 0x3f77e9fd
	.word 0xc04db3be
	.word 0x5ef73cc6
	.word 0x8bc69167
	.word 0xdd42c8b9
	.word 0xa1500be8
	.word 0x34e89e34
	.word 0xf6d33817
	.word 0xc8b428aa
	.word 0xd93f56a9
	.word 0xe5fdb871
	.word 0xc807d9ec
	.word 0x996830f
	.word 0x5ad33e13
	.word 0xcd0f947
	.word 0xab1eea15
	.word 0x36d5e3e2
	.word 0xc808c8ba
	.word 0x2c4566e6
	.word 0xf9a39d4c
	.word 0xdcc93e30
	.word 0x2a26ce89
	.word 0x5e9086b3
	.word 0xd3407c57
	.word 0x3f5f8d85
	.word 0x9837cec5
	.word 0xd2e9a741
	.word 0x5887aea2
	.word 0x84d4b97c
	.word 0x41cdadbe
	.word 0xfa2b3111
	.word 0xc9affc98
	.word 0x51fa4551
	.word 0xcec0d68c
	.word 0x6d69d7a1
	.word 0xdef8346
	.word 0x8b9c2eb5
	.word 0x3b1255db
	.word 0x5ccafc82
	.word 0xbdcf77cf
	.word 0x889ce0c7
	.word 0x3e73ccd8
	.word 0xbe33f07f
	.word 0x7da9d51f
	.word 0x8007d585
	.word 0x5fde6809
	.word 0x2ef9b635
	.word 0x690d631e
	.word 0x1fba0f4a
	.word 0x3a3aa2c3
	.word 0x31dfc5fd
	.word 0xe8bbe4d0
	.word 0xacddfa97
	.word 0x86599e7b
	.word 0x327cca17
	.word 0x883e6f04
	.word 0x64d9fd93
	.word 0x83e753b1
	.word 0x8ab4995b
	.word 0x6ebf35ae
	.word 0xc243765c
	.word 0x3dc050b6
	.word 0x6de656d2
	.word 0xdca3b2b3
	.word 0x216faa0e
	.word 0xc8e49c51
	.word 0xc5781070
	.word 0xce757449
	.word 0x3a4f7e33
	.word 0xbe96b8a9
	.word 0x8aed7f03
	.word 0x731ff938
	.word 0xa5c78e90
	.word 0x4b3c6f3d
	.word 0x3f04728d
	.word 0x20a1a487
	.word 0x15b46fec
	.word 0x61a24bb8
	.word 0xbf2dca69
	.word 0xb61bcbc2
	.word 0x39ef97a7
	.word 0xc335bb50
	.word 0x17d697e4
	.word 0x78bb2e4c
	.word 0x984d3bae
	.word 0x81b4dc90
	.word 0xa770d853
	.word 0xba4cfda3
	.word 0x4651ac81
	.word 0x8d56110
	.word 0xadf00eb6
	.word 0x557c99a3
	.word 0x5d642a7a
	.word 0x742e10ca
	.word 0xaabf76f6
	.word 0x94d70681
	.word 0x660c623a
	.word 0x1784c5b1
	.word 0x2da491a0
	.word 0x511a6a4a
	.word 0x64a80cd9
	.word 0xa48691d9
	.word 0x67f9c49f
	.word 0xf93df18
	.word 0xa465dece
	.word 0x57d0b17e
	.word 0xa6af90f
	.word 0x4c0cd9a9
	.word 0x14da9162
	.word 0xe8a07010
	.word 0x5c9d5056
	.word 0xa487cfe6
	.word 0x668a69e9
	.word 0x30b0f66e
	.word 0xca096670
	.word 0x94bf0118
	.word 0x986f3b29
	.word 0xf8628bd3
	.word 0x48dc761d
	.word 0x6ab23552
	.word 0x19ce22b5
	.word 0x41571e88
	.word 0x2ffbb477
	.word 0x266057c6
	.word 0x4e57a994
	.word 0x492e83bf
	.word 0xf26a9f61
	.word 0xe2f672a5
	.word 0x10910dc6
	.word 0xc5f30ade
	.word 0x258ae585
	.word 0x9c5b4abf
	.word 0x390afe0
	.word 0x1e46c1a8
	.word 0x24dc280a
	.word 0x7a0427d9
	.word 0x5c42a2a1
	.word 0xa7d0aae
	.word 0xde49b15c
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
