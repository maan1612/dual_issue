
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	sltiu x27, x30, 660
i_1:
	and x6, x16, x26
i_2:
	xor x30, x17, x26
i_3:
	slti x1, x1, -1110
i_4:
	lhu x11, -64(x2)
i_5:
	blt x14, x30, i_14
i_6:
	sw x26, -336(x2)
i_7:
	bne x30, x14, i_10
i_8:
	sb x18, 87(x2)
i_9:
	lw x29, 176(x2)
i_10:
	xori x19, x27, -1468
i_11:
	sh x19, 348(x2)
i_12:
	lb x1, 231(x2)
i_13:
	sb x19, 456(x2)
i_14:
	lh x30, -286(x2)
i_15:
	auipc x26, 108460
i_16:
	sb x27, 211(x2)
i_17:
	slt x11, x23, x13
i_18:
	and x4, x30, x26
i_19:
	auipc x4, 696373
i_20:
	ori x22, x19, -791
i_21:
	lbu x25, 283(x2)
i_22:
	srli x15, x15, 2
i_23:
	sh x7, 438(x2)
i_24:
	lui x16, 130387
i_25:
	or x29, x22, x21
i_26:
	nop
i_27:
	and x29, x11, x17
i_28:
	addi x17, x0, -1873
i_29:
	addi x25, x0, -1869
i_30:
	srli x29, x15, 3
i_31:
	nop
i_32:
	sw x11, 112(x2)
i_33:
	sb x26, 106(x2)
i_34:
	bne x21, x29, i_44
i_35:
	lb x11, -101(x2)
i_36:
	and x3, x28, x31
i_37:
	addi x17 , x17 , 1
	bne x17, x25, i_30
i_38:
	sw x19, 72(x2)
i_39:
	sw x15, -196(x2)
i_40:
	lhu x25, -412(x2)
i_41:
	xori x3, x30, -1833
i_42:
	sltu x29, x4, x16
i_43:
	slti x26, x11, 1535
i_44:
	nop
i_45:
	nop
i_46:
	addi x4, x0, 2039
i_47:
	addi x1, x0, 2043
i_48:
	lhu x17, -186(x2)
i_49:
	addi x21, x0, 19
i_50:
	sll x9, x12, x21
i_51:
	addi x4 , x4 , 1
	blt x4, x1, i_48
i_52:
	lw x4, -224(x2)
i_53:
	sh x21, 246(x2)
i_54:
	lw x29, 164(x2)
i_55:
	lh x27, 80(x2)
i_56:
	blt x31, x18, i_60
i_57:
	lhu x23, 244(x2)
i_58:
	lw x18, 408(x2)
i_59:
	andi x12, x3, 421
i_60:
	addi x23, x7, -1538
i_61:
	lh x15, -402(x2)
i_62:
	sw x22, -148(x2)
i_63:
	bne x11, x27, i_70
i_64:
	lbu x18, -112(x2)
i_65:
	lh x19, -104(x2)
i_66:
	and x11, x4, x14
i_67:
	sb x1, 130(x2)
i_68:
	lw x1, -440(x2)
i_69:
	addi x19, x0, 9
i_70:
	sll x24, x21, x19
i_71:
	sw x25, -424(x2)
i_72:
	lw x15, -420(x2)
i_73:
	sw x24, -444(x2)
i_74:
	nop
i_75:
	addi x5, x26, -1599
i_76:
	addi x16, x0, 1855
i_77:
	addi x1, x0, 1857
i_78:
	sltu x15, x5, x9
i_79:
	addi x15, x14, 1153
i_80:
	sw x6, 396(x2)
i_81:
	beq x15, x3, i_84
i_82:
	bne x5, x15, i_89
i_83:
	lhu x4, -446(x2)
i_84:
	lb x5, 413(x2)
i_85:
	lb x15, 56(x2)
i_86:
	addi x16 , x16 , 1
	bne x16, x1, i_78
i_87:
	andi x8, x27, -1260
i_88:
	xori x4, x31, -1125
i_89:
	andi x30, x28, -330
i_90:
	lw x20, -404(x2)
i_91:
	lui x8, 560692
i_92:
	lw x27, -344(x2)
i_93:
	lw x19, 100(x2)
i_94:
	lbu x15, 209(x2)
i_95:
	sh x2, 278(x2)
i_96:
	lbu x27, -55(x2)
i_97:
	beq x7, x5, i_103
i_98:
	lbu x14, 156(x2)
i_99:
	srli x14, x2, 1
i_100:
	srli x6, x18, 3
i_101:
	lhu x18, -312(x2)
i_102:
	slti x11, x18, 1631
i_103:
	lhu x17, 248(x2)
i_104:
	lbu x14, -284(x2)
i_105:
	lhu x16, 416(x2)
i_106:
	sw x30, -116(x2)
i_107:
	bge x13, x12, i_109
i_108:
	lb x12, 447(x2)
i_109:
	auipc x23, 965394
i_110:
	lhu x30, -62(x2)
i_111:
	auipc x30, 877345
i_112:
	sb x12, 56(x2)
i_113:
	lhu x24, -76(x2)
i_114:
	lw x21, 156(x2)
i_115:
	sb x28, 171(x2)
i_116:
	srai x30, x9, 1
i_117:
	lhu x16, -54(x2)
i_118:
	lb x4, -177(x2)
i_119:
	lb x16, 305(x2)
i_120:
	addi x4, x0, 6
i_121:
	srl x9, x29, x4
i_122:
	sw x28, -260(x2)
i_123:
	slti x9, x9, 1353
i_124:
	sub x28, x4, x16
i_125:
	lhu x3, -460(x2)
i_126:
	sw x4, -408(x2)
i_127:
	srli x25, x4, 4
i_128:
	addi x4, x18, -531
i_129:
	sh x6, -328(x2)
i_130:
	sh x25, 310(x2)
i_131:
	lh x4, 260(x2)
i_132:
	sb x26, -404(x2)
i_133:
	lw x15, -420(x2)
i_134:
	bgeu x10, x1, i_135
i_135:
	lb x10, 433(x2)
i_136:
	nop
i_137:
	addi x3, x0, 1901
i_138:
	addi x26, x0, 1904
i_139:
	sb x28, 281(x2)
i_140:
	sw x13, -428(x2)
i_141:
	nop
i_142:
	ori x9, x9, 70
i_143:
	and x20, x31, x23
i_144:
	nop
i_145:
	or x10, x27, x10
i_146:
	lh x17, -458(x2)
i_147:
	addi x3 , x3 , 1
	bne x3, x26, i_139
i_148:
	lw x26, 172(x2)
i_149:
	lhu x24, -228(x2)
i_150:
	lh x3, 458(x2)
i_151:
	lh x27, -292(x2)
i_152:
	andi x17, x20, -2012
i_153:
	slt x27, x30, x25
i_154:
	sw x19, 460(x2)
i_155:
	sh x16, 450(x2)
i_156:
	srli x10, x17, 4
i_157:
	bgeu x20, x3, i_160
i_158:
	lui x16, 637460
i_159:
	lbu x9, -431(x2)
i_160:
	lhu x17, 336(x2)
i_161:
	sw x17, -116(x2)
i_162:
	lh x16, -154(x2)
i_163:
	addi x3, x0, 1905
i_164:
	addi x10, x0, 1909
i_165:
	srai x27, x27, 3
i_166:
	lb x17, 207(x2)
i_167:
	lbu x21, 457(x2)
i_168:
	lbu x20, -352(x2)
i_169:
	slt x17, x17, x15
i_170:
	xori x5, x22, -246
i_171:
	lw x5, -152(x2)
i_172:
	sh x30, 438(x2)
i_173:
	sb x6, -433(x2)
i_174:
	sltiu x1, x8, 422
i_175:
	addi x3 , x3 , 1
	bgeu x10, x3, i_165
i_176:
	sub x6, x21, x4
i_177:
	or x14, x10, x29
i_178:
	lhu x29, 252(x2)
i_179:
	lh x27, 384(x2)
i_180:
	beq x29, x14, i_184
i_181:
	sltu x15, x23, x5
i_182:
	lb x5, 14(x2)
i_183:
	srai x5, x10, 3
i_184:
	slti x5, x22, -1300
i_185:
	lbu x11, 465(x2)
i_186:
	addi x12, x0, 1985
i_187:
	addi x3, x0, 1988
i_188:
	ori x28, x12, -310
i_189:
	addi x12 , x12 , 1
	bltu x12, x3, i_188
i_190:
	addi x13, x0, 26
i_191:
	sra x28, x13, x13
i_192:
	addi x10, x0, 1995
i_193:
	addi x20, x0, 1999
i_194:
	addi x10 , x10 , 1
	bltu x10, x20, i_194
i_195:
	addi x12, x0, 7
i_196:
	srl x30, x5, x12
i_197:
	sub x12, x28, x24
i_198:
	lb x3, 288(x2)
i_199:
	lui x25, 564244
i_200:
	sb x13, -249(x2)
i_201:
	add x25, x18, x10
i_202:
	lw x15, -428(x2)
i_203:
	auipc x25, 660860
i_204:
	bge x4, x27, i_208
i_205:
	lh x25, 88(x2)
i_206:
	sw x12, -460(x2)
i_207:
	srai x15, x12, 2
i_208:
	sh x16, 84(x2)
i_209:
	xori x12, x21, 1227
i_210:
	sltu x19, x15, x15
i_211:
	blt x1, x12, i_221
i_212:
	xor x15, x15, x10
i_213:
	lbu x19, -431(x2)
i_214:
	lhu x9, 450(x2)
i_215:
	nop
i_216:
	sw x5, 484(x2)
i_217:
	lbu x24, -129(x2)
i_218:
	slt x9, x25, x3
i_219:
	sw x9, 356(x2)
i_220:
	nop
i_221:
	nop
i_222:
	sb x22, 247(x2)
i_223:
	addi x12, x0, -1959
i_224:
	addi x10, x0, -1956
i_225:
	srai x9, x29, 1
i_226:
	addi x12 , x12 , 1
	bgeu x10, x12, i_225
i_227:
	add x5, x14, x5
i_228:
	addi x25, x13, 760
i_229:
	sb x13, -280(x2)
i_230:
	lh x25, 278(x2)
i_231:
	bltu x21, x9, i_233
i_232:
	sb x9, 193(x2)
i_233:
	bge x16, x29, i_238
i_234:
	lbu x16, 360(x2)
i_235:
	sh x25, 122(x2)
i_236:
	lbu x6, 218(x2)
i_237:
	sh x3, 180(x2)
i_238:
	sb x6, 193(x2)
i_239:
	sh x8, 306(x2)
i_240:
	lh x16, -22(x2)
i_241:
	and x19, x19, x7
i_242:
	bgeu x6, x5, i_250
i_243:
	addi x9, x0, 4
i_244:
	srl x9, x30, x9
i_245:
	bgeu x25, x16, i_252
i_246:
	blt x8, x9, i_255
i_247:
	lbu x9, 367(x2)
i_248:
	lb x25, -280(x2)
i_249:
	addi x3, x0, 28
i_250:
	sll x16, x27, x3
i_251:
	auipc x18, 476669
i_252:
	add x16, x9, x1
i_253:
	addi x19, x0, 13
i_254:
	sra x20, x17, x19
i_255:
	or x20, x3, x20
i_256:
	slt x21, x4, x21
i_257:
	sltiu x12, x7, 967
i_258:
	srli x17, x17, 2
i_259:
	beq x2, x14, i_267
i_260:
	xori x14, x11, -58
i_261:
	lbu x14, 293(x2)
i_262:
	sub x12, x2, x7
i_263:
	or x17, x25, x14
i_264:
	beq x10, x2, i_269
i_265:
	sb x5, 229(x2)
i_266:
	sw x11, 16(x2)
i_267:
	sltu x11, x12, x26
i_268:
	sh x9, 256(x2)
i_269:
	lbu x23, -88(x2)
i_270:
	lb x26, 7(x2)
i_271:
	lb x11, -41(x2)
i_272:
	lhu x11, -28(x2)
i_273:
	lbu x24, 479(x2)
i_274:
	slt x15, x23, x5
i_275:
	lw x5, 276(x2)
i_276:
	addi x14, x0, 17
i_277:
	sll x3, x5, x14
i_278:
	blt x10, x8, i_279
i_279:
	beq x23, x23, i_289
i_280:
	lw x8, -448(x2)
i_281:
	bge x8, x11, i_283
i_282:
	lh x14, -382(x2)
i_283:
	lh x10, 218(x2)
i_284:
	sltiu x17, x28, 615
i_285:
	addi x25, x0, 19
i_286:
	sra x14, x20, x25
i_287:
	lb x20, 88(x2)
i_288:
	lb x4, -187(x2)
i_289:
	lb x3, 198(x2)
i_290:
	lbu x23, 487(x2)
i_291:
	lhu x30, 200(x2)
i_292:
	sh x4, 166(x2)
i_293:
	lh x25, -196(x2)
i_294:
	lw x23, -196(x2)
i_295:
	lhu x30, -374(x2)
i_296:
	lbu x27, 342(x2)
i_297:
	or x20, x23, x12
i_298:
	lh x20, -270(x2)
i_299:
	lh x5, 40(x2)
i_300:
	addi x9, x0, 1923
i_301:
	addi x27, x0, 1927
i_302:
	addi x9 , x9 , 1
	bltu x9, x27, i_302
i_303:
	or x23, x3, x28
i_304:
	slt x16, x4, x5
i_305:
	sb x7, 275(x2)
i_306:
	andi x23, x27, 1426
i_307:
	bge x14, x2, i_314
i_308:
	lbu x13, -248(x2)
i_309:
	lhu x8, 22(x2)
i_310:
	nop
i_311:
	lw x23, 144(x2)
i_312:
	lhu x10, -360(x2)
i_313:
	addi x29, x0, 17
i_314:
	srl x3, x19, x29
i_315:
	xor x29, x29, x30
i_316:
	addi x13, x0, 1952
i_317:
	addi x16, x0, 1954
i_318:
	sw x5, 36(x2)
i_319:
	andi x10, x27, 1698
i_320:
	addi x13 , x13 , 1
	bgeu x16, x13, i_318
i_321:
	sb x26, -230(x2)
i_322:
	beq x7, x3, i_330
i_323:
	addi x6, x0, 10
i_324:
	srl x18, x26, x6
i_325:
	lui x29, 251641
i_326:
	auipc x4, 1039090
i_327:
	srli x6, x30, 4
i_328:
	sw x9, -360(x2)
i_329:
	sw x12, 332(x2)
i_330:
	sb x31, -488(x2)
i_331:
	bgeu x23, x13, i_340
i_332:
	addi x15, x21, 1187
i_333:
	sltu x27, x13, x11
i_334:
	slt x27, x13, x29
i_335:
	add x28, x16, x9
i_336:
	blt x6, x20, i_340
i_337:
	sb x8, -43(x2)
i_338:
	slti x1, x16, -1614
i_339:
	sb x4, 335(x2)
i_340:
	sb x20, -89(x2)
i_341:
	sw x23, -180(x2)
i_342:
	bne x31, x14, i_343
i_343:
	sh x9, 128(x2)
i_344:
	ori x13, x25, 2045
i_345:
	sw x8, -324(x2)
i_346:
	sltu x22, x1, x15
i_347:
	xor x21, x9, x23
i_348:
	lw x9, 240(x2)
i_349:
	addi x1, x0, -2012
i_350:
	addi x10, x0, -2010
i_351:
	sb x9, -298(x2)
i_352:
	addi x1 , x1 , 1
	blt x1, x10, i_351
i_353:
	addi x22, x0, 1
i_354:
	sll x13, x21, x22
i_355:
	bge x3, x20, i_360
i_356:
	addi x21, x19, 609
i_357:
	addi x23, x0, 3
i_358:
	sra x21, x3, x23
i_359:
	nop
i_360:
	lbu x24, -69(x2)
i_361:
	sh x10, -66(x2)
i_362:
	addi x20, x0, -1932
i_363:
	addi x29, x0, -1928
i_364:
	and x18, x19, x8
i_365:
	sw x20, 344(x2)
i_366:
	lui x27, 270043
i_367:
	addi x20 , x20 , 1
	bgeu x29, x20, i_364
i_368:
	srli x21, x30, 2
i_369:
	lbu x11, 256(x2)
i_370:
	nop
i_371:
	addi x28, x0, -1903
i_372:
	addi x24, x0, -1899
i_373:
	lhu x30, -80(x2)
i_374:
	lhu x18, 388(x2)
i_375:
	nop
i_376:
	beq x11, x10, i_377
i_377:
	lbu x11, 186(x2)
i_378:
	beq x6, x15, i_387
i_379:
	nop
i_380:
	lhu x15, 170(x2)
i_381:
	addi x28 , x28 , 1
	blt x28, x24, i_373
i_382:
	sh x17, -174(x2)
i_383:
	srli x30, x18, 3
i_384:
	lh x25, 408(x2)
i_385:
	auipc x5, 249761
i_386:
	lbu x17, 126(x2)
i_387:
	addi x12, x0, 20
i_388:
	sra x17, x3, x12
i_389:
	slti x24, x24, 859
i_390:
	sw x1, 112(x2)
i_391:
	sh x13, 372(x2)
i_392:
	sb x21, -121(x2)
i_393:
	srli x28, x12, 3
i_394:
	sb x4, -108(x2)
i_395:
	bne x5, x24, i_404
i_396:
	lhu x18, 352(x2)
i_397:
	slt x19, x20, x26
i_398:
	sub x29, x25, x30
i_399:
	sh x19, -6(x2)
i_400:
	lh x26, 196(x2)
i_401:
	lb x19, -119(x2)
i_402:
	sw x8, -304(x2)
i_403:
	sw x7, 12(x2)
i_404:
	sub x20, x8, x26
i_405:
	lw x15, 368(x2)
i_406:
	lh x12, -384(x2)
i_407:
	sub x19, x13, x17
i_408:
	auipc x4, 877802
i_409:
	addi x19, x0, 10
i_410:
	sra x18, x31, x19
i_411:
	srli x23, x10, 2
i_412:
	srli x15, x26, 3
i_413:
	slti x29, x23, 1592
i_414:
	bgeu x3, x26, i_424
i_415:
	bge x14, x29, i_420
i_416:
	slli x14, x29, 3
i_417:
	bge x10, x12, i_419
i_418:
	sw x6, 236(x2)
i_419:
	sh x11, -370(x2)
i_420:
	nop
i_421:
	srai x15, x31, 1
i_422:
	lh x9, -286(x2)
i_423:
	or x16, x14, x31
i_424:
	lb x15, 21(x2)
i_425:
	and x18, x6, x17
i_426:
	addi x29, x0, -1836
i_427:
	addi x23, x0, -1832
i_428:
	addi x29 , x29 , 1
	bge x23, x29, i_428
i_429:
	lbu x18, 373(x2)
i_430:
	lw x17, 132(x2)
i_431:
	sb x16, -357(x2)
i_432:
	lh x9, -426(x2)
i_433:
	lhu x16, -312(x2)
i_434:
	sb x4, 411(x2)
i_435:
	lw x16, -160(x2)
i_436:
	beq x15, x30, i_446
i_437:
	andi x16, x15, 646
i_438:
	lbu x28, -314(x2)
i_439:
	blt x9, x17, i_443
i_440:
	sh x4, -232(x2)
i_441:
	lbu x9, -422(x2)
i_442:
	sb x23, -389(x2)
i_443:
	srai x11, x25, 1
i_444:
	or x12, x1, x5
i_445:
	lbu x8, -7(x2)
i_446:
	nop
i_447:
	sw x3, -196(x2)
i_448:
	addi x24, x0, -1981
i_449:
	addi x6, x0, -1979
i_450:
	addi x24 , x24 , 1
	bge x6, x24, i_450
i_451:
	lbu x18, -301(x2)
i_452:
	addi x11, x0, 29
i_453:
	sra x16, x16, x11
i_454:
	lb x4, -132(x2)
i_455:
	lh x19, -322(x2)
i_456:
	or x18, x14, x3
i_457:
	bne x6, x2, i_461
i_458:
	lh x11, 136(x2)
i_459:
	sh x19, -380(x2)
i_460:
	slt x5, x11, x5
i_461:
	nop
i_462:
	lbu x21, 298(x2)
i_463:
	addi x3, x0, -1880
i_464:
	addi x15, x0, -1877
i_465:
	slli x1, x8, 1
i_466:
	beq x3, x25, i_474
i_467:
	add x16, x1, x25
i_468:
	addi x3 , x3 , 1
	bne x3, x15, i_465
i_469:
	lh x25, 348(x2)
i_470:
	lbu x5, 371(x2)
i_471:
	add x5, x25, x14
i_472:
	sw x16, -352(x2)
i_473:
	lui x15, 58991
i_474:
	sb x15, 477(x2)
i_475:
	sb x3, -15(x2)
i_476:
	sltu x16, x16, x16
i_477:
	bgeu x14, x10, i_483
i_478:
	bge x15, x14, i_485
i_479:
	sw x2, -456(x2)
i_480:
	addi x16, x0, 1
i_481:
	sll x5, x15, x16
i_482:
	sb x25, -31(x2)
i_483:
	sw x4, 488(x2)
i_484:
	ori x15, x15, -260
i_485:
	bne x3, x11, i_491
i_486:
	lbu x11, 403(x2)
i_487:
	bltu x16, x29, i_491
i_488:
	auipc x11, 340249
i_489:
	xori x15, x13, 460
i_490:
	lw x29, 468(x2)
i_491:
	slt x27, x29, x26
i_492:
	sltiu x11, x5, -1279
i_493:
	sw x30, 244(x2)
i_494:
	lbu x11, -441(x2)
i_495:
	lbu x5, -107(x2)
i_496:
	lbu x25, -149(x2)
i_497:
	blt x19, x11, i_504
i_498:
	andi x11, x11, 1818
i_499:
	lh x11, 220(x2)
i_500:
	lh x3, -114(x2)
i_501:
	lb x29, -109(x2)
i_502:
	sw x2, -404(x2)
i_503:
	addi x22, x3, -189
i_504:
	lw x17, -244(x2)
i_505:
	lh x13, 74(x2)
i_506:
	lb x3, -442(x2)
i_507:
	sub x5, x13, x26
i_508:
	bgeu x11, x19, i_514
i_509:
	lw x18, 412(x2)
i_510:
	auipc x15, 725663
i_511:
	lh x11, 96(x2)
i_512:
	sw x20, 276(x2)
i_513:
	lbu x30, -140(x2)
i_514:
	addi x20, x0, 3
i_515:
	sll x16, x16, x20
i_516:
	addi x17, x0, 1966
i_517:
	addi x5, x0, 1969
i_518:
	addi x17 , x17 , 1
	bge x5, x17, i_518
i_519:
	lbu x30, -113(x2)
i_520:
	ori x21, x26, -756
i_521:
	nop
i_522:
	addi x22, x0, -2015
i_523:
	addi x6, x0, -2011
i_524:
	addi x21, x0, 25
i_525:
	sll x9, x23, x21
i_526:
	addi x20, x0, -2024
i_527:
	addi x29, x0, -2021
i_528:
	lw x21, 316(x2)
i_529:
	addi x20 , x20 , 1
	blt x20, x29, i_528
i_530:
	slti x16, x23, 1438
i_531:
	sh x17, -206(x2)
i_532:
	addi x22 , x22 , 1
	bne x22, x6, i_524
i_533:
	sw x16, 204(x2)
i_534:
	lhu x17, -346(x2)
i_535:
	beq x17, x8, i_544
i_536:
	lh x26, -138(x2)
i_537:
	ori x13, x29, -16
i_538:
	beq x19, x24, i_542
i_539:
	sub x16, x20, x16
i_540:
	sb x21, -211(x2)
i_541:
	lbu x20, 151(x2)
i_542:
	srli x9, x18, 1
i_543:
	lhu x9, -90(x2)
i_544:
	auipc x9, 1006277
i_545:
	slti x18, x24, 172
i_546:
	lui x24, 477152
i_547:
	sh x20, 146(x2)
i_548:
	add x3, x15, x7
i_549:
	addi x13, x0, 9
i_550:
	sra x18, x18, x13
i_551:
	sb x24, 285(x2)
i_552:
	bltu x9, x15, i_562
i_553:
	sh x26, 486(x2)
i_554:
	sltu x24, x7, x18
i_555:
	sb x7, 53(x2)
i_556:
	lh x4, -98(x2)
i_557:
	addi x19, x0, 21
i_558:
	sll x21, x22, x19
i_559:
	lui x11, 519263
i_560:
	lbu x8, -70(x2)
i_561:
	auipc x8, 487311
i_562:
	sw x16, -204(x2)
i_563:
	slli x16, x18, 3
i_564:
	addi x12, x0, 22
i_565:
	sra x30, x16, x12
i_566:
	lh x27, 474(x2)
i_567:
	ori x23, x20, 563
i_568:
	lw x1, 464(x2)
i_569:
	sh x14, -342(x2)
i_570:
	addi x23, x0, 25
i_571:
	sra x11, x31, x23
i_572:
	lb x12, -466(x2)
i_573:
	sw x12, -460(x2)
i_574:
	lui x21, 455945
i_575:
	sw x21, -428(x2)
i_576:
	xori x21, x21, -1445
i_577:
	lh x28, -64(x2)
i_578:
	lbu x27, -146(x2)
i_579:
	lh x6, 488(x2)
i_580:
	lb x20, 30(x2)
i_581:
	bne x28, x5, i_585
i_582:
	sh x31, 20(x2)
i_583:
	lh x30, 136(x2)
i_584:
	bgeu x13, x19, i_585
i_585:
	lh x27, -252(x2)
i_586:
	ori x22, x19, -805
i_587:
	addi x12, x0, 12
i_588:
	srl x9, x27, x12
i_589:
	sltiu x27, x31, 943
i_590:
	or x5, x28, x9
i_591:
	bge x7, x17, i_595
i_592:
	lui x5, 1012909
i_593:
	ori x9, x9, -1093
i_594:
	blt x28, x28, i_596
i_595:
	xori x28, x11, 1489
i_596:
	lhu x9, -274(x2)
i_597:
	lbu x9, 41(x2)
i_598:
	sb x30, -444(x2)
i_599:
	lhu x28, -366(x2)
i_600:
	addi x14, x0, -1864
i_601:
	addi x29, x0, -1862
i_602:
	sb x2, 59(x2)
i_603:
	lbu x9, -349(x2)
i_604:
	ori x30, x8, -1333
i_605:
	addi x8, x0, 24
i_606:
	srl x21, x21, x8
i_607:
	sw x29, -376(x2)
i_608:
	andi x8, x25, -1578
i_609:
	lw x6, 140(x2)
i_610:
	bltu x6, x7, i_616
i_611:
	addi x8, x0, 23
i_612:
	sll x3, x8, x8
i_613:
	addi x14 , x14 , 1
	bge x29, x14, i_602
i_614:
	add x8, x29, x14
i_615:
	sh x17, -116(x2)
i_616:
	xor x28, x16, x8
i_617:
	lbu x13, 397(x2)
i_618:
	bgeu x20, x21, i_625
i_619:
	bge x13, x19, i_628
i_620:
	sh x19, -354(x2)
i_621:
	lw x6, -456(x2)
i_622:
	lh x13, -304(x2)
i_623:
	lb x1, -360(x2)
i_624:
	lhu x8, -140(x2)
i_625:
	or x26, x31, x4
i_626:
	beq x22, x30, i_627
i_627:
	bgeu x30, x31, i_633
i_628:
	sub x8, x11, x4
i_629:
	lbu x11, 411(x2)
i_630:
	addi x6, x10, 690
i_631:
	bgeu x15, x6, i_639
i_632:
	and x21, x10, x9
i_633:
	lb x1, 405(x2)
i_634:
	sltu x27, x30, x5
i_635:
	lw x9, 140(x2)
i_636:
	srai x21, x12, 2
i_637:
	xori x25, x14, -335
i_638:
	slt x27, x30, x1
i_639:
	sb x9, -202(x2)
i_640:
	lb x9, 374(x2)
i_641:
	addi x1, x0, 1917
i_642:
	addi x19, x0, 1920
i_643:
	sub x14, x4, x6
i_644:
	sh x23, -238(x2)
i_645:
	slt x6, x8, x29
i_646:
	addi x12, x0, 1
i_647:
	srl x14, x27, x12
i_648:
	addi x1 , x1 , 1
	bge x19, x1, i_643
i_649:
	lbu x25, -420(x2)
i_650:
	addi x10, x0, 31
i_651:
	sll x17, x25, x10
i_652:
	addi x12, x0, 13
i_653:
	srl x8, x23, x12
i_654:
	sb x3, 363(x2)
i_655:
	blt x15, x16, i_664
i_656:
	xori x22, x10, 2004
i_657:
	addi x16, x0, 15
i_658:
	srl x22, x18, x16
i_659:
	lhu x25, 26(x2)
i_660:
	bge x15, x22, i_665
i_661:
	sh x8, 152(x2)
i_662:
	bge x25, x24, i_663
i_663:
	sh x20, 344(x2)
i_664:
	lw x22, 220(x2)
i_665:
	bgeu x30, x9, i_675
i_666:
	sw x15, 356(x2)
i_667:
	add x22, x28, x2
i_668:
	bne x14, x11, i_671
i_669:
	xor x25, x22, x20
i_670:
	xor x26, x22, x17
i_671:
	sw x16, -32(x2)
i_672:
	srli x25, x20, 4
i_673:
	sb x22, 58(x2)
i_674:
	sb x14, -212(x2)
i_675:
	sb x21, -156(x2)
i_676:
	sw x30, -128(x2)
i_677:
	addi x14, x0, -2023
i_678:
	addi x22, x0, -2021
i_679:
	lhu x10, -156(x2)
i_680:
	addi x14 , x14 , 1
	bge x22, x14, i_679
i_681:
	beq x8, x19, i_684
i_682:
	auipc x14, 21232
i_683:
	andi x26, x6, -1354
i_684:
	bge x31, x4, i_686
i_685:
	xori x13, x23, -14
i_686:
	srli x4, x18, 1
i_687:
	beq x6, x7, i_689
i_688:
	lhu x28, -62(x2)
i_689:
	lbu x8, -43(x2)
i_690:
	addi x23, x0, 10
i_691:
	sra x29, x1, x23
i_692:
	lb x21, 153(x2)
i_693:
	lhu x25, -94(x2)
i_694:
	sh x13, -346(x2)
i_695:
	bge x24, x27, i_699
i_696:
	lbu x8, -299(x2)
i_697:
	lbu x14, 54(x2)
i_698:
	sh x8, 6(x2)
i_699:
	sh x12, -334(x2)
i_700:
	sw x29, 476(x2)
i_701:
	sh x8, -446(x2)
i_702:
	lh x27, 340(x2)
i_703:
	lb x29, -167(x2)
i_704:
	sw x3, 484(x2)
i_705:
	bne x13, x21, i_712
i_706:
	addi x23, x14, -1164
i_707:
	lbu x14, -52(x2)
i_708:
	sw x14, -124(x2)
i_709:
	beq x4, x9, i_711
i_710:
	andi x11, x29, 458
i_711:
	sltu x9, x2, x9
i_712:
	addi x9, x28, -1399
i_713:
	xor x8, x9, x19
i_714:
	slli x23, x2, 2
i_715:
	sltiu x4, x11, 541
i_716:
	addi x15, x0, 6
i_717:
	sra x23, x4, x15
i_718:
	or x18, x27, x9
i_719:
	lhu x27, -360(x2)
i_720:
	sw x16, 36(x2)
i_721:
	slti x1, x8, 1790
i_722:
	srai x8, x12, 3
i_723:
	sltu x25, x11, x25
i_724:
	andi x9, x28, 1470
i_725:
	lhu x30, 26(x2)
i_726:
	bge x9, x28, i_729
i_727:
	sw x23, -296(x2)
i_728:
	blt x8, x5, i_734
i_729:
	or x8, x8, x1
i_730:
	addi x11, x0, 25
i_731:
	sra x5, x21, x11
i_732:
	sw x25, -52(x2)
i_733:
	addi x25, x0, 1
i_734:
	srl x26, x5, x25
i_735:
	sh x26, 304(x2)
i_736:
	addi x3, x0, -1921
i_737:
	addi x1, x0, -1917
i_738:
	lw x10, 436(x2)
i_739:
	addi x3 , x3 , 1
	bge x1, x3, i_738
i_740:
	lb x3, -396(x2)
i_741:
	sb x16, 385(x2)
i_742:
	lw x1, 248(x2)
i_743:
	lh x15, 142(x2)
i_744:
	add x15, x15, x22
i_745:
	bltu x30, x13, i_755
i_746:
	xor x3, x14, x15
i_747:
	sw x20, -456(x2)
i_748:
	or x4, x3, x19
i_749:
	sh x3, -388(x2)
i_750:
	xor x26, x20, x4
i_751:
	nop
i_752:
	lb x17, 380(x2)
i_753:
	sw x26, 72(x2)
i_754:
	sw x31, -144(x2)
i_755:
	lb x19, -255(x2)
i_756:
	sw x15, 140(x2)
i_757:
	addi x3, x0, 2043
i_758:
	addi x24, x0, 2045
i_759:
	xori x1, x9, 9
i_760:
	lui x23, 862214
i_761:
	bgeu x17, x11, i_768
i_762:
	sw x23, -476(x2)
i_763:
	lbu x15, -171(x2)
i_764:
	addi x23, x13, -1017
i_765:
	addi x3 , x3 , 1
	bgeu x24, x3, i_759
i_766:
	sltu x23, x10, x14
i_767:
	lhu x20, 60(x2)
i_768:
	and x13, x5, x20
i_769:
	lb x20, -207(x2)
i_770:
	slli x13, x3, 1
i_771:
	lh x3, 366(x2)
i_772:
	sb x16, -37(x2)
i_773:
	auipc x16, 624626
i_774:
	blt x3, x18, i_783
i_775:
	andi x16, x15, -931
i_776:
	blt x18, x16, i_781
i_777:
	lhu x18, -100(x2)
i_778:
	bltu x18, x10, i_785
i_779:
	nop
i_780:
	lw x14, 172(x2)
i_781:
	lbu x3, -420(x2)
i_782:
	srai x28, x4, 3
i_783:
	nop
i_784:
	lhu x25, -170(x2)
i_785:
	sub x3, x4, x31
i_786:
	add x26, x1, x8
i_787:
	addi x27, x0, -1908
i_788:
	addi x30, x0, -1904
i_789:
	sltiu x6, x4, 701
i_790:
	and x4, x23, x15
i_791:
	bltu x19, x18, i_801
i_792:
	addi x27 , x27 , 1
	blt x27, x30, i_789
i_793:
	lw x15, 184(x2)
i_794:
	add x6, x19, x16
i_795:
	lh x3, 62(x2)
i_796:
	sb x19, 25(x2)
i_797:
	lw x15, -132(x2)
i_798:
	sh x22, 412(x2)
i_799:
	lbu x8, -117(x2)
i_800:
	lh x19, -294(x2)
i_801:
	sh x9, -454(x2)
i_802:
	lhu x28, 100(x2)
i_803:
	srai x23, x7, 4
i_804:
	add x26, x5, x26
i_805:
	beq x8, x12, i_806
i_806:
	lbu x24, 299(x2)
i_807:
	lui x24, 115251
i_808:
	addi x12, x0, 26
i_809:
	sll x11, x26, x12
i_810:
	sh x11, 382(x2)
i_811:
	lui x23, 938487
i_812:
	andi x26, x14, -180
i_813:
	lh x14, 298(x2)
i_814:
	addi x5, x0, 16
i_815:
	srl x10, x29, x5
i_816:
	addi x12, x14, 1195
i_817:
	auipc x25, 1002757
i_818:
	xor x25, x5, x5
i_819:
	lh x16, -200(x2)
i_820:
	slti x5, x31, -603
i_821:
	addi x16, x0, 30
i_822:
	srl x14, x18, x16
i_823:
	addi x18, x0, 1841
i_824:
	addi x22, x0, 1843
i_825:
	addi x16, x0, 24
i_826:
	sll x16, x14, x16
i_827:
	bne x6, x15, i_829
i_828:
	lhu x5, 86(x2)
i_829:
	lhu x6, -150(x2)
i_830:
	sh x27, 226(x2)
i_831:
	sh x14, -362(x2)
i_832:
	addi x18 , x18 , 1
	bgeu x22, x18, i_824
i_833:
	xor x29, x6, x29
i_834:
	sw x27, -468(x2)
i_835:
	beq x31, x12, i_842
i_836:
	lh x26, -100(x2)
i_837:
	sh x21, 194(x2)
i_838:
	sw x7, -116(x2)
i_839:
	or x6, x16, x24
i_840:
	add x6, x26, x23
i_841:
	beq x28, x13, i_846
i_842:
	sh x4, 32(x2)
i_843:
	addi x23, x23, 1759
i_844:
	blt x2, x15, i_852
i_845:
	lb x26, 209(x2)
i_846:
	slti x23, x23, -770
i_847:
	xori x23, x1, -1051
i_848:
	lw x9, 84(x2)
i_849:
	addi x23, x0, 19
i_850:
	sra x29, x18, x23
i_851:
	xor x15, x23, x24
i_852:
	blt x14, x15, i_862
i_853:
	beq x7, x1, i_854
i_854:
	sw x28, 68(x2)
i_855:
	add x28, x26, x28
i_856:
	lhu x20, 314(x2)
i_857:
	and x20, x26, x16
i_858:
	nop
i_859:
	sw x19, 236(x2)
i_860:
	andi x23, x25, -1179
i_861:
	sw x27, -380(x2)
i_862:
	slti x16, x23, -1217
i_863:
	lh x18, 410(x2)
i_864:
	addi x12, x0, -2030
i_865:
	addi x9, x0, -2026
i_866:
	sb x2, -346(x2)
i_867:
	sw x6, -288(x2)
i_868:
	addi x12 , x12 , 1
	bgeu x9, x12, i_866
i_869:
	lh x17, 306(x2)
i_870:
	lb x24, 373(x2)
i_871:
	blt x14, x11, i_872
i_872:
	bgeu x19, x20, i_875
i_873:
	bge x2, x4, i_883
i_874:
	lw x21, -420(x2)
i_875:
	lhu x18, -208(x2)
i_876:
	lw x12, -360(x2)
i_877:
	sw x12, 24(x2)
i_878:
	add x12, x12, x4
i_879:
	addi x14, x0, 6
i_880:
	sll x25, x7, x14
i_881:
	bge x14, x21, i_883
i_882:
	beq x11, x14, i_886
i_883:
	lh x24, 114(x2)
i_884:
	lb x25, 8(x2)
i_885:
	bge x26, x21, i_892
i_886:
	sb x22, -99(x2)
i_887:
	sltiu x24, x17, -1638
i_888:
	addi x11, x0, 13
i_889:
	sra x17, x17, x11
i_890:
	lb x20, 300(x2)
i_891:
	lhu x26, 38(x2)
i_892:
	lw x27, 392(x2)
i_893:
	addi x27, x0, 27
i_894:
	sra x27, x11, x27
i_895:
	andi x22, x4, -330
i_896:
	lb x10, -198(x2)
i_897:
	sb x25, 276(x2)
i_898:
	add x4, x14, x23
i_899:
	lhu x10, -362(x2)
i_900:
	beq x4, x17, i_901
i_901:
	sh x31, -106(x2)
i_902:
	andi x28, x15, -2020
i_903:
	bne x30, x24, i_904
i_904:
	lh x28, -156(x2)
i_905:
	srai x24, x22, 1
i_906:
	srli x28, x10, 3
i_907:
	auipc x27, 599263
i_908:
	and x28, x28, x24
i_909:
	and x10, x19, x17
i_910:
	lbu x28, -36(x2)
i_911:
	lw x14, -352(x2)
i_912:
	sh x21, 34(x2)
i_913:
	lw x6, -420(x2)
i_914:
	sb x10, 133(x2)
i_915:
	bltu x28, x27, i_920
i_916:
	bge x31, x24, i_921
i_917:
	addi x20, x0, 5
i_918:
	sll x27, x24, x20
i_919:
	blt x5, x1, i_923
i_920:
	slti x20, x8, -35
i_921:
	sh x20, -396(x2)
i_922:
	andi x1, x6, -1378
i_923:
	sb x1, 174(x2)
i_924:
	addi x18, x0, 28
i_925:
	srl x8, x9, x18
i_926:
	addi x13, x0, -2038
i_927:
	addi x10, x0, -2035
i_928:
	andi x30, x13, 1866
i_929:
	lbu x9, 384(x2)
i_930:
	bgeu x4, x30, i_931
i_931:
	lb x4, 238(x2)
i_932:
	slt x4, x29, x15
i_933:
	lb x27, -371(x2)
i_934:
	sh x29, 230(x2)
i_935:
	blt x20, x16, i_942
i_936:
	lbu x12, -264(x2)
i_937:
	bge x9, x15, i_941
i_938:
	addi x13 , x13 , 1
	blt x13, x10, i_928
i_939:
	xor x8, x16, x11
i_940:
	bgeu x28, x3, i_942
i_941:
	lb x22, 138(x2)
i_942:
	sh x15, -208(x2)
i_943:
	lhu x25, -422(x2)
i_944:
	addi x9, x15, -1384
i_945:
	sub x15, x25, x25
i_946:
	xor x18, x29, x13
i_947:
	lhu x11, 460(x2)
i_948:
	slt x11, x27, x25
i_949:
	sb x24, -333(x2)
i_950:
	sw x11, -60(x2)
i_951:
	sb x26, -59(x2)
i_952:
	lhu x15, -236(x2)
i_953:
	andi x24, x8, -759
i_954:
	sb x16, 97(x2)
i_955:
	slli x15, x15, 1
i_956:
	addi x23, x11, 1991
i_957:
	blt x19, x24, i_960
i_958:
	lb x15, 402(x2)
i_959:
	or x11, x21, x11
i_960:
	andi x25, x23, 1389
i_961:
	sh x16, -194(x2)
i_962:
	lh x16, 10(x2)
i_963:
	slti x11, x3, 1013
i_964:
	bgeu x14, x11, i_969
i_965:
	lhu x11, 140(x2)
i_966:
	sb x13, 107(x2)
i_967:
	beq x18, x24, i_973
i_968:
	lhu x16, 88(x2)
i_969:
	lb x25, -115(x2)
i_970:
	lh x26, -106(x2)
i_971:
	slli x16, x2, 1
i_972:
	lbu x25, -121(x2)
i_973:
	sw x26, 0(x2)
i_974:
	sb x3, -260(x2)
i_975:
	sw x27, -192(x2)
i_976:
	lb x26, -370(x2)
i_977:
	lhu x22, -314(x2)
i_978:
	lh x29, -288(x2)
i_979:
	srli x14, x14, 4
i_980:
	lhu x29, 488(x2)
i_981:
	bne x22, x28, i_989
i_982:
	sh x31, -280(x2)
i_983:
	sb x4, 401(x2)
i_984:
	sw x3, -52(x2)
i_985:
	lh x22, -270(x2)
i_986:
	lui x26, 526091
i_987:
	add x22, x12, x30
i_988:
	lw x18, 40(x2)
i_989:
	sb x4, -113(x2)
i_990:
	add x27, x26, x29
i_991:
	sub x6, x6, x6
i_992:
	add x14, x1, x26
i_993:
	sltiu x29, x14, 1652
i_994:
	sub x30, x6, x29
i_995:
	sltiu x25, x27, -1968
i_996:
	lh x17, -60(x2)
i_997:
	lb x10, -397(x2)
i_998:
	srli x6, x15, 1
i_999:
	addi x15, x6, 1602
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0x32786a3a
	.word 0x41ae3a0a
	.word 0x73a6f1f3
	.word 0x7f148e54
	.word 0xb6ec29bf
	.word 0x74a6fdc8
	.word 0x8ee770f8
	.word 0x8d09de78
	.word 0x25ac5de8
	.word 0x82fcdb5a
	.word 0x383d80ae
	.word 0xe6e60c60
	.word 0xe7d48739
	.word 0x341df76b
	.word 0xb2ee3e02
	.word 0x627d3600
	.word 0x9d80464b
	.word 0x8eaa0be0
	.word 0x283a837f
	.word 0x9ce42184
	.word 0xa59bd0
	.word 0x31e3bbe
	.word 0x3832d621
	.word 0xd2c0a260
	.word 0x3f9b4f98
	.word 0xafeed54b
	.word 0x4019c07
	.word 0xe45c129f
	.word 0xf563d955
	.word 0xf020a185
	.word 0x3c23af7e
	.word 0x296fcb05
	.word 0xb6a140fd
	.word 0x8fa1c346
	.word 0x815db8ad
	.word 0x7fc9ec11
	.word 0x6caae879
	.word 0xee26afe3
	.word 0x88096af7
	.word 0xd4dc7356
	.word 0x37c43deb
	.word 0x3fca1e2
	.word 0x346040fa
	.word 0x3fd90fbe
	.word 0x729454b0
	.word 0x10b1d2b9
	.word 0xc17224a1
	.word 0x2e8231a4
	.word 0x7762f66b
	.word 0x8cd1f0a9
	.word 0xb009de3e
	.word 0x26dd5565
	.word 0xbb25654a
	.word 0xd50c7969
	.word 0x393f28a5
	.word 0xe95bda4e
	.word 0xfd074f82
	.word 0x55f3e9c8
	.word 0xffce2f53
	.word 0x917a18bf
	.word 0xf96d5633
	.word 0x64b1c13f
	.word 0xb6e395f7
	.word 0x98b68075
	.word 0xf4572790
	.word 0x2226372d
	.word 0x2041bcac
	.word 0xe9d2421
	.word 0x7a970a56
	.word 0xb482c9a7
	.word 0xcb6941a6
	.word 0x6ea0d510
	.word 0xdc483069
	.word 0xd3099580
	.word 0xcd98d871
	.word 0xc658765c
	.word 0xf8730360
	.word 0x988daffd
	.word 0xfd72e4ea
	.word 0x1c215d7f
	.word 0xd71a8d64
	.word 0x55300ffe
	.word 0xab6c1a32
	.word 0x8b4bd03e
	.word 0xdc222825
	.word 0x37c7f04c
	.word 0x9de30eca
	.word 0xa8575a31
	.word 0x436c1c0a
	.word 0x307f9cde
	.word 0x267e34fe
	.word 0x714618a6
	.word 0xc1e5c276
	.word 0x251447ac
	.word 0x7f8485b6
	.word 0xd0dc6e50
	.word 0x45593606
	.word 0x8fd2e0cc
	.word 0x117f7737
	.word 0xfbd5667b
	.word 0x3ca13a17
	.word 0x74a6a6c6
	.word 0xd0edb796
	.word 0xc1821469
	.word 0x212585c1
	.word 0x5903bcff
	.word 0x16d527ca
	.word 0xb254e14e
	.word 0xc0ab438f
	.word 0x23395a47
	.word 0x394da7a
	.word 0x418bf94f
	.word 0x1627558c
	.word 0x7aa14fbe
	.word 0x9e4eb394
	.word 0x4d8f38f0
	.word 0x376526f6
	.word 0x6b6799fb
	.word 0x23af3d05
	.word 0xc01d4bc
	.word 0xb7787b55
	.word 0xea1935c3
	.word 0xea328858
	.word 0xa6db451d
	.word 0x8829434c
	.word 0x60c84ca9
	.word 0xa6fc1e16
	.word 0x4661708f
	.word 0x63fef683
	.word 0xe1fadd80
	.word 0xe4162cd7
	.word 0x70920f84
	.word 0xa06ba4f3
	.word 0x5a0cd4ec
	.word 0x8f85fe7e
	.word 0xfd4699a4
	.word 0x8bc34bd8
	.word 0x529c0f62
	.word 0xc1d9455b
	.word 0x9f6d5179
	.word 0x85d8daec
	.word 0x7c96023a
	.word 0x14f6bde9
	.word 0x1475d275
	.word 0x8aa4b446
	.word 0x4fac7a24
	.word 0x191c77a4
	.word 0xa2f2bafa
	.word 0x2d7aacf7
	.word 0x4dd3d6d6
	.word 0x5816fe82
	.word 0xb205f7d7
	.word 0xd5c10073
	.word 0x68fa543b
	.word 0xf16cfcd9
	.word 0x7da2b93a
	.word 0x74667cad
	.word 0xff2ee3bd
	.word 0x6c8f063e
	.word 0x96c38308
	.word 0xfdc077b2
	.word 0x32d6fc82
	.word 0xa92b45e2
	.word 0x328cfa14
	.word 0x2f45fd37
	.word 0x9c82b914
	.word 0x56c9fd44
	.word 0x8bb44809
	.word 0x1675938f
	.word 0x4aca2218
	.word 0xaa4fe53e
	.word 0x9bc4c58d
	.word 0x1e8e06d9
	.word 0x1f3aea51
	.word 0x95ee7d6e
	.word 0xa49c3330
	.word 0xbf559be6
	.word 0xf0da4358
	.word 0xbdd656e4
	.word 0xd647f5ed
	.word 0x2eedc9ce
	.word 0xafc1ccf1
	.word 0x19131afa
	.word 0x4701c4e3
	.word 0x7f90f166
	.word 0x3ba4addf
	.word 0xc24c0e3e
	.word 0x406f25e
	.word 0xe8762c7f
	.word 0x23419ab0
	.word 0x3b3fcb46
	.word 0xc1763267
	.word 0x1e3ba074
	.word 0xf1808be3
	.word 0xded1ada9
	.word 0x2b819def
	.word 0xc9464227
	.word 0xfb1ffc4f
	.word 0xe0fc7c5a
	.word 0x8b9e9a3a
	.word 0xdf1ebb05
	.word 0x3a173792
	.word 0x116cd693
	.word 0x5f94dbc4
	.word 0x2923dd4e
	.word 0xb3048c3f
	.word 0xaf4d9d70
	.word 0xab709830
	.word 0x8ada1b22
	.word 0xc1b63f25
	.word 0x3a3e4ef5
	.word 0x877c5d6c
	.word 0x60e85da8
	.word 0x9f095190
	.word 0x3ee1687d
	.word 0x9c3796a
	.word 0x2353f5fc
	.word 0x9af940fd
	.word 0xf633aeaf
	.word 0xd511a82a
	.word 0x77ca554c
	.word 0x2c17c36
	.word 0xadb6118b
	.word 0x15e11dba
	.word 0x1fc5b4d1
	.word 0x28c0335d
	.word 0xb7f1423
	.word 0xe5488537
	.word 0xecc0a36c
	.word 0xd6ce5df4
	.word 0x39108f31
	.word 0xf1751af1
	.word 0x6dac42c3
	.word 0x3a7e90ab
	.word 0x7351f366
	.word 0x4fb980a5
	.word 0xca1b4296
	.word 0x11290d5e
	.word 0x54783cb7
	.word 0x7a390625
	.word 0xfee56a33
	.word 0x9e85add4
	.word 0x8d288912
	.word 0x39c1db7f
	.word 0x2fdb3dca
	.word 0xe1e3485b
	.word 0x6b1992b5
	.word 0x4ce66c39
	.word 0x52bf3e7
	.word 0x3ffcd951
	.word 0xbfd37142
	.word 0xd6d3823b
	.word 0xbd2a51c4
	.word 0x345eb456
	.word 0xab56fcd8
	.word 0x2664779
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
