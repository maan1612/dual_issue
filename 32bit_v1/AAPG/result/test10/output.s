
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	lb x1, -52(x2)
i_1:
	slti x13, x3, -502
i_2:
	bne x13, x28, i_11
i_3:
	lw x15, -284(x2)
i_4:
	beq x11, x26, i_8
i_5:
	lbu x24, -342(x2)
i_6:
	addi x1, x18, 576
i_7:
	lb x9, 481(x2)
i_8:
	xori x13, x3, 1129
i_9:
	srai x20, x21, 4
i_10:
	sh x21, -128(x2)
i_11:
	and x21, x1, x30
i_12:
	lw x12, 76(x2)
i_13:
	slti x6, x25, -449
i_14:
	and x16, x22, x27
i_15:
	sub x9, x15, x28
i_16:
	bne x2, x6, i_24
i_17:
	srli x28, x22, 2
i_18:
	bltu x11, x8, i_24
i_19:
	bgeu x22, x9, i_27
i_20:
	lb x28, -289(x2)
i_21:
	sw x23, -180(x2)
i_22:
	addi x10, x6, -93
i_23:
	srli x30, x5, 1
i_24:
	sw x11, 368(x2)
i_25:
	auipc x27, 31390
i_26:
	lbu x1, 213(x2)
i_27:
	addi x5, x1, 876
i_28:
	sltiu x1, x6, -1495
i_29:
	lhu x27, -124(x2)
i_30:
	add x6, x10, x13
i_31:
	lbu x1, -322(x2)
i_32:
	sb x6, 112(x2)
i_33:
	lhu x10, 454(x2)
i_34:
	addi x29, x0, 5
i_35:
	srl x12, x24, x29
i_36:
	lw x9, -56(x2)
i_37:
	sltiu x8, x26, -455
i_38:
	sw x31, 12(x2)
i_39:
	sh x28, -104(x2)
i_40:
	lhu x12, 152(x2)
i_41:
	sb x4, 405(x2)
i_42:
	lw x4, 408(x2)
i_43:
	lbu x13, 362(x2)
i_44:
	srai x27, x4, 1
i_45:
	sb x12, -221(x2)
i_46:
	lw x14, 212(x2)
i_47:
	srli x19, x17, 4
i_48:
	lbu x17, 158(x2)
i_49:
	addi x6, x0, 18
i_50:
	sll x17, x18, x6
i_51:
	addi x27, x0, -1986
i_52:
	addi x4, x0, -1982
i_53:
	bltu x23, x17, i_55
i_54:
	lb x23, -195(x2)
i_55:
	lhu x25, 334(x2)
i_56:
	sh x26, -346(x2)
i_57:
	lui x16, 779698
i_58:
	lw x6, -484(x2)
i_59:
	blt x5, x27, i_66
i_60:
	sh x30, 346(x2)
i_61:
	lb x23, -168(x2)
i_62:
	addi x27 , x27 , 1
	bne x27, x4, i_52
i_63:
	lw x28, -420(x2)
i_64:
	lb x14, -143(x2)
i_65:
	sb x7, 244(x2)
i_66:
	lb x23, -278(x2)
i_67:
	auipc x18, 525755
i_68:
	bltu x3, x18, i_73
i_69:
	blt x31, x14, i_79
i_70:
	sb x12, -345(x2)
i_71:
	bne x8, x3, i_81
i_72:
	sb x7, -342(x2)
i_73:
	nop
i_74:
	sub x12, x12, x18
i_75:
	nop
i_76:
	nop
i_77:
	lb x12, 335(x2)
i_78:
	add x6, x23, x16
i_79:
	lh x8, -130(x2)
i_80:
	lhu x23, -182(x2)
i_81:
	sb x24, 276(x2)
i_82:
	add x16, x20, x26
i_83:
	addi x25, x0, -2039
i_84:
	addi x28, x0, -2036
i_85:
	lhu x4, 334(x2)
i_86:
	add x6, x5, x8
i_87:
	addi x25 , x25 , 1
	bltu x25, x28, i_85
i_88:
	lbu x26, -173(x2)
i_89:
	lh x4, -78(x2)
i_90:
	lbu x27, 106(x2)
i_91:
	sh x20, -320(x2)
i_92:
	lbu x20, 330(x2)
i_93:
	lh x8, 442(x2)
i_94:
	addi x1, x0, 2
i_95:
	sra x25, x9, x1
i_96:
	add x1, x1, x21
i_97:
	slt x3, x8, x25
i_98:
	addi x20, x0, 8
i_99:
	sra x25, x25, x20
i_100:
	bgeu x5, x3, i_107
i_101:
	add x1, x27, x28
i_102:
	lw x9, -48(x2)
i_103:
	lhu x3, -356(x2)
i_104:
	sltu x23, x1, x23
i_105:
	lh x1, -392(x2)
i_106:
	blt x21, x21, i_114
i_107:
	bge x1, x9, i_113
i_108:
	sb x8, -159(x2)
i_109:
	sb x1, -105(x2)
i_110:
	nop
i_111:
	sltu x22, x19, x27
i_112:
	nop
i_113:
	nop
i_114:
	lui x1, 486588
i_115:
	sub x30, x12, x5
i_116:
	addi x15, x0, 2036
i_117:
	addi x14, x0, 2039
i_118:
	nop
i_119:
	sw x30, 340(x2)
i_120:
	lw x27, 156(x2)
i_121:
	addi x15 , x15 , 1
	bne x15, x14, i_118
i_122:
	addi x3, x0, 7
i_123:
	sll x30, x16, x3
i_124:
	lhu x17, 96(x2)
i_125:
	ori x17, x3, -1225
i_126:
	bge x16, x12, i_135
i_127:
	bge x17, x8, i_131
i_128:
	srli x12, x19, 3
i_129:
	sb x15, -40(x2)
i_130:
	lui x15, 756786
i_131:
	sh x14, -350(x2)
i_132:
	lhu x21, -474(x2)
i_133:
	bltu x25, x27, i_140
i_134:
	and x9, x9, x13
i_135:
	bltu x31, x31, i_136
i_136:
	bge x4, x4, i_138
i_137:
	bgeu x1, x20, i_147
i_138:
	lbu x15, -214(x2)
i_139:
	srli x28, x4, 3
i_140:
	slli x20, x15, 2
i_141:
	bltu x16, x28, i_146
i_142:
	sh x21, 60(x2)
i_143:
	blt x4, x27, i_150
i_144:
	lw x28, 220(x2)
i_145:
	and x4, x18, x29
i_146:
	addi x1, x0, 26
i_147:
	sra x28, x25, x1
i_148:
	sltu x25, x4, x18
i_149:
	sb x19, -173(x2)
i_150:
	sh x1, -162(x2)
i_151:
	auipc x24, 457386
i_152:
	beq x3, x27, i_161
i_153:
	blt x5, x25, i_161
i_154:
	bge x8, x19, i_161
i_155:
	sw x1, -272(x2)
i_156:
	blt x27, x4, i_164
i_157:
	xori x4, x7, -163
i_158:
	sh x1, -176(x2)
i_159:
	addi x24, x16, 130
i_160:
	bne x30, x15, i_168
i_161:
	lh x1, -340(x2)
i_162:
	lbu x29, 348(x2)
i_163:
	ori x10, x22, -1519
i_164:
	sb x21, 202(x2)
i_165:
	sh x11, 380(x2)
i_166:
	sb x9, 48(x2)
i_167:
	lh x10, -478(x2)
i_168:
	lbu x24, -42(x2)
i_169:
	sw x4, 436(x2)
i_170:
	sltu x10, x13, x4
i_171:
	lb x27, 245(x2)
i_172:
	ori x13, x8, -106
i_173:
	lui x26, 21484
i_174:
	addi x10, x0, -1881
i_175:
	addi x19, x0, -1878
i_176:
	addi x10 , x10 , 1
	bltu x10, x19, i_176
i_177:
	lbu x15, 123(x2)
i_178:
	lh x4, 150(x2)
i_179:
	bgeu x8, x31, i_182
i_180:
	sh x28, 466(x2)
i_181:
	bge x30, x4, i_182
i_182:
	sub x6, x15, x28
i_183:
	sub x5, x25, x23
i_184:
	srai x3, x26, 4
i_185:
	srai x23, x5, 2
i_186:
	lb x21, 456(x2)
i_187:
	addi x6, x4, 1680
i_188:
	ori x1, x20, 129
i_189:
	lbu x17, 199(x2)
i_190:
	sltu x1, x1, x21
i_191:
	sub x10, x10, x14
i_192:
	lbu x20, 208(x2)
i_193:
	sb x5, -295(x2)
i_194:
	srai x11, x10, 3
i_195:
	sltiu x14, x28, 812
i_196:
	bge x9, x27, i_199
i_197:
	srli x24, x20, 2
i_198:
	lw x21, -32(x2)
i_199:
	lw x22, -136(x2)
i_200:
	sh x29, -94(x2)
i_201:
	xor x29, x29, x23
i_202:
	bge x3, x22, i_203
i_203:
	lb x18, 127(x2)
i_204:
	slli x5, x21, 3
i_205:
	lw x21, -476(x2)
i_206:
	blt x6, x5, i_210
i_207:
	srli x29, x12, 3
i_208:
	lb x12, -441(x2)
i_209:
	sw x1, 104(x2)
i_210:
	bge x12, x4, i_220
i_211:
	sw x30, -352(x2)
i_212:
	lw x12, -64(x2)
i_213:
	lbu x27, 364(x2)
i_214:
	lhu x21, -82(x2)
i_215:
	lh x1, 376(x2)
i_216:
	lhu x28, 154(x2)
i_217:
	beq x21, x12, i_227
i_218:
	lb x21, -111(x2)
i_219:
	andi x28, x9, 1620
i_220:
	bltu x24, x13, i_228
i_221:
	lb x8, 441(x2)
i_222:
	addi x16, x0, 6
i_223:
	sll x21, x21, x16
i_224:
	sub x8, x27, x6
i_225:
	slli x23, x25, 1
i_226:
	sh x16, 250(x2)
i_227:
	lh x21, -208(x2)
i_228:
	sb x4, -165(x2)
i_229:
	sb x27, 362(x2)
i_230:
	beq x15, x9, i_238
i_231:
	blt x22, x23, i_235
i_232:
	bge x8, x30, i_235
i_233:
	sw x5, -476(x2)
i_234:
	bgeu x12, x24, i_236
i_235:
	lbu x15, -203(x2)
i_236:
	xor x15, x22, x1
i_237:
	lbu x20, -473(x2)
i_238:
	beq x1, x8, i_240
i_239:
	sltiu x3, x7, 1062
i_240:
	sw x7, 92(x2)
i_241:
	sub x20, x24, x9
i_242:
	sltiu x28, x27, 103
i_243:
	addi x5, x20, 1954
i_244:
	and x20, x24, x13
i_245:
	lhu x29, 312(x2)
i_246:
	blt x11, x12, i_247
i_247:
	add x20, x26, x31
i_248:
	lbu x22, -246(x2)
i_249:
	sh x17, 468(x2)
i_250:
	sh x2, 92(x2)
i_251:
	lui x16, 295428
i_252:
	sw x6, -132(x2)
i_253:
	sh x27, -456(x2)
i_254:
	lb x16, 16(x2)
i_255:
	lb x8, 76(x2)
i_256:
	lhu x8, -270(x2)
i_257:
	lb x30, 137(x2)
i_258:
	lb x27, 341(x2)
i_259:
	sh x18, -274(x2)
i_260:
	lui x8, 480045
i_261:
	slt x26, x16, x8
i_262:
	lb x15, 144(x2)
i_263:
	sh x17, -134(x2)
i_264:
	lhu x26, -72(x2)
i_265:
	sltu x1, x24, x14
i_266:
	lb x25, 404(x2)
i_267:
	lw x28, 128(x2)
i_268:
	bne x4, x3, i_274
i_269:
	sw x10, -368(x2)
i_270:
	bne x10, x10, i_280
i_271:
	lb x22, -473(x2)
i_272:
	slt x16, x9, x6
i_273:
	nop
i_274:
	nop
i_275:
	lhu x23, 308(x2)
i_276:
	add x21, x21, x15
i_277:
	slt x26, x22, x24
i_278:
	nop
i_279:
	lb x26, -427(x2)
i_280:
	lw x26, -288(x2)
i_281:
	lh x14, -14(x2)
i_282:
	addi x4, x0, 1967
i_283:
	addi x10, x0, 1969
i_284:
	sb x1, -394(x2)
i_285:
	addi x4 , x4 , 1
	bge x10, x4, i_284
i_286:
	bge x9, x24, i_294
i_287:
	ori x24, x16, -1512
i_288:
	ori x28, x1, 1745
i_289:
	xor x1, x9, x22
i_290:
	lb x22, 17(x2)
i_291:
	nop
i_292:
	slli x24, x4, 2
i_293:
	lb x19, -401(x2)
i_294:
	addi x4, x0, 29
i_295:
	sll x11, x22, x4
i_296:
	addi x23, x0, -2003
i_297:
	addi x21, x0, -2001
i_298:
	add x15, x18, x1
i_299:
	and x19, x27, x23
i_300:
	addi x23 , x23 , 1
	blt x23, x21, i_298
i_301:
	bltu x22, x19, i_311
i_302:
	addi x19, x0, 30
i_303:
	srl x15, x4, x19
i_304:
	sh x20, -396(x2)
i_305:
	sh x29, -442(x2)
i_306:
	sub x20, x19, x17
i_307:
	sh x9, 154(x2)
i_308:
	auipc x6, 972370
i_309:
	lh x17, -272(x2)
i_310:
	sw x17, -424(x2)
i_311:
	lhu x28, 402(x2)
i_312:
	andi x26, x28, -1462
i_313:
	lhu x11, 294(x2)
i_314:
	lw x15, 68(x2)
i_315:
	lb x28, 317(x2)
i_316:
	sw x11, -408(x2)
i_317:
	lb x11, -13(x2)
i_318:
	lw x12, -228(x2)
i_319:
	auipc x11, 451469
i_320:
	lbu x18, 414(x2)
i_321:
	beq x21, x9, i_328
i_322:
	nop
i_323:
	srli x18, x12, 1
i_324:
	sw x14, 44(x2)
i_325:
	nop
i_326:
	lbu x14, 298(x2)
i_327:
	and x29, x19, x29
i_328:
	sw x10, -420(x2)
i_329:
	lbu x27, -76(x2)
i_330:
	addi x30, x0, -1863
i_331:
	addi x9, x0, -1861
i_332:
	lbu x26, -351(x2)
i_333:
	lbu x17, 116(x2)
i_334:
	addi x30 , x30 , 1
	blt x30, x9, i_332
i_335:
	lh x8, -232(x2)
i_336:
	sh x25, 28(x2)
i_337:
	sh x17, 380(x2)
i_338:
	sw x19, 288(x2)
i_339:
	bgeu x31, x27, i_346
i_340:
	lbu x8, 403(x2)
i_341:
	lhu x8, 64(x2)
i_342:
	lhu x25, 434(x2)
i_343:
	lh x1, 290(x2)
i_344:
	sh x17, 466(x2)
i_345:
	lbu x12, -302(x2)
i_346:
	lbu x26, 488(x2)
i_347:
	lh x15, 212(x2)
i_348:
	addi x17, x0, -1901
i_349:
	addi x19, x0, -1898
i_350:
	lh x22, -398(x2)
i_351:
	sb x1, 78(x2)
i_352:
	bltu x26, x8, i_358
i_353:
	sb x2, 1(x2)
i_354:
	lhu x16, 346(x2)
i_355:
	addi x17 , x17 , 1
	bgeu x19, x17, i_350
i_356:
	beq x13, x1, i_361
i_357:
	lhu x16, 290(x2)
i_358:
	lb x24, 464(x2)
i_359:
	andi x24, x6, 1171
i_360:
	lw x26, -208(x2)
i_361:
	addi x16, x0, 30
i_362:
	sll x18, x15, x16
i_363:
	addi x14, x0, 1945
i_364:
	addi x1, x0, 1947
i_365:
	lb x11, -132(x2)
i_366:
	lw x6, 120(x2)
i_367:
	addi x14 , x14 , 1
	blt x14, x1, i_365
i_368:
	ori x9, x26, -187
i_369:
	lw x22, -348(x2)
i_370:
	sh x23, -228(x2)
i_371:
	lbu x12, 76(x2)
i_372:
	addi x9, x0, -1875
i_373:
	addi x11, x0, -1873
i_374:
	addi x21, x0, 8
i_375:
	srl x6, x16, x21
i_376:
	lhu x23, -446(x2)
i_377:
	bge x7, x20, i_379
i_378:
	lbu x27, 202(x2)
i_379:
	bgeu x28, x23, i_380
i_380:
	lhu x27, 198(x2)
i_381:
	lbu x27, -365(x2)
i_382:
	lh x23, 52(x2)
i_383:
	bgeu x21, x7, i_389
i_384:
	bltu x6, x27, i_388
i_385:
	addi x9 , x9 , 1
	bgeu x11, x9, i_374
i_386:
	sw x21, -456(x2)
i_387:
	nop
i_388:
	addi x5, x0, 31
i_389:
	srl x8, x6, x5
i_390:
	ori x6, x25, 285
i_391:
	addi x26, x0, 1912
i_392:
	addi x27, x0, 1915
i_393:
	andi x6, x6, 701
i_394:
	addi x26 , x26 , 1
	bge x27, x26, i_393
i_395:
	lhu x6, 178(x2)
i_396:
	lui x13, 415996
i_397:
	sh x28, 170(x2)
i_398:
	addi x16, x0, 13
i_399:
	sll x21, x4, x16
i_400:
	slti x16, x20, -1882
i_401:
	sub x26, x13, x3
i_402:
	lw x10, 216(x2)
i_403:
	sltu x10, x23, x10
i_404:
	addi x16, x18, -1224
i_405:
	sw x11, -92(x2)
i_406:
	lh x8, 400(x2)
i_407:
	lhu x18, -298(x2)
i_408:
	sb x16, -245(x2)
i_409:
	lb x3, 206(x2)
i_410:
	andi x14, x11, 242
i_411:
	sw x15, 316(x2)
i_412:
	add x9, x13, x9
i_413:
	andi x15, x14, 1416
i_414:
	lb x15, 306(x2)
i_415:
	sb x14, 6(x2)
i_416:
	andi x9, x3, -1889
i_417:
	addi x14, x0, 1962
i_418:
	addi x15, x0, 1964
i_419:
	bge x25, x14, i_427
i_420:
	addi x14 , x14 , 1
	bne  x15, x14, i_419
i_421:
	sb x14, -219(x2)
i_422:
	lbu x15, -4(x2)
i_423:
	sb x25, -214(x2)
i_424:
	srai x10, x2, 2
i_425:
	addi x15, x0, 18
i_426:
	sll x14, x5, x15
i_427:
	sltiu x8, x8, -748
i_428:
	lb x13, -90(x2)
i_429:
	beq x24, x13, i_432
i_430:
	xori x9, x31, 1716
i_431:
	bgeu x14, x8, i_441
i_432:
	bgeu x23, x21, i_442
i_433:
	lw x28, -436(x2)
i_434:
	lbu x23, 186(x2)
i_435:
	lw x26, 316(x2)
i_436:
	sh x7, -194(x2)
i_437:
	lh x6, -416(x2)
i_438:
	addi x6, x0, 28
i_439:
	sra x10, x25, x6
i_440:
	lh x11, -332(x2)
i_441:
	auipc x11, 124774
i_442:
	lh x26, 384(x2)
i_443:
	lh x21, 430(x2)
i_444:
	xor x8, x31, x6
i_445:
	sb x14, 480(x2)
i_446:
	lb x12, -205(x2)
i_447:
	sub x13, x28, x15
i_448:
	sh x22, 118(x2)
i_449:
	sb x21, 165(x2)
i_450:
	bne x21, x26, i_455
i_451:
	lbu x11, 65(x2)
i_452:
	lbu x5, -19(x2)
i_453:
	sw x8, 336(x2)
i_454:
	sh x25, 476(x2)
i_455:
	addi x21, x0, 4
i_456:
	sll x29, x11, x21
i_457:
	bgeu x13, x11, i_462
i_458:
	lw x4, -452(x2)
i_459:
	slti x24, x11, 1392
i_460:
	lh x22, -168(x2)
i_461:
	xori x3, x22, -346
i_462:
	blt x4, x16, i_470
i_463:
	andi x11, x21, 110
i_464:
	lbu x23, 110(x2)
i_465:
	lh x16, 416(x2)
i_466:
	sb x9, -256(x2)
i_467:
	xor x8, x31, x15
i_468:
	bltu x23, x11, i_474
i_469:
	blt x4, x7, i_474
i_470:
	lbu x8, 400(x2)
i_471:
	bltu x5, x9, i_474
i_472:
	lbu x9, -480(x2)
i_473:
	lw x9, -316(x2)
i_474:
	lhu x29, -426(x2)
i_475:
	add x10, x22, x3
i_476:
	lbu x28, 227(x2)
i_477:
	lhu x10, -346(x2)
i_478:
	lbu x20, 178(x2)
i_479:
	lw x19, 336(x2)
i_480:
	xor x19, x19, x9
i_481:
	sh x5, -350(x2)
i_482:
	sb x6, 1(x2)
i_483:
	lui x16, 91273
i_484:
	sub x9, x25, x19
i_485:
	addi x21, x0, 11
i_486:
	sra x28, x19, x21
i_487:
	lw x9, -368(x2)
i_488:
	beq x12, x7, i_490
i_489:
	bge x6, x16, i_496
i_490:
	lw x14, 348(x2)
i_491:
	lb x19, -45(x2)
i_492:
	lb x1, 325(x2)
i_493:
	sltu x19, x7, x6
i_494:
	lbu x6, 200(x2)
i_495:
	lhu x22, -50(x2)
i_496:
	beq x8, x19, i_499
i_497:
	lb x4, 81(x2)
i_498:
	and x8, x17, x19
i_499:
	lb x11, -410(x2)
i_500:
	lb x23, 391(x2)
i_501:
	lhu x17, 46(x2)
i_502:
	sb x26, 195(x2)
i_503:
	sw x26, -356(x2)
i_504:
	lbu x29, 89(x2)
i_505:
	lb x28, -274(x2)
i_506:
	or x16, x7, x18
i_507:
	lbu x1, -209(x2)
i_508:
	addi x26, x0, 1906
i_509:
	addi x17, x0, 1909
i_510:
	addi x26 , x26 , 1
	bne x26, x17, i_510
i_511:
	lhu x29, -212(x2)
i_512:
	sh x19, 326(x2)
i_513:
	lh x14, 162(x2)
i_514:
	addi x28, x0, 1957
i_515:
	addi x27, x0, 1961
i_516:
	lh x5, 482(x2)
i_517:
	xori x15, x29, -413
i_518:
	lw x26, -372(x2)
i_519:
	lbu x4, -486(x2)
i_520:
	ori x4, x20, -1879
i_521:
	slli x20, x10, 4
i_522:
	lh x20, -250(x2)
i_523:
	addi x28 , x28 , 1
	bne  x27, x28, i_516
i_524:
	sw x6, 132(x2)
i_525:
	and x20, x2, x17
i_526:
	lh x12, -370(x2)
i_527:
	add x26, x4, x31
i_528:
	lbu x20, -241(x2)
i_529:
	sb x9, 374(x2)
i_530:
	lw x26, 264(x2)
i_531:
	lw x22, -244(x2)
i_532:
	lw x14, -184(x2)
i_533:
	lhu x13, 174(x2)
i_534:
	bltu x5, x15, i_544
i_535:
	slli x28, x13, 2
i_536:
	add x8, x4, x26
i_537:
	lb x21, -441(x2)
i_538:
	add x28, x13, x12
i_539:
	lw x28, -296(x2)
i_540:
	lh x14, 90(x2)
i_541:
	slti x25, x25, 625
i_542:
	auipc x17, 788037
i_543:
	xor x14, x23, x25
i_544:
	srai x25, x17, 3
i_545:
	auipc x15, 944507
i_546:
	lb x12, 58(x2)
i_547:
	lbu x24, -174(x2)
i_548:
	addi x9, x0, 10
i_549:
	sra x25, x28, x9
i_550:
	blt x25, x20, i_556
i_551:
	lbu x17, 301(x2)
i_552:
	lb x30, -475(x2)
i_553:
	lui x21, 838778
i_554:
	lw x3, -240(x2)
i_555:
	sw x29, 4(x2)
i_556:
	lh x25, 286(x2)
i_557:
	ori x4, x23, 1751
i_558:
	beq x6, x25, i_563
i_559:
	slli x25, x7, 3
i_560:
	blt x27, x15, i_562
i_561:
	lbu x6, 293(x2)
i_562:
	bge x20, x10, i_567
i_563:
	blt x29, x26, i_571
i_564:
	beq x18, x13, i_565
i_565:
	and x18, x16, x27
i_566:
	slli x27, x31, 1
i_567:
	bne x6, x23, i_577
i_568:
	lw x26, 188(x2)
i_569:
	bne x26, x26, i_571
i_570:
	sh x17, -472(x2)
i_571:
	andi x11, x8, 636
i_572:
	or x1, x27, x27
i_573:
	bne x1, x11, i_580
i_574:
	beq x12, x28, i_575
i_575:
	lb x11, -398(x2)
i_576:
	xori x26, x27, -568
i_577:
	nop
i_578:
	addi x27, x2, -957
i_579:
	lhu x30, 140(x2)
i_580:
	addi x18, x0, 29
i_581:
	srl x15, x27, x18
i_582:
	addi x26, x0, 1995
i_583:
	addi x8, x0, 1997
i_584:
	addi x26 , x26 , 1
	blt x26, x8, i_584
i_585:
	srli x25, x12, 2
i_586:
	addi x30, x0, 5
i_587:
	sll x9, x28, x30
i_588:
	ori x22, x29, 125
i_589:
	lh x25, 400(x2)
i_590:
	srli x9, x27, 2
i_591:
	lw x9, 172(x2)
i_592:
	sh x3, -42(x2)
i_593:
	sw x15, -8(x2)
i_594:
	slli x28, x2, 2
i_595:
	addi x1, x0, 1921
i_596:
	addi x4, x0, 1925
i_597:
	addi x1 , x1 , 1
	blt x1, x4, i_597
i_598:
	bltu x27, x18, i_602
i_599:
	lb x6, -249(x2)
i_600:
	addi x15, x0, 24
i_601:
	sra x27, x28, x15
i_602:
	sw x6, 68(x2)
i_603:
	sw x25, 36(x2)
i_604:
	lbu x6, 48(x2)
i_605:
	xori x25, x9, 265
i_606:
	auipc x6, 964641
i_607:
	or x26, x15, x8
i_608:
	xor x27, x1, x23
i_609:
	sltu x10, x26, x5
i_610:
	addi x5, x0, -1988
i_611:
	addi x11, x0, -1984
i_612:
	nop
i_613:
	lh x10, -40(x2)
i_614:
	andi x14, x13, 1962
i_615:
	sltu x28, x22, x1
i_616:
	addi x5 , x5 , 1
	bltu x5, x11, i_612
i_617:
	lhu x17, 192(x2)
i_618:
	and x25, x17, x16
i_619:
	sh x11, 198(x2)
i_620:
	lw x4, 140(x2)
i_621:
	addi x25, x0, 14
i_622:
	srl x11, x27, x25
i_623:
	bne x23, x4, i_628
i_624:
	bltu x4, x13, i_628
i_625:
	addi x10, x0, 7
i_626:
	sll x18, x22, x10
i_627:
	andi x5, x10, 537
i_628:
	sb x29, -129(x2)
i_629:
	lb x5, -425(x2)
i_630:
	sltiu x10, x11, -1140
i_631:
	slti x12, x10, -1204
i_632:
	blt x11, x5, i_638
i_633:
	addi x8, x0, 3
i_634:
	srl x12, x5, x8
i_635:
	and x11, x23, x29
i_636:
	lw x19, -484(x2)
i_637:
	lw x6, -480(x2)
i_638:
	sh x16, -18(x2)
i_639:
	and x19, x14, x31
i_640:
	lh x12, 430(x2)
i_641:
	bltu x21, x11, i_650
i_642:
	bne x12, x6, i_650
i_643:
	lb x1, -230(x2)
i_644:
	sb x26, 293(x2)
i_645:
	lb x10, -382(x2)
i_646:
	lw x6, 400(x2)
i_647:
	sh x7, -324(x2)
i_648:
	bge x16, x3, i_653
i_649:
	lb x15, -140(x2)
i_650:
	sw x7, -416(x2)
i_651:
	ori x23, x1, 1720
i_652:
	lw x17, -112(x2)
i_653:
	lb x17, -343(x2)
i_654:
	bne x22, x11, i_662
i_655:
	sw x2, -252(x2)
i_656:
	lh x1, 442(x2)
i_657:
	sb x9, 173(x2)
i_658:
	bltu x16, x1, i_668
i_659:
	addi x9, x9, 1894
i_660:
	lb x27, -15(x2)
i_661:
	slt x10, x7, x9
i_662:
	lh x13, 206(x2)
i_663:
	add x8, x23, x20
i_664:
	addi x19, x0, 21
i_665:
	sll x13, x16, x19
i_666:
	bgeu x9, x18, i_674
i_667:
	lw x18, -480(x2)
i_668:
	sw x26, 160(x2)
i_669:
	lhu x19, 138(x2)
i_670:
	sb x16, 94(x2)
i_671:
	add x11, x28, x22
i_672:
	sltu x19, x17, x19
i_673:
	lw x17, 176(x2)
i_674:
	lhu x6, -370(x2)
i_675:
	sh x2, -266(x2)
i_676:
	sb x28, 439(x2)
i_677:
	lhu x24, -184(x2)
i_678:
	lbu x17, 486(x2)
i_679:
	bgeu x5, x19, i_686
i_680:
	lh x4, -14(x2)
i_681:
	addi x17, x13, 1157
i_682:
	xori x11, x17, 1253
i_683:
	addi x11, x0, 14
i_684:
	sra x4, x9, x11
i_685:
	sw x23, 112(x2)
i_686:
	sh x5, 42(x2)
i_687:
	bne x4, x11, i_697
i_688:
	blt x9, x28, i_691
i_689:
	and x3, x11, x3
i_690:
	lbu x20, 82(x2)
i_691:
	xori x14, x21, -90
i_692:
	lh x1, -370(x2)
i_693:
	sh x15, -276(x2)
i_694:
	lbu x21, 399(x2)
i_695:
	lb x13, -265(x2)
i_696:
	srai x6, x5, 3
i_697:
	lui x15, 406951
i_698:
	lbu x13, -31(x2)
i_699:
	add x13, x27, x11
i_700:
	lhu x27, 342(x2)
i_701:
	addi x15, x0, -1940
i_702:
	addi x6, x0, -1936
i_703:
	andi x27, x20, -812
i_704:
	add x20, x31, x20
i_705:
	lbu x12, -265(x2)
i_706:
	addi x1, x0, 18
i_707:
	srl x25, x12, x1
i_708:
	lw x24, -448(x2)
i_709:
	sub x1, x20, x5
i_710:
	addi x15 , x15 , 1
	bge x6, x15, i_703
i_711:
	bne x22, x29, i_715
i_712:
	blt x20, x15, i_714
i_713:
	lw x12, 316(x2)
i_714:
	slti x22, x20, -1004
i_715:
	blt x2, x1, i_718
i_716:
	lw x22, 192(x2)
i_717:
	sh x1, 264(x2)
i_718:
	bne x24, x20, i_728
i_719:
	bne x26, x16, i_724
i_720:
	sub x14, x24, x7
i_721:
	lhu x29, -222(x2)
i_722:
	lhu x11, 338(x2)
i_723:
	addi x4, x4, -622
i_724:
	lbu x1, -212(x2)
i_725:
	add x5, x8, x11
i_726:
	lw x13, -404(x2)
i_727:
	lb x5, -422(x2)
i_728:
	lh x30, 430(x2)
i_729:
	lhu x5, 30(x2)
i_730:
	addi x15, x0, -1898
i_731:
	addi x25, x0, -1895
i_732:
	lw x21, -440(x2)
i_733:
	lw x8, -200(x2)
i_734:
	addi x15 , x15 , 1
	bgeu x25, x15, i_732
i_735:
	sh x3, 286(x2)
i_736:
	lb x30, 8(x2)
i_737:
	lw x8, -392(x2)
i_738:
	bge x4, x8, i_739
i_739:
	lui x8, 815870
i_740:
	lw x4, 304(x2)
i_741:
	sltiu x19, x6, 552
i_742:
	sh x21, -428(x2)
i_743:
	lbu x8, 336(x2)
i_744:
	sb x30, 378(x2)
i_745:
	sb x19, 400(x2)
i_746:
	slti x22, x25, -715
i_747:
	lw x30, 212(x2)
i_748:
	addi x6, x0, 23
i_749:
	srl x15, x21, x6
i_750:
	bltu x10, x6, i_758
i_751:
	bge x12, x20, i_756
i_752:
	lw x23, 460(x2)
i_753:
	blt x29, x21, i_756
i_754:
	xori x21, x13, -1134
i_755:
	srli x25, x22, 3
i_756:
	auipc x16, 200308
i_757:
	sltiu x6, x25, 1883
i_758:
	sw x6, -112(x2)
i_759:
	andi x17, x25, -1530
i_760:
	addi x11, x31, 19
i_761:
	beq x8, x1, i_766
i_762:
	lb x11, -275(x2)
i_763:
	lhu x1, -476(x2)
i_764:
	addi x20, x0, 16
i_765:
	sra x3, x4, x20
i_766:
	lbu x11, -414(x2)
i_767:
	sh x31, 240(x2)
i_768:
	lui x24, 1033021
i_769:
	lbu x24, -166(x2)
i_770:
	sltiu x3, x1, -915
i_771:
	auipc x4, 357923
i_772:
	sltiu x29, x4, -1135
i_773:
	add x15, x18, x4
i_774:
	beq x11, x4, i_780
i_775:
	lw x29, -472(x2)
i_776:
	lhu x5, 220(x2)
i_777:
	sw x4, 408(x2)
i_778:
	srai x8, x27, 2
i_779:
	sw x23, -356(x2)
i_780:
	xori x28, x3, -1936
i_781:
	blt x14, x3, i_782
i_782:
	sb x24, -450(x2)
i_783:
	and x28, x5, x15
i_784:
	sltu x26, x9, x15
i_785:
	addi x4, x28, 564
i_786:
	sltiu x15, x6, 8
i_787:
	lw x5, 336(x2)
i_788:
	addi x28, x0, -1996
i_789:
	addi x20, x0, -1994
i_790:
	sb x21, -64(x2)
i_791:
	srai x23, x25, 3
i_792:
	sh x17, 364(x2)
i_793:
	addi x28 , x28 , 1
	bltu x28, x20, i_790
i_794:
	beq x30, x20, i_797
i_795:
	srai x1, x27, 2
i_796:
	lb x1, 278(x2)
i_797:
	blt x18, x20, i_800
i_798:
	sh x9, -210(x2)
i_799:
	slt x18, x18, x13
i_800:
	sh x10, 138(x2)
i_801:
	sltiu x12, x6, 1304
i_802:
	addi x8, x27, 2015
i_803:
	lhu x10, -374(x2)
i_804:
	sb x25, 471(x2)
i_805:
	add x10, x7, x14
i_806:
	sh x17, -216(x2)
i_807:
	addi x5, x0, 22
i_808:
	srl x1, x29, x5
i_809:
	sw x15, -420(x2)
i_810:
	andi x5, x10, 1182
i_811:
	sub x20, x21, x29
i_812:
	lui x21, 959024
i_813:
	or x3, x20, x20
i_814:
	sw x31, 280(x2)
i_815:
	srli x5, x1, 4
i_816:
	lw x29, 424(x2)
i_817:
	srai x19, x3, 1
i_818:
	srli x27, x25, 2
i_819:
	lw x19, 4(x2)
i_820:
	bne x11, x18, i_826
i_821:
	lhu x3, 58(x2)
i_822:
	lw x29, 220(x2)
i_823:
	slli x19, x29, 1
i_824:
	beq x6, x26, i_830
i_825:
	lh x16, -146(x2)
i_826:
	and x16, x15, x5
i_827:
	sw x9, 132(x2)
i_828:
	lb x3, -421(x2)
i_829:
	slt x28, x2, x21
i_830:
	nop
i_831:
	sw x18, 112(x2)
i_832:
	addi x12, x0, -1847
i_833:
	addi x28, x0, -1844
i_834:
	xori x9, x3, 1828
i_835:
	lw x16, 80(x2)
i_836:
	addi x21, x0, -1897
i_837:
	addi x18, x0, -1895
i_838:
	addi x21 , x21 , 1
	bge x18, x21, i_838
i_839:
	lhu x16, 440(x2)
i_840:
	add x29, x4, x13
i_841:
	addi x12 , x12 , 1
	blt x12, x28, i_834
i_842:
	sb x27, 374(x2)
i_843:
	lhu x12, -192(x2)
i_844:
	sb x14, 444(x2)
i_845:
	xori x10, x16, -1416
i_846:
	andi x27, x11, -1616
i_847:
	slt x12, x28, x12
i_848:
	and x11, x19, x31
i_849:
	sb x25, -255(x2)
i_850:
	sltiu x10, x14, -598
i_851:
	sw x10, 288(x2)
i_852:
	sh x10, 394(x2)
i_853:
	addi x10, x0, -1844
i_854:
	addi x5, x0, -1842
i_855:
	lb x18, 223(x2)
i_856:
	and x13, x21, x26
i_857:
	ori x11, x16, 1116
i_858:
	addi x26, x14, -943
i_859:
	sb x27, -290(x2)
i_860:
	lh x16, -150(x2)
i_861:
	addi x26, x0, 11
i_862:
	srl x21, x19, x26
i_863:
	addi x10 , x10 , 1
	bltu x10, x5, i_855
i_864:
	bltu x9, x27, i_870
i_865:
	bge x21, x25, i_868
i_866:
	sw x21, 264(x2)
i_867:
	add x21, x12, x19
i_868:
	lhu x6, -56(x2)
i_869:
	bltu x26, x8, i_875
i_870:
	sb x17, 402(x2)
i_871:
	sh x18, 124(x2)
i_872:
	slli x26, x10, 2
i_873:
	lhu x8, 120(x2)
i_874:
	sh x28, -348(x2)
i_875:
	add x10, x14, x11
i_876:
	sb x24, 218(x2)
i_877:
	sw x21, 408(x2)
i_878:
	bne x19, x28, i_884
i_879:
	addi x30, x0, 20
i_880:
	sra x22, x13, x30
i_881:
	lw x24, -160(x2)
i_882:
	lbu x13, -235(x2)
i_883:
	bgeu x19, x31, i_890
i_884:
	lb x9, 354(x2)
i_885:
	auipc x29, 712560
i_886:
	blt x29, x25, i_896
i_887:
	bltu x14, x13, i_888
i_888:
	lw x29, 408(x2)
i_889:
	lw x3, -228(x2)
i_890:
	xori x17, x16, -424
i_891:
	lh x17, 110(x2)
i_892:
	sh x18, -158(x2)
i_893:
	ori x25, x13, 2005
i_894:
	lbu x24, 174(x2)
i_895:
	sltiu x18, x8, -907
i_896:
	slli x15, x18, 3
i_897:
	bgeu x7, x16, i_905
i_898:
	lh x15, 116(x2)
i_899:
	sltiu x30, x16, -1608
i_900:
	sltiu x26, x1, -1822
i_901:
	lbu x10, 34(x2)
i_902:
	bgeu x8, x27, i_912
i_903:
	sh x11, 358(x2)
i_904:
	slti x30, x8, -1049
i_905:
	addi x8, x0, 25
i_906:
	sll x8, x21, x8
i_907:
	lbu x8, -20(x2)
i_908:
	lw x24, 124(x2)
i_909:
	lh x24, 20(x2)
i_910:
	bltu x8, x8, i_913
i_911:
	lbu x20, -275(x2)
i_912:
	slli x5, x10, 4
i_913:
	bne x21, x7, i_923
i_914:
	lbu x10, -423(x2)
i_915:
	sh x30, 264(x2)
i_916:
	lh x5, 482(x2)
i_917:
	blt x5, x7, i_923
i_918:
	sh x25, 478(x2)
i_919:
	lh x13, 104(x2)
i_920:
	auipc x25, 993907
i_921:
	sw x29, -360(x2)
i_922:
	bge x7, x11, i_928
i_923:
	bgeu x15, x13, i_933
i_924:
	xor x9, x14, x25
i_925:
	bge x26, x20, i_926
i_926:
	sb x28, 317(x2)
i_927:
	xori x1, x11, -649
i_928:
	srli x4, x21, 4
i_929:
	sw x30, 364(x2)
i_930:
	lb x18, -255(x2)
i_931:
	slti x10, x7, -1971
i_932:
	sw x25, 468(x2)
i_933:
	add x25, x28, x26
i_934:
	lb x23, -122(x2)
i_935:
	lhu x8, 314(x2)
i_936:
	addi x18, x0, 4
i_937:
	srl x23, x29, x18
i_938:
	lh x18, -418(x2)
i_939:
	lw x5, 72(x2)
i_940:
	sb x12, 435(x2)
i_941:
	lh x15, 78(x2)
i_942:
	lb x5, 221(x2)
i_943:
	auipc x22, 360015
i_944:
	andi x23, x20, -1010
i_945:
	lw x29, -24(x2)
i_946:
	sb x23, 448(x2)
i_947:
	xor x11, x27, x26
i_948:
	bgeu x20, x16, i_957
i_949:
	bltu x18, x25, i_956
i_950:
	add x6, x23, x14
i_951:
	or x13, x23, x23
i_952:
	beq x27, x8, i_958
i_953:
	lb x3, -484(x2)
i_954:
	srai x27, x14, 3
i_955:
	sub x13, x30, x16
i_956:
	lb x19, -36(x2)
i_957:
	sw x9, 204(x2)
i_958:
	lhu x3, 162(x2)
i_959:
	lb x13, 32(x2)
i_960:
	sh x8, -252(x2)
i_961:
	lbu x15, -147(x2)
i_962:
	sw x1, 96(x2)
i_963:
	lbu x3, 108(x2)
i_964:
	slli x23, x30, 4
i_965:
	lw x3, 416(x2)
i_966:
	bgeu x23, x16, i_970
i_967:
	addi x11, x0, 3
i_968:
	srl x25, x8, x11
i_969:
	add x8, x5, x24
i_970:
	lh x13, 216(x2)
i_971:
	sh x11, 238(x2)
i_972:
	sb x2, 118(x2)
i_973:
	or x19, x18, x4
i_974:
	addi x28, x0, 1931
i_975:
	addi x16, x0, 1935
i_976:
	sh x19, -286(x2)
i_977:
	slli x19, x14, 2
i_978:
	nop
i_979:
	addi x28 , x28 , 1
	blt x28, x16, i_976
i_980:
	lhu x16, 138(x2)
i_981:
	sb x31, 373(x2)
i_982:
	lhu x4, -382(x2)
i_983:
	blt x22, x17, i_988
i_984:
	ori x19, x28, -822
i_985:
	or x3, x4, x14
i_986:
	lw x4, -20(x2)
i_987:
	lw x30, 428(x2)
i_988:
	lbu x24, -380(x2)
i_989:
	sb x24, 112(x2)
i_990:
	addi x14, x0, 1885
i_991:
	addi x20, x0, 1889
i_992:
	lw x19, -468(x2)
i_993:
	nop
i_994:
	bltu x17, x7, i_1002
i_995:
	lbu x30, 445(x2)
i_996:
	addi x14 , x14 , 1
	bne  x20, x14, i_992
i_997:
	lb x19, -408(x2)
i_998:
	sh x11, 396(x2)
i_999:
	xor x8, x5, x4
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0xf8a9861e
	.word 0x290c99d7
	.word 0x6cceb2a8
	.word 0x944b9bb7
	.word 0x2491225e
	.word 0x7b4c0f3
	.word 0x6e7e44ce
	.word 0xa3ce91a
	.word 0x56b7f4c5
	.word 0xb9126861
	.word 0x97b51323
	.word 0x718dcb9e
	.word 0xe691cb15
	.word 0xc4a6736a
	.word 0xb194ccbb
	.word 0xcfd4a4f3
	.word 0x52703482
	.word 0x2c3f0bdf
	.word 0xde605edb
	.word 0x37345e15
	.word 0x653ba0e9
	.word 0x7af1cda3
	.word 0x364d6646
	.word 0xea87534
	.word 0x44deb213
	.word 0xe149b9d8
	.word 0x77262dc5
	.word 0x766625a9
	.word 0xc333ad82
	.word 0x8a5e1f69
	.word 0xe22edc2e
	.word 0x6824d490
	.word 0xeea7938f
	.word 0xd6a449f1
	.word 0xbe5f60e5
	.word 0x7d3b42e5
	.word 0x541cae0a
	.word 0xe1b2aeb0
	.word 0x89723f10
	.word 0xc7a42cbe
	.word 0xbcf1d4aa
	.word 0xcbbc737d
	.word 0x8c92c747
	.word 0x628650b5
	.word 0xa86dc975
	.word 0xf0f39e6
	.word 0xc2947d45
	.word 0x47847989
	.word 0x9956975b
	.word 0xc9b0c5c
	.word 0x4b75cb81
	.word 0x893b5692
	.word 0x1f529f1c
	.word 0x4799f21
	.word 0x834514e3
	.word 0xda58bdb9
	.word 0xbfc7b242
	.word 0xec82184f
	.word 0xc5d9e283
	.word 0x5fba4eb2
	.word 0x3d8b7fa0
	.word 0xe7431b6e
	.word 0x9d1e3132
	.word 0xa2d1f039
	.word 0x77707854
	.word 0xb2b23587
	.word 0x306e3567
	.word 0x6826e43d
	.word 0x2ede80f8
	.word 0x1d636bc1
	.word 0xcf2fcca3
	.word 0x92567ae7
	.word 0x8f2ebbbe
	.word 0x26b3a197
	.word 0x29600a75
	.word 0x5e5461b9
	.word 0x8c3e76a
	.word 0x4cb5cfcf
	.word 0x68236aed
	.word 0x84940422
	.word 0x6c2eb4cd
	.word 0x82aca849
	.word 0x3a6bc7a
	.word 0x7a8e4c1c
	.word 0x32371596
	.word 0x72f74dfd
	.word 0xa693d755
	.word 0x1ad564db
	.word 0x986f7801
	.word 0x924d9da
	.word 0xaa6b506c
	.word 0x2ab48e3a
	.word 0x64ccb7a9
	.word 0x827a5998
	.word 0x266915bf
	.word 0x3503f30d
	.word 0x4b503c2
	.word 0xb26c9c01
	.word 0x78471cb
	.word 0x88224f7e
	.word 0xf1d8677e
	.word 0x1d3a9869
	.word 0x80e752be
	.word 0x2c92542c
	.word 0xded2e9a8
	.word 0x9ec6e324
	.word 0xc703f1a5
	.word 0x16ffa00c
	.word 0xbf351393
	.word 0x45baec26
	.word 0x78bbada8
	.word 0x5aaca7ee
	.word 0x4a24f642
	.word 0xd10f77fb
	.word 0xb65524a0
	.word 0xffafd519
	.word 0x19df6844
	.word 0x5049baac
	.word 0xfd69b440
	.word 0xe87295a9
	.word 0x522dba00
	.word 0xba3d42fc
	.word 0x50d190bb
	.word 0xa99308da
	.word 0x2c8a355a
	.word 0xa4b7beaf
	.word 0x9b78fe46
	.word 0x583b4705
	.word 0x419cf067
	.word 0xb9f42d2b
	.word 0x5b0c82ca
	.word 0x4005a1b4
	.word 0x121e2ca2
	.word 0x43422ab0
	.word 0x6bcb4563
	.word 0x2ce3ed5f
	.word 0xa4176c7d
	.word 0x7085b08f
	.word 0x6eff7bfe
	.word 0x531370e9
	.word 0xcd50a505
	.word 0x6c6a4fd2
	.word 0xb8835a5
	.word 0x9fc0728
	.word 0x3888668d
	.word 0xdd1274a
	.word 0x20468b1d
	.word 0x5e59453a
	.word 0x20f109ee
	.word 0xef42a4e9
	.word 0x37307eef
	.word 0x79ee992f
	.word 0x75a1079c
	.word 0x1c296bd6
	.word 0x4eeed9d6
	.word 0xb72ed4aa
	.word 0x80a0de40
	.word 0xec02656e
	.word 0xac1ce354
	.word 0x57719d32
	.word 0x22b04e6f
	.word 0x20e54eb8
	.word 0x1f451f2c
	.word 0xca8ff0d6
	.word 0x7a2973b6
	.word 0x22bafb03
	.word 0xb6c56d5b
	.word 0x9f073fec
	.word 0x248ddf26
	.word 0x44ef42e
	.word 0xf6941c59
	.word 0x7130100e
	.word 0x5884e390
	.word 0x97cbb699
	.word 0x875c22f4
	.word 0x87e7efaf
	.word 0x96ee7065
	.word 0xe9de67e8
	.word 0x887f6670
	.word 0x387b28cc
	.word 0x74bd419a
	.word 0xcfa73e5a
	.word 0xefe17c02
	.word 0xf3085d19
	.word 0x471de7b2
	.word 0xcc967471
	.word 0x90dc5bf2
	.word 0xfd06d863
	.word 0x409425c4
	.word 0x35f8b424
	.word 0x21c4fd9
	.word 0xa9800201
	.word 0xaa24fcf2
	.word 0x22a8cae9
	.word 0xd8e0b9e1
	.word 0x3cfb52e8
	.word 0x34f8959
	.word 0x95d956c6
	.word 0x7e18df2b
	.word 0x49d03f62
	.word 0x5142121a
	.word 0x3b7b4906
	.word 0x5d4d7140
	.word 0x76256179
	.word 0x3a058dbe
	.word 0xa469690f
	.word 0x9960c15b
	.word 0xcbc93966
	.word 0xd398ecbf
	.word 0x5fa218
	.word 0x6604230d
	.word 0x39768ad
	.word 0x6a838d33
	.word 0xe4a01ec0
	.word 0x3df8088c
	.word 0x1e7866e6
	.word 0xe8e8dbd5
	.word 0x746f633c
	.word 0x197f95e0
	.word 0x65e65347
	.word 0x6321eb4
	.word 0x68ce7dfa
	.word 0x79143264
	.word 0xebd4a411
	.word 0xf97a7695
	.word 0xf9fca196
	.word 0x6379515
	.word 0xf0375458
	.word 0xac860f21
	.word 0xc9f08935
	.word 0xa2a67041
	.word 0x1fd14393
	.word 0x19852249
	.word 0x34610f89
	.word 0xe70150de
	.word 0xe716fd37
	.word 0x7a3a96f8
	.word 0x56415454
	.word 0x59512a43
	.word 0x3e1a56fa
	.word 0x635b769f
	.word 0xeee20116
	.word 0xc352d3c8
	.word 0x22b88e04
	.word 0xa5aa0fd
	.word 0xa0621129
	.word 0xed0d2b5a
	.word 0xec7a0e1f
	.word 0x44f1f9d3
	.word 0x4bcb3d9b
	.word 0x348fd2cc
	.word 0x24d825cd
	.word 0x82b9cab2
	.word 0x882e5e49
	.word 0x8b64b043
	.word 0xa6fdd92c
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
