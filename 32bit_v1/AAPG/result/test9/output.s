
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	sh x7, 78(x2)
i_1:
	lb x5, -237(x2)
i_2:
	beq x2, x23, i_9
i_3:
	sh x20, 326(x2)
i_4:
	slt x9, x5, x19
i_5:
	sh x14, 158(x2)
i_6:
	sw x13, 252(x2)
i_7:
	srli x3, x9, 4
i_8:
	sw x27, -420(x2)
i_9:
	sltu x9, x17, x3
i_10:
	lh x16, -268(x2)
i_11:
	addi x22, x0, -1843
i_12:
	addi x20, x0, -1839
i_13:
	sw x12, -80(x2)
i_14:
	auipc x11, 556240
i_15:
	nop
i_16:
	addi x22 , x22 , 1
	bne x22, x20, i_13
i_17:
	lw x26, 332(x2)
i_18:
	lw x16, -4(x2)
i_19:
	lui x17, 216900
i_20:
	lh x6, 188(x2)
i_21:
	lhu x22, 316(x2)
i_22:
	beq x17, x1, i_25
i_23:
	lw x26, 264(x2)
i_24:
	lb x19, -240(x2)
i_25:
	lh x19, 98(x2)
i_26:
	lhu x8, 198(x2)
i_27:
	sb x3, -328(x2)
i_28:
	sw x27, -104(x2)
i_29:
	bne x5, x6, i_37
i_30:
	sb x25, 474(x2)
i_31:
	sub x10, x20, x30
i_32:
	lhu x5, 292(x2)
i_33:
	lbu x20, 228(x2)
i_34:
	sw x28, 256(x2)
i_35:
	lh x24, -472(x2)
i_36:
	addi x29, x0, 3
i_37:
	srl x28, x13, x29
i_38:
	addi x6, x28, -1021
i_39:
	sw x6, -436(x2)
i_40:
	slt x6, x7, x9
i_41:
	sb x24, 410(x2)
i_42:
	slt x22, x20, x3
i_43:
	addi x6, x0, 2005
i_44:
	addi x18, x0, 2009
i_45:
	bgeu x9, x2, i_46
i_46:
	blt x21, x6, i_48
i_47:
	lh x9, 418(x2)
i_48:
	lh x15, -292(x2)
i_49:
	lh x13, 202(x2)
i_50:
	addi x12, x0, 30
i_51:
	srl x9, x15, x12
i_52:
	addi x6 , x6 , 1
	blt x6, x18, i_45
i_53:
	sw x23, -456(x2)
i_54:
	add x16, x28, x9
i_55:
	lb x14, 46(x2)
i_56:
	addi x22, x16, -418
i_57:
	auipc x12, 397185
i_58:
	lh x17, -34(x2)
i_59:
	sw x10, -400(x2)
i_60:
	bgeu x30, x28, i_70
i_61:
	sltu x21, x22, x29
i_62:
	addi x4, x0, 27
i_63:
	sra x28, x17, x4
i_64:
	xor x29, x28, x24
i_65:
	lh x28, -442(x2)
i_66:
	ori x25, x13, 254
i_67:
	bne x19, x15, i_69
i_68:
	bgeu x25, x29, i_76
i_69:
	slli x29, x29, 4
i_70:
	auipc x28, 990646
i_71:
	sub x25, x9, x25
i_72:
	sltiu x9, x31, 847
i_73:
	xori x15, x2, 1704
i_74:
	sub x21, x3, x16
i_75:
	lw x26, -160(x2)
i_76:
	bgeu x5, x11, i_78
i_77:
	lbu x9, -17(x2)
i_78:
	auipc x27, 20654
i_79:
	sltu x19, x14, x31
i_80:
	xor x11, x2, x18
i_81:
	addi x22, x0, 1885
i_82:
	addi x25, x0, 1889
i_83:
	lb x18, -73(x2)
i_84:
	addi x22 , x22 , 1
	bltu x22, x25, i_83
i_85:
	bltu x16, x22, i_88
i_86:
	sh x30, -82(x2)
i_87:
	bltu x30, x23, i_89
i_88:
	lbu x22, -49(x2)
i_89:
	sb x20, 208(x2)
i_90:
	xor x11, x31, x8
i_91:
	addi x3, x0, 1898
i_92:
	addi x23, x0, 1900
i_93:
	lbu x20, 187(x2)
i_94:
	add x8, x6, x3
i_95:
	addi x3 , x3 , 1
	bltu x3, x23, i_93
i_96:
	sub x11, x7, x27
i_97:
	lh x25, -10(x2)
i_98:
	lw x15, -56(x2)
i_99:
	lb x25, -346(x2)
i_100:
	addi x15, x0, 1
i_101:
	srl x11, x20, x15
i_102:
	lh x25, 132(x2)
i_103:
	bge x25, x14, i_107
i_104:
	lw x8, -452(x2)
i_105:
	lh x30, 106(x2)
i_106:
	ori x26, x10, 1204
i_107:
	sltu x9, x26, x20
i_108:
	lb x20, 313(x2)
i_109:
	addi x14, x0, -1964
i_110:
	addi x8, x0, -1960
i_111:
	auipc x30, 694891
i_112:
	sltu x25, x23, x25
i_113:
	andi x3, x1, -1843
i_114:
	lw x26, -4(x2)
i_115:
	ori x9, x10, 17
i_116:
	sltiu x9, x1, 1727
i_117:
	sh x27, -242(x2)
i_118:
	lbu x20, -127(x2)
i_119:
	lbu x6, 252(x2)
i_120:
	bltu x11, x30, i_128
i_121:
	addi x14 , x14 , 1
	bgeu x8, x14, i_111
i_122:
	sh x29, -428(x2)
i_123:
	bne x18, x13, i_133
i_124:
	bltu x22, x19, i_126
i_125:
	add x5, x30, x8
i_126:
	bltu x26, x16, i_127
i_127:
	sh x8, 2(x2)
i_128:
	blt x16, x7, i_137
i_129:
	bgeu x4, x26, i_131
i_130:
	andi x21, x27, -366
i_131:
	and x27, x5, x22
i_132:
	lhu x22, -68(x2)
i_133:
	lbu x27, 140(x2)
i_134:
	sw x31, 36(x2)
i_135:
	blt x30, x5, i_139
i_136:
	lw x21, -436(x2)
i_137:
	blt x20, x19, i_140
i_138:
	slli x27, x17, 3
i_139:
	sltu x20, x20, x1
i_140:
	sw x19, 444(x2)
i_141:
	xor x11, x29, x10
i_142:
	addi x19, x0, 1966
i_143:
	addi x24, x0, 1968
i_144:
	srai x28, x28, 1
i_145:
	blt x20, x14, i_148
i_146:
	sw x24, 356(x2)
i_147:
	sub x11, x10, x26
i_148:
	lb x23, 101(x2)
i_149:
	addi x1, x9, -1302
i_150:
	lb x23, -335(x2)
i_151:
	addi x19 , x19 , 1
	bne x19, x24, i_144
i_152:
	sw x10, 160(x2)
i_153:
	lbu x1, -385(x2)
i_154:
	lui x1, 762665
i_155:
	and x25, x16, x23
i_156:
	or x4, x11, x4
i_157:
	srai x24, x17, 2
i_158:
	addi x15, x0, 19
i_159:
	sra x21, x16, x15
i_160:
	sb x28, 97(x2)
i_161:
	lbu x22, 4(x2)
i_162:
	lb x10, 364(x2)
i_163:
	addi x26, x0, -1881
i_164:
	addi x21, x0, -1877
i_165:
	addi x26 , x26 , 1
	bgeu x21, x26, i_165
i_166:
	lhu x13, 10(x2)
i_167:
	andi x10, x11, -304
i_168:
	addi x26, x21, 108
i_169:
	andi x24, x21, -424
i_170:
	srli x13, x21, 3
i_171:
	lbu x10, -308(x2)
i_172:
	bne x30, x29, i_178
i_173:
	sb x22, -409(x2)
i_174:
	lb x16, -36(x2)
i_175:
	blt x24, x25, i_177
i_176:
	addi x16, x0, 2
i_177:
	sll x16, x16, x16
i_178:
	lhu x30, -264(x2)
i_179:
	sw x18, 108(x2)
i_180:
	lw x4, -288(x2)
i_181:
	lh x20, -140(x2)
i_182:
	addi x12, x0, -1857
i_183:
	addi x16, x0, -1855
i_184:
	addi x12 , x12 , 1
	blt x12, x16, i_184
i_185:
	sh x2, 50(x2)
i_186:
	sb x25, -379(x2)
i_187:
	lb x9, 275(x2)
i_188:
	slti x8, x20, 86
i_189:
	sb x30, -169(x2)
i_190:
	sltu x15, x22, x7
i_191:
	sb x30, -387(x2)
i_192:
	auipc x4, 591258
i_193:
	sb x25, 368(x2)
i_194:
	bltu x29, x4, i_197
i_195:
	sh x8, -64(x2)
i_196:
	lui x3, 956866
i_197:
	beq x3, x22, i_206
i_198:
	blt x5, x20, i_204
i_199:
	xor x4, x13, x17
i_200:
	and x9, x21, x29
i_201:
	add x4, x10, x3
i_202:
	bne x31, x16, i_211
i_203:
	lw x16, -136(x2)
i_204:
	addi x8, x0, 9
i_205:
	sra x8, x14, x8
i_206:
	lw x13, 412(x2)
i_207:
	addi x13, x17, -894
i_208:
	sh x8, -356(x2)
i_209:
	sw x13, -352(x2)
i_210:
	sw x13, 176(x2)
i_211:
	add x17, x17, x12
i_212:
	xori x11, x1, -316
i_213:
	lw x11, 80(x2)
i_214:
	bge x8, x3, i_219
i_215:
	bgeu x13, x20, i_221
i_216:
	ori x30, x30, 844
i_217:
	lhu x11, 30(x2)
i_218:
	sb x26, -107(x2)
i_219:
	blt x11, x30, i_223
i_220:
	ori x28, x29, 656
i_221:
	sh x17, 362(x2)
i_222:
	lhu x30, 442(x2)
i_223:
	lh x27, -450(x2)
i_224:
	blt x11, x26, i_228
i_225:
	lh x11, -150(x2)
i_226:
	addi x29, x0, 13
i_227:
	sll x13, x29, x29
i_228:
	xor x14, x30, x30
i_229:
	lh x12, 126(x2)
i_230:
	xori x18, x25, 466
i_231:
	add x15, x19, x24
i_232:
	sw x17, 176(x2)
i_233:
	sh x22, 348(x2)
i_234:
	blt x19, x12, i_243
i_235:
	slli x13, x28, 3
i_236:
	beq x22, x8, i_246
i_237:
	lb x4, 434(x2)
i_238:
	bne x27, x17, i_244
i_239:
	slt x17, x10, x27
i_240:
	lhu x3, 234(x2)
i_241:
	sltiu x10, x29, 807
i_242:
	lw x12, 216(x2)
i_243:
	sw x9, -44(x2)
i_244:
	lbu x8, 231(x2)
i_245:
	sub x20, x17, x16
i_246:
	lh x12, 392(x2)
i_247:
	ori x14, x31, -1986
i_248:
	addi x16, x0, -1905
i_249:
	addi x9, x0, -1903
i_250:
	sh x29, 34(x2)
i_251:
	sb x10, -51(x2)
i_252:
	sb x29, 159(x2)
i_253:
	auipc x29, 645048
i_254:
	addi x16 , x16 , 1
	bgeu x9, x16, i_250
i_255:
	lb x11, 415(x2)
i_256:
	bltu x13, x14, i_257
i_257:
	blt x28, x3, i_258
i_258:
	sb x13, -150(x2)
i_259:
	srai x6, x22, 3
i_260:
	beq x6, x11, i_269
i_261:
	lhu x13, 458(x2)
i_262:
	auipc x12, 408677
i_263:
	srai x10, x25, 2
i_264:
	lw x25, -364(x2)
i_265:
	lbu x9, -402(x2)
i_266:
	lh x17, 184(x2)
i_267:
	add x17, x10, x14
i_268:
	nop
i_269:
	addi x10, x23, 1278
i_270:
	lw x23, 80(x2)
i_271:
	addi x14, x0, -1851
i_272:
	addi x4, x0, -1849
i_273:
	lh x10, -364(x2)
i_274:
	sltu x24, x20, x20
i_275:
	nop
i_276:
	addi x14 , x14 , 1
	bltu x14, x4, i_273
i_277:
	sub x23, x31, x27
i_278:
	bgeu x2, x28, i_282
i_279:
	sb x3, -438(x2)
i_280:
	addi x24, x23, -1178
i_281:
	sb x23, 98(x2)
i_282:
	add x5, x17, x5
i_283:
	bltu x23, x10, i_293
i_284:
	sh x2, -104(x2)
i_285:
	bge x10, x5, i_294
i_286:
	addi x28, x0, 11
i_287:
	sra x10, x5, x28
i_288:
	lh x10, 486(x2)
i_289:
	bge x30, x25, i_298
i_290:
	sh x29, -78(x2)
i_291:
	addi x25, x8, -566
i_292:
	addi x24, x0, 23
i_293:
	sll x29, x29, x24
i_294:
	sb x19, -35(x2)
i_295:
	and x27, x25, x10
i_296:
	bge x21, x9, i_303
i_297:
	sb x6, -82(x2)
i_298:
	slli x24, x29, 2
i_299:
	sb x2, -310(x2)
i_300:
	sh x27, 426(x2)
i_301:
	srli x29, x7, 2
i_302:
	bge x1, x25, i_307
i_303:
	sh x25, 144(x2)
i_304:
	bgeu x7, x12, i_314
i_305:
	lui x14, 44121
i_306:
	srai x1, x29, 2
i_307:
	addi x15, x0, 9
i_308:
	sra x29, x27, x15
i_309:
	sb x19, -55(x2)
i_310:
	beq x26, x31, i_320
i_311:
	bgeu x20, x20, i_315
i_312:
	lh x20, -150(x2)
i_313:
	xori x13, x20, -1823
i_314:
	sh x12, -346(x2)
i_315:
	sh x1, 106(x2)
i_316:
	or x26, x17, x13
i_317:
	lh x12, 72(x2)
i_318:
	srai x12, x3, 1
i_319:
	lbu x21, -379(x2)
i_320:
	nop
i_321:
	srli x8, x26, 2
i_322:
	addi x19, x0, 1853
i_323:
	addi x24, x0, 1855
i_324:
	addi x19 , x19 , 1
	blt x19, x24, i_324
i_325:
	andi x15, x21, -1479
i_326:
	sb x23, -251(x2)
i_327:
	sb x4, -442(x2)
i_328:
	xor x4, x4, x8
i_329:
	sw x21, -424(x2)
i_330:
	sb x19, -410(x2)
i_331:
	xori x10, x9, -1375
i_332:
	lh x14, -78(x2)
i_333:
	ori x8, x30, 48
i_334:
	lw x26, 164(x2)
i_335:
	slti x29, x31, -304
i_336:
	lh x29, 268(x2)
i_337:
	bge x26, x29, i_347
i_338:
	blt x25, x7, i_346
i_339:
	sub x29, x10, x9
i_340:
	ori x22, x30, 1070
i_341:
	lh x22, 380(x2)
i_342:
	lb x22, -20(x2)
i_343:
	sub x23, x26, x16
i_344:
	sw x22, -368(x2)
i_345:
	lhu x26, -388(x2)
i_346:
	sh x25, -54(x2)
i_347:
	lb x5, 381(x2)
i_348:
	sb x20, 141(x2)
i_349:
	addi x16, x0, -1980
i_350:
	addi x1, x0, -1978
i_351:
	lh x22, -372(x2)
i_352:
	addi x16 , x16 , 1
	bge x1, x16, i_351
i_353:
	slt x25, x10, x5
i_354:
	lw x1, -244(x2)
i_355:
	sltiu x14, x10, -182
i_356:
	sb x1, -410(x2)
i_357:
	beq x22, x1, i_363
i_358:
	lhu x15, 166(x2)
i_359:
	lw x26, 248(x2)
i_360:
	lbu x13, 301(x2)
i_361:
	lh x29, 240(x2)
i_362:
	sb x11, 201(x2)
i_363:
	sb x19, 328(x2)
i_364:
	lui x29, 617072
i_365:
	addi x14, x13, -210
i_366:
	sb x14, 227(x2)
i_367:
	lb x24, 441(x2)
i_368:
	sh x23, 38(x2)
i_369:
	beq x20, x6, i_374
i_370:
	blt x3, x29, i_375
i_371:
	add x15, x22, x31
i_372:
	srli x3, x1, 2
i_373:
	addi x20, x0, 16
i_374:
	sra x1, x16, x20
i_375:
	slli x19, x3, 3
i_376:
	lw x19, -112(x2)
i_377:
	lh x16, 378(x2)
i_378:
	andi x30, x7, 1503
i_379:
	lb x27, -156(x2)
i_380:
	bge x3, x28, i_384
i_381:
	lbu x22, -127(x2)
i_382:
	sb x30, -440(x2)
i_383:
	auipc x30, 660637
i_384:
	lw x28, -344(x2)
i_385:
	lb x28, 10(x2)
i_386:
	addi x28, x0, 19
i_387:
	sra x22, x23, x28
i_388:
	lhu x28, -28(x2)
i_389:
	sb x28, 205(x2)
i_390:
	bne x5, x17, i_398
i_391:
	sltu x23, x11, x10
i_392:
	bne x28, x28, i_393
i_393:
	sb x19, 179(x2)
i_394:
	srai x10, x28, 1
i_395:
	auipc x10, 1027721
i_396:
	sh x27, -456(x2)
i_397:
	sh x22, 424(x2)
i_398:
	addi x20, x0, 6
i_399:
	sra x19, x4, x20
i_400:
	lhu x9, 92(x2)
i_401:
	addi x12, x0, 9
i_402:
	sll x4, x16, x12
i_403:
	slti x20, x28, 311
i_404:
	addi x24, x0, 18
i_405:
	srl x16, x20, x24
i_406:
	slt x14, x19, x12
i_407:
	lhu x12, -396(x2)
i_408:
	lbu x5, -157(x2)
i_409:
	lb x12, -54(x2)
i_410:
	lhu x25, 264(x2)
i_411:
	lbu x25, 263(x2)
i_412:
	lhu x12, -170(x2)
i_413:
	lw x13, 204(x2)
i_414:
	beq x28, x26, i_417
i_415:
	bne x31, x11, i_421
i_416:
	lb x13, -114(x2)
i_417:
	sub x11, x2, x13
i_418:
	bgeu x13, x4, i_422
i_419:
	lh x13, 444(x2)
i_420:
	sw x11, 320(x2)
i_421:
	bgeu x24, x6, i_430
i_422:
	blt x19, x11, i_431
i_423:
	srli x1, x16, 2
i_424:
	beq x1, x11, i_430
i_425:
	sb x5, -303(x2)
i_426:
	lhu x1, 286(x2)
i_427:
	lb x9, 457(x2)
i_428:
	sb x9, 385(x2)
i_429:
	sltiu x8, x9, 1127
i_430:
	lw x9, -452(x2)
i_431:
	beq x1, x5, i_439
i_432:
	add x8, x25, x15
i_433:
	sw x8, -100(x2)
i_434:
	sh x9, -352(x2)
i_435:
	sh x30, -178(x2)
i_436:
	addi x26, x0, 18
i_437:
	sll x30, x11, x26
i_438:
	lb x5, 260(x2)
i_439:
	blt x8, x16, i_441
i_440:
	lb x25, -42(x2)
i_441:
	auipc x3, 41074
i_442:
	sw x23, -332(x2)
i_443:
	sw x19, 364(x2)
i_444:
	lhu x30, 50(x2)
i_445:
	xor x4, x2, x20
i_446:
	bge x2, x3, i_449
i_447:
	sh x5, 486(x2)
i_448:
	sub x30, x13, x5
i_449:
	sh x30, -124(x2)
i_450:
	lb x20, 167(x2)
i_451:
	lbu x23, -100(x2)
i_452:
	lbu x5, 93(x2)
i_453:
	lhu x10, 424(x2)
i_454:
	sw x13, -104(x2)
i_455:
	xori x4, x24, 1580
i_456:
	add x5, x25, x2
i_457:
	slt x24, x13, x19
i_458:
	lh x10, 56(x2)
i_459:
	sw x10, -268(x2)
i_460:
	srai x29, x20, 1
i_461:
	sb x11, -71(x2)
i_462:
	sb x10, -252(x2)
i_463:
	bltu x18, x29, i_466
i_464:
	lh x13, -332(x2)
i_465:
	sltu x10, x15, x24
i_466:
	slti x29, x24, -225
i_467:
	lhu x4, 6(x2)
i_468:
	sw x24, 272(x2)
i_469:
	beq x9, x29, i_475
i_470:
	lb x29, -103(x2)
i_471:
	slti x16, x15, 687
i_472:
	lh x23, 24(x2)
i_473:
	blt x6, x4, i_483
i_474:
	bgeu x5, x17, i_482
i_475:
	sb x16, -471(x2)
i_476:
	bge x1, x4, i_484
i_477:
	lbu x28, -89(x2)
i_478:
	bgeu x19, x7, i_480
i_479:
	sltu x17, x24, x8
i_480:
	sw x31, 368(x2)
i_481:
	lh x30, -392(x2)
i_482:
	lw x24, -128(x2)
i_483:
	xor x24, x24, x2
i_484:
	bne x24, x3, i_492
i_485:
	sb x26, -365(x2)
i_486:
	sb x1, 91(x2)
i_487:
	addi x1, x0, 23
i_488:
	sll x18, x7, x1
i_489:
	slt x20, x9, x27
i_490:
	lbu x25, -389(x2)
i_491:
	sb x6, -114(x2)
i_492:
	addi x20, x0, 18
i_493:
	sll x30, x4, x20
i_494:
	addi x1, x0, 17
i_495:
	sra x30, x25, x1
i_496:
	lw x4, -372(x2)
i_497:
	lbu x29, 482(x2)
i_498:
	xor x4, x8, x1
i_499:
	addi x26, x0, 30
i_500:
	sra x5, x1, x26
i_501:
	addi x30, x0, 2028
i_502:
	addi x1, x0, 2031
i_503:
	slli x4, x23, 3
i_504:
	lhu x23, 350(x2)
i_505:
	addi x30 , x30 , 1
	blt x30, x1, i_502
i_506:
	sw x1, -328(x2)
i_507:
	srai x26, x30, 1
i_508:
	srai x1, x24, 3
i_509:
	lb x24, -486(x2)
i_510:
	addi x24, x0, 24
i_511:
	sll x24, x29, x24
i_512:
	bgeu x20, x29, i_516
i_513:
	sh x23, 184(x2)
i_514:
	lh x12, -456(x2)
i_515:
	lb x29, 288(x2)
i_516:
	or x23, x25, x23
i_517:
	nop
i_518:
	addi x17, x0, -1865
i_519:
	addi x14, x0, -1862
i_520:
	sh x14, 38(x2)
i_521:
	lhu x22, 432(x2)
i_522:
	lbu x24, -168(x2)
i_523:
	lw x23, 340(x2)
i_524:
	lb x19, -174(x2)
i_525:
	addi x17 , x17 , 1
	bne x17, x14, i_520
i_526:
	sw x3, -216(x2)
i_527:
	lw x3, -24(x2)
i_528:
	addi x3, x0, 31
i_529:
	sll x12, x16, x3
i_530:
	srli x25, x30, 2
i_531:
	xori x3, x29, 418
i_532:
	addi x25, x0, 20
i_533:
	sra x25, x13, x25
i_534:
	slt x25, x30, x11
i_535:
	auipc x24, 84141
i_536:
	sh x8, 440(x2)
i_537:
	add x17, x7, x10
i_538:
	lh x19, -484(x2)
i_539:
	sw x24, 300(x2)
i_540:
	lh x3, 32(x2)
i_541:
	sh x22, 84(x2)
i_542:
	sb x12, 257(x2)
i_543:
	lbu x3, 476(x2)
i_544:
	ori x6, x19, 142
i_545:
	sb x13, -364(x2)
i_546:
	lbu x18, -304(x2)
i_547:
	lhu x13, 440(x2)
i_548:
	sh x17, 410(x2)
i_549:
	lhu x21, 320(x2)
i_550:
	xor x13, x26, x24
i_551:
	lh x8, 130(x2)
i_552:
	sw x21, -92(x2)
i_553:
	lh x21, -366(x2)
i_554:
	addi x12, x0, 14
i_555:
	sra x11, x8, x12
i_556:
	blt x15, x11, i_561
i_557:
	lb x12, -75(x2)
i_558:
	srai x12, x31, 2
i_559:
	slt x18, x8, x13
i_560:
	slli x3, x18, 4
i_561:
	slti x28, x10, 859
i_562:
	xor x18, x18, x1
i_563:
	sw x4, 208(x2)
i_564:
	xori x10, x3, -1297
i_565:
	beq x9, x29, i_571
i_566:
	beq x14, x3, i_570
i_567:
	sw x29, 348(x2)
i_568:
	sb x10, -34(x2)
i_569:
	add x11, x4, x10
i_570:
	sw x4, -288(x2)
i_571:
	addi x23, x25, -1012
i_572:
	sw x8, -348(x2)
i_573:
	sh x31, -230(x2)
i_574:
	srai x28, x29, 3
i_575:
	sh x19, -194(x2)
i_576:
	xori x28, x10, -584
i_577:
	lhu x11, 446(x2)
i_578:
	sw x29, 412(x2)
i_579:
	blt x26, x12, i_582
i_580:
	sb x2, -206(x2)
i_581:
	srli x29, x26, 3
i_582:
	lb x26, 298(x2)
i_583:
	and x14, x21, x25
i_584:
	lb x23, -173(x2)
i_585:
	lw x29, 76(x2)
i_586:
	lbu x24, 148(x2)
i_587:
	sh x12, 286(x2)
i_588:
	sb x17, -389(x2)
i_589:
	lbu x22, 472(x2)
i_590:
	addi x4, x0, 9
i_591:
	srl x12, x12, x4
i_592:
	bltu x31, x11, i_595
i_593:
	lbu x6, 431(x2)
i_594:
	addi x1, x0, 31
i_595:
	srl x29, x24, x1
i_596:
	lb x24, 442(x2)
i_597:
	ori x18, x23, 1504
i_598:
	bge x7, x18, i_605
i_599:
	xor x18, x4, x16
i_600:
	sh x25, -250(x2)
i_601:
	bge x7, x6, i_603
i_602:
	bge x16, x24, i_609
i_603:
	sw x14, -400(x2)
i_604:
	lh x22, 266(x2)
i_605:
	xori x6, x3, 1596
i_606:
	andi x6, x16, 1631
i_607:
	sw x18, 352(x2)
i_608:
	add x25, x11, x28
i_609:
	or x9, x5, x10
i_610:
	sh x11, 74(x2)
i_611:
	sw x8, 148(x2)
i_612:
	blt x7, x21, i_613
i_613:
	ori x16, x3, 211
i_614:
	lh x9, 240(x2)
i_615:
	addi x26, x0, -2018
i_616:
	addi x8, x0, -2016
i_617:
	andi x6, x3, -1481
i_618:
	lw x29, -4(x2)
i_619:
	lb x17, 285(x2)
i_620:
	lui x6, 696641
i_621:
	sltu x19, x9, x7
i_622:
	add x20, x21, x4
i_623:
	sh x6, 282(x2)
i_624:
	srli x14, x10, 2
i_625:
	lhu x6, -182(x2)
i_626:
	addi x4, x21, 1411
i_627:
	addi x26 , x26 , 1
	blt x26, x8, i_617
i_628:
	sh x11, 88(x2)
i_629:
	lh x5, -246(x2)
i_630:
	lh x3, -112(x2)
i_631:
	sb x2, -450(x2)
i_632:
	lh x14, 82(x2)
i_633:
	auipc x14, 589307
i_634:
	sh x11, 224(x2)
i_635:
	lbu x11, 439(x2)
i_636:
	addi x30, x0, 3
i_637:
	srl x11, x14, x30
i_638:
	lw x19, -312(x2)
i_639:
	lw x6, -308(x2)
i_640:
	bgeu x9, x16, i_648
i_641:
	addi x27, x0, 6
i_642:
	sra x6, x11, x27
i_643:
	sh x20, 210(x2)
i_644:
	sb x23, 484(x2)
i_645:
	lh x15, -20(x2)
i_646:
	bne x18, x26, i_651
i_647:
	addi x23, x0, 29
i_648:
	sll x20, x27, x23
i_649:
	sw x21, 320(x2)
i_650:
	sh x23, -220(x2)
i_651:
	lh x10, 294(x2)
i_652:
	xor x15, x20, x10
i_653:
	lb x16, -121(x2)
i_654:
	sltiu x15, x2, 1974
i_655:
	lw x17, -136(x2)
i_656:
	and x26, x19, x14
i_657:
	bne x30, x17, i_662
i_658:
	andi x17, x18, 789
i_659:
	andi x19, x1, -1928
i_660:
	lw x6, -56(x2)
i_661:
	sb x4, -331(x2)
i_662:
	lh x29, 368(x2)
i_663:
	addi x9, x0, 14
i_664:
	srl x29, x22, x9
i_665:
	addi x1, x0, 1988
i_666:
	addi x14, x0, 1990
i_667:
	lw x25, -176(x2)
i_668:
	addi x1 , x1 , 1
	bltu x1, x14, i_666
i_669:
	lw x28, 160(x2)
i_670:
	bne x31, x14, i_673
i_671:
	srai x6, x18, 1
i_672:
	bne x6, x27, i_680
i_673:
	bne x12, x15, i_680
i_674:
	addi x26, x0, 17
i_675:
	sra x8, x27, x26
i_676:
	lhu x6, 210(x2)
i_677:
	slli x21, x21, 3
i_678:
	lw x14, 224(x2)
i_679:
	addi x9, x0, 9
i_680:
	srl x8, x28, x9
i_681:
	add x21, x8, x23
i_682:
	andi x3, x20, -1394
i_683:
	xori x5, x5, -869
i_684:
	lw x28, 276(x2)
i_685:
	andi x21, x14, -562
i_686:
	lh x28, -124(x2)
i_687:
	sltiu x22, x1, 1126
i_688:
	bge x8, x20, i_695
i_689:
	bgeu x24, x21, i_693
i_690:
	xori x13, x1, -1486
i_691:
	and x30, x15, x5
i_692:
	sb x3, 82(x2)
i_693:
	blt x14, x2, i_694
i_694:
	lw x21, 184(x2)
i_695:
	lui x3, 927124
i_696:
	bgeu x2, x3, i_697
i_697:
	addi x16, x0, 10
i_698:
	sra x13, x28, x16
i_699:
	sw x31, -204(x2)
i_700:
	sh x22, -246(x2)
i_701:
	sub x5, x28, x15
i_702:
	lb x10, -68(x2)
i_703:
	ori x27, x10, 2043
i_704:
	sh x16, -30(x2)
i_705:
	lb x6, 185(x2)
i_706:
	or x16, x28, x5
i_707:
	lhu x10, -288(x2)
i_708:
	addi x30, x0, 16
i_709:
	srl x28, x10, x30
i_710:
	lb x24, 212(x2)
i_711:
	sh x28, -70(x2)
i_712:
	lhu x28, -96(x2)
i_713:
	lhu x21, 46(x2)
i_714:
	lhu x30, -32(x2)
i_715:
	addi x13, x0, 9
i_716:
	sra x10, x15, x13
i_717:
	blt x13, x4, i_718
i_718:
	bne x16, x14, i_726
i_719:
	sub x10, x15, x10
i_720:
	lbu x14, 436(x2)
i_721:
	lbu x15, -14(x2)
i_722:
	sltu x3, x28, x22
i_723:
	ori x10, x4, 137
i_724:
	add x26, x4, x18
i_725:
	auipc x10, 854161
i_726:
	lh x4, 256(x2)
i_727:
	beq x15, x11, i_729
i_728:
	srai x16, x14, 4
i_729:
	addi x28, x0, 8
i_730:
	sll x18, x14, x28
i_731:
	sub x13, x10, x21
i_732:
	bltu x27, x24, i_733
i_733:
	xor x13, x28, x24
i_734:
	and x24, x4, x28
i_735:
	bltu x9, x24, i_742
i_736:
	sb x22, -141(x2)
i_737:
	add x15, x27, x5
i_738:
	sub x15, x15, x23
i_739:
	bne x3, x4, i_746
i_740:
	srai x10, x25, 4
i_741:
	lb x1, -424(x2)
i_742:
	addi x25, x0, 21
i_743:
	sra x25, x1, x25
i_744:
	lhu x15, 260(x2)
i_745:
	sh x4, -292(x2)
i_746:
	lb x15, -2(x2)
i_747:
	ori x26, x11, 331
i_748:
	addi x23, x0, 2039
i_749:
	addi x10, x0, 2043
i_750:
	addi x23 , x23 , 1
	bgeu x10, x23, i_750
i_751:
	lbu x1, 193(x2)
i_752:
	lhu x1, -150(x2)
i_753:
	lbu x19, 5(x2)
i_754:
	lh x12, 366(x2)
i_755:
	lh x26, 354(x2)
i_756:
	sb x23, 381(x2)
i_757:
	srai x20, x14, 3
i_758:
	sw x3, -360(x2)
i_759:
	bltu x23, x20, i_768
i_760:
	addi x15, x0, 9
i_761:
	sll x9, x4, x15
i_762:
	addi x18, x0, 8
i_763:
	sll x22, x19, x18
i_764:
	sb x13, 264(x2)
i_765:
	slli x22, x15, 2
i_766:
	sb x22, 355(x2)
i_767:
	lhu x15, 388(x2)
i_768:
	ori x13, x22, -1805
i_769:
	lui x6, 780739
i_770:
	sb x4, -42(x2)
i_771:
	slt x18, x30, x18
i_772:
	sh x30, 230(x2)
i_773:
	srli x13, x12, 4
i_774:
	bge x22, x18, i_782
i_775:
	lh x9, -386(x2)
i_776:
	sh x2, -194(x2)
i_777:
	add x10, x28, x18
i_778:
	nop
i_779:
	lh x17, 394(x2)
i_780:
	nop
i_781:
	lh x9, 362(x2)
i_782:
	lui x27, 473383
i_783:
	slti x3, x26, 346
i_784:
	addi x15, x0, -1912
i_785:
	addi x18, x0, -1908
i_786:
	addi x15 , x15 , 1
	blt x15, x18, i_786
i_787:
	bge x22, x3, i_794
i_788:
	lw x6, 132(x2)
i_789:
	lui x13, 136894
i_790:
	lh x26, -232(x2)
i_791:
	lb x16, -342(x2)
i_792:
	bltu x13, x14, i_799
i_793:
	auipc x22, 931195
i_794:
	slt x18, x19, x15
i_795:
	add x14, x20, x16
i_796:
	sw x27, 428(x2)
i_797:
	sb x3, 72(x2)
i_798:
	lb x20, -65(x2)
i_799:
	addi x10, x0, 1
i_800:
	srl x10, x10, x10
i_801:
	addi x22, x0, -1922
i_802:
	addi x3, x0, -1919
i_803:
	bne x25, x29, i_812
i_804:
	addi x22 , x22 , 1
	blt x22, x3, i_803
i_805:
	bgeu x18, x26, i_812
i_806:
	lh x30, 60(x2)
i_807:
	beq x3, x2, i_817
i_808:
	lw x16, 460(x2)
i_809:
	addi x18, x0, 8
i_810:
	sra x14, x13, x18
i_811:
	sw x31, 332(x2)
i_812:
	slti x13, x6, 1455
i_813:
	blt x23, x13, i_818
i_814:
	lbu x6, 144(x2)
i_815:
	sw x5, 220(x2)
i_816:
	slli x3, x13, 3
i_817:
	blt x25, x23, i_820
i_818:
	lw x13, 460(x2)
i_819:
	lb x1, -444(x2)
i_820:
	lbu x1, 337(x2)
i_821:
	bltu x11, x31, i_828
i_822:
	lbu x22, 459(x2)
i_823:
	sh x6, -72(x2)
i_824:
	xori x19, x8, -200
i_825:
	sh x8, 430(x2)
i_826:
	sb x17, -410(x2)
i_827:
	xor x22, x5, x23
i_828:
	lbu x10, 388(x2)
i_829:
	xori x1, x15, -148
i_830:
	lhu x27, -298(x2)
i_831:
	lbu x14, 470(x2)
i_832:
	lbu x21, 145(x2)
i_833:
	lbu x10, -110(x2)
i_834:
	add x9, x29, x25
i_835:
	lw x10, 388(x2)
i_836:
	addi x17, x0, 18
i_837:
	sra x21, x3, x17
i_838:
	ori x15, x7, 683
i_839:
	andi x13, x22, 29
i_840:
	lw x17, 248(x2)
i_841:
	slli x12, x31, 1
i_842:
	sw x24, -488(x2)
i_843:
	sb x2, -310(x2)
i_844:
	addi x28, x0, 1920
i_845:
	addi x4, x0, 1922
i_846:
	sb x15, 275(x2)
i_847:
	bgeu x3, x13, i_854
i_848:
	sw x17, -456(x2)
i_849:
	addi x28 , x28 , 1
	bge x4, x28, i_846
i_850:
	lbu x17, -259(x2)
i_851:
	bge x4, x26, i_853
i_852:
	lh x21, -116(x2)
i_853:
	lw x28, 332(x2)
i_854:
	addi x28, x0, 15
i_855:
	sra x28, x9, x28
i_856:
	sh x28, 200(x2)
i_857:
	lbu x17, 78(x2)
i_858:
	andi x17, x24, 979
i_859:
	sh x21, 276(x2)
i_860:
	sltu x3, x3, x4
i_861:
	xor x9, x19, x12
i_862:
	sb x15, 262(x2)
i_863:
	lhu x24, -30(x2)
i_864:
	auipc x21, 1004165
i_865:
	lbu x18, -205(x2)
i_866:
	lw x17, -448(x2)
i_867:
	sw x12, -304(x2)
i_868:
	lw x21, -112(x2)
i_869:
	srli x12, x21, 1
i_870:
	sb x30, 455(x2)
i_871:
	addi x29, x9, 117
i_872:
	sb x12, -324(x2)
i_873:
	bgeu x27, x7, i_882
i_874:
	sb x22, 90(x2)
i_875:
	blt x6, x31, i_885
i_876:
	bltu x17, x24, i_879
i_877:
	lw x18, 312(x2)
i_878:
	sb x16, 27(x2)
i_879:
	lw x12, 444(x2)
i_880:
	lb x12, 68(x2)
i_881:
	sltiu x21, x18, -2009
i_882:
	addi x18, x0, 24
i_883:
	sra x1, x16, x18
i_884:
	sltu x21, x25, x10
i_885:
	addi x10, x4, 669
i_886:
	add x18, x29, x14
i_887:
	addi x29, x0, 1968
i_888:
	addi x16, x0, 1971
i_889:
	lui x27, 929238
i_890:
	addi x29 , x29 , 1
	bne x29, x16, i_889
i_891:
	sb x21, 202(x2)
i_892:
	lw x10, 108(x2)
i_893:
	andi x27, x28, -1038
i_894:
	addi x24, x0, 17
i_895:
	sll x14, x6, x24
i_896:
	addi x29, x0, 1974
i_897:
	addi x18, x0, 1978
i_898:
	blt x25, x22, i_904
i_899:
	sh x13, -20(x2)
i_900:
	sltiu x27, x31, 420
i_901:
	addi x29 , x29 , 1
	blt x29, x18, i_897
i_902:
	lhu x22, -146(x2)
i_903:
	srli x13, x27, 3
i_904:
	sb x19, 311(x2)
i_905:
	lui x18, 592838
i_906:
	sw x8, 408(x2)
i_907:
	sb x20, -183(x2)
i_908:
	add x13, x18, x27
i_909:
	sw x3, 332(x2)
i_910:
	sb x30, -472(x2)
i_911:
	bltu x26, x13, i_914
i_912:
	add x5, x22, x18
i_913:
	lh x13, 62(x2)
i_914:
	blt x18, x18, i_921
i_915:
	lw x25, -372(x2)
i_916:
	lh x25, 454(x2)
i_917:
	lbu x13, 354(x2)
i_918:
	auipc x25, 922827
i_919:
	lb x13, 325(x2)
i_920:
	srli x28, x13, 2
i_921:
	sw x27, -28(x2)
i_922:
	sh x6, -246(x2)
i_923:
	sh x29, 200(x2)
i_924:
	beq x28, x28, i_927
i_925:
	blt x28, x25, i_929
i_926:
	lh x28, -384(x2)
i_927:
	lbu x28, -88(x2)
i_928:
	lui x26, 681753
i_929:
	ori x29, x2, 294
i_930:
	sw x23, -80(x2)
i_931:
	addi x13, x0, -1950
i_932:
	addi x19, x0, -1948
i_933:
	beq x3, x3, i_940
i_934:
	auipc x9, 689851
i_935:
	addi x13 , x13 , 1
	bgeu x19, x13, i_933
i_936:
	srai x17, x27, 2
i_937:
	lh x22, 40(x2)
i_938:
	lhu x28, 474(x2)
i_939:
	bltu x11, x18, i_945
i_940:
	addi x9, x0, 28
i_941:
	sll x9, x19, x9
i_942:
	lh x18, -92(x2)
i_943:
	lhu x9, -302(x2)
i_944:
	beq x4, x21, i_950
i_945:
	xor x9, x9, x16
i_946:
	addi x27, x0, 5
i_947:
	sra x6, x31, x27
i_948:
	lb x9, -252(x2)
i_949:
	sw x15, 36(x2)
i_950:
	lw x6, 240(x2)
i_951:
	or x18, x27, x26
i_952:
	and x15, x15, x8
i_953:
	sh x30, -210(x2)
i_954:
	sh x18, -244(x2)
i_955:
	slli x19, x12, 4
i_956:
	bne x29, x28, i_958
i_957:
	slti x18, x4, 1465
i_958:
	lhu x22, -82(x2)
i_959:
	sw x9, -308(x2)
i_960:
	sh x6, -104(x2)
i_961:
	sb x6, -279(x2)
i_962:
	sub x6, x4, x21
i_963:
	sltu x28, x6, x18
i_964:
	slli x15, x6, 2
i_965:
	slt x6, x29, x28
i_966:
	sh x17, 182(x2)
i_967:
	addi x23, x0, 1953
i_968:
	addi x22, x0, 1957
i_969:
	lw x18, -216(x2)
i_970:
	lhu x29, -270(x2)
i_971:
	bne x6, x2, i_980
i_972:
	sh x18, -400(x2)
i_973:
	blt x2, x12, i_979
i_974:
	lbu x25, -432(x2)
i_975:
	addi x23 , x23 , 1
	bne x23, x22, i_969
i_976:
	lhu x28, -16(x2)
i_977:
	lh x12, 270(x2)
i_978:
	addi x5, x15, -1829
i_979:
	lui x27, 720654
i_980:
	xor x6, x12, x2
i_981:
	sh x4, -162(x2)
i_982:
	lui x18, 456115
i_983:
	sb x26, -282(x2)
i_984:
	sb x11, -458(x2)
i_985:
	and x5, x22, x23
i_986:
	blt x5, x9, i_993
i_987:
	slti x14, x8, 270
i_988:
	slli x5, x20, 3
i_989:
	sh x30, 48(x2)
i_990:
	sb x18, 159(x2)
i_991:
	srli x8, x5, 3
i_992:
	lw x5, 200(x2)
i_993:
	sw x6, 308(x2)
i_994:
	bltu x6, x8, i_1001
i_995:
	sub x25, x5, x25
i_996:
	sw x5, 180(x2)
i_997:
	srli x27, x5, 4
i_998:
	addi x25, x28, 1160
i_999:
	lbu x4, 214(x2)
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0x6b3e5827
	.word 0x40c983cf
	.word 0xc2a3eef6
	.word 0x25b52570
	.word 0x360489a4
	.word 0xb1e90d32
	.word 0x9a0c5bb3
	.word 0xd90820fb
	.word 0x52fc67e5
	.word 0x7ee7189a
	.word 0x6ccf14ab
	.word 0xb354d9b8
	.word 0xba9b6ba
	.word 0xc7a3466f
	.word 0x9688d15e
	.word 0xad2b314b
	.word 0x7898e2f9
	.word 0x7eb82f1e
	.word 0x9bf4409a
	.word 0xbd45835
	.word 0x8dc3d209
	.word 0x2d6fb7b4
	.word 0x36360a28
	.word 0x52603bcf
	.word 0x23ccf67
	.word 0x22832a0d
	.word 0xab68d1be
	.word 0xe63bcd40
	.word 0xa33c82a4
	.word 0x1b2b3397
	.word 0xc8a18936
	.word 0xc53690c1
	.word 0x50db52c2
	.word 0x2deac67e
	.word 0x41a40f0b
	.word 0xd48c5436
	.word 0x1c86fb25
	.word 0x53721c2d
	.word 0xe078d5de
	.word 0x7c53ac2a
	.word 0x389b2567
	.word 0x19226567
	.word 0xd775c0a7
	.word 0x3133bde6
	.word 0x14c8efd6
	.word 0x3ba056ba
	.word 0x8d15d0b6
	.word 0xc88655cd
	.word 0x2d49962f
	.word 0x15654447
	.word 0xb34661dd
	.word 0x851e1b29
	.word 0xcfe71c0a
	.word 0x493bf5df
	.word 0xc9791803
	.word 0x7373bb2f
	.word 0x7e0a649
	.word 0x6bd7ea16
	.word 0xd088087d
	.word 0xf500636e
	.word 0xa0300a8e
	.word 0x9404f1cc
	.word 0xa7979c0b
	.word 0x296d208b
	.word 0x39ac9617
	.word 0x24ed040
	.word 0xe009b576
	.word 0x1f751282
	.word 0xdd9664a
	.word 0xdc7fcf0d
	.word 0x63a93331
	.word 0x9c8deeca
	.word 0x535746d4
	.word 0x673f4000
	.word 0xf443f76
	.word 0xae5802df
	.word 0x7790a235
	.word 0xc769aefb
	.word 0x292b6bdf
	.word 0x47d3ce24
	.word 0x4cc3f9c5
	.word 0x80137cd7
	.word 0xfe4b65e9
	.word 0x6c40896f
	.word 0x2d3fa550
	.word 0xd94e42f2
	.word 0xa629c7d5
	.word 0xd8e8d165
	.word 0xc9923929
	.word 0xb88745e
	.word 0xf6505fcc
	.word 0x158faf4e
	.word 0x8995319
	.word 0x3b27d08c
	.word 0xdd9b3106
	.word 0x1e199cf7
	.word 0xf6bd7901
	.word 0x6fde285f
	.word 0x51193876
	.word 0xd3b8148b
	.word 0xdcb2c666
	.word 0xd92e67a4
	.word 0xcdc43180
	.word 0x24b33a03
	.word 0x67837740
	.word 0x338b97aa
	.word 0xf4a07a6
	.word 0x5dbfeb3f
	.word 0x978253a8
	.word 0xc8a69e95
	.word 0xb15776f9
	.word 0x13e1fe92
	.word 0xe65ae2d9
	.word 0x7295378b
	.word 0xb44688d2
	.word 0x504796ca
	.word 0xbda1ba8
	.word 0x86ff9aab
	.word 0x6eb00f26
	.word 0x9fa176a4
	.word 0x3d8b03c0
	.word 0xfab1a954
	.word 0x6648db38
	.word 0x2690ae87
	.word 0x80f58ec5
	.word 0xc95cf8fb
	.word 0xf873aebf
	.word 0x1dd83e3b
	.word 0xa633238e
	.word 0xfd6293f9
	.word 0x5cfbf505
	.word 0x1de5ea37
	.word 0xe46ff741
	.word 0xa54afd19
	.word 0x34b84a15
	.word 0x1f153f00
	.word 0x77168d21
	.word 0xd63dc5f2
	.word 0xd1c45567
	.word 0xa7444d32
	.word 0x8426cb17
	.word 0x677d43eb
	.word 0x88bc0484
	.word 0x826fde4a
	.word 0x1227647c
	.word 0x3d81832f
	.word 0xf0ee3ac6
	.word 0x7acb013
	.word 0x4233e027
	.word 0x29378553
	.word 0x46143389
	.word 0x6b04f7d6
	.word 0x566f2d8a
	.word 0x7412a9a5
	.word 0xe0b93e5b
	.word 0x832f2b1b
	.word 0x8ade96a2
	.word 0x71c7bb44
	.word 0xb3e7c61
	.word 0x9e964b2
	.word 0xad6ffa05
	.word 0x5231973
	.word 0xddfdd32a
	.word 0x6dcdd229
	.word 0x2115135
	.word 0x6c2891f2
	.word 0x210e9e8b
	.word 0x34a8507e
	.word 0x25b45087
	.word 0x13e7c89e
	.word 0xc92183c6
	.word 0x94b952fc
	.word 0xefbc96e
	.word 0x778fbf5e
	.word 0x1d200aa9
	.word 0x33f6be3d
	.word 0x23fe9780
	.word 0x89c4e4a7
	.word 0xedbb81fe
	.word 0x89e35970
	.word 0xf4cce53d
	.word 0xa9c88f07
	.word 0xdd8cf862
	.word 0xcfdc353b
	.word 0x19edc94
	.word 0x16bfcff1
	.word 0x9d8b275a
	.word 0xb247f405
	.word 0x4dd40084
	.word 0x9b2b02d
	.word 0xd82d5762
	.word 0xa9e7b704
	.word 0x44912f49
	.word 0xb4ec5f
	.word 0x6fa44fa6
	.word 0x4a2d1fac
	.word 0x85199e5e
	.word 0x8506db0c
	.word 0x511e2f47
	.word 0xb4ec3513
	.word 0x942b2f97
	.word 0x1554e008
	.word 0x6a4389fa
	.word 0x2efb7e57
	.word 0x5f450d32
	.word 0xe44603c8
	.word 0x869a77cc
	.word 0xcb9c19c0
	.word 0xbf29a1e7
	.word 0xa498052c
	.word 0x8b97e824
	.word 0x9917eb72
	.word 0xacc30d30
	.word 0x4d1ce467
	.word 0xaa00af44
	.word 0xc842f7a6
	.word 0x14551ab6
	.word 0xdaa7c060
	.word 0xc83d8a74
	.word 0x4acdf5b0
	.word 0xb076142f
	.word 0xe6ea8523
	.word 0xe57a0c41
	.word 0x82cca9be
	.word 0x9aaeb20b
	.word 0xa6f1e475
	.word 0xf6db74f
	.word 0xb35886dd
	.word 0xe18fd8d1
	.word 0x1d61b518
	.word 0xf20dd0e9
	.word 0x950757c9
	.word 0xd36852ab
	.word 0x38893392
	.word 0x43bba18c
	.word 0xb582bd88
	.word 0x8478cc06
	.word 0x259d2de5
	.word 0x3fc2c7ac
	.word 0x91b4e12d
	.word 0xac5cae95
	.word 0xb75af1ee
	.word 0x18cc195b
	.word 0xd790d03a
	.word 0x4881d175
	.word 0x68a03abc
	.word 0x4aa69dcc
	.word 0x49111955
	.word 0xdffb2ec2
	.word 0x7557349f
	.word 0x77e2c813
	.word 0x2aed6234
	.word 0x554a6605
	.word 0xb48ea7e0
	.word 0x1cea77b2
	.word 0x8bb179d1
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
