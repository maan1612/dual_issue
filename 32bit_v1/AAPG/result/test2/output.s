
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	bgeu x27, x31, i_5
i_1:
	nop
i_2:
	bne x28, x3, i_7
i_3:
	blt x15, x22, i_7
i_4:
	sb x15, -365(x2)
i_5:
	addi x15, x0, 29
i_6:
	sll x15, x31, x15
i_7:
	addi x11, x0, 28
i_8:
	sra x15, x6, x11
i_9:
	add x18, x4, x15
i_10:
	beq x16, x17, i_15
i_11:
	lw x20, -296(x2)
i_12:
	sb x17, -413(x2)
i_13:
	bne x11, x5, i_19
i_14:
	sltiu x11, x28, -18
i_15:
	bgeu x7, x9, i_19
i_16:
	sw x21, 84(x2)
i_17:
	lw x21, -292(x2)
i_18:
	sb x20, 153(x2)
i_19:
	addi x19, x9, -1095
i_20:
	add x9, x13, x30
i_21:
	lhu x24, -194(x2)
i_22:
	nop
i_23:
	nop
i_24:
	addi x21, x0, -1858
i_25:
	addi x9, x0, -1856
i_26:
	blt x8, x5, i_35
i_27:
	lbu x8, -36(x2)
i_28:
	lw x24, 348(x2)
i_29:
	add x30, x31, x18
i_30:
	lb x30, -144(x2)
i_31:
	sb x10, 126(x2)
i_32:
	addi x21 , x21 , 1
	bgeu x9, x21, i_26
i_33:
	lb x28, -421(x2)
i_34:
	sub x3, x31, x21
i_35:
	sb x26, 138(x2)
i_36:
	sb x9, -349(x2)
i_37:
	lb x28, -220(x2)
i_38:
	sltu x30, x9, x18
i_39:
	srai x28, x21, 2
i_40:
	addi x21, x0, -1915
i_41:
	addi x26, x0, -1913
i_42:
	addi x9, x0, 25
i_43:
	sll x30, x9, x9
i_44:
	sb x17, 14(x2)
i_45:
	addi x21 , x21 , 1
	bgeu x26, x21, i_42
i_46:
	lh x9, 248(x2)
i_47:
	bltu x3, x27, i_56
i_48:
	add x25, x5, x21
i_49:
	lhu x30, 348(x2)
i_50:
	bge x2, x14, i_51
i_51:
	lbu x30, -376(x2)
i_52:
	sltu x28, x30, x1
i_53:
	beq x3, x3, i_63
i_54:
	lb x3, -448(x2)
i_55:
	lbu x24, 386(x2)
i_56:
	lui x28, 627073
i_57:
	lhu x5, -220(x2)
i_58:
	bne x3, x30, i_63
i_59:
	lw x8, -12(x2)
i_60:
	srli x17, x28, 2
i_61:
	lhu x12, 306(x2)
i_62:
	addi x20, x0, 27
i_63:
	sll x26, x8, x20
i_64:
	lh x26, 260(x2)
i_65:
	lb x9, -257(x2)
i_66:
	lh x5, -322(x2)
i_67:
	lw x14, 196(x2)
i_68:
	auipc x23, 255054
i_69:
	blt x15, x1, i_71
i_70:
	sh x14, -74(x2)
i_71:
	lh x9, -220(x2)
i_72:
	sub x29, x5, x3
i_73:
	auipc x11, 334716
i_74:
	sb x31, 442(x2)
i_75:
	sw x6, -88(x2)
i_76:
	lb x3, -26(x2)
i_77:
	sb x15, 481(x2)
i_78:
	sh x8, 320(x2)
i_79:
	sh x23, -74(x2)
i_80:
	auipc x9, 845484
i_81:
	lh x23, 182(x2)
i_82:
	bltu x29, x15, i_92
i_83:
	sh x3, -86(x2)
i_84:
	sb x26, -389(x2)
i_85:
	lw x4, -28(x2)
i_86:
	nop
i_87:
	nop
i_88:
	nop
i_89:
	slli x21, x10, 2
i_90:
	sh x16, -188(x2)
i_91:
	lw x23, 340(x2)
i_92:
	sb x9, -375(x2)
i_93:
	lbu x14, -90(x2)
i_94:
	addi x1, x0, -1948
i_95:
	addi x9, x0, -1946
i_96:
	lbu x23, 278(x2)
i_97:
	sltiu x21, x25, 395
i_98:
	lhu x21, -270(x2)
i_99:
	lb x29, 211(x2)
i_100:
	addi x1 , x1 , 1
	bne x1, x9, i_96
i_101:
	add x19, x14, x30
i_102:
	lui x22, 94911
i_103:
	sw x13, -140(x2)
i_104:
	addi x19, x0, 6
i_105:
	sra x22, x29, x19
i_106:
	lui x29, 856706
i_107:
	auipc x29, 882729
i_108:
	or x20, x14, x20
i_109:
	or x17, x7, x17
i_110:
	bge x6, x14, i_118
i_111:
	lhu x17, 352(x2)
i_112:
	sb x17, 277(x2)
i_113:
	srli x20, x5, 2
i_114:
	sltiu x20, x2, -283
i_115:
	lw x13, 192(x2)
i_116:
	slti x19, x8, 1245
i_117:
	sw x12, -128(x2)
i_118:
	lh x23, -56(x2)
i_119:
	blt x9, x2, i_129
i_120:
	lh x13, -90(x2)
i_121:
	lhu x23, 278(x2)
i_122:
	lb x8, -106(x2)
i_123:
	lh x9, 148(x2)
i_124:
	lh x3, -138(x2)
i_125:
	srli x23, x29, 2
i_126:
	sltu x3, x2, x7
i_127:
	and x29, x29, x4
i_128:
	or x4, x29, x16
i_129:
	lhu x24, 118(x2)
i_130:
	lw x4, 468(x2)
i_131:
	addi x13, x0, 1935
i_132:
	addi x5, x0, 1938
i_133:
	slt x14, x22, x11
i_134:
	sb x13, -238(x2)
i_135:
	lh x30, -98(x2)
i_136:
	addi x13 , x13 , 1
	blt x13, x5, i_133
i_137:
	lb x27, 140(x2)
i_138:
	lh x29, -226(x2)
i_139:
	addi x13, x0, 2027
i_140:
	addi x26, x0, 2031
i_141:
	lhu x8, 454(x2)
i_142:
	lh x15, 282(x2)
i_143:
	bge x22, x21, i_148
i_144:
	bge x15, x21, i_149
i_145:
	sh x28, 178(x2)
i_146:
	sw x30, -168(x2)
i_147:
	nop
i_148:
	add x29, x17, x24
i_149:
	slti x24, x20, -1260
i_150:
	lhu x12, 316(x2)
i_151:
	andi x5, x12, -1710
i_152:
	addi x13 , x13 , 1
	bge x26, x13, i_141
i_153:
	lhu x5, 312(x2)
i_154:
	srli x22, x12, 1
i_155:
	lbu x12, -229(x2)
i_156:
	lb x20, -306(x2)
i_157:
	ori x12, x31, 611
i_158:
	addi x13, x22, 1123
i_159:
	lb x13, 475(x2)
i_160:
	or x12, x8, x15
i_161:
	lb x28, -433(x2)
i_162:
	slli x30, x23, 1
i_163:
	sw x4, -32(x2)
i_164:
	xor x12, x7, x12
i_165:
	lhu x17, 450(x2)
i_166:
	addi x4, x0, 5
i_167:
	sra x5, x17, x4
i_168:
	bge x17, x18, i_174
i_169:
	slli x18, x18, 2
i_170:
	ori x30, x5, -1499
i_171:
	lbu x24, 237(x2)
i_172:
	lh x25, 352(x2)
i_173:
	lb x21, 101(x2)
i_174:
	lb x15, 362(x2)
i_175:
	lh x12, -422(x2)
i_176:
	lb x11, 226(x2)
i_177:
	beq x24, x12, i_184
i_178:
	srli x25, x24, 2
i_179:
	xor x24, x23, x10
i_180:
	lh x13, -2(x2)
i_181:
	addi x16, x0, 22
i_182:
	sra x24, x14, x16
i_183:
	add x28, x30, x14
i_184:
	sh x22, -40(x2)
i_185:
	lb x23, -317(x2)
i_186:
	lw x5, -80(x2)
i_187:
	or x8, x5, x6
i_188:
	lbu x5, 192(x2)
i_189:
	add x6, x8, x28
i_190:
	lhu x24, 290(x2)
i_191:
	lhu x5, 198(x2)
i_192:
	beq x5, x4, i_196
i_193:
	sb x4, 146(x2)
i_194:
	sh x6, 488(x2)
i_195:
	lh x8, 258(x2)
i_196:
	bne x8, x3, i_198
i_197:
	lw x10, 472(x2)
i_198:
	sb x11, -23(x2)
i_199:
	addi x8, x19, -214
i_200:
	lbu x19, -70(x2)
i_201:
	lb x14, 57(x2)
i_202:
	lhu x22, -234(x2)
i_203:
	lh x14, -174(x2)
i_204:
	lb x19, 261(x2)
i_205:
	and x13, x4, x18
i_206:
	lh x13, 86(x2)
i_207:
	addi x8, x0, 4
i_208:
	sll x13, x22, x8
i_209:
	sb x10, -201(x2)
i_210:
	lhu x21, 428(x2)
i_211:
	slti x18, x1, 1746
i_212:
	addi x19, x0, -2007
i_213:
	addi x13, x0, -2004
i_214:
	addi x19 , x19 , 1
	blt x19, x13, i_214
i_215:
	andi x18, x16, 1661
i_216:
	beq x8, x14, i_226
i_217:
	lh x16, -40(x2)
i_218:
	or x18, x13, x6
i_219:
	add x18, x21, x19
i_220:
	sb x5, 24(x2)
i_221:
	lw x14, 272(x2)
i_222:
	sb x29, -302(x2)
i_223:
	srai x1, x21, 1
i_224:
	addi x8, x12, 666
i_225:
	lb x12, -272(x2)
i_226:
	lui x1, 829288
i_227:
	sh x31, -384(x2)
i_228:
	srai x22, x21, 1
i_229:
	lw x1, -444(x2)
i_230:
	lw x29, -436(x2)
i_231:
	sb x13, 407(x2)
i_232:
	sltiu x21, x17, 1327
i_233:
	and x1, x31, x26
i_234:
	sb x19, 468(x2)
i_235:
	lui x17, 694253
i_236:
	lb x26, -376(x2)
i_237:
	lw x14, -396(x2)
i_238:
	sw x3, 200(x2)
i_239:
	lhu x1, -252(x2)
i_240:
	sw x28, -8(x2)
i_241:
	lhu x21, 192(x2)
i_242:
	lhu x13, -212(x2)
i_243:
	addi x26, x0, 22
i_244:
	sra x4, x6, x26
i_245:
	lb x21, 246(x2)
i_246:
	beq x21, x10, i_254
i_247:
	bge x21, x25, i_248
i_248:
	lb x21, 365(x2)
i_249:
	lbu x19, -104(x2)
i_250:
	blt x4, x30, i_260
i_251:
	sltiu x26, x25, -454
i_252:
	add x4, x8, x21
i_253:
	addi x20, x1, 1613
i_254:
	srai x8, x13, 4
i_255:
	and x14, x19, x21
i_256:
	ori x21, x2, 3
i_257:
	sb x5, 483(x2)
i_258:
	andi x21, x15, -64
i_259:
	lw x15, -76(x2)
i_260:
	auipc x5, 732622
i_261:
	ori x15, x21, 276
i_262:
	bltu x29, x28, i_263
i_263:
	slt x21, x20, x14
i_264:
	sh x17, 354(x2)
i_265:
	bne x8, x21, i_267
i_266:
	bgeu x17, x5, i_271
i_267:
	lb x17, -85(x2)
i_268:
	bgeu x8, x5, i_272
i_269:
	beq x19, x5, i_271
i_270:
	lbu x1, -465(x2)
i_271:
	lw x16, -332(x2)
i_272:
	lb x16, -130(x2)
i_273:
	sb x21, -66(x2)
i_274:
	lbu x18, 317(x2)
i_275:
	lb x28, 411(x2)
i_276:
	addi x23, x0, 8
i_277:
	srl x3, x15, x23
i_278:
	beq x26, x1, i_284
i_279:
	sltu x28, x5, x12
i_280:
	lbu x5, 368(x2)
i_281:
	lw x19, 396(x2)
i_282:
	beq x25, x26, i_288
i_283:
	blt x10, x29, i_292
i_284:
	add x19, x18, x10
i_285:
	or x19, x25, x10
i_286:
	lb x10, 36(x2)
i_287:
	lw x19, -232(x2)
i_288:
	sltiu x12, x12, 83
i_289:
	slt x25, x12, x12
i_290:
	sh x21, -326(x2)
i_291:
	lbu x26, 252(x2)
i_292:
	addi x16, x0, 2
i_293:
	srl x20, x10, x16
i_294:
	andi x24, x28, -1219
i_295:
	lbu x3, 193(x2)
i_296:
	sw x3, 324(x2)
i_297:
	sw x4, 372(x2)
i_298:
	lh x5, -84(x2)
i_299:
	lw x26, -252(x2)
i_300:
	sb x18, -203(x2)
i_301:
	lbu x3, -91(x2)
i_302:
	slt x29, x24, x28
i_303:
	sw x1, -424(x2)
i_304:
	sltiu x29, x10, -1689
i_305:
	bltu x19, x11, i_310
i_306:
	lw x29, 440(x2)
i_307:
	lui x10, 418169
i_308:
	lbu x5, 152(x2)
i_309:
	sh x27, 58(x2)
i_310:
	beq x2, x25, i_316
i_311:
	lw x3, 40(x2)
i_312:
	ori x25, x8, 897
i_313:
	sltiu x13, x21, 1838
i_314:
	slli x24, x13, 2
i_315:
	or x3, x3, x8
i_316:
	lbu x13, 166(x2)
i_317:
	lbu x16, 196(x2)
i_318:
	bne x6, x21, i_324
i_319:
	xor x3, x30, x2
i_320:
	blt x19, x9, i_324
i_321:
	lw x30, 284(x2)
i_322:
	srli x3, x11, 1
i_323:
	sw x22, 20(x2)
i_324:
	bne x30, x26, i_330
i_325:
	srai x23, x3, 1
i_326:
	add x20, x31, x19
i_327:
	bge x16, x1, i_330
i_328:
	lw x15, -192(x2)
i_329:
	slli x28, x6, 2
i_330:
	andi x1, x7, 1133
i_331:
	lbu x1, 423(x2)
i_332:
	lw x13, 444(x2)
i_333:
	lbu x5, -208(x2)
i_334:
	lb x11, -221(x2)
i_335:
	lui x17, 432382
i_336:
	sw x2, 64(x2)
i_337:
	lhu x10, -382(x2)
i_338:
	blt x10, x10, i_339
i_339:
	lw x5, 216(x2)
i_340:
	addi x27, x0, 14
i_341:
	srl x17, x22, x27
i_342:
	sb x13, 283(x2)
i_343:
	sh x3, 174(x2)
i_344:
	sh x20, -8(x2)
i_345:
	and x12, x17, x28
i_346:
	sltu x25, x16, x1
i_347:
	and x17, x17, x28
i_348:
	lb x16, 224(x2)
i_349:
	lbu x3, 225(x2)
i_350:
	lhu x16, -262(x2)
i_351:
	lb x17, -231(x2)
i_352:
	addi x24, x0, 2036
i_353:
	addi x14, x0, 2040
i_354:
	nop
i_355:
	beq x4, x18, i_365
i_356:
	addi x24 , x24 , 1
	blt x24, x14, i_354
i_357:
	addi x29, x24, -313
i_358:
	lh x18, -290(x2)
i_359:
	lbu x27, 389(x2)
i_360:
	lhu x29, 324(x2)
i_361:
	srai x23, x31, 4
i_362:
	xor x19, x26, x26
i_363:
	lh x1, 2(x2)
i_364:
	sb x3, 448(x2)
i_365:
	nop
i_366:
	sh x19, 366(x2)
i_367:
	addi x15, x0, 1925
i_368:
	addi x29, x0, 1928
i_369:
	nop
i_370:
	sh x1, 388(x2)
i_371:
	sb x20, -202(x2)
i_372:
	slli x17, x15, 3
i_373:
	nop
i_374:
	addi x3, x0, 25
i_375:
	srl x26, x17, x3
i_376:
	addi x15 , x15 , 1
	blt x15, x29, i_369
i_377:
	srli x3, x31, 4
i_378:
	sb x26, -39(x2)
i_379:
	bltu x6, x20, i_387
i_380:
	sh x24, 110(x2)
i_381:
	slli x3, x30, 4
i_382:
	sh x3, -438(x2)
i_383:
	lw x30, -128(x2)
i_384:
	addi x14, x0, 30
i_385:
	sra x12, x18, x14
i_386:
	blt x30, x23, i_396
i_387:
	sh x27, 118(x2)
i_388:
	lbu x23, -286(x2)
i_389:
	addi x30, x0, 14
i_390:
	sra x30, x30, x30
i_391:
	andi x3, x28, -1564
i_392:
	addi x8, x16, -1772
i_393:
	lb x21, -216(x2)
i_394:
	addi x14, x17, -912
i_395:
	auipc x30, 272826
i_396:
	lbu x30, -145(x2)
i_397:
	bgeu x3, x14, i_401
i_398:
	addi x12, x0, 6
i_399:
	srl x21, x21, x12
i_400:
	lhu x15, -366(x2)
i_401:
	lb x20, 68(x2)
i_402:
	lw x30, -220(x2)
i_403:
	addi x8, x0, -1991
i_404:
	addi x10, x0, -1989
i_405:
	lh x20, 472(x2)
i_406:
	nop
i_407:
	srai x15, x15, 4
i_408:
	lhu x20, -446(x2)
i_409:
	addi x8 , x8 , 1
	bltu x8, x10, i_405
i_410:
	add x4, x13, x25
i_411:
	sb x10, -342(x2)
i_412:
	or x25, x15, x27
i_413:
	lh x23, 62(x2)
i_414:
	or x22, x4, x12
i_415:
	addi x29, x0, 11
i_416:
	srl x12, x17, x29
i_417:
	addi x29, x0, 11
i_418:
	sll x23, x4, x29
i_419:
	slli x17, x19, 2
i_420:
	lb x9, 146(x2)
i_421:
	sh x24, -150(x2)
i_422:
	lh x6, -22(x2)
i_423:
	andi x9, x3, 60
i_424:
	lh x29, -60(x2)
i_425:
	sh x20, 300(x2)
i_426:
	lh x19, -422(x2)
i_427:
	and x11, x26, x4
i_428:
	xori x19, x29, -1925
i_429:
	sw x8, 128(x2)
i_430:
	bge x29, x16, i_437
i_431:
	blt x28, x1, i_437
i_432:
	lhu x28, 418(x2)
i_433:
	beq x4, x28, i_442
i_434:
	sltiu x28, x28, -1774
i_435:
	blt x27, x2, i_437
i_436:
	sw x30, -212(x2)
i_437:
	lb x19, 329(x2)
i_438:
	addi x18, x0, 2
i_439:
	sll x27, x10, x18
i_440:
	srai x28, x18, 1
i_441:
	lw x18, 344(x2)
i_442:
	xori x8, x6, -1487
i_443:
	sb x18, -361(x2)
i_444:
	bgeu x2, x8, i_449
i_445:
	lhu x6, 64(x2)
i_446:
	slli x22, x27, 3
i_447:
	lw x8, 64(x2)
i_448:
	lhu x6, 94(x2)
i_449:
	sh x5, 46(x2)
i_450:
	sb x8, -366(x2)
i_451:
	auipc x8, 119694
i_452:
	lh x22, -14(x2)
i_453:
	sub x12, x25, x29
i_454:
	srli x6, x8, 2
i_455:
	sltiu x29, x8, 1797
i_456:
	lh x15, 190(x2)
i_457:
	lw x24, 292(x2)
i_458:
	bltu x18, x25, i_459
i_459:
	lw x27, 468(x2)
i_460:
	lh x29, -174(x2)
i_461:
	sb x10, 131(x2)
i_462:
	bge x31, x3, i_468
i_463:
	addi x12, x19, 265
i_464:
	lhu x8, -312(x2)
i_465:
	bge x29, x14, i_470
i_466:
	bgeu x24, x22, i_468
i_467:
	bgeu x7, x8, i_477
i_468:
	sh x24, -440(x2)
i_469:
	sw x14, 220(x2)
i_470:
	bgeu x23, x15, i_474
i_471:
	lbu x16, 218(x2)
i_472:
	blt x26, x1, i_481
i_473:
	sw x22, -200(x2)
i_474:
	auipc x18, 183665
i_475:
	lw x4, 368(x2)
i_476:
	nop
i_477:
	sb x1, 223(x2)
i_478:
	auipc x1, 484480
i_479:
	andi x4, x4, 160
i_480:
	auipc x4, 905475
i_481:
	lbu x4, 393(x2)
i_482:
	lui x1, 153398
i_483:
	addi x11, x0, -1973
i_484:
	addi x27, x0, -1969
i_485:
	slt x22, x4, x31
i_486:
	addi x11 , x11 , 1
	blt x11, x27, i_485
i_487:
	slli x28, x1, 3
i_488:
	beq x11, x22, i_489
i_489:
	bge x15, x4, i_497
i_490:
	lw x9, -184(x2)
i_491:
	lw x27, -128(x2)
i_492:
	bltu x25, x11, i_502
i_493:
	addi x17, x0, 25
i_494:
	srl x21, x8, x17
i_495:
	lui x9, 196876
i_496:
	ori x23, x14, -1298
i_497:
	sw x15, 296(x2)
i_498:
	lbu x21, 279(x2)
i_499:
	slli x20, x10, 3
i_500:
	lb x10, 12(x2)
i_501:
	lh x19, -396(x2)
i_502:
	add x1, x30, x18
i_503:
	bgeu x24, x16, i_507
i_504:
	lh x29, -112(x2)
i_505:
	ori x24, x22, 1085
i_506:
	lhu x19, -82(x2)
i_507:
	slli x12, x29, 1
i_508:
	srai x9, x30, 4
i_509:
	lh x12, 256(x2)
i_510:
	auipc x30, 520723
i_511:
	bltu x24, x30, i_519
i_512:
	xori x30, x22, -1132
i_513:
	sw x9, -316(x2)
i_514:
	lb x6, 437(x2)
i_515:
	lh x6, -274(x2)
i_516:
	lbu x12, 1(x2)
i_517:
	lh x12, -436(x2)
i_518:
	addi x30, x0, 26
i_519:
	srl x6, x8, x30
i_520:
	auipc x9, 250645
i_521:
	addi x12, x0, 17
i_522:
	sra x18, x4, x12
i_523:
	sub x22, x17, x3
i_524:
	slti x12, x9, -213
i_525:
	lw x5, -268(x2)
i_526:
	lui x9, 1030581
i_527:
	xori x8, x26, -371
i_528:
	andi x3, x13, 1476
i_529:
	add x24, x10, x24
i_530:
	lhu x21, -476(x2)
i_531:
	blt x31, x12, i_535
i_532:
	lw x8, -456(x2)
i_533:
	bgeu x12, x10, i_543
i_534:
	lbu x11, -75(x2)
i_535:
	lhu x26, -170(x2)
i_536:
	lhu x8, 58(x2)
i_537:
	lhu x10, 206(x2)
i_538:
	lh x8, 358(x2)
i_539:
	bltu x24, x31, i_540
i_540:
	lh x29, 8(x2)
i_541:
	add x11, x7, x13
i_542:
	slti x29, x1, 846
i_543:
	lb x15, 2(x2)
i_544:
	nop
i_545:
	addi x24, x0, 1894
i_546:
	addi x27, x0, 1898
i_547:
	nop
i_548:
	add x29, x27, x11
i_549:
	sub x19, x24, x7
i_550:
	addi x24 , x24 , 1
	bne x24, x27, i_547
i_551:
	lh x27, 474(x2)
i_552:
	lbu x11, -296(x2)
i_553:
	lh x23, -74(x2)
i_554:
	lb x9, -150(x2)
i_555:
	sw x18, -36(x2)
i_556:
	sh x16, 406(x2)
i_557:
	lw x23, -336(x2)
i_558:
	lh x19, -392(x2)
i_559:
	lbu x9, -434(x2)
i_560:
	addi x5, x0, 3
i_561:
	sra x6, x23, x5
i_562:
	sw x12, -432(x2)
i_563:
	lw x19, -424(x2)
i_564:
	sb x6, -4(x2)
i_565:
	bltu x19, x15, i_571
i_566:
	lw x18, -320(x2)
i_567:
	lw x9, -56(x2)
i_568:
	bltu x21, x26, i_570
i_569:
	sh x13, 4(x2)
i_570:
	sb x30, -152(x2)
i_571:
	nop
i_572:
	nop
i_573:
	addi x28, x0, -1875
i_574:
	addi x19, x0, -1873
i_575:
	lhu x18, -266(x2)
i_576:
	nop
i_577:
	lh x20, 68(x2)
i_578:
	bgeu x28, x30, i_582
i_579:
	addi x12, x20, 1368
i_580:
	add x24, x28, x29
i_581:
	nop
i_582:
	add x29, x30, x10
i_583:
	lw x18, -52(x2)
i_584:
	lw x14, -388(x2)
i_585:
	addi x28 , x28 , 1
	bgeu x19, x28, i_575
i_586:
	addi x9, x24, -468
i_587:
	bne x18, x30, i_590
i_588:
	lh x22, -424(x2)
i_589:
	beq x21, x6, i_595
i_590:
	or x12, x5, x9
i_591:
	sw x18, 188(x2)
i_592:
	srli x18, x28, 3
i_593:
	bne x27, x30, i_602
i_594:
	sb x16, -338(x2)
i_595:
	sh x18, -402(x2)
i_596:
	lh x1, 170(x2)
i_597:
	nop
i_598:
	slli x11, x15, 2
i_599:
	nop
i_600:
	sw x1, -8(x2)
i_601:
	sw x23, -204(x2)
i_602:
	sh x18, -26(x2)
i_603:
	lh x4, 250(x2)
i_604:
	addi x20, x0, -1863
i_605:
	addi x14, x0, -1859
i_606:
	nop
i_607:
	addi x20 , x20 , 1
	bgeu x14, x20, i_606
i_608:
	bge x26, x22, i_618
i_609:
	lb x10, 352(x2)
i_610:
	sh x14, -94(x2)
i_611:
	ori x29, x6, -1446
i_612:
	blt x1, x29, i_622
i_613:
	sub x10, x1, x1
i_614:
	lw x1, 424(x2)
i_615:
	sltu x3, x20, x18
i_616:
	and x8, x4, x24
i_617:
	lh x1, 218(x2)
i_618:
	lw x20, -292(x2)
i_619:
	lbu x29, 440(x2)
i_620:
	lb x10, 142(x2)
i_621:
	bge x18, x1, i_630
i_622:
	beq x16, x10, i_632
i_623:
	lui x4, 746498
i_624:
	nop
i_625:
	sh x16, 300(x2)
i_626:
	lb x29, 313(x2)
i_627:
	lhu x21, -48(x2)
i_628:
	addi x28, x0, 27
i_629:
	sll x28, x29, x28
i_630:
	nop
i_631:
	sltu x22, x27, x6
i_632:
	lbu x26, -306(x2)
i_633:
	andi x4, x8, -409
i_634:
	addi x19, x0, -1899
i_635:
	addi x9, x0, -1895
i_636:
	sb x6, -168(x2)
i_637:
	addi x19 , x19 , 1
	bgeu x9, x19, i_636
i_638:
	slti x8, x24, 1156
i_639:
	lw x11, -140(x2)
i_640:
	lb x4, 127(x2)
i_641:
	sw x26, 276(x2)
i_642:
	bgeu x20, x11, i_648
i_643:
	lh x22, -282(x2)
i_644:
	sw x26, -428(x2)
i_645:
	sw x6, 208(x2)
i_646:
	lbu x21, -44(x2)
i_647:
	blt x1, x23, i_648
i_648:
	lbu x4, -45(x2)
i_649:
	blt x15, x25, i_653
i_650:
	lhu x21, 432(x2)
i_651:
	sh x8, 482(x2)
i_652:
	auipc x25, 148411
i_653:
	lhu x20, -6(x2)
i_654:
	ori x22, x28, 1416
i_655:
	xor x24, x15, x20
i_656:
	slli x11, x30, 1
i_657:
	or x20, x24, x9
i_658:
	sltu x29, x25, x22
i_659:
	lw x9, -324(x2)
i_660:
	sw x26, 212(x2)
i_661:
	lh x25, -20(x2)
i_662:
	bgeu x19, x23, i_664
i_663:
	nop
i_664:
	xor x17, x27, x16
i_665:
	lb x24, -293(x2)
i_666:
	addi x21, x0, 2040
i_667:
	addi x20, x0, 2043
i_668:
	lb x17, 92(x2)
i_669:
	nop
i_670:
	sh x17, 52(x2)
i_671:
	addi x21 , x21 , 1
	bne x21, x20, i_668
i_672:
	sw x11, -280(x2)
i_673:
	lw x11, 396(x2)
i_674:
	xor x22, x20, x7
i_675:
	and x21, x15, x12
i_676:
	lhu x24, -12(x2)
i_677:
	sw x5, -276(x2)
i_678:
	lw x27, -80(x2)
i_679:
	lw x16, 176(x2)
i_680:
	sb x15, -14(x2)
i_681:
	lbu x5, -323(x2)
i_682:
	lhu x3, -114(x2)
i_683:
	lw x21, 116(x2)
i_684:
	lh x9, -486(x2)
i_685:
	bltu x15, x12, i_693
i_686:
	add x24, x7, x12
i_687:
	sltu x15, x9, x4
i_688:
	lh x1, 214(x2)
i_689:
	lw x4, -296(x2)
i_690:
	sh x6, 142(x2)
i_691:
	lh x4, 448(x2)
i_692:
	andi x21, x27, -727
i_693:
	addi x15, x0, 25
i_694:
	srl x8, x31, x15
i_695:
	addi x24, x0, -1880
i_696:
	addi x17, x0, -1876
i_697:
	srli x18, x7, 3
i_698:
	sh x8, 204(x2)
i_699:
	sb x18, -441(x2)
i_700:
	nop
i_701:
	addi x24 , x24 , 1
	bne x24, x17, i_697
i_702:
	add x8, x25, x3
i_703:
	lhu x28, -34(x2)
i_704:
	sltu x23, x28, x15
i_705:
	sh x30, -482(x2)
i_706:
	sh x13, -18(x2)
i_707:
	lbu x15, 61(x2)
i_708:
	lb x23, 285(x2)
i_709:
	lh x13, 138(x2)
i_710:
	lbu x15, -445(x2)
i_711:
	sh x16, 10(x2)
i_712:
	bge x13, x15, i_722
i_713:
	sh x20, -416(x2)
i_714:
	lb x13, 56(x2)
i_715:
	lbu x11, -393(x2)
i_716:
	blt x21, x18, i_725
i_717:
	addi x26, x0, 15
i_718:
	sra x4, x18, x26
i_719:
	slli x24, x24, 1
i_720:
	addi x22, x0, 27
i_721:
	sra x18, x18, x22
i_722:
	srli x1, x7, 2
i_723:
	beq x24, x5, i_725
i_724:
	srai x27, x24, 1
i_725:
	blt x30, x31, i_732
i_726:
	addi x27, x0, 2
i_727:
	sra x9, x18, x27
i_728:
	sb x16, 473(x2)
i_729:
	lw x13, 208(x2)
i_730:
	sw x13, -28(x2)
i_731:
	lhu x13, -254(x2)
i_732:
	lh x13, -72(x2)
i_733:
	sh x7, -220(x2)
i_734:
	slti x21, x13, -15
i_735:
	lb x9, 326(x2)
i_736:
	srli x6, x15, 4
i_737:
	lbu x13, 294(x2)
i_738:
	lb x3, 296(x2)
i_739:
	lbu x13, 306(x2)
i_740:
	blt x13, x3, i_747
i_741:
	srai x26, x18, 2
i_742:
	bge x4, x12, i_750
i_743:
	sw x3, 44(x2)
i_744:
	sb x18, 38(x2)
i_745:
	lw x22, -244(x2)
i_746:
	lb x20, -294(x2)
i_747:
	lbu x26, 264(x2)
i_748:
	lw x26, 208(x2)
i_749:
	lh x22, 466(x2)
i_750:
	sltu x22, x20, x21
i_751:
	and x10, x1, x6
i_752:
	lw x10, -168(x2)
i_753:
	xori x1, x2, 1064
i_754:
	lb x11, 456(x2)
i_755:
	sw x24, 416(x2)
i_756:
	lbu x10, 190(x2)
i_757:
	lh x29, -234(x2)
i_758:
	srai x21, x20, 4
i_759:
	lui x5, 116974
i_760:
	lhu x26, -172(x2)
i_761:
	sw x9, 272(x2)
i_762:
	lhu x26, -422(x2)
i_763:
	slt x23, x2, x11
i_764:
	lhu x9, 140(x2)
i_765:
	sltu x13, x18, x22
i_766:
	sltu x23, x23, x25
i_767:
	bgeu x3, x2, i_776
i_768:
	addi x20, x0, 2
i_769:
	srl x14, x22, x20
i_770:
	add x14, x22, x20
i_771:
	ori x14, x13, -819
i_772:
	and x21, x16, x18
i_773:
	lw x21, 420(x2)
i_774:
	lh x26, 424(x2)
i_775:
	lbu x21, -430(x2)
i_776:
	sb x11, 428(x2)
i_777:
	nop
i_778:
	addi x22, x0, 1845
i_779:
	addi x13, x0, 1847
i_780:
	beq x14, x7, i_790
i_781:
	addi x22 , x22 , 1
	blt x22, x13, i_780
i_782:
	lbu x20, -152(x2)
i_783:
	lb x10, 411(x2)
i_784:
	addi x27, x0, 22
i_785:
	srl x21, x21, x27
i_786:
	sltiu x10, x31, -669
i_787:
	lh x21, 90(x2)
i_788:
	ori x30, x4, 1012
i_789:
	sw x27, 288(x2)
i_790:
	slti x30, x8, -1783
i_791:
	lw x11, -236(x2)
i_792:
	addi x21, x0, 3
i_793:
	sra x21, x31, x21
i_794:
	lbu x30, 111(x2)
i_795:
	sb x14, 43(x2)
i_796:
	sh x12, 268(x2)
i_797:
	add x11, x30, x19
i_798:
	lhu x17, 90(x2)
i_799:
	slli x1, x6, 1
i_800:
	sub x6, x16, x11
i_801:
	add x28, x20, x17
i_802:
	and x11, x13, x6
i_803:
	add x28, x11, x24
i_804:
	lbu x21, -108(x2)
i_805:
	sw x21, 352(x2)
i_806:
	slti x13, x17, 1144
i_807:
	sltiu x25, x29, 1117
i_808:
	lb x12, -242(x2)
i_809:
	lbu x1, 444(x2)
i_810:
	xor x21, x6, x6
i_811:
	sw x9, -300(x2)
i_812:
	sw x28, -8(x2)
i_813:
	lh x28, -298(x2)
i_814:
	beq x30, x6, i_817
i_815:
	sh x7, 6(x2)
i_816:
	sw x20, 148(x2)
i_817:
	auipc x13, 819440
i_818:
	sh x10, 366(x2)
i_819:
	sh x21, 216(x2)
i_820:
	or x30, x9, x31
i_821:
	sb x14, 330(x2)
i_822:
	sh x22, -102(x2)
i_823:
	sltiu x16, x13, -1946
i_824:
	sb x15, -392(x2)
i_825:
	sw x22, 112(x2)
i_826:
	srli x22, x28, 1
i_827:
	slti x21, x10, 995
i_828:
	lh x12, 180(x2)
i_829:
	sb x22, 179(x2)
i_830:
	sb x22, 189(x2)
i_831:
	addi x12, x26, 1522
i_832:
	lb x17, -428(x2)
i_833:
	srai x30, x10, 1
i_834:
	addi x10, x0, 22
i_835:
	sra x10, x12, x10
i_836:
	addi x10, x0, 23
i_837:
	sll x30, x4, x10
i_838:
	bne x15, x10, i_839
i_839:
	addi x15, x0, 12
i_840:
	sra x30, x28, x15
i_841:
	lb x18, -96(x2)
i_842:
	addi x8, x0, 13
i_843:
	srl x28, x30, x8
i_844:
	lhu x15, 128(x2)
i_845:
	sub x30, x26, x19
i_846:
	lw x6, -356(x2)
i_847:
	lhu x30, 120(x2)
i_848:
	addi x26, x0, 14
i_849:
	sll x10, x23, x26
i_850:
	lui x1, 754381
i_851:
	sb x13, 122(x2)
i_852:
	sh x30, 328(x2)
i_853:
	lw x13, -132(x2)
i_854:
	add x28, x23, x6
i_855:
	auipc x25, 435257
i_856:
	sb x25, 423(x2)
i_857:
	lh x10, 384(x2)
i_858:
	xor x1, x6, x20
i_859:
	beq x17, x12, i_863
i_860:
	slti x1, x1, 705
i_861:
	lh x11, -116(x2)
i_862:
	lw x17, -108(x2)
i_863:
	blt x3, x29, i_870
i_864:
	lhu x3, 326(x2)
i_865:
	lb x17, -361(x2)
i_866:
	sw x20, -168(x2)
i_867:
	sub x24, x24, x3
i_868:
	add x17, x2, x28
i_869:
	sb x25, 324(x2)
i_870:
	lb x9, -200(x2)
i_871:
	lb x8, 128(x2)
i_872:
	srai x1, x23, 4
i_873:
	lbu x29, -414(x2)
i_874:
	addi x17, x0, -1944
i_875:
	addi x16, x0, -1940
i_876:
	add x3, x2, x21
i_877:
	lw x19, -140(x2)
i_878:
	lh x14, 154(x2)
i_879:
	lhu x6, -54(x2)
i_880:
	bne x6, x30, i_881
i_881:
	lbu x19, 39(x2)
i_882:
	blt x30, x1, i_889
i_883:
	lw x11, 92(x2)
i_884:
	lbu x8, 262(x2)
i_885:
	lhu x19, -484(x2)
i_886:
	addi x17 , x17 , 1
	blt x17, x16, i_876
i_887:
	slli x27, x19, 2
i_888:
	sb x27, 295(x2)
i_889:
	lb x29, 375(x2)
i_890:
	sb x27, 473(x2)
i_891:
	bgeu x9, x30, i_897
i_892:
	lhu x5, 270(x2)
i_893:
	addi x10, x0, 28
i_894:
	sra x12, x20, x10
i_895:
	addi x10, x19, 832
i_896:
	sh x9, -282(x2)
i_897:
	lb x10, 38(x2)
i_898:
	nop
i_899:
	addi x22, x0, 2002
i_900:
	addi x12, x0, 2005
i_901:
	lh x10, 160(x2)
i_902:
	sltu x18, x1, x15
i_903:
	srai x17, x29, 1
i_904:
	sub x17, x1, x11
i_905:
	addi x17, x0, 3
i_906:
	sra x11, x5, x17
i_907:
	sb x24, -483(x2)
i_908:
	addi x17, x0, 18
i_909:
	srl x11, x12, x17
i_910:
	addi x22 , x22 , 1
	bge x12, x22, i_901
i_911:
	addi x11, x11, -1088
i_912:
	addi x11, x0, 2
i_913:
	sra x11, x9, x11
i_914:
	beq x13, x11, i_924
i_915:
	lw x23, 436(x2)
i_916:
	or x9, x25, x23
i_917:
	bne x3, x6, i_919
i_918:
	sb x11, 57(x2)
i_919:
	addi x20, x0, 31
i_920:
	sra x27, x11, x20
i_921:
	sw x23, -116(x2)
i_922:
	auipc x23, 676899
i_923:
	sw x11, 424(x2)
i_924:
	sb x11, -323(x2)
i_925:
	lbu x20, 8(x2)
i_926:
	xor x11, x7, x20
i_927:
	lb x20, -166(x2)
i_928:
	lh x19, 272(x2)
i_929:
	sub x17, x20, x11
i_930:
	sh x11, 480(x2)
i_931:
	sh x7, -274(x2)
i_932:
	lb x17, -334(x2)
i_933:
	srli x19, x19, 4
i_934:
	lw x21, 336(x2)
i_935:
	sub x23, x1, x3
i_936:
	ori x17, x14, -21
i_937:
	bge x11, x11, i_939
i_938:
	ori x25, x24, 903
i_939:
	blt x22, x22, i_943
i_940:
	bge x15, x16, i_943
i_941:
	xor x22, x2, x18
i_942:
	sw x27, 4(x2)
i_943:
	lbu x16, 188(x2)
i_944:
	sb x19, 406(x2)
i_945:
	sb x3, -256(x2)
i_946:
	slti x10, x30, -999
i_947:
	ori x3, x16, 1017
i_948:
	sltu x19, x31, x20
i_949:
	lbu x30, -323(x2)
i_950:
	slli x20, x23, 4
i_951:
	sh x8, -358(x2)
i_952:
	sw x30, -196(x2)
i_953:
	slli x15, x15, 1
i_954:
	auipc x11, 411521
i_955:
	andi x1, x21, 1095
i_956:
	auipc x21, 531006
i_957:
	add x30, x22, x26
i_958:
	lui x8, 25942
i_959:
	addi x10, x0, -1971
i_960:
	addi x13, x0, -1969
i_961:
	addi x15, x24, 593
i_962:
	lh x15, -252(x2)
i_963:
	sltiu x15, x24, -1885
i_964:
	slt x3, x14, x6
i_965:
	addi x26, x0, -2041
i_966:
	addi x22, x0, -2037
i_967:
	lhu x24, 156(x2)
i_968:
	addi x26 , x26 , 1
	bltu x26, x22, i_967
i_969:
	addi x11, x0, 19
i_970:
	sra x24, x24, x11
i_971:
	addi x10 , x10 , 1
	bgeu x13, x10, i_961
i_972:
	lw x13, 212(x2)
i_973:
	blt x1, x25, i_981
i_974:
	bgeu x21, x9, i_977
i_975:
	sb x24, -192(x2)
i_976:
	lhu x15, -350(x2)
i_977:
	lhu x15, -224(x2)
i_978:
	xori x13, x25, 651
i_979:
	lbu x13, 340(x2)
i_980:
	addi x13, x0, 30
i_981:
	sll x4, x21, x13
i_982:
	bge x22, x6, i_992
i_983:
	sw x4, 288(x2)
i_984:
	lh x3, 348(x2)
i_985:
	lhu x19, 246(x2)
i_986:
	lhu x4, 300(x2)
i_987:
	bgeu x29, x4, i_990
i_988:
	sh x6, 82(x2)
i_989:
	bne x3, x13, i_994
i_990:
	auipc x4, 621959
i_991:
	lb x28, 373(x2)
i_992:
	lhu x11, 486(x2)
i_993:
	slti x17, x24, 1285
i_994:
	slt x18, x13, x13
i_995:
	sh x28, -414(x2)
i_996:
	lw x13, -216(x2)
i_997:
	sb x13, -455(x2)
i_998:
	bge x30, x10, i_1003
i_999:
	lb x28, -99(x2)
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0xc7612ed7
	.word 0x8d2897ea
	.word 0xc76cc1b9
	.word 0x21fb7b32
	.word 0x4dab571d
	.word 0xe32763b8
	.word 0xeadaf542
	.word 0x3997a38b
	.word 0xf11d02a5
	.word 0xec0c337f
	.word 0x46eb4ff7
	.word 0x4923760e
	.word 0x8d2c06ca
	.word 0x682b6269
	.word 0xcc0392aa
	.word 0xeef4d288
	.word 0x11677fb5
	.word 0x3c7e84b
	.word 0xbfe13774
	.word 0xfacc271f
	.word 0xc409a28
	.word 0x4dd65fb0
	.word 0x26861869
	.word 0x80578a64
	.word 0xd5fa0064
	.word 0xd7509f72
	.word 0x94b23c69
	.word 0xe4c847f
	.word 0xf2439f40
	.word 0xf42e8a6f
	.word 0x1dceb03b
	.word 0xee18d48a
	.word 0x3d4e72c6
	.word 0xba40461d
	.word 0xe5620795
	.word 0xc68481b3
	.word 0xcefe4994
	.word 0x3acee8a0
	.word 0x6ceab178
	.word 0x677aafcc
	.word 0x24406d11
	.word 0xafda03e3
	.word 0xef167fd
	.word 0xcfb56d1e
	.word 0x3633c262
	.word 0xe81ca713
	.word 0xf6c788d8
	.word 0xb5fc2584
	.word 0xda613401
	.word 0xf3299994
	.word 0xe9414ff5
	.word 0x53379831
	.word 0x304e6ff8
	.word 0x9f782d6e
	.word 0x7cdad747
	.word 0x96d6e0ff
	.word 0x6beb74f2
	.word 0x83dddc15
	.word 0xe87fe442
	.word 0x851dd03b
	.word 0x7cba4dee
	.word 0x90dfd5a5
	.word 0x79a293c
	.word 0xf26c4cab
	.word 0x9deec002
	.word 0xb70ac8d2
	.word 0x9afdb8d2
	.word 0xe0012967
	.word 0x869f8de8
	.word 0x577c4bd1
	.word 0x226e7aed
	.word 0xd8c3bf84
	.word 0xf126eb62
	.word 0x9aa3fbd6
	.word 0x41fdec67
	.word 0xc9f84332
	.word 0x4a627c44
	.word 0xc08fb959
	.word 0x45c69a69
	.word 0x6c95d43a
	.word 0xdfd6a964
	.word 0xa3fb6fc2
	.word 0xa21fbfc5
	.word 0x1825af1d
	.word 0x80206e31
	.word 0x6f87244f
	.word 0xf659f9c1
	.word 0x8edf8113
	.word 0x332d6b24
	.word 0xcd7a90d7
	.word 0x6c8ef92d
	.word 0xe0cc232f
	.word 0x79a6b0d8
	.word 0xba1082e7
	.word 0xb6042af4
	.word 0x93f6f32
	.word 0x739f3aa3
	.word 0xca3dde3f
	.word 0x851a7fcd
	.word 0xbc272d75
	.word 0x10e1a5ea
	.word 0x75f06d03
	.word 0x41fbca68
	.word 0x5a46c1c
	.word 0xb6e1a009
	.word 0x5a9863fb
	.word 0xa61014d5
	.word 0xe452336a
	.word 0x448426be
	.word 0x54abfa60
	.word 0x8f6d6269
	.word 0x63f0d807
	.word 0x98b5261e
	.word 0x2b353fb1
	.word 0xaf1a6620
	.word 0x4a1e90ba
	.word 0x3d5dec0f
	.word 0x2652fce8
	.word 0x36454e7f
	.word 0x81d594ad
	.word 0x1f6a1167
	.word 0x66d06820
	.word 0x7be23ae2
	.word 0xef6cdfe2
	.word 0x9c30e824
	.word 0x1cc92a84
	.word 0x49d419bf
	.word 0x355863d2
	.word 0x901588a9
	.word 0x43a5887c
	.word 0x8b04cbc6
	.word 0x3c9cc4f
	.word 0xefe82677
	.word 0xfeeeedf3
	.word 0xafe604b8
	.word 0x956c4d54
	.word 0xe7efe19c
	.word 0xd4e94527
	.word 0xc6f073c5
	.word 0xb937e3a8
	.word 0x15d38d8
	.word 0xc8068729
	.word 0xbf9005c3
	.word 0x16bb5a63
	.word 0x522d5f6c
	.word 0x3198a516
	.word 0x871a10f9
	.word 0x58da3769
	.word 0xb0dbb1ff
	.word 0xb748171a
	.word 0xccdabc8f
	.word 0x4f4a78b9
	.word 0x5fb13562
	.word 0xeacc821a
	.word 0x6fc68f54
	.word 0xba70dea
	.word 0xf7a62984
	.word 0xaeca78ef
	.word 0x556208d0
	.word 0x2c06c472
	.word 0xef0630e1
	.word 0x4a52137d
	.word 0x1f685e80
	.word 0x30ca86d
	.word 0x4cd85429
	.word 0xd12ada1c
	.word 0x2fbbcda
	.word 0xa76983ff
	.word 0x926ef883
	.word 0x93de3bf4
	.word 0xb1dc727a
	.word 0xef817375
	.word 0x85d08291
	.word 0x7764ed16
	.word 0xe4c64d52
	.word 0x922d8a91
	.word 0x2c9efee5
	.word 0xc0776021
	.word 0xdae53d8a
	.word 0x469512e9
	.word 0xfcd99c59
	.word 0xc8f775f4
	.word 0x82c490b4
	.word 0x88e8086e
	.word 0x9b0c925b
	.word 0x76c4a87d
	.word 0xe7f8ed4a
	.word 0xf5c003f7
	.word 0x3e0613b9
	.word 0x884becb0
	.word 0xaf3ee70d
	.word 0x4e362350
	.word 0x9c3301ad
	.word 0x985b6422
	.word 0x6b4299b8
	.word 0x481bf626
	.word 0x6183cde3
	.word 0x997db00d
	.word 0xda556f2f
	.word 0xab02e6d
	.word 0xd8f1ed16
	.word 0x964610a1
	.word 0x28cd6823
	.word 0x40ae87df
	.word 0x3251e9e5
	.word 0xc7bacc82
	.word 0x4f5750c6
	.word 0x5dfda43f
	.word 0x1ab0e494
	.word 0x7830f1de
	.word 0xe8b27750
	.word 0x9f17ba2e
	.word 0xab7f334e
	.word 0xa0cafad7
	.word 0x6a1be9fc
	.word 0xb8873153
	.word 0x1c55c53b
	.word 0x577a589a
	.word 0xfb0dd63d
	.word 0x389024b9
	.word 0x15413b03
	.word 0x1008d7d0
	.word 0x78703d94
	.word 0x55edf2b5
	.word 0xeeeabafd
	.word 0xae988038
	.word 0xada0008f
	.word 0x25b62c12
	.word 0xd811b3ff
	.word 0x89c03dab
	.word 0x25fcb990
	.word 0x4b99acec
	.word 0x550bfb90
	.word 0x3ef6700b
	.word 0xa81ad1bd
	.word 0x451abe37
	.word 0xb665a5dc
	.word 0x3f61304
	.word 0x5a97ea6a
	.word 0x39d89d6
	.word 0x51810854
	.word 0x874accf5
	.word 0xef8ac740
	.word 0x8f0409bc
	.word 0xca002d29
	.word 0x4fd7d6b0
	.word 0xbab81137
	.word 0x110c3ad2
	.word 0xbd5e8e01
	.word 0xa0a7c540
	.word 0x95ec16ab
	.word 0x2a57c792
	.word 0x113bc778
	.word 0xe490db25
	.word 0x3c57bff5
	.word 0xd068ee19
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
