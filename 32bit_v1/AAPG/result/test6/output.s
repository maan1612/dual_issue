
	.text
	.align	4
	.globl	main
	.type	main, @function
main:
  lui x2, 0x40000   ## x2<- 0x40000000
  slli x2,x2,1      ## x2<- 0x80000000
  lui x3, 0x10      ## x2<- 0x00010000   
  add x2,x2,x3      ## x2<- 0x80010000
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x2)
	lw x3, 12(x2)
	lw x4, 16(x2)
	lw x5, 20(x2)
	lw x6, 24(x2)
	lw x7, 28(x2)
	lw x8, 32(x2)
	lw x9, 36(x2)
	lw x10, 40(x2)
	lw x11, 44(x2)
	lw x12, 48(x2)
	lw x13, 52(x2)
	lw x14, 56(x2)
	lw x15, 60(x2)
	lw x16, 64(x2)
	lw x17, 68(x2)
	lw x18, 72(x2)
	lw x19, 76(x2)
	lw x20, 80(x2)
	lw x21, 84(x2)
	lw x22, 88(x2)
	lw x23, 92(x2)
	lw x24, 96(x2)
	lw x25, 100(x2)
	lw x26, 104(x2)
	lw x27, 108(x2)
	lw x28, 112(x2)
	lw x29, 116(x2)
	lw x30, 120(x2)
	lw x31, 124(x2)
i_0:
	and x15, x6, x6
i_1:
	sw x15, -52(x2)
i_2:
	lbu x28, -280(x2)
i_3:
	blt x31, x16, i_6
i_4:
	lbu x16, -144(x2)
i_5:
	bltu x1, x7, i_10
i_6:
	beq x16, x17, i_9
i_7:
	sh x1, -396(x2)
i_8:
	lw x1, 144(x2)
i_9:
	sh x17, -278(x2)
i_10:
	sb x15, 177(x2)
i_11:
	sw x1, -296(x2)
i_12:
	slli x18, x1, 1
i_13:
	andi x19, x9, 335
i_14:
	slti x22, x21, -1912
i_15:
	ori x9, x9, -1889
i_16:
	sb x12, 417(x2)
i_17:
	sb x9, 248(x2)
i_18:
	lw x27, 356(x2)
i_19:
	lbu x15, -168(x2)
i_20:
	or x9, x20, x16
i_21:
	sh x15, 406(x2)
i_22:
	beq x15, x15, i_27
i_23:
	lb x16, 377(x2)
i_24:
	sltu x16, x30, x10
i_25:
	lhu x9, 390(x2)
i_26:
	beq x21, x4, i_29
i_27:
	lbu x9, 420(x2)
i_28:
	addi x10, x0, 14
i_29:
	srl x9, x9, x10
i_30:
	blt x9, x27, i_40
i_31:
	addi x4, x0, 16
i_32:
	srl x27, x26, x4
i_33:
	ori x3, x25, -1957
i_34:
	add x16, x6, x10
i_35:
	lb x25, 467(x2)
i_36:
	lw x26, 428(x2)
i_37:
	lhu x3, 134(x2)
i_38:
	bne x4, x2, i_46
i_39:
	srai x25, x27, 3
i_40:
	lw x25, -88(x2)
i_41:
	bne x4, x1, i_43
i_42:
	addi x26, x0, 9
i_43:
	sra x3, x2, x26
i_44:
	sh x28, -318(x2)
i_45:
	nop
i_46:
	nop
i_47:
	lw x26, -176(x2)
i_48:
	addi x28, x0, 1926
i_49:
	addi x21, x0, 1930
i_50:
	addi x5, x0, 31
i_51:
	sra x4, x29, x5
i_52:
	srai x9, x4, 3
i_53:
	lui x29, 632360
i_54:
	addi x28 , x28 , 1
	bgeu x21, x28, i_50
i_55:
	lhu x29, 328(x2)
i_56:
	addi x12, x0, 12
i_57:
	sll x1, x29, x12
i_58:
	beq x10, x28, i_65
i_59:
	andi x27, x9, 1321
i_60:
	bne x13, x17, i_69
i_61:
	bgeu x19, x15, i_63
i_62:
	nop
i_63:
	lw x26, 348(x2)
i_64:
	lhu x8, -218(x2)
i_65:
	sw x8, 116(x2)
i_66:
	sb x4, -113(x2)
i_67:
	lw x29, 432(x2)
i_68:
	nop
i_69:
	lbu x29, -8(x2)
i_70:
	slt x10, x28, x16
i_71:
	addi x12, x0, -2017
i_72:
	addi x19, x0, -2014
i_73:
	beq x26, x13, i_79
i_74:
	addi x12 , x12 , 1
	blt x12, x19, i_73
i_75:
	sw x31, 388(x2)
i_76:
	sw x1, 464(x2)
i_77:
	sb x26, 90(x2)
i_78:
	addi x1, x0, 20
i_79:
	srl x27, x19, x1
i_80:
	lhu x22, -248(x2)
i_81:
	addi x9, x0, 1899
i_82:
	addi x8, x0, 1902
i_83:
	srai x22, x9, 1
i_84:
	bltu x9, x29, i_88
i_85:
	addi x9 , x9 , 1
	blt x9, x8, i_83
i_86:
	lw x9, -224(x2)
i_87:
	slt x11, x14, x4
i_88:
	andi x4, x3, -1553
i_89:
	add x4, x3, x5
i_90:
	addi x9, x0, 1896
i_91:
	addi x28, x0, 1899
i_92:
	lh x3, -198(x2)
i_93:
	srli x11, x4, 4
i_94:
	sltu x16, x29, x8
i_95:
	lbu x4, -316(x2)
i_96:
	sw x3, 316(x2)
i_97:
	lbu x6, 451(x2)
i_98:
	sub x8, x1, x28
i_99:
	sltiu x4, x31, 1856
i_100:
	addi x9 , x9 , 1
	bgeu x28, x9, i_92
i_101:
	andi x28, x6, -27
i_102:
	bge x8, x10, i_111
i_103:
	sw x19, -420(x2)
i_104:
	srli x14, x14, 3
i_105:
	sh x1, 142(x2)
i_106:
	sb x12, 17(x2)
i_107:
	lui x20, 971926
i_108:
	srli x10, x13, 2
i_109:
	lb x8, 39(x2)
i_110:
	bltu x11, x20, i_112
i_111:
	sub x9, x23, x19
i_112:
	auipc x10, 954714
i_113:
	lb x23, -467(x2)
i_114:
	addi x3, x0, 1932
i_115:
	addi x28, x0, 1935
i_116:
	sb x19, 261(x2)
i_117:
	lbu x8, 449(x2)
i_118:
	lhu x10, -8(x2)
i_119:
	lw x10, -148(x2)
i_120:
	sb x23, -83(x2)
i_121:
	addi x3 , x3 , 1
	blt x3, x28, i_116
i_122:
	beq x10, x9, i_129
i_123:
	xor x29, x14, x6
i_124:
	sh x10, -72(x2)
i_125:
	andi x14, x10, -189
i_126:
	sw x25, 20(x2)
i_127:
	bge x20, x21, i_129
i_128:
	addi x24, x0, 4
i_129:
	sll x10, x14, x24
i_130:
	addi x25, x0, 14
i_131:
	sra x30, x29, x25
i_132:
	lhu x24, -420(x2)
i_133:
	add x12, x3, x10
i_134:
	sb x8, -206(x2)
i_135:
	lbu x24, 320(x2)
i_136:
	sw x24, 216(x2)
i_137:
	addi x6, x0, -1975
i_138:
	addi x1, x0, -1971
i_139:
	bge x31, x7, i_149
i_140:
	lbu x24, -154(x2)
i_141:
	addi x6 , x6 , 1
	blt x6, x1, i_139
i_142:
	add x28, x4, x29
i_143:
	lbu x5, -129(x2)
i_144:
	sb x24, -348(x2)
i_145:
	nop
i_146:
	lw x8, -252(x2)
i_147:
	addi x3, x19, -1431
i_148:
	sw x27, -36(x2)
i_149:
	sh x31, -468(x2)
i_150:
	lhu x16, 268(x2)
i_151:
	addi x4, x0, -2048
i_152:
	addi x11, x0, -2045
i_153:
	addi x4 , x4 , 1
	blt x4, x11, i_153
i_154:
	lbu x29, -83(x2)
i_155:
	lh x10, -320(x2)
i_156:
	lb x18, -279(x2)
i_157:
	lh x10, -240(x2)
i_158:
	lb x23, -133(x2)
i_159:
	addi x28, x0, 4
i_160:
	srl x10, x30, x28
i_161:
	lhu x23, -124(x2)
i_162:
	sh x6, -304(x2)
i_163:
	sw x13, 52(x2)
i_164:
	ori x17, x31, 910
i_165:
	lb x30, 392(x2)
i_166:
	lhu x30, 332(x2)
i_167:
	blt x12, x6, i_172
i_168:
	bne x16, x25, i_175
i_169:
	lhu x22, 380(x2)
i_170:
	lw x30, -436(x2)
i_171:
	lw x23, -292(x2)
i_172:
	sw x2, -16(x2)
i_173:
	lbu x4, -382(x2)
i_174:
	sw x28, 240(x2)
i_175:
	sh x31, 258(x2)
i_176:
	sh x10, -36(x2)
i_177:
	blt x17, x17, i_179
i_178:
	sw x10, -44(x2)
i_179:
	slt x23, x4, x2
i_180:
	sh x21, 166(x2)
i_181:
	sh x17, 182(x2)
i_182:
	lhu x24, 442(x2)
i_183:
	beq x28, x4, i_185
i_184:
	sh x30, 478(x2)
i_185:
	lw x10, -132(x2)
i_186:
	lbu x15, -347(x2)
i_187:
	lui x3, 194613
i_188:
	sw x3, -192(x2)
i_189:
	slli x3, x2, 3
i_190:
	bne x30, x4, i_195
i_191:
	addi x28, x18, 73
i_192:
	addi x29, x0, 22
i_193:
	sll x17, x15, x29
i_194:
	sw x25, 420(x2)
i_195:
	sb x27, -164(x2)
i_196:
	nop
i_197:
	addi x9, x0, 1980
i_198:
	addi x12, x0, 1983
i_199:
	lh x22, -144(x2)
i_200:
	or x11, x22, x18
i_201:
	addi x5, x0, 1
i_202:
	sll x8, x22, x5
i_203:
	bge x13, x8, i_207
i_204:
	lh x20, -36(x2)
i_205:
	sh x30, -124(x2)
i_206:
	lb x8, 65(x2)
i_207:
	andi x5, x21, 878
i_208:
	sw x7, -4(x2)
i_209:
	addi x9 , x9 , 1
	bgeu x12, x9, i_199
i_210:
	xor x9, x25, x21
i_211:
	lbu x6, -43(x2)
i_212:
	addi x20, x0, 29
i_213:
	sll x20, x14, x20
i_214:
	xori x12, x3, 154
i_215:
	addi x13, x0, 25
i_216:
	sra x4, x11, x13
i_217:
	andi x11, x19, 1393
i_218:
	lw x11, -364(x2)
i_219:
	lb x12, -418(x2)
i_220:
	andi x9, x9, -1761
i_221:
	addi x24, x0, -1940
i_222:
	addi x23, x0, -1936
i_223:
	lh x26, -84(x2)
i_224:
	nop
i_225:
	bltu x22, x8, i_226
i_226:
	nop
i_227:
	addi x10, x0, 2
i_228:
	sll x10, x30, x10
i_229:
	lhu x13, 140(x2)
i_230:
	addi x24 , x24 , 1
	bge x23, x24, i_223
i_231:
	auipc x20, 883954
i_232:
	or x14, x20, x2
i_233:
	addi x27, x0, 2
i_234:
	sra x29, x11, x27
i_235:
	addi x3, x0, 27
i_236:
	sra x14, x2, x3
i_237:
	blt x17, x18, i_243
i_238:
	addi x1, x0, 21
i_239:
	srl x13, x31, x1
i_240:
	lhu x16, 96(x2)
i_241:
	lhu x3, -324(x2)
i_242:
	lbu x26, -124(x2)
i_243:
	lh x23, -32(x2)
i_244:
	slli x20, x18, 1
i_245:
	lw x21, 316(x2)
i_246:
	bltu x16, x23, i_254
i_247:
	bne x8, x21, i_255
i_248:
	bltu x20, x16, i_253
i_249:
	lw x20, 328(x2)
i_250:
	slli x14, x2, 4
i_251:
	lw x3, 356(x2)
i_252:
	lhu x3, 244(x2)
i_253:
	sb x16, -149(x2)
i_254:
	lhu x9, -48(x2)
i_255:
	srai x6, x5, 2
i_256:
	bltu x3, x22, i_264
i_257:
	lbu x30, 356(x2)
i_258:
	sh x18, -402(x2)
i_259:
	lw x6, 84(x2)
i_260:
	or x22, x29, x12
i_261:
	slli x22, x5, 4
i_262:
	xor x18, x22, x5
i_263:
	nop
i_264:
	slt x16, x19, x22
i_265:
	lw x22, -32(x2)
i_266:
	addi x17, x0, -1946
i_267:
	addi x23, x0, -1944
i_268:
	addi x17 , x17 , 1
	bne x17, x23, i_268
i_269:
	lhu x14, -272(x2)
i_270:
	bge x24, x8, i_274
i_271:
	sh x4, 330(x2)
i_272:
	lh x28, 110(x2)
i_273:
	lh x17, 266(x2)
i_274:
	nop
i_275:
	lh x3, 268(x2)
i_276:
	addi x14, x0, 1990
i_277:
	addi x1, x0, 1992
i_278:
	ori x18, x30, 443
i_279:
	addi x14 , x14 , 1
	bltu x14, x1, i_278
i_280:
	lh x5, 310(x2)
i_281:
	and x1, x6, x22
i_282:
	sw x3, -180(x2)
i_283:
	sb x10, 164(x2)
i_284:
	sb x6, -37(x2)
i_285:
	lh x13, 388(x2)
i_286:
	sh x28, -198(x2)
i_287:
	sb x14, 84(x2)
i_288:
	lh x14, 214(x2)
i_289:
	addi x26, x0, -1976
i_290:
	addi x5, x0, -1973
i_291:
	beq x9, x19, i_299
i_292:
	addi x23, x0, 31
i_293:
	sll x21, x7, x23
i_294:
	addi x26 , x26 , 1
	bltu x26, x5, i_291
i_295:
	lh x5, -92(x2)
i_296:
	lui x19, 915479
i_297:
	slti x17, x31, -1362
i_298:
	lw x5, 128(x2)
i_299:
	lb x22, -422(x2)
i_300:
	lhu x5, 146(x2)
i_301:
	sb x4, 443(x2)
i_302:
	addi x14, x0, 1889
i_303:
	addi x15, x0, 1891
i_304:
	lbu x21, -212(x2)
i_305:
	lw x5, 456(x2)
i_306:
	lh x9, -308(x2)
i_307:
	lhu x9, 286(x2)
i_308:
	nop
i_309:
	lhu x6, 166(x2)
i_310:
	lw x29, -36(x2)
i_311:
	slli x11, x5, 4
i_312:
	sw x23, 368(x2)
i_313:
	lw x9, 244(x2)
i_314:
	addi x14 , x14 , 1
	bne x14, x15, i_304
i_315:
	lbu x23, 428(x2)
i_316:
	srai x19, x19, 4
i_317:
	sub x10, x16, x23
i_318:
	auipc x19, 181597
i_319:
	sb x10, -180(x2)
i_320:
	lbu x26, -92(x2)
i_321:
	slt x23, x18, x22
i_322:
	xor x18, x13, x6
i_323:
	slt x6, x2, x30
i_324:
	add x4, x18, x12
i_325:
	bltu x19, x2, i_334
i_326:
	addi x6, x0, 30
i_327:
	sra x18, x16, x6
i_328:
	sh x28, 230(x2)
i_329:
	sw x6, -448(x2)
i_330:
	xor x6, x21, x4
i_331:
	slli x17, x17, 1
i_332:
	srai x28, x19, 2
i_333:
	blt x19, x26, i_340
i_334:
	lb x8, -387(x2)
i_335:
	sw x26, -392(x2)
i_336:
	lw x29, -280(x2)
i_337:
	sb x4, 412(x2)
i_338:
	bne x28, x6, i_347
i_339:
	beq x27, x26, i_341
i_340:
	xor x17, x21, x15
i_341:
	lbu x9, 406(x2)
i_342:
	sw x8, 280(x2)
i_343:
	sh x17, -160(x2)
i_344:
	add x8, x5, x9
i_345:
	and x6, x28, x21
i_346:
	srli x9, x27, 1
i_347:
	addi x11, x0, 21
i_348:
	sll x1, x17, x11
i_349:
	sltu x6, x9, x17
i_350:
	slt x22, x6, x6
i_351:
	xori x17, x24, -161
i_352:
	or x24, x13, x14
i_353:
	beq x24, x2, i_356
i_354:
	beq x11, x21, i_355
i_355:
	lbu x17, 384(x2)
i_356:
	lh x11, -134(x2)
i_357:
	add x17, x3, x24
i_358:
	lhu x27, 6(x2)
i_359:
	lw x8, -200(x2)
i_360:
	sw x23, 300(x2)
i_361:
	sb x15, -316(x2)
i_362:
	sltiu x30, x19, -559
i_363:
	lh x28, -102(x2)
i_364:
	xor x15, x13, x21
i_365:
	sltiu x8, x4, -1358
i_366:
	lb x27, -476(x2)
i_367:
	lb x17, 366(x2)
i_368:
	sw x15, -140(x2)
i_369:
	xor x3, x18, x5
i_370:
	lhu x27, 466(x2)
i_371:
	lw x15, -128(x2)
i_372:
	sh x7, -326(x2)
i_373:
	lb x4, -25(x2)
i_374:
	sh x1, -314(x2)
i_375:
	addi x3, x0, 11
i_376:
	sll x14, x16, x3
i_377:
	lbu x15, 387(x2)
i_378:
	sb x14, 105(x2)
i_379:
	bgeu x10, x23, i_388
i_380:
	slt x10, x15, x2
i_381:
	andi x23, x25, -1443
i_382:
	bge x10, x8, i_391
i_383:
	lbu x25, 199(x2)
i_384:
	sw x24, -192(x2)
i_385:
	xor x12, x23, x10
i_386:
	srli x12, x30, 2
i_387:
	or x9, x10, x14
i_388:
	lw x24, -56(x2)
i_389:
	slti x25, x20, -312
i_390:
	lw x14, 312(x2)
i_391:
	sb x12, 119(x2)
i_392:
	lh x12, 226(x2)
i_393:
	lw x24, -68(x2)
i_394:
	addi x27, x19, 2045
i_395:
	lh x14, -454(x2)
i_396:
	sh x1, -44(x2)
i_397:
	sw x11, -248(x2)
i_398:
	nop
i_399:
	addi x6, x0, -2033
i_400:
	addi x14, x0, -2031
i_401:
	sb x24, -384(x2)
i_402:
	lbu x27, -29(x2)
i_403:
	addi x26, x0, -1859
i_404:
	addi x15, x0, -1857
i_405:
	lbu x13, -200(x2)
i_406:
	addi x26 , x26 , 1
	bgeu x15, x26, i_405
i_407:
	sb x12, 148(x2)
i_408:
	lh x24, 0(x2)
i_409:
	addi x6 , x6 , 1
	blt x6, x14, i_401
i_410:
	lb x6, -337(x2)
i_411:
	lb x14, -283(x2)
i_412:
	lbu x25, -289(x2)
i_413:
	sb x16, 330(x2)
i_414:
	srli x16, x7, 1
i_415:
	andi x25, x30, 1576
i_416:
	xori x28, x6, 127
i_417:
	bge x19, x24, i_418
i_418:
	lh x11, -440(x2)
i_419:
	lh x22, 166(x2)
i_420:
	lbu x13, 208(x2)
i_421:
	lh x9, 352(x2)
i_422:
	sb x2, -176(x2)
i_423:
	addi x22, x0, -1997
i_424:
	addi x24, x0, -1994
i_425:
	bltu x15, x31, i_434
i_426:
	srli x14, x4, 4
i_427:
	nop
i_428:
	sh x25, -196(x2)
i_429:
	lbu x16, 232(x2)
i_430:
	addi x22 , x22 , 1
	bgeu x24, x22, i_425
i_431:
	lw x20, 404(x2)
i_432:
	sh x31, 408(x2)
i_433:
	lw x9, 320(x2)
i_434:
	sw x12, 20(x2)
i_435:
	lw x10, -68(x2)
i_436:
	addi x1, x0, -1948
i_437:
	addi x9, x0, -1945
i_438:
	bge x9, x13, i_448
i_439:
	lb x10, 50(x2)
i_440:
	lbu x10, 270(x2)
i_441:
	xor x22, x17, x9
i_442:
	nop
i_443:
	lb x16, 216(x2)
i_444:
	addi x22, x0, 26
i_445:
	sra x30, x15, x22
i_446:
	addi x1 , x1 , 1
	bne x1, x9, i_438
i_447:
	blt x30, x22, i_449
i_448:
	sw x22, 152(x2)
i_449:
	slti x27, x16, -679
i_450:
	sb x1, 71(x2)
i_451:
	auipc x27, 523924
i_452:
	addi x22, x0, 1881
i_453:
	addi x11, x0, 1884
i_454:
	sb x7, -146(x2)
i_455:
	lbu x16, 290(x2)
i_456:
	beq x15, x22, i_457
i_457:
	lw x16, -224(x2)
i_458:
	bgeu x23, x4, i_466
i_459:
	sw x31, -4(x2)
i_460:
	sub x6, x22, x6
i_461:
	sw x7, -372(x2)
i_462:
	addi x22 , x22 , 1
	blt x22, x11, i_454
i_463:
	or x30, x5, x3
i_464:
	addi x8, x0, 11
i_465:
	srl x28, x5, x8
i_466:
	xor x29, x15, x15
i_467:
	xor x15, x10, x15
i_468:
	andi x26, x29, -1306
i_469:
	or x15, x20, x15
i_470:
	sw x24, 120(x2)
i_471:
	srai x16, x21, 2
i_472:
	bgeu x26, x11, i_475
i_473:
	bgeu x17, x12, i_475
i_474:
	lb x8, -107(x2)
i_475:
	bltu x2, x9, i_476
i_476:
	sb x16, -471(x2)
i_477:
	sb x14, -434(x2)
i_478:
	slli x16, x9, 2
i_479:
	auipc x6, 967180
i_480:
	lb x9, -307(x2)
i_481:
	lb x1, -237(x2)
i_482:
	lb x8, -207(x2)
i_483:
	lh x6, 258(x2)
i_484:
	and x9, x6, x21
i_485:
	and x21, x3, x15
i_486:
	lw x28, 472(x2)
i_487:
	lb x19, 327(x2)
i_488:
	lbu x26, 257(x2)
i_489:
	slti x28, x10, 1308
i_490:
	addi x1, x0, 2024
i_491:
	addi x24, x0, 2026
i_492:
	sltu x11, x2, x19
i_493:
	sw x7, -128(x2)
i_494:
	sw x31, 112(x2)
i_495:
	addi x1 , x1 , 1
	blt x1, x24, i_492
i_496:
	slt x30, x14, x15
i_497:
	lh x13, 68(x2)
i_498:
	lw x28, 4(x2)
i_499:
	srli x26, x7, 1
i_500:
	addi x3, x0, 27
i_501:
	srl x3, x1, x3
i_502:
	lbu x20, 168(x2)
i_503:
	lh x11, -380(x2)
i_504:
	lbu x26, -220(x2)
i_505:
	sh x14, -246(x2)
i_506:
	and x6, x24, x24
i_507:
	ori x3, x26, -1824
i_508:
	slli x14, x14, 1
i_509:
	bge x1, x11, i_514
i_510:
	sb x23, 487(x2)
i_511:
	lhu x1, 460(x2)
i_512:
	sb x1, 243(x2)
i_513:
	sh x20, -274(x2)
i_514:
	lbu x3, 441(x2)
i_515:
	and x28, x2, x27
i_516:
	addi x8, x0, 5
i_517:
	sll x28, x1, x8
i_518:
	srai x11, x25, 4
i_519:
	blt x31, x12, i_520
i_520:
	and x15, x7, x15
i_521:
	andi x4, x7, 1086
i_522:
	lhu x12, -468(x2)
i_523:
	auipc x11, 247286
i_524:
	sh x12, -216(x2)
i_525:
	sh x18, 466(x2)
i_526:
	add x11, x11, x1
i_527:
	sb x15, 125(x2)
i_528:
	slli x19, x24, 3
i_529:
	slli x28, x6, 3
i_530:
	addi x11, x0, 2005
i_531:
	addi x1, x0, 2008
i_532:
	xor x19, x26, x13
i_533:
	bge x27, x15, i_535
i_534:
	lw x19, 264(x2)
i_535:
	lh x15, 454(x2)
i_536:
	slli x18, x30, 2
i_537:
	lw x21, -228(x2)
i_538:
	addi x11 , x11 , 1
	blt x11, x1, i_532
i_539:
	slt x14, x5, x16
i_540:
	addi x24, x0, 20
i_541:
	srl x24, x10, x24
i_542:
	sh x4, -178(x2)
i_543:
	bltu x15, x30, i_551
i_544:
	or x30, x27, x7
i_545:
	bltu x8, x8, i_554
i_546:
	sltu x15, x17, x25
i_547:
	lbu x21, 104(x2)
i_548:
	bltu x25, x17, i_557
i_549:
	lh x10, 98(x2)
i_550:
	lbu x3, 110(x2)
i_551:
	addi x21, x21, 378
i_552:
	sw x13, 268(x2)
i_553:
	srai x12, x24, 1
i_554:
	sw x10, 192(x2)
i_555:
	lh x21, -92(x2)
i_556:
	lb x24, -66(x2)
i_557:
	lh x21, -272(x2)
i_558:
	sb x13, 275(x2)
i_559:
	lb x12, -486(x2)
i_560:
	bne x2, x6, i_568
i_561:
	addi x6, x10, 1395
i_562:
	nop
i_563:
	lbu x13, -212(x2)
i_564:
	xor x20, x20, x1
i_565:
	lhu x4, -124(x2)
i_566:
	lw x1, -32(x2)
i_567:
	sb x20, 116(x2)
i_568:
	sltiu x20, x26, -257
i_569:
	lui x8, 875450
i_570:
	addi x10, x0, 1954
i_571:
	addi x22, x0, 1956
i_572:
	addi x10 , x10 , 1
	bge x22, x10, i_572
i_573:
	slli x10, x24, 4
i_574:
	lbu x20, 280(x2)
i_575:
	lbu x24, 173(x2)
i_576:
	addi x15, x0, 29
i_577:
	sra x9, x3, x15
i_578:
	sub x9, x4, x6
i_579:
	slli x1, x21, 3
i_580:
	bltu x28, x9, i_589
i_581:
	lbu x29, -425(x2)
i_582:
	bge x30, x8, i_585
i_583:
	bltu x9, x28, i_584
i_584:
	lb x1, 33(x2)
i_585:
	auipc x10, 261237
i_586:
	sb x9, 300(x2)
i_587:
	sltiu x1, x10, 555
i_588:
	blt x18, x15, i_590
i_589:
	sh x14, -58(x2)
i_590:
	or x19, x22, x10
i_591:
	sw x12, 324(x2)
i_592:
	addi x10, x0, -2024
i_593:
	addi x29, x0, -2022
i_594:
	sub x5, x19, x5
i_595:
	lw x5, 228(x2)
i_596:
	lbu x18, -310(x2)
i_597:
	lw x28, -60(x2)
i_598:
	lw x21, -192(x2)
i_599:
	and x5, x26, x4
i_600:
	addi x10 , x10 , 1
	bgeu x29, x10, i_594
i_601:
	and x26, x5, x8
i_602:
	slli x22, x19, 4
i_603:
	lh x26, -320(x2)
i_604:
	addi x16, x0, 30
i_605:
	sra x12, x16, x16
i_606:
	addi x5, x27, -342
i_607:
	sw x12, 140(x2)
i_608:
	beq x12, x7, i_609
i_609:
	sw x25, -372(x2)
i_610:
	sltu x29, x2, x28
i_611:
	lbu x28, 347(x2)
i_612:
	sb x24, 414(x2)
i_613:
	lh x6, 86(x2)
i_614:
	lw x16, -212(x2)
i_615:
	sw x6, 216(x2)
i_616:
	ori x13, x4, -1255
i_617:
	sub x6, x9, x1
i_618:
	lh x28, -206(x2)
i_619:
	addi x20, x0, 11
i_620:
	srl x23, x14, x20
i_621:
	lw x14, -100(x2)
i_622:
	lhu x19, 158(x2)
i_623:
	lbu x20, -401(x2)
i_624:
	bne x13, x1, i_626
i_625:
	sw x5, 184(x2)
i_626:
	lh x24, 248(x2)
i_627:
	lhu x18, 426(x2)
i_628:
	sh x3, 12(x2)
i_629:
	sb x16, -426(x2)
i_630:
	sub x24, x9, x10
i_631:
	sh x20, -208(x2)
i_632:
	auipc x12, 383906
i_633:
	addi x24, x0, 2031
i_634:
	addi x18, x0, 2033
i_635:
	lh x20, 354(x2)
i_636:
	lb x25, -187(x2)
i_637:
	addi x8, x0, -1875
i_638:
	addi x11, x0, -1873
i_639:
	add x25, x26, x21
i_640:
	addi x8 , x8 , 1
	blt x8, x11, i_639
i_641:
	lw x3, -24(x2)
i_642:
	addi x24 , x24 , 1
	bgeu x18, x24, i_635
i_643:
	slli x4, x7, 4
i_644:
	sltu x17, x12, x13
i_645:
	addi x23, x0, 21
i_646:
	srl x9, x11, x23
i_647:
	and x29, x20, x29
i_648:
	ori x20, x10, 865
i_649:
	bge x13, x11, i_658
i_650:
	slli x29, x29, 4
i_651:
	slt x23, x29, x3
i_652:
	beq x29, x14, i_654
i_653:
	sw x30, 296(x2)
i_654:
	lhu x29, -118(x2)
i_655:
	sw x27, 32(x2)
i_656:
	blt x14, x13, i_659
i_657:
	bge x14, x23, i_658
i_658:
	bne x22, x23, i_666
i_659:
	bltu x29, x25, i_668
i_660:
	addi x20, x0, 26
i_661:
	sll x23, x24, x20
i_662:
	lhu x25, -242(x2)
i_663:
	slli x10, x21, 2
i_664:
	slli x26, x20, 3
i_665:
	lbu x8, -3(x2)
i_666:
	sub x22, x8, x4
i_667:
	lbu x27, 65(x2)
i_668:
	srli x12, x2, 1
i_669:
	nop
i_670:
	sh x20, 268(x2)
i_671:
	addi x17, x0, 1838
i_672:
	addi x24, x0, 1842
i_673:
	sb x5, 63(x2)
i_674:
	sw x9, 456(x2)
i_675:
	addi x17 , x17 , 1
	bne  x24, x17, i_673
i_676:
	beq x10, x24, i_680
i_677:
	lbu x5, 13(x2)
i_678:
	sltiu x28, x12, 1352
i_679:
	lb x13, 30(x2)
i_680:
	xor x17, x6, x26
i_681:
	sh x7, -84(x2)
i_682:
	beq x6, x25, i_689
i_683:
	lbu x12, 379(x2)
i_684:
	srli x4, x20, 3
i_685:
	lbu x26, 232(x2)
i_686:
	slt x28, x26, x10
i_687:
	lw x28, -24(x2)
i_688:
	addi x26, x0, 17
i_689:
	sra x15, x26, x26
i_690:
	lbu x19, -419(x2)
i_691:
	sb x18, 317(x2)
i_692:
	blt x20, x22, i_699
i_693:
	lh x22, 174(x2)
i_694:
	sb x19, 224(x2)
i_695:
	sb x2, 262(x2)
i_696:
	nop
i_697:
	and x5, x15, x1
i_698:
	nop
i_699:
	lw x19, -472(x2)
i_700:
	sw x4, -68(x2)
i_701:
	addi x20, x0, 1891
i_702:
	addi x22, x0, 1894
i_703:
	bne x31, x19, i_707
i_704:
	lh x19, -316(x2)
i_705:
	or x8, x27, x5
i_706:
	lw x8, 0(x2)
i_707:
	addi x6, x0, 6
i_708:
	srl x5, x19, x6
i_709:
	slli x5, x25, 1
i_710:
	addi x20 , x20 , 1
	bgeu x22, x20, i_703
i_711:
	addi x19, x0, 3
i_712:
	srl x27, x1, x19
i_713:
	bgeu x17, x7, i_722
i_714:
	sh x26, 464(x2)
i_715:
	add x28, x19, x6
i_716:
	beq x26, x30, i_721
i_717:
	sw x9, -444(x2)
i_718:
	lh x9, 260(x2)
i_719:
	bne x19, x6, i_721
i_720:
	xori x20, x29, -1020
i_721:
	sub x30, x16, x9
i_722:
	lh x19, -272(x2)
i_723:
	slt x29, x7, x19
i_724:
	sw x19, -328(x2)
i_725:
	sw x19, -332(x2)
i_726:
	sub x9, x14, x15
i_727:
	sw x14, -408(x2)
i_728:
	sw x17, 384(x2)
i_729:
	addi x15, x0, 1918
i_730:
	addi x21, x0, 1921
i_731:
	addi x17, x0, 19
i_732:
	srl x14, x28, x17
i_733:
	sb x17, -240(x2)
i_734:
	bne x14, x3, i_735
i_735:
	sw x28, -196(x2)
i_736:
	lw x17, 108(x2)
i_737:
	lhu x23, -160(x2)
i_738:
	lh x11, 172(x2)
i_739:
	sh x31, 2(x2)
i_740:
	addi x15 , x15 , 1
	bgeu x21, x15, i_731
i_741:
	srli x28, x17, 1
i_742:
	lb x28, 333(x2)
i_743:
	sb x11, -285(x2)
i_744:
	addi x11, x0, -1852
i_745:
	addi x18, x0, -1849
i_746:
	ori x26, x21, -1926
i_747:
	or x20, x5, x29
i_748:
	nop
i_749:
	lb x22, -478(x2)
i_750:
	xori x12, x25, -929
i_751:
	sb x20, 380(x2)
i_752:
	lh x25, 90(x2)
i_753:
	sw x12, 332(x2)
i_754:
	addi x16, x0, 19
i_755:
	srl x20, x25, x16
i_756:
	sb x10, -240(x2)
i_757:
	addi x11 , x11 , 1
	bne  x18, x11, i_746
i_758:
	xor x16, x28, x2
i_759:
	bne x14, x13, i_769
i_760:
	sh x22, 330(x2)
i_761:
	sh x25, -186(x2)
i_762:
	beq x26, x27, i_769
i_763:
	lb x22, -370(x2)
i_764:
	lb x29, -173(x2)
i_765:
	slt x26, x20, x25
i_766:
	andi x30, x29, -985
i_767:
	sltu x5, x21, x13
i_768:
	lw x17, 252(x2)
i_769:
	or x17, x12, x8
i_770:
	lw x19, 376(x2)
i_771:
	lui x16, 557220
i_772:
	lb x6, -374(x2)
i_773:
	addi x25, x0, 2
i_774:
	sra x25, x28, x25
i_775:
	addi x16, x22, -1424
i_776:
	sb x15, 451(x2)
i_777:
	addi x16, x0, 12
i_778:
	srl x27, x19, x16
i_779:
	addi x19, x0, 19
i_780:
	sll x30, x12, x19
i_781:
	beq x9, x16, i_785
i_782:
	bgeu x24, x18, i_790
i_783:
	srai x24, x2, 4
i_784:
	lw x18, -256(x2)
i_785:
	lh x26, 104(x2)
i_786:
	beq x21, x23, i_792
i_787:
	xor x18, x25, x20
i_788:
	slt x9, x12, x26
i_789:
	sb x30, 284(x2)
i_790:
	sw x19, -72(x2)
i_791:
	addi x30, x0, 21
i_792:
	srl x8, x28, x30
i_793:
	xori x19, x12, -1625
i_794:
	sw x28, -4(x2)
i_795:
	srai x28, x2, 1
i_796:
	lhu x12, -430(x2)
i_797:
	lh x8, -482(x2)
i_798:
	sltiu x10, x10, -657
i_799:
	addi x8, x0, 10
i_800:
	sll x18, x21, x8
i_801:
	slti x3, x22, -262
i_802:
	addi x22, x0, 30
i_803:
	sra x28, x28, x22
i_804:
	slt x1, x10, x20
i_805:
	srli x17, x25, 1
i_806:
	sh x28, 184(x2)
i_807:
	lhu x17, 382(x2)
i_808:
	sw x15, 376(x2)
i_809:
	slt x17, x28, x28
i_810:
	sb x13, -181(x2)
i_811:
	sw x1, 16(x2)
i_812:
	auipc x1, 812739
i_813:
	lbu x23, 436(x2)
i_814:
	xori x18, x1, -1900
i_815:
	addi x14, x10, -91
i_816:
	lui x18, 582444
i_817:
	lui x23, 411289
i_818:
	sh x15, 206(x2)
i_819:
	lbu x17, -80(x2)
i_820:
	bltu x14, x18, i_821
i_821:
	slt x14, x21, x12
i_822:
	lb x10, 113(x2)
i_823:
	sh x10, -318(x2)
i_824:
	lw x3, -460(x2)
i_825:
	auipc x28, 989333
i_826:
	lhu x6, 276(x2)
i_827:
	srai x13, x12, 3
i_828:
	slli x10, x8, 2
i_829:
	sw x26, -236(x2)
i_830:
	xori x8, x9, -1417
i_831:
	addi x11, x8, -1053
i_832:
	beq x13, x5, i_837
i_833:
	ori x5, x26, -1628
i_834:
	lb x11, -109(x2)
i_835:
	lb x17, -184(x2)
i_836:
	bltu x19, x26, i_842
i_837:
	lhu x1, 294(x2)
i_838:
	lhu x11, -378(x2)
i_839:
	sltu x16, x16, x7
i_840:
	slli x21, x7, 3
i_841:
	lb x29, 15(x2)
i_842:
	lh x11, 142(x2)
i_843:
	sltu x18, x3, x15
i_844:
	lb x23, -4(x2)
i_845:
	lhu x8, -78(x2)
i_846:
	xori x18, x18, 1635
i_847:
	slt x21, x8, x7
i_848:
	slt x28, x7, x29
i_849:
	sh x22, -164(x2)
i_850:
	lb x17, -447(x2)
i_851:
	lb x22, 160(x2)
i_852:
	lw x27, -56(x2)
i_853:
	beq x8, x28, i_856
i_854:
	lh x29, 318(x2)
i_855:
	lb x13, -119(x2)
i_856:
	lbu x19, 261(x2)
i_857:
	lh x11, 326(x2)
i_858:
	lhu x12, 210(x2)
i_859:
	bge x23, x24, i_867
i_860:
	slli x22, x17, 2
i_861:
	sb x3, 194(x2)
i_862:
	lui x5, 542440
i_863:
	sw x15, 304(x2)
i_864:
	bgeu x27, x10, i_866
i_865:
	lbu x22, 51(x2)
i_866:
	sb x5, 60(x2)
i_867:
	lw x10, 460(x2)
i_868:
	lw x17, -24(x2)
i_869:
	addi x18, x0, 4
i_870:
	sra x5, x28, x18
i_871:
	sub x29, x10, x25
i_872:
	sw x7, 280(x2)
i_873:
	lw x25, 44(x2)
i_874:
	srai x10, x4, 1
i_875:
	auipc x19, 936916
i_876:
	lw x11, 92(x2)
i_877:
	lhu x22, -266(x2)
i_878:
	lhu x4, 466(x2)
i_879:
	bgeu x25, x3, i_885
i_880:
	sh x12, -218(x2)
i_881:
	beq x29, x10, i_882
i_882:
	lb x25, 193(x2)
i_883:
	lhu x25, -86(x2)
i_884:
	slti x25, x16, 1493
i_885:
	ori x19, x2, -1400
i_886:
	srli x20, x20, 2
i_887:
	lw x16, 68(x2)
i_888:
	lw x1, 392(x2)
i_889:
	blt x16, x29, i_893
i_890:
	slli x12, x10, 2
i_891:
	lbu x22, 376(x2)
i_892:
	srli x4, x15, 2
i_893:
	ori x12, x15, -1920
i_894:
	blt x17, x13, i_904
i_895:
	lb x12, -458(x2)
i_896:
	lb x17, -320(x2)
i_897:
	ori x29, x8, -1622
i_898:
	andi x29, x27, -1786
i_899:
	lh x24, 160(x2)
i_900:
	slti x11, x4, 3
i_901:
	lbu x13, 343(x2)
i_902:
	sltiu x14, x4, 220
i_903:
	slti x11, x12, 2037
i_904:
	lw x12, -436(x2)
i_905:
	lhu x15, 260(x2)
i_906:
	lw x15, 36(x2)
i_907:
	sh x2, -340(x2)
i_908:
	lb x18, 91(x2)
i_909:
	srai x19, x3, 2
i_910:
	lh x19, -44(x2)
i_911:
	lb x10, 12(x2)
i_912:
	bgeu x6, x5, i_921
i_913:
	lh x14, 478(x2)
i_914:
	sh x20, -176(x2)
i_915:
	sltu x24, x19, x22
i_916:
	add x3, x5, x5
i_917:
	blt x26, x7, i_925
i_918:
	lhu x8, 96(x2)
i_919:
	blt x17, x10, i_923
i_920:
	bltu x11, x1, i_928
i_921:
	slti x14, x8, -346
i_922:
	lw x27, -96(x2)
i_923:
	slti x20, x9, 890
i_924:
	lh x24, 156(x2)
i_925:
	sw x5, 264(x2)
i_926:
	blt x11, x18, i_936
i_927:
	slli x9, x24, 4
i_928:
	lh x11, -444(x2)
i_929:
	sb x20, -55(x2)
i_930:
	sw x12, -228(x2)
i_931:
	addi x1, x0, 3
i_932:
	sll x23, x9, x1
i_933:
	xori x12, x28, 416
i_934:
	xor x9, x9, x3
i_935:
	andi x9, x15, 1018
i_936:
	lhu x30, 468(x2)
i_937:
	lbu x30, -264(x2)
i_938:
	bgeu x12, x6, i_948
i_939:
	lbu x9, -11(x2)
i_940:
	addi x28, x0, 24
i_941:
	sll x3, x6, x28
i_942:
	addi x6, x0, 16
i_943:
	sra x16, x18, x6
i_944:
	sh x6, -456(x2)
i_945:
	srai x8, x20, 2
i_946:
	lhu x20, 326(x2)
i_947:
	sh x28, 478(x2)
i_948:
	sb x2, 177(x2)
i_949:
	lhu x9, 146(x2)
i_950:
	sh x8, -460(x2)
i_951:
	bgeu x24, x13, i_955
i_952:
	lw x20, 472(x2)
i_953:
	nop
i_954:
	sh x11, 282(x2)
i_955:
	sb x11, 37(x2)
i_956:
	nop
i_957:
	addi x11, x0, 1968
i_958:
	addi x17, x0, 1972
i_959:
	lw x28, -420(x2)
i_960:
	lui x28, 1011605
i_961:
	lbu x22, 228(x2)
i_962:
	sh x14, -306(x2)
i_963:
	addi x11 , x11 , 1
	bne x11, x17, i_959
i_964:
	add x28, x15, x10
i_965:
	xori x10, x15, -153
i_966:
	bne x25, x10, i_975
i_967:
	sw x23, 460(x2)
i_968:
	bgeu x28, x10, i_972
i_969:
	sw x9, 220(x2)
i_970:
	add x28, x10, x28
i_971:
	sh x28, -300(x2)
i_972:
	xori x28, x5, -1758
i_973:
	lh x15, -400(x2)
i_974:
	slt x17, x28, x15
i_975:
	or x15, x5, x9
i_976:
	lb x15, 6(x2)
i_977:
	addi x5, x0, 1858
i_978:
	addi x9, x0, 1860
i_979:
	sb x15, 154(x2)
i_980:
	sh x31, 12(x2)
i_981:
	srli x8, x10, 2
i_982:
	beq x11, x5, i_988
i_983:
	auipc x17, 1042478
i_984:
	sltiu x20, x5, 425
i_985:
	addi x5 , x5 , 1
	bge x9, x5, i_979
i_986:
	addi x25, x0, 14
i_987:
	sra x25, x1, x25
i_988:
	bgeu x28, x24, i_998
i_989:
	lbu x21, 134(x2)
i_990:
	blt x6, x2, i_993
i_991:
	sw x20, -168(x2)
i_992:
	addi x30, x0, 3
i_993:
	sra x15, x26, x30
i_994:
	or x8, x25, x15
i_995:
	addi x9, x16, -63
i_996:
	bgeu x11, x19, i_999
i_997:
	sw x15, -88(x2)
i_998:
	addi x26, x0, 28
i_999:
	srl x4, x2, x26
i_1000:
	nop
i_1001:
	nop
i_1002:
	nop
i_1003:
	nop
i_1004:
	nop
i_1005:
	nop
i_1006:
	nop
i_1007:
	nop
i_1008:
	nop
i_1009:
	nop

 j tohost_exit

 tohost_exit:
  addi x2, x0,0x1
  slli x2, x2, 0x1f # 0x80000000
  lui x3, 0x10  # 
  add x2,x2,x3
  li a4, 1
  sw a4, %lo(tohost)(x2) # store 1 to tohost.
  loop: j loop
  
.size	main, .-main

.data
.align 4
.globl data
data: 

	.word 0xd25a9cfc
	.word 0x113b3fdd
	.word 0xdd949b46
	.word 0xb26cc11e
	.word 0xe209d5e1
	.word 0x2bcf36e0
	.word 0xb9922547
	.word 0xd16c6b24
	.word 0x83022980
	.word 0x55668ef8
	.word 0x8dcc54d4
	.word 0x994d1bf9
	.word 0xeeefd0c5
	.word 0xbc13f5f8
	.word 0x336548eb
	.word 0xaedda901
	.word 0x4c07ebf5
	.word 0xd08f11a0
	.word 0x17566007
	.word 0xfa0da3d0
	.word 0x5da32a95
	.word 0xb9622aba
	.word 0x4bbf305b
	.word 0xdb478d67
	.word 0xbbd8de10
	.word 0x1db743a7
	.word 0xc7ec5d3e
	.word 0x43554f6e
	.word 0x44b1f898
	.word 0xfcf6505c
	.word 0x8d29a2b2
	.word 0xace7da1e
	.word 0xefdd73cd
	.word 0xd3c295b9
	.word 0x6b4ef130
	.word 0x3a6b2183
	.word 0x2a0787e9
	.word 0x6cc05be7
	.word 0xcacb8ed1
	.word 0x7670c816
	.word 0xe101eecd
	.word 0x1397c61b
	.word 0xa05d5d2f
	.word 0x811ceee6
	.word 0xbcebd172
	.word 0x97b9d5db
	.word 0xc7a56de7
	.word 0xe5713179
	.word 0x449c60fd
	.word 0xdfb12cd4
	.word 0x5e243142
	.word 0xa70e391
	.word 0x32dae3c4
	.word 0xee9c03ce
	.word 0xb371752c
	.word 0x38eaef1b
	.word 0xd161b68
	.word 0xe9618222
	.word 0x49c423d7
	.word 0x96e6e232
	.word 0x23599a43
	.word 0xa7e625b5
	.word 0xaae56b65
	.word 0x1d0288a4
	.word 0x816a32d7
	.word 0x57574df6
	.word 0xe674939
	.word 0xc97ab441
	.word 0x45a475e8
	.word 0x6ab7aa40
	.word 0x9b35644b
	.word 0xe3f250
	.word 0xdfa300ef
	.word 0x2eaf16a
	.word 0xa951e07e
	.word 0x6c0edaf1
	.word 0xe0ea6023
	.word 0x1d2b581e
	.word 0x4a373edc
	.word 0x6f95b9e0
	.word 0x91a411cb
	.word 0xf76cb6bd
	.word 0x22b71a03
	.word 0x1a6f9a6
	.word 0xbeccdae9
	.word 0xfed1090e
	.word 0x712c613d
	.word 0xbcb94243
	.word 0xf9a2f83e
	.word 0x6a7e1692
	.word 0x2d47aec4
	.word 0x3a15fe0e
	.word 0x3e0d800f
	.word 0xbee5796c
	.word 0xd0db0b2
	.word 0x4c435aa6
	.word 0xe8721b5f
	.word 0x2c7d5507
	.word 0x5d06fdbe
	.word 0x77e81ea
	.word 0x4b786225
	.word 0x6eb18e19
	.word 0x4f018113
	.word 0x799cd3a2
	.word 0x95fc33ef
	.word 0x66086a18
	.word 0xf4d219c5
	.word 0x97594a0a
	.word 0xfc84694d
	.word 0xbc18b46b
	.word 0xf46032c2
	.word 0x7b587adf
	.word 0x5bff4636
	.word 0x9faf843e
	.word 0x85ce2dff
	.word 0xf9a7a745
	.word 0xad265249
	.word 0x6624e5bf
	.word 0x822608f9
	.word 0x5e9a3109
	.word 0x57436eb
	.word 0x1d2efe63
	.word 0xca8d8369
	.word 0x812d1291
	.word 0xdc9160b9
	.word 0xa7afe4e8
	.word 0x3baba8c5
	.word 0xaf3a619d
	.word 0x27dcafca
	.word 0x81beb534
	.word 0xe1b07529
	.word 0x974294a8
	.word 0xa6715ccc
	.word 0x8f94dd15
	.word 0x984d4e8a
	.word 0x2b88afda
	.word 0x62d14ff7
	.word 0xa3065a0e
	.word 0x3b0f31b0
	.word 0x2b20636
	.word 0x2557f250
	.word 0xac423122
	.word 0xf8ec697a
	.word 0x87a2554c
	.word 0x8b53811c
	.word 0xe5f97a95
	.word 0x9b69d705
	.word 0xa80f766f
	.word 0xd3baa677
	.word 0x7e36d6a0
	.word 0xc5fd5065
	.word 0xec7fa341
	.word 0xf0940ac0
	.word 0x48e4664b
	.word 0xeaeb52d2
	.word 0x4ea1f8cc
	.word 0x9ea2f6ed
	.word 0xa5fc224f
	.word 0x18bbce92
	.word 0xd617d857
	.word 0x1fda5350
	.word 0x81e5d0c5
	.word 0x56a24782
	.word 0xc320205d
	.word 0x2e64475f
	.word 0xdec9a463
	.word 0x4cffc090
	.word 0xec219c65
	.word 0x5b8579ae
	.word 0x2f301121
	.word 0xe58e1bb8
	.word 0x3f45519e
	.word 0xada4c728
	.word 0x91bbcc8c
	.word 0x4fa057e0
	.word 0x433687b7
	.word 0x8a14d23a
	.word 0x41328547
	.word 0x2a619d22
	.word 0xe9d737e1
	.word 0x3c9eff72
	.word 0xd4a2b1d
	.word 0x73558322
	.word 0x4ba28781
	.word 0xecf4e5f8
	.word 0x3b08a53a
	.word 0x5ffdf676
	.word 0xa4ae78a2
	.word 0x77b3ef0e
	.word 0x5539e001
	.word 0x1a3263f7
	.word 0x760f5110
	.word 0x3ce39ed5
	.word 0xf94ebedc
	.word 0x6827da0e
	.word 0xf695cc41
	.word 0x3d957351
	.word 0x97aba4c4
	.word 0xd13a8dab
	.word 0xb5ee09fb
	.word 0xd8e4fde2
	.word 0xfd61e997
	.word 0xf9e435af
	.word 0xd517ac2a
	.word 0xd87b7014
	.word 0xc25f5e25
	.word 0xb4550e23
	.word 0xc28c7164
	.word 0x31f58464
	.word 0xedd537b3
	.word 0xc067983f
	.word 0x9eef5c6b
	.word 0x379d6eac
	.word 0xaaba9e29
	.word 0x7f67340b
	.word 0xf25dd24
	.word 0x33efed43
	.word 0x77c62156
	.word 0xbff6b6f1
	.word 0x927d18d3
	.word 0x807568e6
	.word 0x18a9ea2d
	.word 0x29c324b5
	.word 0xe753fa31
	.word 0xc06404b6
	.word 0xa0e8a022
	.word 0x9484228c
	.word 0x1f955fa2
	.word 0x6877aaf0
	.word 0x94b34cab
	.word 0xc19f23de
	.word 0x8aa548b9
	.word 0x705858b4
	.word 0x30740349
	.word 0x68eff4b
	.word 0x2f133cf1
	.word 0xecd021d3
	.word 0xeec841b3
	.word 0xe26c120a
	.word 0x1fe1289c
	.word 0xc2b9d6ab
	.word 0x2bfc7c2b
	.word 0x41d17e7f
	.word 0x1a797563
	.word 0xca1ea27b
	.word 0x16ed7775
	.word 0x9ec3add7
	.word 0x25d94fa0
	.word 0x49631f18
	.word 0xa4a491fb
	.word 0x892487c7
	.word 0xbef5ef8
	.word 0x2fd60f34
	.word 0x3e319eac
	.word 0x1f51e48a
	.word 0xdc54a149
.size	data, .-data
.section ".tohost","aw",@progbits
.align 4
.globl tohost
tohost: .word 0
.align 4
.globl fromhost
fromhost: .word 0
