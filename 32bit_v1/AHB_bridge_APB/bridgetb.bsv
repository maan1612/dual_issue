package bridgetb;

import bridge::*;
import Clocks::*;
import ahb::*;
import apb::*;
(*synthesize*)
/*---------------------------------------------------------------------------------------------------------------------------------*/
module mkbridgetb(Empty);

	ClockDividerIfc clk_divider <- mkClockDivider (3);
	Clock ahb_clk= clk_divider.fastClock;
	Clock apb_clk= clk_divider.slowClock;
	Reset ahb_rst  <- mkAsyncResetFromCR(0,ahb_clk); 
	Reset apb_rst  <- mkAsyncResetFromCR(2,apb_clk);
	Reg #(Bit#(20))rg_clock1 <-mkReg(0);
	Reg #(Bit#(20))rg_clock2 <-mkReg(0,clocked_by apb_clk,reset_by apb_rst);
	Reg #(Bit#(32))rg_data_AHB   <-mkReg(4,clocked_by ahb_clk,reset_by ahb_rst);
	Reg #(Bit#(32))rg_data_APB   <-mkReg(5,clocked_by apb_clk,reset_by apb_rst);


	Ifc_ahb obj_ahb <- mkahb(clocked_by ahb_clk,reset_by ahb_rst);
	Ifc_apb obj_apb <-mkapb(clocked_by apb_clk,reset_by apb_rst);
	Ifc_bridge obj_bridge <- mkbridge(clocked_by ahb_clk,reset_by ahb_rst,apb_clk,apb_rst);

/*---------------------------------------------------------------------------------------------------------------------------------*/

	rule rl_clock1;
		$display("clock1=%d",rg_clock1);
		//$display("------------------------------------------------------------------------------------------------------");
			rg_clock1<=rg_clock1+1;
		if(rg_clock1==60)
			$finish(0);
	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/
	
	rule rl_clock2;
		rg_clock2<=rg_clock2+1;
	$display("-------------------------------------------------------------------------------------------------------------------------");
		$display("clock2=%d",rg_clock2);
	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/

	//AHB INPUTS
	rule rl_proc_to_ahb1;

		if(rg_clock1==0)
			obj_ahb._reset_from_proc(1'd1);
	endrule


	rule rl_proc_to_ahb2;

		if(rg_clock1==1)
			obj_ahb._address_control_from_proc(32'd10,1'b0,3'b001,3'b011,4'b0011,1'b1);
	endrule


	rule rl_proc_to_ahb3;

		if(rg_clock1>=1)
			obj_ahb._data_from_proc(rg_data_AHB);
		rg_data_AHB<=rg_data_AHB+4;

	endrule


	rule rl_proc_to_ahb4;

		obj_ahb._response_from_proc_for_read_data(1'b1);
		
	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/

	//BRIDGE TO AHB

	rule rl_bridge_to_ahb1;

		obj_ahb._data_from_bridge(obj_bridge.bridge_HRDATA_(),obj_bridge.bridge_slave_valid_data());

	endrule

	rule rl_bridge_to_ahb2;

		obj_ahb._response_from_bridge(obj_bridge.bridge_HREADY_(),obj_bridge.bridge_HRESP_());

	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/
//BRIDGE INPUT

	//PROCESSOR TO BRIDGE
	rule rl_proc_to_bridge1;

		obj_bridge._reset_from_proc(); 

	endrule

/*-------------------------------------------------------------------------------------------------------------------------------------*/

	//AHB TO BRIDGE
	rule rl_ahb_to_bridge1;

		obj_bridge._address_control_from_AHB(obj_ahb.hADDR_(),obj_ahb.hWRITE_(),obj_ahb.hSIZE_(),obj_ahb.hBURST_(),obj_ahb.hPROT_(),obj_ahb.hTRANS_(),obj_ahb.hSELX_(),obj_ahb.hMASTLOCK_(),obj_ahb.hvalid_address_and_control_());

	endrule

	rule rl_ahb_to_bridge2;

		obj_bridge._data_from_AHB(obj_ahb.hWDATA_(),obj_ahb.hvalid_data_());

	endrule

rule rl_ahb_to_bridge3;
		obj_bridge._response_from_AHB(obj_ahb.ahb_to_bridge_data_ready_()); 
	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/

	//APB TO BRIDGE
	
	rule rl_apb_to_bridge1;

		obj_bridge._response_from_APB(obj_apb.pREADY_(),obj_apb.bridge_deque_());
		
	endrule

	rule rl_apb_to_bridge2;
		
		obj_bridge._data_from_APB(obj_apb.pRDATA_(),obj_apb.valid_slave_data_());

	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/

//APB INUT
	//PROCESSOR TO APB

	rule rl_proc_to_apb1;
		if(rg_clock2==0)
		obj_apb._reset_from_proc(1'd1); 
	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/	
	
	//BRIDGE TO APB

	rule rl_bridge_to_apb1;

		obj_apb._address_and_control_from_bridge(obj_bridge.bridge_HADDR_(),obj_bridge.bridge_HWRITE_(),obj_bridge.bridge_HSIZE_(), obj_bridge.bridge_HBURST_(),obj_bridge.bridge_HPROT_(),obj_bridge.bridge_HTRANS_(),obj_bridge.bridge_HMASTLOCK_(),obj_bridge.bridge_HSELX_());

	endrule

	rule rl_bridge_to_apb2;

		obj_apb._data_from_bridge(obj_bridge.bridge_HWDATA_());	

	endrule

	rule rl_bridge_to_apb3;

		obj_apb._busy_from_bridge(obj_bridge.bridge_hrdata_ready());

	endrule
/*---------------------------------------------------------------------------------------------------------------------------------*/
	
	//SLAVE TO APB

	rule rl_slave_to_apb1;
		
		if(obj_apb.pWRITE_()==0 && obj_apb.pSELX_()==1 && obj_apb.pENABLE_()==1)begin
		obj_apb._data_from_slave(rg_data_APB);
			rg_data_APB<=rg_data_APB+4;
		$display("%d=rg_data_APB",rg_data_APB);
		end

	endrule
	rule rl_slave_to_apb2;
		/*if(rg_clock2==6)
		obj_apb._response_from_slave(0,1);
		else*/

		obj_apb._response_from_slave(1,1);
	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/

	//AHB OUTPUT.

	rule rl_display_ahb_output;
		$display("AHB >>>> HADDR=%d, HWDATA=%d,HRDATA=%d, HSIZE=%d, HBURST=%d, HPROT=%d, HTRANS=%d, HMASTLOCK=%d, HSELX=%d, HWRITE=%d,HVALID=%d,HVALID_ADDRESS=%d",obj_ahb.hADDR_(),obj_ahb.hWDATA_(),obj_ahb.hRDATA_(),obj_ahb.hSIZE_(),obj_ahb.hBURST_(),obj_ahb.hPROT_(),obj_ahb.hTRANS_(),obj_ahb.hMASTLOCK_(),obj_ahb.hSELX_(),obj_ahb.hWRITE_(),obj_ahb.hvalid_data_(),obj_ahb.hvalid_address_and_control_());		
		$display(" ");
	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/

	//BRIDGE OUTPUT

	rule rl_display1_bridge_output;
		$display("Bridge >>>>> HADDR=%d, HWDATA=%d, HSIZE=%d, HBURST=%d, HPROT=%d, HTRANS=%d, HMASTLOCK=5d, HSELX=%d, HWRITE=%d,",obj_bridge.bridge_HADDR_(),obj_bridge.bridge_HWDATA_(),obj_bridge.bridge_HSIZE_(),obj_bridge.bridge_HBURST_(),obj_bridge.bridge_HPROT_(),obj_bridge.bridge_HTRANS_(),obj_bridge.bridge_HMASTLOCK_(),obj_bridge.bridge_HSELX_(),obj_bridge.bridge_HWRITE_());
		$display(" ");
	endrule

/*---------------------------------------------------------------------------------------------------------------------------------*/

	//BRIDGE OUTPUT

	rule rl_display2_bridge_output;
		$display("Bridge >>>>>HRDATA=%d,HREADY=%d,HRESP=%d",obj_bridge.bridge_HRDATA_(),obj_bridge.bridge_HREADY_(),obj_bridge.bridge_HRESP_());
		$display(" ");
	endrule


/*---------------------------------------------------------------------------------------------------------------------------------*/

	//APB OUTPUT

	rule rl_display_apb_output;
		$display("APB >>>>> PSELX=%d, PENABLE=%d, PWRITE=%d, PADDR=%d, PREADY=%d, PRDATA=%d, PWDATA=%d, PSLVERR=%d",obj_apb.pSELX_(),obj_apb.pENABLE_(),obj_apb.pWRITE_(),obj_apb.pADDR_(),obj_apb.pREADY_(),obj_apb.pRDATA_(),obj_apb.pWDATA_(),obj_apb.pSLVERR_());
		$display(" ");
	endrule

endmodule
endpackage
	
