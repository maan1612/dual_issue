/*
Package Name : AMBA High Performance Bus[AHB]
Authors: Venkatesh,Arjun Menon,Patanjali
Email Id: ee13m083@ee.iitm.ac.in,c.arjunmenon@gmail.com,patanjali.slpsk@gmail.com
Last Updated:11/1/2015

This package has been developed based on the AMBA 3 AHB lite protocol Version 1.0 PDF from ARM.
Web: www.arm.com
*/

//our 1st try at AHB master.


package ambahighperform;

import ConfigReg::*;
import FIFOF::*;
import SpecialFIFOs::*;
import riscv::*;

`include "defined_parameters.bsv"
/*
(*synthesize*)
module mkTb_ahb(Empty);
	Reg#(int) counter <- mkConfigReg(0);
	AHB_ifc ahb <- mkahb;

	rule reset(counter == 0);
		ahb.reset_from_proc(1'b1);
	endrule

	rule start1(counter == 1);
		ahb.address_from_proc('d28,1'b1,3'b010,2'b10,1'b1,3'b110,4'b0011,2'b01);
	endrule

	rule start2;
		let address = ahb.address_of_slave();
		let bridgedata = ahb.write_data();
		let procdata = ahb.read_data();
		let selection = ahb.select_slave();
		$display("address is %d\t writedata is %d \t readdata is %d \t counter is %d \t",address,bridgedata,procdata,counter);
		if (selection == 2'b01)
			$display("imem is selected \n");
		else if(selection == 2'b00)
			$display("dmem is selected \n");
		counter <= counter + 1;
	endrule
	
	rule start3(counter > 20);
		$finish;
	endrule
endmodule
*/

interface AHB_ifc;

	method Action reset_from_proc(bit reset_proc); //reset signal sent to the ahb interface by the processor
	method Action address_from_proc(Bit#(`address_bus) address,Bit#(3) data_size,Bit#(3)burst_type); //address and control signals sent out to the ahb by the processor
	method Action data_from_proc(Bit#(`data_bus)data_write); //write data from the processor
	method Action data_from_bridge(Bit#(`data_bus) bridge_data); //read data from the bridge
	method Action ready_from_bridge(bit hready); //ready signal from bridge for transfer initiation from bridge side
	method Action valid_signal_from_bridge(bit hresp); //valid signal from bridge for read data

//Removed the acknowledgement signal from proc to slave.Removed hsend signals[data_ready and address_ready].hvalid_data or hvalid_address_and_control.Not mentioned in the PDF.Also removed the slave_valid_signal from data_from_bridge.rechristened the ready signal from ahb to bridge as enable.rg_AHB_to_bridge_data_valid and rg_enable_master signals are removed.Removed address and data fire signals and replaced them with 1 signal.removed rg_address_HSEND since only one ready signal from AHB to proc is given in PDF.

	method bit valid_signal_to_proc(); // valid signal to processor for read data
	method bit ready_signal_to_proc(); // ready signal sent to processor for transfer initiation
	method Bit#(`address_bus) address_of_slave(); // address sent to bridge
	method Bit#(`data_bus) write_data(); // write data sent to bridge
	method Bit#(`data_bus) read_data(); // read data sent to processor
	method Bit#(3) data_size(); // size of the data transfer sent to bridge
	method Bit#(3) burst_type(); //burst type of the data transfer sent to bridge
	method Bit#(4) prot_type(); //protection type of the data transfer sent to bridge
	method Bit#(2) transfer_type(); //transfer type of the data sent to bridge
	method bit mast_lock(); //master lock signal sent to the bridge
	method Bit#(2) select_slave(); //slave select signal sent to bridge
	method bit write_enable(); //read or write signal sent to the bridge
	method bit ahb_to_bridge_data_enable(); //enable signal sent to the bridge
endinterface

/*AHB-lite is a high-performance bus interface that supports single master and multiple slaves.The processor we use will be the master 
and we design single AHB slave in this module.Later on we move to multiple slaves.Implementing all type of burst modes as well.The overall
architecture is "Processor-AHB master-Bridge[AHB slave]-APB master-APB​ slave"*/

(*synthesize*)
module mkahb(AHB_ifc);

	Wire #(Bit#(`address_bus)) wr_address_of_slave  <-mkDWire(0);//HAddr.address from the processor.
	Wire #(Bit#(`data_bus)) wr_write_data   	<-mkDWire(0);//HWdata.data from the processor during write operation.
	Wire #(Bit #(3)) wr_data_size     		<-mkDWire(0);//HSize.size of the word to be transferred from the processor
	Wire #(Bit #(3)) wr_burst_type    		<-mkDWire(0);//HBurst.burst type of the transfer from the processor
	Wire #(bit)  proc_reset         		<-mkDWire(1);//HReset.reset from the processor.Active low
	Wire #(Bit#(2))  wr_sel		  		<-mkDWire(2'b10);//HSelx.slave selection
	Wire #(bit)  wr_AHB_response	   	        <-mkDWire(0);//HResp for read datafrom the bridge.
	Wire #(Bool)     fire		                <- mkDWire(False);//Enable.indicating data available during the write process
	Wire #(Bit#(4))  wr_beat_count                  <- mkDWire(0);//counter for maintaining the data packet count during burst mode
	Wire #(bit)  wr_slave_ready        		<- mkDWire(1);//HReady.indicator for bridge availibility for next input.
	Wire #(Bit#(`data_bus)) wr_read_data		<- mkDWire(0);//HRdata.read data from the bridge.

	Reg #(Bit#(4))   rg_beat_count                  <-mkConfigReg(0);//counter for maintaining the data packet count during burst mode
	Reg #(bit)   rg_idle                            <-mkConfigReg(0);//idle state indicator.1 implies the system is in idle state
	Reg #(Bit#(`address_bus))  rg_address_of_slave	<-mkConfigReg(0);//HAddr.address of slave from the processor
	Reg #(bit)   rg_write    			<-mkConfigReg(0);//HWrite.0=read,1=write.default is read
	Reg #(Bit#(3))   rg_data_size  			<-mkConfigReg(0);//HSize.word size 
	Reg #(Bit#(3))   rg_burst_type  		<-mkConfigReg(0);//HBurst.burst type
	Reg #(Bit#(4))   rg_prot_type   		<-mkConfigReg(4'b0011);//HProt.protection signals.
	Reg #(Bit#(2))   rg_transfer_type		<-mkConfigReg(0);//HTrans.transfer type.refer to manual.
	Reg #(bit)   rg_mast_lock 			<-mkConfigReg(1);//HMastLock.masterlock in case of multi master configuration.
	Reg #(Bit#(2))   rg_sel     			<-mkConfigReg(2'b10);//slave selection 
	Reg #(Bit#(`data_bus))  rg_write_data  		<-mkConfigReg(0);//write data
	Reg #(Bit#(`data_bus))  rg_read_data   		<-mkConfigReg(0);//read data
	Reg #(bit)   rg_enable_master	    	        <-mkConfigReg(1);//enable signal to bridge for read data.
	Reg #(bit)  rg_AHB_ready			<-mkConfigReg(0);//ready signal to processor for next transaction.
	Reg #(bit)  rg_AHB_response                     <-mkConfigReg(1);//response signal to processor for read data.default is invalid
	Reg #(int) idlecounter                          <-mkConfigReg(16);//default maximum no.of wait states is 16

        Ifc_riscv processor                             <-mkriscv(); // instantiating the processor

	FIFOF#(Bit#(`data_bus)) fifo_HWDATA     	<-mkSizedBypassFIFOF(`fifo1);//for storing write data
	FIFOF#(Bit#(`data_bus)) fifo_HRDATA     	<-mkSizedBypassFIFOF(`fifo2);//for storing read data
	FIFOF#(bit)  fifo_AHB_response	 	        <-mkSizedBypassFIFOF(`fifo2);//HResp for read datafrom bridge.

//Ask doubt about the size of data bus - narrow and wide for master and slave.

	rule address_bus(proc_reset==1 && wr_slave_ready == 1 && !fire ); //rule for generating address according to different conditions
		
		Bit #(`address_bus) lv_HADDR_next     = 0;//next address storage variable
		Bit #(`address_bus) offset = 0; //offset bit for data_size
		bit   lv_address_valid_bit=0; //enable signal to bridge for next transfer
		Bit #(1)  lv_burst_type     = 0; //burst type checker variable
		idlecounter                 <= 16;
	 
		case(rg_burst_type) //burst_type
			3'b000,3'b001,3'b011,3'b101,3'b111: lv_burst_type = 0;  //incremental burst mode
			3'b010,3'b100,3'b110:		    lv_burst_type = 1;  //wrap around burst mode
		endcase

/*		case(`data_bus_size)
			'd32:begin case (rg_data_size)
				3'b000:offset = 'd1; //byte transfer
				3'b001:offset = 'd2; //halfword
				3'b010:offset = 'd4; //word
				endcase
				end

			'd64:begin case (rg_data_size)
				3'b000:offset = 'd1; //byte transfer
				3'b001:offset = 'd2; //halfword
				3'b010:offset = 'd4; //word
				3'b011:offset = 'd8; //doubleword
				3'b100:offset = 'd16; //4 word line
				3'b101:offset = 'd32; //8 word line
				3'b110:offset = 'd64; 
				endcase
				end

			'd128:begin case (rg_data_size)
				3'b000:offset = 'd1; //byte transfer
				3'b001:offset = 'd2; //halfword
				3'b010:offset = 'd4; //word
				3'b011:offset = 'd8; //doubleword
				3'b100:offset = 'd16; //4 word line
				3'b101:offset = 'd32; //8 word line
				3'b110:offset = 'd64; 
				3'b111:offset = 'd128;
				endcase
				end	
*/
		case(rg_data_size) //size of data transfer
			3'b000:offset = 'd1; //byte transfer
			3'b001:offset = 'd2; //halfword
			3'b010:offset = 'd4; //word
			3'b011:offset = 'd8; //doubleword
			3'b100:offset = 'd16; //4 word line
			3'b101:offset = 'd32; //8 word line
			3'b110:offset = 'd64; 
			3'b111:offset = 'd128;
		endcase
 		
		case(rg_transfer_type) //type of transfer
			2'b00: begin
				 lv_HADDR_next=0;            //IDLE
				 lv_address_valid_bit=0;
				end
			2'b01: begin
				lv_HADDR_next = rg_address_of_slave;  //BUSY
				lv_address_valid_bit=rg_enable_master ;
				end
			2'b10:begin			  //NON_SEQUENTIAL
				if(wr_slave_ready==0)begin
					lv_HADDR_next = rg_address_of_slave;
					lv_address_valid_bit=rg_enable_master ;
					end
				else if(rg_burst_type==3'b000)begin //non sequential is a single burst transfer
					lv_HADDR_next=0;
					lv_address_valid_bit=0;
				end
				/*else begin 		
					lv_HADDR_next = rg_address_of_slave+offset;
					lv_address_valid_bit=1;
					end */						
					//lv_HADDR_next = rg_address_of_slave+offset; 
			        end		
			2'b11:begin			  //SEQUENTIAL
				if(wr_slave_ready==0)begin
					lv_HADDR_next = rg_address_of_slave;
					lv_address_valid_bit=rg_enable_master ;
					end
				else if(rg_beat_count>0)begin
						if(lv_burst_type == 0)begin //incremental bursts
						lv_HADDR_next= rg_address_of_slave+offset;
						lv_address_valid_bit = 1;
				end
					else begin //wrap around bursts
						lv_HADDR_next= rg_address_of_slave+offset;
						case(rg_burst_type)
							3'b010: begin 				//WRAP4
							case(rg_data_size)
								3'b001:	begin			//Half-word, boundary of 8 bytes
									if(lv_HADDR_next>={(rg_address_of_slave[`address_bus-1:3]+1),3'd0})
									lv_HADDR_next= {rg_address_of_slave[`address_bus-1:3],3'd0};
								end
								3'b010:	begin			//word, boundary of 16 bytes
									if(lv_HADDR_next>={(rg_address_of_slave[`address_bus-1:4]+1),4'd0})
									lv_HADDR_next= {rg_address_of_slave[`address_bus-1:4],4'd0};
								end
								3'b011:	begin			//double-word, boundary of 32 bytes
									if(lv_HADDR_next>={(rg_address_of_slave[`address_bus-1:5]+1),5'd0})
									lv_HADDR_next= {rg_address_of_slave[`address_bus-1:5],5'd0};
								end
				
							endcase
							end
							3'b100: begin					//WRAP8
							case(rg_data_size)
								3'b001:	begin			//Half-word, boundary of 16 bytes
									if(lv_HADDR_next>={(rg_address_of_slave[`address_bus-1:4]+1),4'd0})
									lv_HADDR_next= {rg_address_of_slave[`address_bus-1:4],4'd0};
								end
								3'b010:	begin			//word, boundary of 32 bytes
									if(lv_HADDR_next>={(rg_address_of_slave[`address_bus-1:5]+1),5'd0})
									lv_HADDR_next= {rg_address_of_slave[`address_bus-1:5],5'd0};
								end
								default: lv_HADDR_next= 0;//we're sending a maximum of 8 words
								/*3'b011:	begin		//double-word, boundary of 64 bytes
									if(lv_HADDR_next>={(rg_HADDR[`address_bus-1:6]+1),6'd0})
									lv_HADDR_next= {rg_HADDR[`address_bus-1:6],6'd0};
								end*/
					
							endcase
							end
						3'b110:	begin					//WRAP16
							case(rg_data_size)
								3'b001:	begin			//Half-word, boundary of 32 bytes
									if(lv_HADDR_next>={(rg_address_of_slave[`address_bus-1:5]+1),5'd0})
									lv_HADDR_next= {rg_address_of_slave[`address_bus-1:5],5'd0};
								end
								default:lv_HADDR_next= 0;//we're sending a maximum of 8 words
								/*3'b010:	begin			//word, boundary of 64 bytes
									if(lv_HADDR_next>={(rg_HADDR[`address_bus-1:6]+1),6'd0})
									lv_HADDR_next= {rg_HADDR[`address_bus-1:6],6'd0};
								end
								3'b011:	begin			//double-word, boundary of 128 bytes
									if(lv_HADDR_next>={(rg_HADDR[`address_bus-1:7]+1),7'd0})
									lv_HADDR_next= {rg_HADDR[`address_bus-1:7],7'd0};
								end*/
					
							endcase
							end
					endcase
					end	
					lv_address_valid_bit=1;					
					end
				else if(rg_beat_count==0 && rg_burst_type==3'b001)begin
					lv_HADDR_next= rg_address_of_slave+offset; //incremental transfer of undefined length
					lv_address_valid_bit=1;
					end
				else if(rg_beat_count==0 && (rg_burst_type==3'b011 || rg_burst_type==3'b101 || rg_burst_type==3'b111))begin
					lv_HADDR_next =0;              //case with incremental burst and beat count is complete
					lv_address_valid_bit=0;
					end
				end
		endcase	
		rg_address_of_slave<=lv_HADDR_next;
		rg_enable_master   <=lv_address_valid_bit;
			
	endrule

	rule beatcounter(proc_reset==1 && wr_slave_ready == 1 && !fire ); //rule for counting the number of beats

		Bit #(4) lv_beat_count_next=0;

		case(rg_transfer_type)
			2'b00:lv_beat_count_next=0; //in case of idle transfer the beat count is 0
			2'b01:lv_beat_count_next=rg_beat_count; //in case of master busy the beat count remains the same
			2'b10,2'b11:if(rg_beat_count>0 && wr_slave_ready==1) //sequential and non sequential transfers
	  				lv_beat_count_next=rg_beat_count-1; //slave is ready => transfer took place.so decrement count
				    else if(rg_beat_count>0 && wr_slave_ready==0)
					lv_beat_count_next=rg_beat_count; //slave not ready then maintain the count
				    else 
					lv_beat_count_next=0; //else count is 0
		endcase
		rg_beat_count<=lv_beat_count_next;
	endrule
	
	rule trans_type(proc_reset==1 && wr_slave_ready == 1 && !fire); //rule for generating transfer type

		Bit#(2) lv_HTRANS_next=0; //next transfer type
		Bit #(4) lv_beat_count_next=0;
		case(rg_transfer_type)
			2'b00:begin //idle state of the master
				if(rg_sel == 2'b01||2'b00) begin
					lv_HTRANS_next=2'b10;//1st transfer,whenever a slave is selected, is nonsequential
				end
				else 
					lv_HTRANS_next=2'b00; //system is idle => next state is idle unless specified by the processor
				end
			2'b01:begin//busy type transfer
				if(!fifo_HWDATA.notEmpty())
					lv_HTRANS_next=2'b01;
				else if(rg_burst_type==3'b000) 
					lv_HTRANS_next=2'b00;
				else if(rg_burst_type==3'b001)begin  //system completed busy state, needs to enter idle state
					lv_HTRANS_next=2'b00;
					rg_idle<=1;
					end
				else if(rg_burst_type==3'b011 || rg_burst_type==3'b101 || rg_burst_type==3'b111)
					lv_HTRANS_next=2'b11;
				end
			2'b10:begin //non-sequential transfer
				if(!fifo_HWDATA.notFull())
					lv_HTRANS_next=2'b01;
				else if(wr_slave_ready==0)
					lv_HTRANS_next=2'b10;
				else if(rg_burst_type==3'b000)
					lv_HTRANS_next=2'b00;
				else if(rg_burst_type==3'b001 || rg_burst_type==3'b011 || rg_burst_type==3'b101 || rg_burst_type==3'b111)
					lv_HTRANS_next=2'b11;
				end
			2'b11:	if(rg_address_of_slave <= 1024)begin //sequential transfer
				 if(!fifo_HWDATA.notFull())
					lv_HTRANS_next=2'b01;
				 else if(wr_slave_ready==0)
					lv_HTRANS_next=2'b11;
				 else if(rg_beat_count>0)
					lv_HTRANS_next=2'b11;
				 else if(rg_beat_count==0 && rg_burst_type==3'b001)
					lv_HTRANS_next=2'b11;
				 else if(rg_beat_count==0 && (rg_burst_type==3'b011 || rg_burst_type==3'b101 || rg_burst_type==3'b111))
					lv_HTRANS_next=2'b00;
				end
				else
					lv_HTRANS_next=2'b10;
			endcase	

			rg_transfer_type<=lv_HTRANS_next;
			
	endrule

//rule for generating writing data

	rule data_bus(proc_reset==1 &&  wr_slave_ready == 1 && rg_enable_master == 1 && rg_write == 1); //rule for write data to slave

		Bit#(`data_bus) lv_HWDATA_next=0;
		case(rg_transfer_type)
		2'b00:begin 
			lv_HWDATA_next=0;
		      end
		2'b01:begin
			lv_HWDATA_next=rg_write_data;
			end
		2'b10:if(wr_slave_ready==1)
			begin			
				lv_HWDATA_next=fifo_HWDATA.first();
				fifo_HWDATA.deq();
			end
		      else if(wr_slave_ready==0)begin
				lv_HWDATA_next=rg_write_data;
				end
		2'b11:if(wr_slave_ready==1)
			begin			
				lv_HWDATA_next=fifo_HWDATA.first();
				fifo_HWDATA.deq();
			end
		      else if(wr_slave_ready==0)begin
				lv_HWDATA_next=rg_write_data;
				end
		endcase
		rg_write_data<=lv_HWDATA_next;
	endrule

	rule read_write(proc_reset==1 && wr_slave_ready == 1 && !fire); //rule to generate read or write operation
		  if(rg_sel == 2'b00) begin//need to write code with processor.*.That's called Integration
			if(processor._data_out matches tagged Valid .data)
				rg_write<=1;
			else 
				rg_write<=0;
			end
		else 
			rg_write<=0;
	endrule

	rule data_read(proc_reset == 1 && rg_enable_master == 1 && rg_write == 0); //rule to generate read data
		if(wr_AHB_response==1 && wr_slave_ready == 1)begin
			fifo_HRDATA.deq();
			fifo_AHB_response.deq();
		end
	endrule

	rule size_of_data(proc_reset==1 && !fire ); //rule to generate the size of data to be transferred
		if(rg_idle==1)
			rg_data_size<=0;
		else 
			rg_data_size<=rg_data_size;
	endrule

	rule type_of_burst(proc_reset==1 && wr_slave_ready == 1 && !fire ); //rule to generate the type of burst
		if(rg_idle==1||wr_AHB_response == 0)//the burst can be terminated if there is an error from a slave or can be left as it is
			rg_burst_type<=0;
		else
			rg_burst_type<=rg_burst_type;
	endrule

/*	rule protection_control(proc_reset==1 && wr_slave_ready == 1 && !fire); //rule for protection control		
		if(rg_idle==1)
			rg_prot_type<=4'b0011;     //unless otherwise mentioned protection type is 4'b0011
		else
			rg_prot_type<=rg_prot_type;
	endrule
as of now protection type is always 4'b0011 & master lock is always 1.We're taping out the basic version of the bus protocol
*/
	rule reset(proc_reset==0); //reset state of the whole interface
		
		rg_address_of_slave      <=0;
		rg_enable_master 	 <=0;
		rg_write	       	 <=0;
		rg_data_size        	 <=0;
		rg_burst_type       	 <=0;
		rg_prot_type        	 <=4'b0011;
		rg_transfer_type       	 <=0;
		rg_write_data       	 <=0;		
		rg_beat_count   	 <=0;
		rg_idle         	 <=1;
		rg_sel                   <=2'b10;
		rg_AHB_response          <=1;
		//$display("reset");
	endrule

	rule proc_data(proc_reset==1); //rule for enqueueing data from processor
		if(fifo_HWDATA.notFull())
		begin
			fifo_HWDATA.enq(wr_write_data);
		end
	endrule
/*
rule selection(proc_reset==1 && !fire); dunno if this concept is correct.TODO Ask Arjun.
	if (rg_prot_type == 0)	//using protection type to select the slave.In this case it is a opcode fetch
		rg_sel <= 2'b01;
	else if(rg_prot_type == 4'b0001)
		rg_sel <= 2'b00; //in this case it is a data access
	else                      //we don't have the remaining cases
		rg_sel <= 2'b10; //user,privileged access,[non]cacheble,[non]bufferable.Hence no slave is selected
*/
	
	rule selecting(proc_reset==1 && wr_slave_ready == 1 && !fire); //rule for selecting the slave.Following the 1kb constraint
		if(wr_address_of_slave < 1024)
			rg_sel <= 2'b01; //if the address are smaller than 1023,imem is selected,hence rg_sel == 1 and imem is coded 
                                         //accordingly
		else if(wr_address_of_slave >=1024 && wr_address_of_slave < 2048)
			rg_sel <= 2'b00; //else if the address is in between 1024 and 2047,dmem is selected,rg_sel == 0 and dmem is coded 
                                         // accordingly
		else
			rg_sel <= 2'b10; //else none of the slaves is selected
	endrule

	rule address_control_from_proc(proc_reset==1 && fire && rg_AHB_ready==1&&wr_slave_ready == 1);//without wait states
		rg_address_of_slave      <= wr_address_of_slave;
		rg_enable_master 	 <= 1;
		rg_write	       	 <= rg_write;
		rg_data_size        	 <= wr_data_size;
		rg_burst_type       	 <= wr_burst_type;		
		rg_beat_count   	 <= wr_beat_count;
		rg_prot_type        	 <= rg_prot_type;
		rg_transfer_type       	 <= rg_transfer_type;
		rg_idle         	 <= 0;
	endrule

	rule waitstates(proc_reset==1 && rg_enable_master == 1 && wr_slave_ready == 0);//insertion of wait states by slave.Maximum of 16 
											//wait states allowed
		if(idlecounter > 0) begin		
			rg_address_of_slave      <= rg_address_of_slave;
			rg_enable_master 	 <= 1;
			rg_write	       	 <= rg_write;
			rg_data_size        	 <= rg_data_size;
			rg_burst_type       	 <= rg_burst_type;
			rg_prot_type        	 <= rg_prot_type;
			rg_transfer_type       	 <= rg_transfer_type;
			rg_write_data       	 <= rg_write_data;		
			rg_beat_count   	 <= rg_beat_count;
			rg_idle         	 <= 1;
			rg_sel                   <= rg_sel;
			idlecounter              <= idlecounter - 1;
			rg_AHB_response          <= 0;
		end
		else
			rg_AHB_response          <= 1;//in case,even after 16 wait states the slave is not ready,then it is invalid transfer
	endrule

	method Action reset_from_proc(bit reset_proc);
		proc_reset		<= reset_proc;
	endmethod

	method Action address_from_proc(Bit#(`address_bus) address,Bit#(3) data_size,Bit#(3)burst_type);
		 wr_address_of_slave   <= address;
		 wr_data_size          <= data_size;
		 wr_burst_type 	       <= burst_type;

		case(burst_type)
			3'b000:wr_beat_count <=4'd0;
			3'b001:wr_beat_count <=4'd1;  
			3'b010:wr_beat_count <=4'd3;  
			3'b011:wr_beat_count <=4'd3;  
			3'b100:wr_beat_count <=4'd7;  
			3'b101:wr_beat_count <=4'd7;  
			3'b110:wr_beat_count <=4'd15; 
			3'b111:wr_beat_count <=4'd15;
		endcase
		fire				  <=  True;  
	//	rg_idle                 <= 0;
	//	rg_enable_master        <= 1; 
	endmethod
	 
	method Action data_from_proc(Bit#(`data_bus) data_write); //method for write data
		wr_write_data 	      <=  data_write;
		fire		      <= True;
	endmethod		

	method Action ready_from_bridge(bit hready); //method for ready signal from bridge to AHB
		wr_slave_ready   <= hready;
	endmethod

	method Action valid_signal_from_bridge(bit hresp); //method for response signal from bridge to AHB
		wr_AHB_response  <= hresp;
	endmethod

	method Action data_from_bridge(Bit#(`data_bus) bridge_data); //method for read data from bridge
		if(fifo_HRDATA.notFull())begin
		fifo_HRDATA.enq(bridge_data);
		end
	endmethod

	method bit valid_signal_to_proc(); //valid signal sent to processor for read data
		return rg_AHB_response;
	endmethod

	method bit ready_signal_to_proc(); //ready signal sent to processor for transfer initiation
		return rg_AHB_ready;
	endmethod

	method Bit#(`address_bus) address_of_slave(); //address sent to slave
		return rg_address_of_slave;
	endmethod
	
	method Bit#(`data_bus) write_data(); //write data sent to bridge
		return rg_write_data;
	endmethod

	method Bit#(`data_bus) read_data(); //read data sent to processor by AHB
		return rg_read_data;
	endmethod

	method Bit#(3) data_size(); //size of data sent to bridge 
		return rg_data_size;
	endmethod

	method Bit#(3) burst_type(); //burst type sent to bridge
		return rg_burst_type;
	endmethod

	method Bit#(4) prot_type(); //protection type sent to bridge
		return rg_prot_type;
	endmethod

	method Bit#(2) transfer_type(); //transfer type sent to bridge
		return rg_transfer_type;
	endmethod

	method bit mast_lock(); //master lock sent to bridge
		return rg_mast_lock;
	endmethod

	method Bit#(2) select_slave(); //slave select signal sent to bridge
		return rg_sel;
	endmethod	

	method bit write_enable(); //read-write signal sent to slave
		return rg_write;
	endmethod

	method bit ahb_to_bridge_data_enable(); //enable signal to bridge
		return rg_enable_master;
	endmethod
endmodule
endpackage
