/*
Package Name : Data Memory Slave
Authors : Venkatesh,ArjunMenon,Patanjali
Email : ee13m083@ee.iitm.ac.in,c.arjunmenon@gmail.com,patanjali.slpsk@gmail.com
Last Updated : 4/1/15

This package has been developed to see if a single master and multiple slave architecture is working fine.Next step would be multiple master 
multiple slave architecture.*/

//data memory. 1st slave

package slave1;

import ConfigReg::*;
import RegFile::*;

`include "defined_parameters.bsv"

/* test bench to check this module alone.remove comments to see the working
//Bit#(32) address,bit psel,bit read_write,bit mast_lock
(*synthesize*)

module mkTb_d_mem(Empty);
	Reg#(int) counter <- mkReg(0);
	Data_slave dmem <- mkdata_slave1;

	rule send(counter == 1);
		dmem.address_control_from_master('d4,1'b1,1'b0,1'b0);
		let ready = dmem.ready_signal_to_master();
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive1(counter == 2);
		let data = dmem.data_to_master();
		let ready = dmem.ready_signal_to_master();
		let valid_signal = dmem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		dmem.address_control_from_master('d5,1'b1,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive2(counter == 3);
		let data = dmem.data_to_master();
		let ready = dmem.ready_signal_to_master();
		let valid_signal = dmem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive3(counter == 4);
		let data = dmem.data_to_master();
		let ready = dmem.ready_signal_to_master();
		let valid_signal = dmem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		dmem.address_control_from_master('d6,1'b1,1'b0,1'b1);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive4(counter == 5);
		let data = dmem.data_to_master();
		let ready = dmem.ready_signal_to_master();
		let valid_signal = dmem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		dmem.address_control_from_master('d7,1'b1,1'b0,1'b1);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive5(counter == 6);
		let data = dmem.data_to_master();
		let ready = dmem.ready_signal_to_master();
		let valid_signal = dmem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		dmem.address_control_from_master('d9,1'b1,1'b0,1'b1);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive6(counter == 7);
		let data = dmem.data_to_master();
		let ready = dmem.ready_signal_to_master();
		let valid_signal = dmem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		dmem.address_control_from_master('d2,1'b0,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive7(counter == 8);
		let data = dmem.data_to_master();
		let ready = dmem.ready_signal_to_master();
		let valid_signal = dmem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		dmem.address_control_from_master('d3,1'b0,1'b1,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive8(counter == 9);
		let data = dmem.data_to_master();
		let ready = dmem.ready_signal_to_master();
		let valid_signal = dmem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		dmem.data_from_master('d284);
		dmem.address_control_from_master('d4,1'b1,1'b1,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive9(counter == 10);
		let data = dmem.data_to_master();
		let valid_signal = dmem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		$finish;
	endrule
	
	rule transfer(counter == 0);
		let ready = dmem.ready_signal_to_master();
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= 0;
	endrule
endmodule
*/

interface Data_slave;
	method Action address_control_from_master(Bit#(`address_bus) address,Bit#(2) psel,bit read_write);  //PAddr.address and control
                                                                                                                  //signals from the master
	method Action data_from_master(Bit#(`data_bus) data);              //writing data into the D-Mem.upd function will be used to update
	method Action enable_from_master(bit enable);                                   //enable signal from the master to enable transfers
	method Bit#(`data_bus) data_to_master();                   //master reading data from slave.sub function will be used to return data
	method bit ready_signal_to_master();     //ready signal from slave to master.An indication that the slave can be used for transfers
	method bit valid_signal_to_master();                                              //valid signal from slave to master for read data
endinterface

/*the purpose of D-Mem is processor fetches data for arithmetic and logic functions and writes back into the D-Mem.It is a peripheral like
RAM,pendrive or a hard-disk*/

/*as of now,error in data occurs only when request is invalid i.e, when the master is busy or when the slave is reset and the master sends 
an address request.No provisions are made for master being fast and slave being slow in which case different data are read for different 
addresses.*/

(*synthesize*)
module mkdata_slave1(Data_slave);
	RegFile#(Bit#(12),Bit#(`data_bus)) ger <- mkRegFile(0,4095); //data memory as regfile.Size of each element in reg is of databus width
	Wire#(bit) wr_enable_signal <- mkDWire(0);                                                          //enable signal from the master
	Wire#(Bit#(`address_bus)) wr_address <- mkDWire(0);                                                        //address from the master
//	Wire#(bit) wr_mast_lock <- mkDWire(0);                                                   //master locking signal from the processor
	Wire#(Bit#(`data_bus)) wr_wdata <- mkDWire(0);                                                       //processor writing 32 bit data
	Wire#(bit) wr_read_write <- mkDWire(0);                                                      //read/write signal from the processor
	Wire#(Bit#(2)) wr_psel <- mkDWire(0);                                                       //select signal for selecting the slave
		
	Reg#(bit) rg_valid_signal <- mkConfigReg(1);//valid signal from the slave along with returning 32 bit data to master.Initially 
						    //invalid
	Reg#(bit) rg_ready_signal <- mkConfigReg(0);                                      //ready signal from the slave to enable transfers
	Reg#(Bit#(`data_bus)) rg_read_data <- mkConfigReg(0);                                                   //master reading 32 bit data

	rule transfer1(wr_psel == 2'b01);                                                            //case when this slave is not selected
		rg_read_data <= 0;                                   //master reads all zeros from this slave if this slave is not selected
		rg_ready_signal <= 0;    //when the slave is not selected,then there are no transfers with master.So in the next cycle this
                                                                                                                    //slave can be selected
		rg_valid_signal <= 1;                                        //data is invalid if any transfer occurs during this condition
	endrule

	rule transfer1a(wr_psel == 2'b00 && wr_enable_signal == 0); //in the setup state only the psel is high.So all transfers are invalid
		rg_read_data <= 0;
		rg_ready_signal <= 0;
		rg_valid_signal <= 1;
	endrule

	rule transfer1b(wr_psel == 2'b00 && wr_enable_signal == 1&&rg_ready_signal == 0);  //in the access state psel and enable are high,
												//not ready.still invalid
		rg_read_data <= 0;
		rg_ready_signal <= 1;
		rg_valid_signal <= 1;
	endrule

	rule transfer2(wr_psel == 2'b00&&rg_ready_signal == 1&&(wr_enable_signal == 1));//case when this slave is
                                                                   //selected.It is a transfer state and this is not a multi-sized transfer
		rg_ready_signal <= 0;                                  //since the slave is busy,it isn't ready for the next cycle transfer
		if(wr_read_write == 0)                            //read = 0,write = 1.Data memory can be updated or read accordingly
			rg_read_data <= ger.sub(wr_address[11:0]); 
			//rg_valid_signal <= 0;        //else while reading the module for ready signal,the valid signal = 1 since psel = 0
		else 
			ger.upd(wr_address[11:0],wr_wdata);
		rg_valid_signal <= 0;     //since the address is valid,the data sent also will be valid.Hence the valid signal is held high
	endrule

/*	rule transfer3((wr_psel == 2'b00)&&(rg_ready_signal == 1)&&(wr_enable_signal == 1)&&(wr_mast_lock == 1)); //case when this slave is
                                                                                              // selected and this is a multisized transfer
		rg_ready_signal <= 1;                                //the slave is busy with this master.So slave needs to be ready always
		if(wr_read_write == 0)                                  //read = 0,write = 1.Data memory can be updated or read accordingly
			rg_read_data <= ger.sub(wr_address[11:0]);                  
		else
			ger.upd(wr_address[11:0],wr_wdata);
		rg_valid_signal <= 0;     //since the address is valid,the data sent also will be valid.Hence the valid signal is held high
	endrule //mastlock signal shouldn't be used for single master cases.
*/
method Action enable_from_master(bit enable);                                                                      //enable signal method
	wr_enable_signal <= enable;
endmethod

method Action address_control_from_master(Bit#(`address_bus) address,Bit#(2) psel,bit read_write); //address and control signals method
	wr_address <= address;
//	wr_mast_lock <= mast_lock;
	wr_read_write <= read_write;
	wr_psel <= psel;
endmethod

method Action data_from_master(Bit#(`data_bus) data);                                                  //method for writing data from master
	wr_wdata <= data;
endmethod

method Bit#(`data_bus) data_to_master();                                                                 //method for reading data to master
	return rg_read_data;
endmethod

method bit ready_signal_to_master();                                                                                  //ready signal method
	return rg_ready_signal;
endmethod

method bit valid_signal_to_master();                                                                                  //valid signal method
	return rg_valid_signal;
endmethod

endmodule
endpackage
