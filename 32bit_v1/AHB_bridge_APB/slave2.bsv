/*
Package Name : Instruction Memory Slave
Authors : Venkatesh,ArjunMenon,Patanjali
Email : ee13m083@ee.iitm.ac.in,c.arjunmenon@gmail.com,patanjali.slpsk@gmail.com
Last Updated : 2/1/15

This package has been developed to test the single master and multiple slave architecture.*/
//instruction memory. 2nd slave

package slave2;

import RegFile::*;
import ConfigReg::*;

`include "defined_parameters.bsv"

/* test bench to check this module alone.remove comments to see its working
(*synthesize*)

module mkTb_i_mem(Empty);
	Reg#(int) counter <- mkReg(0);
	Instruction_slave imem <- mkinstruction_slave2;

	rule send(counter == 1);
		let ready = imem.ready_signal_to_master();
		imem.address_control_from_master('d4,1'b1,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive1(counter == 2);
		let data = imem.data_to_master();
		let ready = imem.ready_signal_to_master();
		let valid_signal = imem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		imem.address_control_from_master('d5,1'b1,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive2(counter == 3);
		let data = imem.data_to_master();
		let ready = imem.ready_signal_to_master();
		let valid_signal = imem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive3(counter == 4);
		let data = imem.data_to_master();
		let ready = imem.ready_signal_to_master();
		let valid_signal = imem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		imem.address_control_from_master('d6,1'b1,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive4(counter == 5);
		let data = imem.data_to_master();
		let ready = imem.ready_signal_to_master();
		let valid_signal = imem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		imem.address_control_from_master('d7,1'b0,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive5(counter == 6);
		let data = imem.data_to_master();
		let ready = imem.ready_signal_to_master();
		let valid_signal = imem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		imem.address_control_from_master('d9,1'b1,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive6(counter == 7);
		let data = imem.data_to_master();
		let valid_signal = imem.valid_signal_to_master();
		let ready = imem.ready_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		imem.address_control_from_master('d2,1'b1,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive7(counter == 8);
		let data = imem.data_to_master();
		let ready = imem.ready_signal_to_master();
		let valid_signal = imem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		imem.address_control_from_master('d3,1'b0,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive8(counter == 9);
		let data = imem.data_to_master();
		let ready = imem.ready_signal_to_master();
		let valid_signal = imem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		imem.address_control_from_master('d4,1'b1,1'b0,1'b0);
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= counter;
	endrule

	rule receive9(counter == 10);
		let data = imem.data_to_master();
		let valid_signal = imem.valid_signal_to_master();
		if(valid_signal == 1)
			$display("data is valid and it's value is %b \t counter is %d\n",data,counter);
		else
			$display("data is invalid.\t counter is %d \n",counter);
		$finish;
	endrule

	rule transfer(counter == 0);
		let ready = imem.ready_signal_to_master();
		if(ready == 1)
			counter <= counter + 1;
		else
			counter <= 0;
	endrule
endmodule*/

interface Instruction_slave;
	method Action address_control_from_master(Bit#(`address_bus) address,Bit#(2) psel,bit read_write);  //PAddr.address is given by
                                             										// apb master 
        method Action enable_from_master(bit enable);                                                       //enable signal from apb master
	method Bit#(`data_bus) data_to_master();                         //32-bit instructions sent to the master in instruction fetch cycle
	method bit ready_signal_to_master();                                                     //similar to PReady.ready signal to master
	method bit valid_signal_to_master();                                                    //similar to PSlvErr.error signal to master
endinterface

/*The purpose of I-mem is to store the Instructions.The processor fetches the instructions by sending the addresses to this slave.This
slave generally won't be written into.Sometimes it might be required[need to figure those conditions also].Only in those specific conditions 
the read-write signal goes high*/
//no rule is declared with mast_lock condition.also the error signal is not taken care of.Need to look into those.

(*synthesize*)
module mkinstruction_slave2(Instruction_slave);
	RegFile#(Bit#(12),Bit#(`data_bus)) ger <- mkRegFile(0,4095);                                              //data memory as a regfile
	Wire#(bit) wr_enable_signal <- mkDWire(0);                                                          //enable signal from the master
	Wire#(Bit#(`address_bus)) wr_address <- mkDWire(0);                                                 //address signal from the master
//	Wire#(bit) wr_mast_lock <- mkDWire(0);                                                   //master locking signal from the processor
	Wire#(Bit#(`data_bus)) wr_wdata <- mkDWire(0);                                                       //processor writing 32 bit data
	Wire#(bit) wr_read_write <- mkDWire(0);                                                         //read/write signal from the master
	Wire#(Bit#(2)) wr_psel <- mkDWire(0);                                                       //select signal for selecting the slave
		
	Reg#(bit) rg_valid_signal <- mkConfigReg(1);              //valid signal sent to master for the read data.initially invalid signals
	Reg#(bit) rg_ready_signal <- mkConfigReg(0);                    //ready signal sent to master for transfer enabling from slave side
	Reg#(Bit#(`data_bus)) rg_read_data <- mkConfigReg(0);                                //master reading the 32 bit data from the slave

	rule transfer1(wr_psel == 2'b00);                                                            //case when this slave is not selected
		rg_read_data <= 0;                                   //master reads all zeros from this slave if this slave is not selected
		rg_ready_signal <= 0;    //when the slave is not selected,then there are no transfers with master.So in the next cycle this
                                                                                                                    //slave can be selected
		rg_valid_signal <= 1;                                        //data is invalid if any transfer occurs during this condition
	endrule

	rule transfer1a(wr_psel == 2'b01 && wr_enable_signal == 0);               //setup state,only the psel is high.transfers are invalid
		rg_read_data <= 0;
		rg_ready_signal <= 0;
		rg_valid_signal <= 1;
	endrule

	rule transfer1b(wr_psel == 2'b01&&wr_enable_signal == 1&&rg_ready_signal == 0);//access state,enable and psel are high.ready is low.transfers are invalid
		rg_read_data <= 0;
		rg_ready_signal <= 1;
		rg_valid_signal <= 1;
	endrule

	rule transfer2((wr_psel == 2'b01)&&(wr_enable_signal == 1)&&(rg_ready_signal == 1)); //case when this slave is
                                                                         //selected,not a multisized tranfer and slave is in transfer state
		rg_ready_signal <= 0; //since this slave is selected,it'll be busy for this cycle.So tranfer can't happen in the next cycle
		if(wr_read_write == 0) begin                     //read = 0,write = 1.Instruction memory can be updated or read accordingly
			rg_read_data <= ger.sub(wr_address[11:0]);                  
			rg_valid_signal <= 0;          //else while reading the module for ready signal,the valid signal = 0 since psel = 0
		end		
		else
			rg_valid_signal <= 1;  //nothing should be written into the I-Mem  during the execution.Change here accordingly for
								                          //writing into the I-Mem when there is cache miss
	endrule

/*	rule transfer3((wr_psel == 2'b01)&&(rg_ready_signal == 1)&&(wr_enable_signal == 1)&&(wr_mast_lock == 1)); //case when this slave is
                                                                    //selected,slave is in transfer state and this is a multisized transfer
 		rg_ready_signal <= 1;                              //the slave is locked with this master.So slave needs to be ready always
		if(wr_read_write == 0)begin                             //read = 0,write = 1.Data memory can be updated or read accordingly
			rg_read_data <= ger.sub(wr_address[11:0]);
			rg_valid_signal <= 0;  //since the address is valid,the data sent also will be valid.Hence the valid signal is held
                                                                                                                                     //high
		end
		else
			rg_valid_signal <= 0;  //nothing should be written into the I-Mem  during the execution.Change here accordingly for
									                  //writing into the I-Mem when there is cache miss
	endrule
*/
method Action enable_from_master(bit enable);                                                                 //enable signal from master
	wr_enable_signal <= enable;
endmethod

method Action address_control_from_master(Bit#(`address_bus) address,Bit#(2) psel,bit read_write);//address and control signals from 
																//master
	wr_address <= address;
//	wr_mast_lock <= mast_lock;
	wr_read_write <= read_write;
	wr_psel <= psel;
endmethod

method Bit#(`data_bus) data_to_master();                                                                          //read data sent to master
	return rg_read_data;
endmethod

method bit ready_signal_to_master();                                       //ready signal sent to master to enable transfer from slave side
	return rg_ready_signal;
endmethod

method bit valid_signal_to_master();                                                        //valid signal for the read data sent to master
	return rg_valid_signal;
endmethod

endmodule
endpackage
