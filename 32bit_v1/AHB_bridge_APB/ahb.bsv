/*
module name: APB
authors: Vighnesh Kamath
email id: vighnesh.kamath200190@gmail.com
last update done on 1st July, 2014.

Wrap type transfer is not included. But provisions are made in here for future development.Take a look at the HTRANS part.
package ahb;
*/
package ahb;
import FIFOF::*;

import ConfigReg::*;
import SpecialFIFOs::*;

/*---------------------------------------------------------------------------------------------------------------------------------*/
interface Ifc_ahb;
	
	/* RESET FROM THE PROCESSOR */

	method Action _reset_from_proc(Bit#(1) reset_from_proc);

	/* ADDRESS & CONTROL SIGNALS FROM THE PROCESSOR */
	method Action _address_control_from_proc(Bit#(32) bus_address,Bit#(1) read_write,Bit#(3)word_size,Bit#(3)burst_type,Bit#(4)prot_type,Bit#(1) _slave_select);

	/* DATA FROM THE PROCESSOR */
	method Action _data_from_proc(Bit#(32)data_write);

	/* DATA FROM THE BRIDGE*/
	method Action _data_from_bridge(Bit#(32) data_read,Bit#(1) slave_valid_data);

	/*RESPONSE SIGNALS FROM THE BRIDGE*/

	method Action _response_from_bridge(Bit#(1)slave_ready,Bit#(1) slave_resp);
	
	method Action _response_from_proc_for_read_data(Bit#(1) proc_read_ready);

	method Bit#(32) hADDR_();
	
	method Bit#(32) hWDATA_();

	method Bit#(32) hRDATA_();
	
	method Bit#(3) hSIZE_();

	method Bit#(3) hBURST_();
	
	method Bit#(4) hPROT_();

	method Bit#(2) hTRANS_();

	method Bit#(1) hMASTLOCK_();
	
	method Bit#(1) hSELX_();
	
	method Bit#(1) hWRITE_();

	method Bit#(1) hvalid_data_();

	method Bit#(1) hvalid_address_and_control_();

	method Bit#(1) ahb_to_bridge_data_ready_();

	method Bit#(1) ahb_to_processor_data_ready_();
	
	method Bit#(1) ahb_to_processor_address_ready_(); 

endinterface
/*---------------------------------------------------------------------------------------------------------------------------------*/

(*synthesize*)

module mkahb(Ifc_ahb);


	/* ADDRESS,CONTROL,DATA & RESPONSE INPUT SIGNALS TO THE AHB */

	Wire #(Bit#(32)) wr_proc_address       <-mkDWire(0);
	Wire #(Bit#(32)) wr_proc_data_write    <-mkDWire(0);
	Wire #(Bit#(1))  wr_proc_read_write    <-mkDWire(0);
	Wire #(Bit #(3)) wr_proc_word_size     <-mkDWire(0);
	Wire #(Bit #(3)) wr_proc_burst_type    <-mkDWire(0);
	Wire #(Bit #(4)) wr_proc_prot_type     <-mkDWire(0);
	Wire #(Bit#(1))  wr_proc_reset         <-mkDWire(0);
	Wire #(Bit#(1))  wr_proc_slave_select  <-mkDWire(0);
	Wire #(Bit#(1))  wr_proc_HRDATA_read_ready   <-mkDWire(0);

	/* INTERNAL REGISTERS AND WIRES */

	Wire #(Bool)     wr_input_address_control_fire  <- mkDWire(False);
	Wire #(Bool)     wr_input_data_fire             <- mkDWire(False);
	Wire #(Bit #(2)) wr_transfer_type     	        <- mkDWire(0);
	Wire #(Bit #(4)) wr_beat_count                  <- mkDWire(0);
	Reg #(Bit#(4))   rg_beat_count                  <- mkConfigReg(0);
	Reg #(Bit#(1))   rg_idle                        <- mkConfigReg(0);
	Reg #(Bit#(1))   rg_AHB_to_bridge_data_valid    		<- mkConfigReg(0);
	Reg #(Bit#(1))  rg_AHB_to_bridge_address_valid          <- mkConfigReg(0);


	/* INPUT RESPONSE SIGNAL FROM THE BRIDGE TO THE AHB */

	Wire #(Bit#(1)) wr_HREADY        <- mkDWire(1);
	Wire #(Bit#(1)) wr_HRESP         <- mkDWire(1);
	Wire #(Bit#(32))wr_HRDATA	 <- mkDWire(0);

	/* OUTPUT SIGNALS FROM THE MASTER TO THE SLAVE */

	Reg #(Bit#(32)) rg_HADDR     		<-mkConfigReg(0);	
	Reg #(Bit#(1))  rg_HWRITE    		<-mkConfigReg(0);
	Reg #(Bit#(3))  rg_HSIZE     		<-mkConfigReg(0);
	Reg #(Bit#(3))  rg_HBURST    		<-mkConfigReg(0);
	Reg #(Bit#(4))  rg_HPROT     		<-mkConfigReg(0);
	Reg #(Bit#(2))  rg_HTRANS    		<-mkConfigReg(0);
	Reg #(Bit#(1))  rg_HMASTLOCK 		<-mkConfigReg(0);
	Reg #(Bit#(1))  rg_Hselx     		<-mkConfigReg(0);
	Reg #(Bit#(32)) rg_HWDATA   		<-mkConfigReg(0);
	Reg #(Bit#(32)) rg_HRDATA    		<-mkConfigReg(0);
	Reg #(Bit#(1)) rg_ahb_to_bridge_HRDATA_fifo_ready    	<-mkConfigReg(1);

	/* OUTPUT SIGNALS FROM THE MASTER TO THE PROCESSOR */
	
	Reg #(Bit #(1)) rg_write_data_HSEND     <-mkConfigReg(0);
	Reg #(Bit #(1)) rg_address_HSEND	<-mkConfigReg(0);

	/* FIFO FOR STORING DATA*/
	FIFOF#(Bit#(32)) fifo_HWDATA     <- mkSizedBypassFIFOF(32);
	FIFOF#(Bit#(32)) fifo_HRDATA     <- mkSizedBypassFIFOF(32);
	FIFOF#(Bit#(1)) fifo_ahb_to_proc_read_valid_data <-mkSizedBypassFIFOF(32);

/*------------------------------------------------------------------------------------------------------------------*/			

	//RULE FOR GENERATING rg_HADDR.

	rule rl_address(wr_proc_reset==0 && !wr_input_address_control_fire );
		
		Bit #(32) lv_HADDR_next     = 0;
		Bit #(32) lv_offset_address = 0;
		Bit #(1)  lv_burst_type     = 0; //1==WRAP4,WRAP8,WRAP6
		Bit#(1)   lv_address_valid_bit=0;
	 
		case(rg_HBURST)
			3'b000,3'b001,3'b011,3'b101,3'b111: lv_burst_type = 0;
			3'b010,3'b100,3'b110:		    lv_burst_type = 1;
		endcase

	//THE wr_offset_address NEEDS TO BE MODIFIED FOR INCLUSION OF WRAP MODES.
		case(rg_HSIZE)
			3'b000:lv_offset_address = 32'd1;
			3'b001:lv_offset_address = 32'd2;
			3'b010:lv_offset_address = 32'd4;
			3'b011:lv_offset_address = 32'd8;
			3'b100:lv_offset_address = 32'd16;
			3'b101:lv_offset_address = 32'd32;
			3'b110:lv_offset_address = 32'd64;
			3'b111:lv_offset_address = 32'd128;
		endcase
 		
		case(rg_HTRANS)
			2'b00: begin
				 lv_HADDR_next=0;            //IDLE
				 lv_address_valid_bit=0;
				end
			2'b01: begin
				lv_HADDR_next = rg_HADDR;  //BUSY
				lv_address_valid_bit=rg_AHB_to_bridge_address_valid ;
				end
			2'b10:begin			  //NON_SEQUENTIAL
				if(wr_HREADY==0)begin
					lv_HADDR_next = rg_HADDR;
					lv_address_valid_bit=rg_AHB_to_bridge_address_valid ;
					end
				else if(rg_HBURST==3'b000)begin
					lv_HADDR_next=0;
					lv_address_valid_bit=0;
				end
				else begin 		
					lv_HADDR_next = rg_HADDR+lv_offset_address;
					lv_address_valid_bit=1;
					end 						
					//lv_HADDR_next = rg_HADDR+lv_offset_address; 
			        end		
			2'b11:begin			  //SEQUENTIAL
				if(wr_HREADY==0)begin
					lv_HADDR_next = rg_HADDR;
					lv_address_valid_bit=rg_AHB_to_bridge_address_valid ;
					end
				else if(rg_beat_count>0)begin
					lv_HADDR_next= rg_HADDR+lv_offset_address;
					lv_address_valid_bit=1;
					end
				else if(rg_beat_count==0 && rg_HBURST==3'b001)begin
					lv_HADDR_next= rg_HADDR+lv_offset_address;
					lv_address_valid_bit=1;
					end
				else if(rg_beat_count==0 && (rg_HBURST==3'b011 || rg_HBURST==3'b101 || rg_HBURST==3'b111))begin
					lv_HADDR_next =0;
					lv_address_valid_bit=0;
					end
				end
		endcase	
		rg_HADDR<=lv_HADDR_next;
		rg_AHB_to_bridge_address_valid  <=lv_address_valid_bit;
		//$display("rl_address");
			
	endrule

/*------------------------------------------------------------------------------------------------------------------*/

	//RULE FOR KEEPING THE COUNT OF BEATS IN BURST MODE.

	rule rl_beatcounter(wr_proc_reset==0 && !wr_input_address_control_fire );

		Bit #(4) lv_beat_count_next=0;

		case(rg_HTRANS)
			2'b00:lv_beat_count_next=0;
			2'b01:lv_beat_count_next=rg_beat_count;
			2'b10,2'b11:if(rg_beat_count>0 && wr_HREADY==1)
	  				lv_beat_count_next=rg_beat_count-1;
				    else if(rg_beat_count>0 && wr_HREADY==0)
					lv_beat_count_next=rg_beat_count;
				    else if(rg_beat_count==0)
					lv_beat_count_next=rg_beat_count;
		endcase
		rg_beat_count<=lv_beat_count_next;
		//$display("rl_beat_count");
	endrule


/*------------------------------------------------------------------------------------------------------------------*/
	
	//RULE FOR GENERATING rg_HTRANS
	
	rule rl_transfer_type(wr_proc_reset==0 && !wr_input_address_control_fire);

		Bit#(2) lv_HTRANS_next=0;
		Bit #(4) lv_beat_count_next=0;
		case(rg_HTRANS)
			2'b00:lv_HTRANS_next=2'b00;
			2'b01:begin
				if(!fifo_HWDATA.notEmpty())
					lv_HTRANS_next=2'b01;
				else if(rg_HBURST==3'b000) 
					lv_HTRANS_next=2'b00;
				else if(rg_HBURST==3'b001)begin 
					lv_HTRANS_next=2'b00;
					rg_idle<=1;
					end
				else if(rg_HBURST==3'b011 || rg_HBURST==3'b101 || rg_HBURST==3'b111)
					lv_HTRANS_next=2'b11;
				end
			2'b10:begin
				if(!fifo_HWDATA.notEmpty())
					lv_HTRANS_next=2'b01;
				else if(wr_HREADY==0)
					lv_HTRANS_next=2'b10;
				else if(rg_HBURST==3'b000)
					lv_HTRANS_next=2'b00;
				else if(rg_HBURST==3'b001 || rg_HBURST==3'b011 || rg_HBURST==3'b101 || rg_HBURST==3'b111)
					lv_HTRANS_next=2'b11;
				end
			2'b11:	begin
				 if(!fifo_HWDATA.notEmpty())
					lv_HTRANS_next=2'b01;
				 else if(wr_HREADY==0)
					lv_HTRANS_next=2'b11;
				 else if(rg_beat_count>0)
					lv_HTRANS_next=2'b11;
				 else if(rg_beat_count==0 && rg_HBURST==3'b001)
					lv_HTRANS_next=2'b11;
				 else if(rg_beat_count==0 && (rg_HBURST==3'b011 || rg_HBURST==3'b101 || rg_HBURST==3'b111))begin
					lv_HTRANS_next=2'b00;
					end
				end
			endcase	

			rg_HTRANS<=lv_HTRANS_next;
			//$display("rl_transfer_type");
			
	endrule	

/*------------------------------------------------------------------------------------------------------------------*/

	//RULE FOR rg_HWDATA

	rule rl_data_write(wr_proc_reset==0);

		Bit#(32) lv_HWDATA_next=0;
		Bit#(1)  lv_valid_bit_next=0;
		case(rg_HTRANS)
		2'b00:begin 
			lv_HWDATA_next=0;
			lv_valid_bit_next=0;
		      end
		2'b01:begin
			lv_HWDATA_next=rg_HWDATA;
		        lv_valid_bit_next  =rg_AHB_to_bridge_data_valid;
			end
		2'b10:if(wr_HREADY==1)
			begin			
				lv_HWDATA_next=fifo_HWDATA.first();
				lv_valid_bit_next=1;
				fifo_HWDATA.deq();
				//fifo_valid_bit.deq();
			end
		      else if(wr_HREADY==0)begin
				lv_HWDATA_next=rg_HWDATA;
				lv_valid_bit_next=rg_AHB_to_bridge_data_valid;
				end
		2'b11:if(wr_HREADY==1)
			begin			
				lv_HWDATA_next=fifo_HWDATA.first();
				lv_valid_bit_next=1;
				fifo_HWDATA.deq();
				//fifo_valid_bit.deq();
			end
		      else if(wr_HREADY==0)begin
				lv_HWDATA_next=rg_HWDATA;
				lv_valid_bit_next=rg_AHB_to_bridge_data_valid;
				end
		endcase
		rg_HWDATA<=lv_HWDATA_next;
		rg_AHB_to_bridge_data_valid<=lv_valid_bit_next;
		if(rg_HWRITE==0)
			rg_HRDATA <=wr_HRDATA;
		//$display("rl_data_write");
	endrule

/*------------------------------------------------------------------------------------------------------------------*/

	//RULE FOR GENERATION OF HWRITE.

	rule rl_read_write(wr_proc_reset==0 && !wr_input_address_control_fire );
		if(rg_idle==1)
			rg_HWRITE<=0;
		else
			rg_HWRITE<=rg_HWRITE;
		//$display("rl_read_write");
	endrule


/*------------------------------------------------------------------------------------------------------------------*/

	//RULE FOR GENERATION OF HRDATA.
	rule rl_read_data;
		if(wr_proc_HRDATA_read_ready==1)begin
			fifo_HRDATA.deq();
			fifo_ahb_to_proc_read_valid_data.deq();
		end
		if(fifo_HRDATA.notFull())
			rg_ahb_to_bridge_HRDATA_fifo_ready	<=	1;
		else
			rg_ahb_to_bridge_HRDATA_fifo_ready	<=	0;
	endrule	

/*------------------------------------------------------------------------------------------------------------------*/
	//RULE FOR GENERATION OF HSIZE.

	rule rl_data_size(wr_proc_reset==0 && !wr_input_address_control_fire );
		if(rg_idle==1)
			rg_HSIZE<=0;
		else 
			rg_HSIZE<=rg_HSIZE;
		//$display("rl_data_size");
	endrule
	
/*------------------------------------------------------------------------------------------------------------------*/

	//RULE FOR GENERATION OF HBURST

	rule rl_burst_type(wr_proc_reset==0 && !wr_input_address_control_fire );
		if(rg_idle==1)
			rg_HBURST<=0;
		else
			rg_HBURST<=rg_HBURST;
		//$display("rl_burst_type");
	endrule

/*------------------------------------------------------------------------------------------------------------------*/

	//RULE FOR GENERATION OF HPROT.
	
	rule rl_protection_control(wr_proc_reset==0 && !wr_input_address_control_fire);
		
		if(rg_idle==1)
			rg_HPROT<=0;
		else
			rg_HPROT<=rg_HPROT;
		//$display("rl_protection_control");	
	endrule



/*------------------------------------------------------------------------------------------------------------------*/

//RULE FOR MASTER RESET.

	rule rl_proc_reset(wr_proc_reset==1);
		
		rg_HADDR       		 <=0;
		rg_AHB_to_bridge_address_valid 	 <=0;
		rg_HWRITE       	 <=0;
		rg_HSIZE        	 <=0;
		rg_HBURST       	 <=0;
		rg_HPROT        	 <=0;
		rg_HTRANS       	 <=0;
		rg_HWDATA       	 <=0;		
		rg_beat_count   	 <=0;
		rg_idle         	 <=1;
		//$display("reset");
	endrule

/*------------------------------------------------------------------------------------------------------------------*/

//RULE FOR INITIAL ADDRESS AND CONTROL SIGNALS FROM THE PROCESSOR.

	rule rl_proc_address_control(wr_proc_reset==0 && wr_input_address_control_fire);
		rg_HADDR       		 <= wr_proc_address;
		rg_HWRITE       	 <= wr_proc_read_write;
		rg_HSIZE       		 <= wr_proc_word_size;
		rg_HBURST      		 <= wr_proc_burst_type;
		rg_HPROT       		 <= wr_proc_prot_type;
		rg_HTRANS      		 <= wr_transfer_type;
		rg_Hselx       		 <= wr_proc_slave_select;
		rg_AHB_to_bridge_address_valid 	 <=1;
		rg_beat_count   	 <= wr_beat_count;
		rg_idle         	 <= 0;
		//$display("rl_proc_address_control");
	endrule

/*------------------------------------------------------------------------------------------------------------------*/

//RULE FOR DATA FROM THE PROCESSOR.

	rule rl_proc_data(wr_proc_reset==0);
		if(fifo_HWDATA.notFull())
		begin
			////$display("%d",wr_proc_data_write);
			fifo_HWDATA.enq(wr_proc_data_write);
		end
		//$display("rl_proc_data");
	endrule

/*------------------------------------------------------------------------------------------------------------------*/

//RULE FOR DISPLAY OF OUTPUTS FROM THE MASTER.
/*	rule rl_display;
		$display("AHB >>>>>>HADDR=%d, HWRITE=%d, HSIZE=%d, HBURST=%d, HTRANS=%d, HWDATA=%d, HRESET=%d, rg_beat_count=%d,rg_idle=%d,rg_HWAIT=%d,wr_HREADY=%d,wr_HRESP=%d",rg_HADDR,rg_HWRITE,rg_HSIZE,rg_HBURST,rg_HTRANS,rg_HWDATA,rg_HRESET,rg_beat_count,rg_idle,rg_HWAIT,wr_HREADY,wr_HRESP);
		//$display("-----------------------------------------------------------------------------------------------------------");
	endrule
*/
/*------------------------------------------------------------------------------------------------------------------*/

	rule rl_fifo_status;
		if(fifo_HWDATA.notFull())
			rg_write_data_HSEND		<=1;
		else
			rg_write_data_HSEND		<=0;

	/*	if(fifo_HADDR.notFull())
			rg_address_HSEND	<=1;
		else
			rg_address_HSEND	<=0;*/
	endrule	

/*------------------------------------------------------------------------------------------------------------------*/
	method Action _reset_from_proc(Bit#(1) reset_from_proc);
		wr_proc_reset		<= reset_from_proc;
	endmethod


	method Action _address_control_from_proc(Bit#(32) _bus_address,Bit#(1) _read_write,Bit#(3) _word_size,Bit#(3) _burst_type,Bit#(4) _prot_type,Bit#(1) _slave_select);
		 wr_proc_address       <= _bus_address;
		 wr_proc_read_write    <= _read_write;
		 wr_proc_word_size     <= _word_size;
		 wr_proc_burst_type    <= _burst_type;
		 wr_proc_prot_type     <= _prot_type;
		 wr_proc_slave_select  <= _slave_select;

		case(_burst_type)
			3'b000:wr_beat_count <=4'd0;
			3'b001:wr_beat_count <=4'd1;  
			3'b010:wr_beat_count <=4'd3;  
			3'b011:wr_beat_count <=4'd3;  
			3'b100:wr_beat_count <=4'd7;  
			3'b101:wr_beat_count <=4'd7;  
			3'b110:wr_beat_count <=4'd15; 
			3'b111:wr_beat_count <=4'd15;
		endcase

		wr_transfer_type                  <=  2'b10;
		wr_input_address_control_fire     <=  True;  
		//$display("method address");    
	endmethod

	 
	method Action _data_from_proc(Bit#(32) _data_write);
		wr_proc_data_write    <=  _data_write;
		wr_input_data_fire    <= True;
		//$display("method data"); 
	endmethod	

	method Action _response_from_proc_for_read_data(Bit#(1) proc_read_ready);
		wr_proc_HRDATA_read_ready	<= proc_read_ready;
	endmethod	
	method Action _response_from_bridge(Bit#(1) _slave_ready,Bit#(1) _slave_response);
		wr_HREADY <= _slave_ready;
		wr_HRESP  <= _slave_response;
	endmethod

	method Action _data_from_bridge(Bit#(32) _data_read,Bit#(1)read_valid_data);
		if(fifo_HRDATA.notFull())begin
		fifo_ahb_to_proc_read_valid_data.enq(read_valid_data);
		fifo_HRDATA.enq(_data_read);
		end
	endmethod

	method Bit#(32) hADDR_();
		return rg_HADDR;
	endmethod
	
	method Bit#(32) hWDATA_();
		return rg_HWDATA;
	endmethod

	method Bit#(32) hRDATA_();
		return rg_HRDATA;
	endmethod

	method Bit#(3) hSIZE_();
		return rg_HSIZE;
	endmethod

	method Bit#(3) hBURST_();
		return rg_HBURST;
	endmethod

	method Bit#(4) hPROT_();
		return rg_HPROT;
	endmethod

	method Bit#(2) hTRANS_();
		return rg_HTRANS;
	endmethod

	method Bit#(1) hMASTLOCK_();
		return rg_HMASTLOCK;
	endmethod

	method Bit#(1) hSELX_();
		return rg_Hselx;
	endmethod	

	method Bit#(1) hWRITE_();
		return rg_HWRITE;
	endmethod

	method Bit#(1) hvalid_data_();
		return rg_AHB_to_bridge_data_valid;
	endmethod
		
	method Bit#(1) hvalid_address_and_control_();
		return rg_AHB_to_bridge_address_valid ;
	endmethod

	method Bit#(1) ahb_to_bridge_data_ready_();
		return rg_ahb_to_bridge_HRDATA_fifo_ready;
	endmethod

	method Bit#(1) ahb_to_processor_data_ready_();
		return rg_write_data_HSEND;	
	endmethod

	method Bit#(1) ahb_to_processor_address_ready_();
		return rg_address_HSEND; 
	endmethod
endmodule
/*---------------------------------------------------------------------------------------------------------------------------------*/
endpackage
