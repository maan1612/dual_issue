//
// Generated by Bluespec Compiler, version 2016.07.beta1 (build 34806, 2016-07-05)
//
// On Thu Feb  9 10:46:26 IST 2017
//
//
// Ports:
// Name                         I/O  size props
// inputs                         O   183
// RDY_inputs                     O     1 const
// CLK                            I     1 clock
// RST_N                          I     1 reset
// inputs__opcode                 I     5
// inputs__funct3                 I     3
// inputs__funct7                 I     7
// inputs__operand1               I    32
// inputs__operand2               I    32
// inputs__operand3               I    32
// inputs__immediate_value        I    20
// inputs_program_counter         I    32
// inputs_dest_addr               I     5
// inputs_pred_type               I     1
// inputs_is_imm                  I     1
// inputs_rd_type                 I     1 unused
// EN_inputs                      I     1
//
// Combinational paths from inputs to outputs:
//   (inputs__opcode,
//    inputs__funct3,
//    inputs__funct7,
//    inputs__operand1,
//    inputs__operand2,
//    inputs__immediate_value,
//    inputs_program_counter,
//    inputs_dest_addr,
//    inputs_pred_type,
//    inputs_is_imm,
//    EN_inputs) -> inputs
//
//

`ifdef BSV_ASSIGNMENT_DELAY
`else
  `define BSV_ASSIGNMENT_DELAY
`endif

`ifdef BSV_POSITIVE_RESET
  `define BSV_RESET_VALUE 1'b1
  `define BSV_RESET_EDGE posedge
`else
  `define BSV_RESET_VALUE 1'b0
  `define BSV_RESET_EDGE negedge
`endif

module mkexecution_unit(CLK,
			RST_N,

			inputs__opcode,
			inputs__funct3,
			inputs__funct7,
			inputs__operand1,
			inputs__operand2,
			inputs__operand3,
			inputs__immediate_value,
			inputs_program_counter,
			inputs_dest_addr,
			inputs_pred_type,
			inputs_is_imm,
			inputs_rd_type,
			EN_inputs,
			inputs,
			RDY_inputs);
  input  CLK;
  input  RST_N;

  // actionvalue method inputs
  input  [4 : 0] inputs__opcode;
  input  [2 : 0] inputs__funct3;
  input  [6 : 0] inputs__funct7;
  input  [31 : 0] inputs__operand1;
  input  [31 : 0] inputs__operand2;
  input  [31 : 0] inputs__operand3;
  input  [19 : 0] inputs__immediate_value;
  input  [31 : 0] inputs_program_counter;
  input  [4 : 0] inputs_dest_addr;
  input  inputs_pred_type;
  input  inputs_is_imm;
  input  inputs_rd_type;
  input  EN_inputs;
  output [182 : 0] inputs;
  output RDY_inputs;

  // signals for module outputs
  wire [182 : 0] inputs;
  wire RDY_inputs;

  // register rg_state
  reg rg_state;
  wire rg_state$D_IN, rg_state$EN;

  // ports of submodule fpu
  wire [37 : 0] fpu$_start;
  wire [31 : 0] fpu$_start_fsr,
		fpu$_start_operand1,
		fpu$_start_operand2,
		fpu$_start_operand3;
  wire [6 : 0] fpu$_start_funct7;
  wire [4 : 0] fpu$_start_opcode, fpu$exception;
  wire [2 : 0] fpu$_start_funct3;
  wire [1 : 0] fpu$_start_immediate_value;
  wire fpu$EN__start, fpu$EN_exception;

  // ports of submodule muldiv
  wire [32 : 0] muldiv$_start;
  wire [31 : 0] muldiv$_start_inp1, muldiv$_start_inp2;
  wire [2 : 0] muldiv$_start_funct;
  wire muldiv$EN__start, muldiv$_start_word32;

  // rule scheduling signals
  wire CAN_FIRE_inputs, WILL_FIRE_inputs;

  // remaining internal signals
  reg [63 : 0] v__h357, v__h508, v__h560, v__h779;
  reg [31 : 0] IF_inputs__funct3_EQ_0b10_2_OR_inputs__funct3__ETC___d207,
	       x__h868,
	       x_aluresult__h887;
  reg [4 : 0] CASE_inputs__opcode_BITS_4_TO_2_0b100_fpu_sta_ETC__q2;
  reg [3 : 0] CASE_fpuexception_BITS_3_TO_0_0_fpuexception_ETC__q3;
  reg IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d38;
  wire [129 : 0] fn_branch___d44;
  wire [102 : 0] IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d281;
  wire [32 : 0] IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148,
		theResult__213_BIT_31_AND_inputs__funct7_BIT_5_ETC__q1;
  wire [31 : 0] IF_inputs__opcode_EQ_0b0_3_OR_inputs__opcode_E_ETC___d208,
		IF_inputs__opcode_EQ_0b1100_0_AND_inputs__func_ETC___d70,
		IF_inputs__opcode_EQ_0b1100_0_OR_inputs__opcod_ETC___d212,
		IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48,
		_theResult____h1213,
		alu_result___1__h1028,
		alu_result___1__h1193,
		alu_result___1__h1205,
		alu_result___1__h6018,
		alu_result___1__h6020,
		alu_result___1__h918,
		alu_result___1__h927,
		op2___1__h1011,
		temp___1__h1246,
		x1_avValue_out_aluresult__h900,
		x__h6075,
		x__h982;
  wire [8 : 0] IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d239;
  wire NOT_inputs__funct3_BIT_0_5_6_AND_inputs__opera_ETC___d87,
       NOT_inputs__opcode_BITS_4_TO_2_EQ_0b100_AND_NO_ETC___d20,
       NOT_inputs__opcode_BITS_4_TO_2_EQ_0b100_AND_NO_ETC___d33,
       inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inputs__ETC___d225,
       x1_avValue_out_signextend__h904,
       x__h1134;

  // actionvalue method inputs
  assign inputs =
	     { IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d38,
	       x__h868,
	       inputs__opcode != 5'b0 && inputs__opcode != 5'b01000 &&
	       inputs__opcode != 5'b00001 &&
	       inputs__opcode != 5'b01001,
	       x__h6075,
	       IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d239,
	       inputs_dest_addr,
	       IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d281 } ;
  assign RDY_inputs = 1'd1 ;
  assign CAN_FIRE_inputs = 1'd1 ;
  assign WILL_FIRE_inputs = EN_inputs ;

  // submodule fpu
  mkfpu fpu(.CLK(CLK),
	    .RST_N(RST_N),
	    ._start_fsr(fpu$_start_fsr),
	    ._start_funct3(fpu$_start_funct3),
	    ._start_funct7(fpu$_start_funct7),
	    ._start_immediate_value(fpu$_start_immediate_value),
	    ._start_opcode(fpu$_start_opcode),
	    ._start_operand1(fpu$_start_operand1),
	    ._start_operand2(fpu$_start_operand2),
	    ._start_operand3(fpu$_start_operand3),
	    .EN__start(fpu$EN__start),
	    .EN_exception(fpu$EN_exception),
	    ._start(fpu$_start),
	    .RDY__start(),
	    .exception(fpu$exception),
	    .RDY_exception());

  // submodule muldiv
  mkmuldiv muldiv(.CLK(CLK),
		  .RST_N(RST_N),
		  ._start_funct(muldiv$_start_funct),
		  ._start_inp1(muldiv$_start_inp1),
		  ._start_inp2(muldiv$_start_inp2),
		  ._start_word32(muldiv$_start_word32),
		  .EN__start(muldiv$EN__start),
		  ._start(muldiv$_start),
		  .RDY__start());

  // register rg_state
  assign rg_state$D_IN = 1'd0 ;
  assign rg_state$EN =
	     EN_inputs &&
	     NOT_inputs__opcode_BITS_4_TO_2_EQ_0b100_AND_NO_ETC___d20 ;

  // submodule fpu
  assign fpu$_start_fsr = 32'd0 ;
  assign fpu$_start_funct3 = inputs__funct3 ;
  assign fpu$_start_funct7 = inputs__funct7 ;
  assign fpu$_start_immediate_value = inputs__immediate_value[1:0] ;
  assign fpu$_start_opcode = inputs__opcode ;
  assign fpu$_start_operand1 = inputs__operand1 ;
  assign fpu$_start_operand2 = inputs__operand2 ;
  assign fpu$_start_operand3 = inputs__operand3 ;
  assign fpu$EN__start =
	     EN_inputs &&
	     (inputs__opcode[4:2] == 3'b100 ||
	      inputs__opcode[4:2] == 3'b101) ;
  assign fpu$EN_exception =
	     EN_inputs &&
	     (inputs__opcode[4:2] == 3'b100 ||
	      inputs__opcode[4:2] == 3'b101) ;

  // submodule muldiv
  assign muldiv$_start_funct = inputs__funct3 ;
  assign muldiv$_start_inp1 = inputs__operand1 ;
  assign muldiv$_start_inp2 = inputs__operand2 ;
  assign muldiv$_start_word32 = inputs__opcode[1] ;
  assign muldiv$EN__start =
	     EN_inputs &&
	     (inputs__opcode == 5'b01100 || inputs__opcode == 5'b01110) &&
	     inputs__funct7 == 7'd1 ;

  // remaining internal signals
  module_fn_branch instance_fn_branch_0(.fn_branch__dest_addr(inputs_dest_addr),
					.fn_branch__opcode(inputs__opcode),
					.fn_branch__funct3(inputs__funct3),
					.fn_branch__current_pc(inputs_program_counter),
					.fn_branch__immediate_value(inputs__immediate_value),
					.fn_branch__operand1(inputs__operand1),
					.fn_branch__operand2(inputs__operand2),
					.fn_branch__prediction(inputs_pred_type),
					.fn_branch(fn_branch___d44));
  assign IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148 =
	     { _theResult____h1213[31] & inputs__funct7[5],
	       _theResult____h1213 } >>
	     IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48[4:0] |
	     ~(33'h1FFFFFFFF >>
	       IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48[4:0]) &
	     {33{theResult__213_BIT_31_AND_inputs__funct7_BIT_5_ETC__q1[32]}} ;
  assign IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d239 =
	     { inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inputs__ETC___d225 ?
		 2'd0 :
		 inputs__funct3[1:0],
	       inputs__opcode[4:2] != 3'b100 &&
	       inputs__opcode[4:2] != 3'b101 &&
	       (inputs__opcode != 5'b01100 && inputs__opcode != 5'b01110 ||
		inputs__funct7 != 7'd1) &&
	       x1_avValue_out_signextend__h904,
	       inputs__opcode == 5'b01000 || inputs__opcode == 5'b01001,
	       CASE_inputs__opcode_BITS_4_TO_2_0b100_fpu_sta_ETC__q2 } ;
  assign IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d281 =
	     { fn_branch___d44[97:33],
	       inputs__opcode[4:2] != 3'b110 || fn_branch___d44[32],
	       (inputs__opcode[4:2] == 3'b110 && !fn_branch___d44[32]) ?
		 fn_branch___d44[31:0] :
		 ((inputs__opcode[4:2] != 3'b110) ?
		    32'd0 :
		    fn_branch___d44[31:0]),
	       (inputs__opcode[4:2] == 3'b100 ||
		inputs__opcode[4:2] == 3'b101) &&
	       fpu$exception[4],
	       CASE_fpuexception_BITS_3_TO_0_0_fpuexception_ETC__q3 } ;
  assign IF_inputs__opcode_EQ_0b0_3_OR_inputs__opcode_E_ETC___d208 =
	     (inputs__opcode == 5'b0 || inputs__opcode == 5'b00001 ||
	      inputs__opcode == 5'b01001 ||
	      inputs__opcode == 5'b01000 ||
	      (inputs__opcode == 5'b00100 || inputs__opcode == 5'b01100) &&
	      inputs__funct3 == 3'b0) ?
	       x__h982 :
	       IF_inputs__funct3_EQ_0b10_2_OR_inputs__funct3__ETC___d207 ;
  assign IF_inputs__opcode_EQ_0b1100_0_AND_inputs__func_ETC___d70 =
	     (inputs__opcode == 5'b01100 && inputs__funct3 == 3'b0 &&
	      inputs__funct7[5]) ?
	       op2___1__h1011 :
	       IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48 ;
  assign IF_inputs__opcode_EQ_0b1100_0_OR_inputs__opcod_ETC___d212 =
	     ((inputs__opcode == 5'b01100 || inputs__opcode == 5'b01110) &&
	      inputs__funct7 == 7'd1) ?
	       muldiv$_start[31:0] :
	       x1_avValue_out_aluresult__h900 ;
  assign IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48 =
	     inputs_is_imm ?
	       { {12{inputs__immediate_value[19]}},
		 inputs__immediate_value } :
	       inputs__operand2 ;
  assign NOT_inputs__funct3_BIT_0_5_6_AND_inputs__opera_ETC___d87 =
	     !inputs__funct3[0] && inputs__operand1[31] &&
	     !IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48[31] ||
	     (~x__h1134 || inputs__funct3[0]) &&
	     inputs__operand1 <
	     IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48 ;
  assign NOT_inputs__opcode_BITS_4_TO_2_EQ_0b100_AND_NO_ETC___d20 =
	     (inputs__opcode == 5'b01100 || inputs__opcode == 5'b01110) &&
	     inputs__funct7 == 7'd1 &&
	     muldiv$_start[32] ;
  assign NOT_inputs__opcode_BITS_4_TO_2_EQ_0b100_AND_NO_ETC___d33 =
	     inputs__opcode[4:2] != 3'b100 && inputs__opcode[4:2] != 3'b101 &&
	     (inputs__opcode != 5'b01100 && inputs__opcode != 5'b01110 ||
	      inputs__funct7 != 7'd1) &&
	     inputs__opcode[4:2] != 3'b110 ;
  assign _theResult____h1213 =
	     (~inputs__funct3[2]) ?
	       { inputs__operand1[0],
		 inputs__operand1[1],
		 inputs__operand1[2],
		 inputs__operand1[3],
		 inputs__operand1[4],
		 inputs__operand1[5],
		 inputs__operand1[6],
		 inputs__operand1[7],
		 inputs__operand1[8],
		 inputs__operand1[9],
		 inputs__operand1[10],
		 inputs__operand1[11],
		 inputs__operand1[12],
		 inputs__operand1[13],
		 inputs__operand1[14],
		 inputs__operand1[15],
		 inputs__operand1[16],
		 inputs__operand1[17],
		 inputs__operand1[18],
		 inputs__operand1[19],
		 inputs__operand1[20],
		 inputs__operand1[21],
		 inputs__operand1[22],
		 inputs__operand1[23],
		 inputs__operand1[24],
		 inputs__operand1[25],
		 inputs__operand1[26],
		 inputs__operand1[27],
		 inputs__operand1[28],
		 inputs__operand1[29],
		 inputs__operand1[30],
		 inputs__operand1[31] } :
	       inputs__operand1 ;
  assign alu_result___1__h1028 =
	     NOT_inputs__funct3_BIT_0_5_6_AND_inputs__opera_ETC___d87 ?
	       32'd1 :
	       32'd0 ;
  assign alu_result___1__h1193 =
	     inputs__operand1 ^
	     IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48 ;
  assign alu_result___1__h1205 =
	     (~inputs__funct3[2]) ?
	       temp___1__h1246 :
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[31:0] ;
  assign alu_result___1__h6018 =
	     inputs__operand1 |
	     IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48 ;
  assign alu_result___1__h6020 =
	     inputs__operand1 &
	     IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48 ;
  assign alu_result___1__h918 =
	     { IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48[19:0],
	       12'd0 } ;
  assign alu_result___1__h927 =
	     alu_result___1__h918 + inputs_program_counter ;
  assign inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inputs__ETC___d225 =
	     inputs__opcode[4:2] == 3'b100 || inputs__opcode[4:2] == 3'b101 ||
	     (inputs__opcode == 5'b01100 || inputs__opcode == 5'b01110) &&
	     inputs__funct7 == 7'd1 ||
	     inputs__opcode[4:2] == 3'b110 ;
  assign op2___1__h1011 =
	     ~IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48 +
	     32'd1 ;
  assign temp___1__h1246 =
	     { IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[0],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[1],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[2],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[3],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[4],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[5],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[6],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[7],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[8],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[9],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[10],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[11],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[12],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[13],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[14],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[15],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[16],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[17],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[18],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[19],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[20],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[21],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[22],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[23],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[24],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[25],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[26],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[27],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[28],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[29],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[30],
	       IF_INV_inputs__funct3_BIT_2_4_5_THEN_inputs__o_ETC___d148[31] } ;
  assign theResult__213_BIT_31_AND_inputs__funct7_BIT_5_ETC__q1 =
	     { _theResult____h1213[31] & inputs__funct7[5],
	       _theResult____h1213 } ;
  assign x1_avValue_out_aluresult__h900 =
	     (inputs__opcode[4:2] == 3'b110) ?
	       fn_branch___d44[129:98] :
	       x_aluresult__h887 ;
  assign x1_avValue_out_signextend__h904 =
	     inputs__opcode[4:2] != 3'b110 && inputs__funct3[2] ;
  assign x__h1134 =
	     inputs__operand1[31] ^
	     IF_inputs_is_imm_THEN_SEXT_inputs__immediate_v_ETC___d48[31] ;
  assign x__h6075 =
	     inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inputs__ETC___d225 ?
	       32'd0 :
	       inputs__operand2 ;
  assign x__h982 =
	     inputs__operand1 +
	     IF_inputs__opcode_EQ_0b1100_0_AND_inputs__func_ETC___d70 ;
  always@(inputs__funct3 or
	  alu_result___1__h1205 or
	  alu_result___1__h1028 or
	  alu_result___1__h1193 or
	  alu_result___1__h6018 or alu_result___1__h6020)
  begin
    case (inputs__funct3)
      3'd0: IF_inputs__funct3_EQ_0b10_2_OR_inputs__funct3__ETC___d207 = 32'd0;
      3'b001, 3'b101:
	  IF_inputs__funct3_EQ_0b10_2_OR_inputs__funct3__ETC___d207 =
	      alu_result___1__h1205;
      3'b010, 3'b011:
	  IF_inputs__funct3_EQ_0b10_2_OR_inputs__funct3__ETC___d207 =
	      alu_result___1__h1028;
      3'b100:
	  IF_inputs__funct3_EQ_0b10_2_OR_inputs__funct3__ETC___d207 =
	      alu_result___1__h1193;
      3'b110:
	  IF_inputs__funct3_EQ_0b10_2_OR_inputs__funct3__ETC___d207 =
	      alu_result___1__h6018;
      3'b111:
	  IF_inputs__funct3_EQ_0b10_2_OR_inputs__funct3__ETC___d207 =
	      alu_result___1__h6020;
    endcase
  end
  always@(inputs__opcode or
	  IF_inputs__opcode_EQ_0b0_3_OR_inputs__opcode_E_ETC___d208 or
	  alu_result___1__h927 or alu_result___1__h918)
  begin
    case (inputs__opcode)
      5'b00101: x_aluresult__h887 = alu_result___1__h927;
      5'b01101: x_aluresult__h887 = alu_result___1__h918;
      default: x_aluresult__h887 =
		   IF_inputs__opcode_EQ_0b0_3_OR_inputs__opcode_E_ETC___d208;
    endcase
  end
  always@(inputs__opcode or
	  IF_inputs__opcode_EQ_0b1100_0_OR_inputs__opcod_ETC___d212 or
	  fpu$_start)
  begin
    case (inputs__opcode[4:2])
      3'b100, 3'b101: x__h868 = fpu$_start[36:5];
      default: x__h868 =
		   IF_inputs__opcode_EQ_0b1100_0_OR_inputs__opcod_ETC___d212;
    endcase
  end
  always@(inputs__opcode or fpu$_start)
  begin
    case (inputs__opcode[4:2])
      3'b100, 3'b101:
	  CASE_inputs__opcode_BITS_4_TO_2_0b100_fpu_sta_ETC__q2 =
	      fpu$_start[4:0];
      default: CASE_inputs__opcode_BITS_4_TO_2_0b100_fpu_sta_ETC__q2 = 5'd0;
    endcase
  end
  always@(fpu$exception)
  begin
    case (fpu$exception[3:0])
      4'd0, 4'd1, 4'd2, 4'd3, 4'd4, 4'd5, 4'd6, 4'd7, 4'd8:
	  CASE_fpuexception_BITS_3_TO_0_0_fpuexception_ETC__q3 =
	      fpu$exception[3:0];
      default: CASE_fpuexception_BITS_3_TO_0_0_fpuexception_ETC__q3 = 4'd11;
    endcase
  end
  always@(inputs__opcode or inputs__funct7 or muldiv$_start or fpu$_start)
  begin
    case (inputs__opcode[4:2])
      3'b100, 3'b101:
	  IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d38 =
	      fpu$_start[37];
      default: IF_inputs__opcode_BITS_4_TO_2_EQ_0b100_OR_inpu_ETC___d38 =
		   inputs__opcode != 5'b01100 && inputs__opcode != 5'b01110 ||
		   inputs__funct7 != 7'd1 ||
		   muldiv$_start[32];
    endcase
  end

  // handling of inlined registers

  always@(posedge CLK)
  begin
    if (RST_N == `BSV_RESET_VALUE)
      begin
        rg_state <= `BSV_ASSIGNMENT_DELAY 1'd0;
      end
    else
      begin
        if (rg_state$EN) rg_state <= `BSV_ASSIGNMENT_DELAY rg_state$D_IN;
      end
  end

  // synopsys translate_off
  `ifdef BSV_NO_INITIAL_BLOCKS
  `else // not BSV_NO_INITIAL_BLOCKS
  initial
  begin
    rg_state = 1'h0;
  end
  `endif // BSV_NO_INITIAL_BLOCKS
  // synopsys translate_on

  // handling of system tasks

  // synopsys translate_off
  always@(negedge CLK)
  begin
    #0;
    if (RST_N != `BSV_RESET_VALUE)
      if (EN_inputs &&
	  (inputs__opcode[4:2] == 3'b100 || inputs__opcode[4:2] == 3'b101))
	begin
	  v__h357 = $time;
	  #0;
	end
    if (RST_N != `BSV_RESET_VALUE)
      if (EN_inputs &&
	  (inputs__opcode[4:2] == 3'b100 || inputs__opcode[4:2] == 3'b101))
	$display(v__h357, "\tEXECUTION UNIT: Giving inputs to FPU ");
    if (RST_N != `BSV_RESET_VALUE)
      if (EN_inputs &&
	  NOT_inputs__opcode_BITS_4_TO_2_EQ_0b100_AND_NO_ETC___d20)
	begin
	  v__h779 = $time;
	  #0;
	end
    if (RST_N != `BSV_RESET_VALUE)
      if (EN_inputs &&
	  NOT_inputs__opcode_BITS_4_TO_2_EQ_0b100_AND_NO_ETC___d20)
	$display(v__h779, "Output from DIV/MUL : %h", muldiv$_start[31:0]);
    if (RST_N != `BSV_RESET_VALUE)
      if (EN_inputs &&
	  (inputs__opcode != 5'b01100 && inputs__opcode != 5'b01110 ||
	   inputs__funct7 != 7'd1) &&
	  inputs__opcode[4:2] == 3'b110)
	begin
	  v__h560 = $time;
	  #0;
	end
    if (RST_N != `BSV_RESET_VALUE)
      if (EN_inputs &&
	  (inputs__opcode != 5'b01100 && inputs__opcode != 5'b01110 ||
	   inputs__funct7 != 7'd1) &&
	  inputs__opcode[4:2] == 3'b110)
	$display(v__h560, "\t Sending inputs to branch unit");
    if (RST_N != `BSV_RESET_VALUE)
      if (EN_inputs &&
	  NOT_inputs__opcode_BITS_4_TO_2_EQ_0b100_AND_NO_ETC___d33)
	begin
	  v__h508 = $time;
	  #0;
	end
    if (RST_N != `BSV_RESET_VALUE)
      if (EN_inputs &&
	  NOT_inputs__opcode_BITS_4_TO_2_EQ_0b100_AND_NO_ETC___d33)
	$display(v__h508,
		 "\t Sending inputs to Arithmetic unit Op1: %h Op2: %h",
		 inputs__operand1,
		 inputs__operand2);
  end
  // synopsys translate_on
endmodule  // mkexecution_unit

