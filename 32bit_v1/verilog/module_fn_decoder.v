//
// Generated by Bluespec Compiler, version 2016.07.beta1 (build 34806, 2016-07-05)
//
// On Thu Feb  9 10:45:13 IST 2017
//
//
// Ports:
// Name                         I/O  size props
// fn_decoder                     O    79
// fn_decoder__instruction        I    32
// fn_decoder_pred_type           I     1
//
// Combinational paths from inputs to outputs:
//   (fn_decoder__instruction, fn_decoder_pred_type) -> fn_decoder
//
//

`ifdef BSV_ASSIGNMENT_DELAY
`else
  `define BSV_ASSIGNMENT_DELAY
`endif

`ifdef BSV_POSITIVE_RESET
  `define BSV_RESET_VALUE 1'b1
  `define BSV_RESET_EDGE posedge
`else
  `define BSV_RESET_VALUE 1'b0
  `define BSV_RESET_EDGE negedge
`endif

module module_fn_decoder(fn_decoder__instruction,
			 fn_decoder_pred_type,
			 fn_decoder);
  // value method fn_decoder
  input  [31 : 0] fn_decoder__instruction;
  input  fn_decoder_pred_type;
  output [78 : 0] fn_decoder;

  // signals for module outputs
  wire [78 : 0] fn_decoder;

  // remaining internal signals
  reg [19 : 0] IF_fn_decoder__instruction_BITS_6_TO_2_EQ_0b11_ETC___d105;
  reg [4 : 0] x__h390, x__h765;
  wire [19 : 0] immediate_value___1__h1447,
		immediate_value___1__h1553,
		immediate_value___1__h1662,
		immediate_value__h28;
  wire [11 : 0] fn_decoder__instruction_BITS_31_TO_20__q1, x__h1557, x__h1665;
  wire [4 : 0] x__h492, x__h722;

  // value method fn_decoder
  assign fn_decoder =
	     { (fn_decoder__instruction[6:2] == 5'b11100) ?
		 3'd4 :
		 ((fn_decoder__instruction == 32'h0000006F) ?
		    3'd3 :
		    ((fn_decoder__instruction[6:4] == 3'b100 ||
		      fn_decoder__instruction[6:4] == 3'b101 ||
		      fn_decoder__instruction[6:4] == 3'b110 ||
		      fn_decoder__instruction[6:4] == 3'b001 ||
		      fn_decoder__instruction[6:4] == 3'b0 ||
		      fn_decoder__instruction[6:4] == 3'b010 ||
		      fn_decoder__instruction[6:4] == 3'b011) ?
		       3'd0 :
		       ((fn_decoder__instruction[6:2] == 5'b11111) ?
			  3'd3 :
			  3'd2))),
	       x__h390,
	       x__h492,
	       x__h722,
	       x__h765,
	       fn_decoder__instruction[6:2] == 5'b00001 ||
	       fn_decoder__instruction[6:4] == 3'b100 ||
	       fn_decoder__instruction[6:4] == 3'b101 &&
	       fn_decoder__instruction[31:28] != 4'b1010 &&
	       fn_decoder__instruction[31:28] != 4'b1101 &&
	       fn_decoder__instruction[31:28] != 4'b1110,
	       fn_decoder__instruction[6:2] == 5'b01001 ||
	       fn_decoder__instruction[6:4] == 3'b100 ||
	       fn_decoder__instruction[6:4] == 3'b101 &&
	       fn_decoder__instruction[31:28] != 4'b1101 &&
	       fn_decoder__instruction[31:28] != 4'b1111,
	       fn_decoder__instruction[6:5] == 2'b10,
	       fn_decoder__instruction[6:2],
	       fn_decoder__instruction[14:12],
	       fn_decoder__instruction[31:25],
	       IF_fn_decoder__instruction_BITS_6_TO_2_EQ_0b11_ETC___d105,
	       fn_decoder__instruction[6:2] == 5'b01101 ||
	       fn_decoder__instruction[6:2] == 5'b11011 ||
	       fn_decoder__instruction[6:2] == 5'b11001 ||
	       fn_decoder__instruction[6:2] == 5'b11000 ||
	       fn_decoder__instruction[6:2] == 5'd0 ||
	       fn_decoder__instruction[6:3] == 4'b0100 ||
	       fn_decoder__instruction[6:4] == 3'b001 ||
	       fn_decoder__instruction[6:2] == 5'b00001 ||
	       fn_decoder__instruction[6:2] == 5'b01001,
	       fn_decoder__instruction[14:12],
	       fn_decoder__instruction[31:20],
	       fn_decoder__instruction[14],
	       fn_decoder_pred_type } ;

  // remaining internal signals
  assign fn_decoder__instruction_BITS_31_TO_20__q1 =
	     fn_decoder__instruction[31:20] ;
  assign immediate_value___1__h1447 =
	     { fn_decoder__instruction[31],
	       fn_decoder__instruction[19:12],
	       fn_decoder__instruction[20],
	       fn_decoder__instruction[30:21] } ;
  assign immediate_value___1__h1553 = { {8{x__h1557[11]}}, x__h1557 } ;
  assign immediate_value___1__h1662 = { {8{x__h1665[11]}}, x__h1665 } ;
  assign immediate_value__h28 =
	     { {8{fn_decoder__instruction_BITS_31_TO_20__q1[11]}},
	       fn_decoder__instruction_BITS_31_TO_20__q1 } ;
  assign x__h1557 =
	     { fn_decoder__instruction[31],
	       fn_decoder__instruction[7],
	       fn_decoder__instruction[30:25],
	       fn_decoder__instruction[11:8] } ;
  assign x__h1665 =
	     { fn_decoder__instruction[31:25],
	       fn_decoder__instruction[11:7] } ;
  assign x__h492 =
	     (fn_decoder__instruction[6:2] == 5'b01101 ||
	      fn_decoder__instruction[6:4] == 3'b001 ||
	      fn_decoder__instruction[6:2] == 5'b11011 ||
	      fn_decoder__instruction[6:2] == 5'd0 ||
	      fn_decoder__instruction[6:4] == 3'b101 &&
	      fn_decoder__instruction[31:30] == 2'b11 ||
	      fn_decoder__instruction[6:2] == 5'b00001 ||
	      fn_decoder__instruction[6:2] == 5'b11100) ?
	       5'd0 :
	       fn_decoder__instruction[24:20] ;
  assign x__h722 =
	     (fn_decoder__instruction[6:4] == 3'b100) ?
	       fn_decoder__instruction[31:27] :
	       5'd0 ;
  always@(fn_decoder__instruction)
  begin
    case (fn_decoder__instruction[6:2])
      5'b00101, 5'b01101, 5'b11011: x__h390 = 5'd0;
      default: x__h390 = fn_decoder__instruction[19:15];
    endcase
  end
  always@(fn_decoder__instruction)
  begin
    case (fn_decoder__instruction[6:2])
      5'b01000, 5'b01001, 5'b11000: x__h765 = 5'd0;
      default: x__h765 = fn_decoder__instruction[11:7];
    endcase
  end
  always@(fn_decoder__instruction or
	  immediate_value__h28 or
	  immediate_value___1__h1662 or
	  immediate_value___1__h1553 or immediate_value___1__h1447)
  begin
    case (fn_decoder__instruction[6:2])
      5'b00101, 5'b01101:
	  IF_fn_decoder__instruction_BITS_6_TO_2_EQ_0b11_ETC___d105 =
	      fn_decoder__instruction[31:12];
      5'b01000:
	  IF_fn_decoder__instruction_BITS_6_TO_2_EQ_0b11_ETC___d105 =
	      immediate_value___1__h1662;
      5'b11000:
	  IF_fn_decoder__instruction_BITS_6_TO_2_EQ_0b11_ETC___d105 =
	      immediate_value___1__h1553;
      5'b11011:
	  IF_fn_decoder__instruction_BITS_6_TO_2_EQ_0b11_ETC___d105 =
	      immediate_value___1__h1447;
      default: IF_fn_decoder__instruction_BITS_6_TO_2_EQ_0b11_ETC___d105 =
		   immediate_value__h28;
    endcase
  end
endmodule  // module_fn_decoder

